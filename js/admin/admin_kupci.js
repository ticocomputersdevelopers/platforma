$(document).ready(function() {
	var kupac_id = $('#JSCheckConfirm').data("id");
	if(kupac_id!=0 && kupac_id!=null){
		var check = confirm(translate("Kupac je vezan za korpu i narudžbine! Da li želite obrisati sve sto je vezano za ovog kupca?"));
		if(check){
			location.href = base_url+'admin/kupci_partneri/kupci/'+kupac_id+'/1/delete';
		}
	}

	var live_base_url= $('#live_base_url').val();
	$(document).on('change','#JSOpstina',function(){
		$.post(live_base_url+'select-narudzbina-mesto',{narudzbina_opstina_id: $(this).val()},function(response){
			var response_arr = response.split('===DEVIDER===');
			$('#JSMesto').html(response_arr[0] ? response_arr[0] : '');
			$('#JSUlica').html(response_arr[1] ? response_arr[1] : '');
		});
	});

	$(document).on('change','#JSMesto',function(){
		$.post(live_base_url+'select-narudzbina-ulica',{narudzbina_mesto_id: $(this).val()},function(response){
			$('#JSUlica').html(response);
		});
	});

	$('.JSExportXML').on('click', function(){
	$('#JSchooseXml').foundation('reveal', 'open');
		});

	$('.JSExportKupci').click(function(){
		var names = $('.JSKolonaExport:checked').map(function(idx, elem) {
		    return $(elem).data('name');
		  }).get();
		var target_url = $(this).data('link');
		var target_url_arr = target_url.split('/');

		var url_names = 'null';
		if(names.length > 0){
			url_names = names.join('-');
		}
		target_url_arr[9] = url_names;
		
		location.href= target_url_arr.join('/');

	});

});