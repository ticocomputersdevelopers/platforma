$(document).ready(function(){
	$('.JSSliderItemsSortable').sortable({
		update: function(event, ui) {
			var slajder_id = $(event.target).data('slajder_id');
			var order = [];
			$('.JSSliderItemsSortable li').each( function(e) {
				order.push( $(this).data('slajder_stavka_id') );
			});
			
			$.ajax({
				type: "POST",
				url: base_url+'admin/slider-items-position',
				data: {slajder_id: slajder_id, order:order},
				success: function(msg) {

				}
			});	
		}
	});					   
	$( ".JSSliderItemsSortable").disableSelection();
});