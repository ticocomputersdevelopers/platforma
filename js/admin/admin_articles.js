
﻿$(document).ready(function () {
 
	var ord = location.pathname.split('/')[16];
	if(ord != null){
		var order = ord.split('-');
		if(order[1] == 'ASC'){
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').attr('data-sort', 'ASC');
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').find('span').html('&#x25B2');
		}else{
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').attr('data-sort', 'DESC');
			$('.table-head').filter('[data-coll="'+ order[0] +'"]').find('span').html('&#x25BC');
		}
		
	}
	
	$('.table-head').click(function(){

		if($(this).data('sort') == 'DESC'){
			var coll = $(this).data('coll');
			var sort = 'ASC';
		}else{
			var coll = $(this).data('coll');
			var sort = 'DESC';
		}
		if(coll != ''){
			location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+criteria[9]+'/'+criteria[10]+'/'+criteria[11]+'/'+criteria[12]+'/'+coll+'-'+sort;
		}
	});
	if ($('#search').length>0) {
		if(criteria[11] != 0){
			$('#search').val(criteria[11].replace(/\+/g, ' '));
		}
	}

	$('#search-btn').click(function(){


		var search = $('#search').val();
		var search1 = search.replace(/\//g, '+').replace(/ /g, '+').replace(/\"/g, '').replace(/\'/g, '');

		if(search != ''){
			location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+criteria[9]+'/'+criteria[10]+'/'+search1+'/'+criteria[12];
		}
	});

	$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    });

	$('#clear-btn').click(function(){

		location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+criteria[9]+'/'+criteria[10]+'/0/'+criteria[12];

	});

	//search cena
	if(criteria != null){
		if(criteria[12] != 'nn-nn'){
			var nabavna_arr = criteria[12].split('-');
			if($.isNumeric(nabavna_arr[0])){
				$('#JSCenaOd').val(nabavna_arr[0]); 
			}
			if($.isNumeric(nabavna_arr[1])){
				$('#JSCenaDo').val(nabavna_arr[1]); 
			}
		}
		
	}
	$('#JSCenaSearch').click(function(){
		var nabavna_od = $('#JSCenaOd').val();
		var nabavna_do = $('#JSCenaDo').val();
		if($.isNumeric(nabavna_od) && $.isNumeric(nabavna_do)){
			location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+criteria[9]+'/'+criteria[10]+'/'+criteria[11]+'/'+nabavna_od+'-'+nabavna_do;
		}
	});
	$('#JSCenaClear').click(function(){

		location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+criteria[9]+'/'+criteria[10]+'/'+criteria[11]+'/nn-nn';

	});

     var roba_ids = new Array();
     var selected_roba_ids;
     
	  $(function() {
	    $( "#selectable" ).selectable({
	    filter: 'tr',
	    stop: function() {
		      	roba_ids = [];
		      	$("td").removeClass("ui-selected");
		      	$("td").find('input').removeClass("ui-selected");
		        
		      	var selectedItems = $('.ui-selected', this);
	          	$('#JSselektovano').html(selectedItems.length);

		        $( ".ui-selected", this ).each(function() {
		          var id = $( this ).data("id");
		          roba_ids.push(id);
		        });
		        roba_ids=roba_ids.filter(function(n){return n !== undefined});
		        selected_roba_ids=roba_ids;
	        }
	    });
	  });
	  //akcije na dugme
	  $( ".JSexecute" ).click(function(){
	  	var dataUpd = $(this).data('execute');

	  	if(roba_ids[0]){
	  		$.post(base_url+'admin/ajax/articles', {action:'execute', data: dataUpd, roba_ids: roba_ids}, function (response){
	  			alertify.success(translate("Izvršeno."));
	  		});
			if(dataUpd[0].table == 'roba'){

				if(dataUpd[0].column == 'flag_cenovnik'){				
				  	if(dataUpd[0].val == 1){
					  	$.each(selected_roba_ids, function( index, value ) {
					  		var val_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_zakljucan').html();
					  		if(val_response == 'NE') {
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<i class="fa fa-check green" aria-hidden="true"></i>');
					  		}
						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
					  		var val_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_zakljucan').html();
							if(val_response == 'NE') {
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<i class="fa fa-times red" aria-hidden="true"></i>');
							}
						});
					}
				}
				else if(dataUpd[0].column == 'flag_prikazi_u_cenovniku'){				
				  	if(dataUpd[0].val == 1){
					  	$.each(selected_roba_ids, function( index, value ) {
					  		var val_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_zakljucan').html();
					  		var active_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_aktivan').html();
					  		if(val_response == 'NE' && active_response == '<i class="fa fa-check green" aria-hidden="true"></i>') {
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<i class="fa fa-check green" aria-hidden="true"></i>');
					  		}
						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
					  		var val_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_zakljucan').html();
							if(val_response == 'NE') {
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<i class="fa fa-times red" aria-hidden="true"></i>');
							}
						});
					}
				}
				else if(dataUpd[0].column == 'flag_aktivan'){				
				  	if(dataUpd[0].val == 1){
					  	$.each(selected_roba_ids, function( index, value ) {
					  		var val_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_zakljucan').html();
					  		if(val_response == 'NE') {
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<i class="fa fa-check green" aria-hidden="true"></i>');
								$("#selectable").find('[data-id="'+ value +'"]').removeClass('text-red');
					  		}
						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
					  		var val_response = $("#selectable").find('[data-id="'+ value +'"]').find('.flag_zakljucan').html();
							if(val_response == 'NE') {
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<i class="fa fa-times red" aria-hidden="true"></i>');
								$("#selectable").find('[data-id="'+ value +'"]').find('.flag_prikazi_u_cenovniku').html('<i class="fa fa-times red" aria-hidden="true"></i>');
								$("#selectable").find('[data-id="'+ value +'"]').addClass('text-red');
							}
						});
					}
				}
				else if(dataUpd[0].column == 'akcija_flag_primeni'){				
				  	if(dataUpd[0].val == 1){
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<span class="akcija-a">A</span>');
						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).html('<span class="akcija-bez">-</span>');
						});
					}
				}
				else if(dataUpd[0].column == 'b2b_akcija_flag_primeni'){				
				  	if(dataUpd[0].val == 1){
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.akcija_flag_primeni').html('<span class="akcija-a" style="background-color: blue;">A</span>');

						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.akcija_flag_primeni').html('<span class="akcija-bez">-</span>');
						});
					}
				}
				else if(dataUpd[0].column == 'flag_zakljucan'){				
				  	if(dataUpd[0].val == 'true'){
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.JSArticleEdit').data('zakljucan','1');
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text('DA');
						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.JSArticleEdit').data('zakljucan','0');
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text('NE');
						});
					}
				}
				else{				
				  	if(dataUpd[0].val == 1 || dataUpd[0].val == 'true'){
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text('DA');
						});
					}else{
					  	$.each(selected_roba_ids, function( index, value ) {
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text('NE');
						});
					}
				}

			}
	  	}
	  	else{
	  		alertify.alert(translate("Nema selektovanih artikala!"));
	  	}
	  });

		$("#JSKarakteristikeDodeli").click(function(){
			if(selected_roba_ids){
				if($("#JSKarakteristikaVrsta").val()!=""){
					$('.close-reveal-modal').trigger('click');
					var data = [{"table":"roba", "column":"web_flag_karakteristike", "val":$("#JSKarakteristikaVrsta").val()}];
			  		$.post(base_url+'admin/ajax/articles', {action:'execute', data: data, roba_ids: roba_ids}, function (response){
			  			alertify.success(translate('Data akcija je izvršena.'));
			  		}); 				
				  	$.each(selected_roba_ids, function( index, value ) {
				  		var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.web_flag_karakteristike');
				  		if(data[0].val == 0){
							obj.text('HTML');
				  		}
				  		else if(data[0].val == 1){
							obj.text('Gen.');
				  		}
				  		else if(data[0].val == 2){
							obj.text('Dob.');
				  		}
					});
				}
			}
		  	else{
		  		alertify.alert(translate("Nema selektovanih artikala!"));
		  	}
		});	  
		$("#JSKarakteristikeObrisi").click(function(){
			if(selected_roba_ids){
				if($("#JSKarakteristikaVrsta").val()!=""){
					$('.close-reveal-modal').trigger('click');
			  		$.post(base_url+'admin/ajax/articles', {action:'delete-karakteristike', kind:$("#JSKarakteristikaVrsta").val(), roba_ids: roba_ids}, function (response){
			  			alertify.success(translate('Data akcija je izvršena.'));
			  		});
				}
			}
		  	else{
		  		alertify.alert(translate("Nema selektovanih artikala!"));
		  	}
		});
		$("#JSHtmlPrebaci").click(function(){
			if(selected_roba_ids){
				$('.close-reveal-modal').trigger('click');
		  		$.post(base_url+'admin/ajax/articles', {action:'html-transfer', roba_ids: roba_ids}, function (response){
		  			alertify.success(translate('Data akcija je izvršena.'));
		  		});
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		$("#JSIzmeni").click(function(){
			if(selected_roba_ids){
				window.open(base_url+'admin/product/'+selected_roba_ids[0], '_blank');
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		$("#JSUrediOpis").click(function(){
			if(selected_roba_ids){
				window.open(base_url+'admin/product_opis/'+selected_roba_ids[0], '_blank');
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		$("#JSKlonirajArtikal").click(function(e){
			if(selected_roba_ids){
				window.open(base_url+'admin/product/0/'+selected_roba_ids[0], '_blank');
			} 
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		$('#JSobradaKarakteristika').on('click', function(){
			if (selected_roba_ids) {
				$('#JSobradaKarakteristikaModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseGroup').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseGroupModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseBrand').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseBrandModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseTip').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseTipModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseLabela').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseLabelaModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchoosePrice').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchoosePriceModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseQuantity').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseQuantityModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseTax').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseTaxModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSTezina').on('click', function(){
			if (selected_roba_ids) {
				$('#JSTezinaModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSTag').on('click', function(){
			if (selected_roba_ids) {
				$('#JSTagModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});

		$('#JSchooseExport').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseExportModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSchooseKatalog').on('click', function(){
			if (selected_roba_ids) {
				$('#JSchooseKatalogModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSmassEdit').on('click', function(){
			if (selected_roba_ids) {
				$('#JSmassEditModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$('#JSakcijaEdit').on('click', function(){
			if (selected_roba_ids) {
				$('#JSakcijaModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});
		$("#JStextEdit").click(function(){
			if(selected_roba_ids){
				$('#JStextEditModal').foundation('reveal', 'open');
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});		
		$("#JSexecuteAkcija").click(function(){
			if(selected_roba_ids){
				$('#JSakcijaModal').foundation('reveal', 'open');
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});	
		/*$("#JSdefMarza").on('click', function(){
			if(selected_roba_ids){
				$('#JSdefMarza').foundation('reveal', 'open');
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});	*/
		$('#JSGarancije').on('click', function(){
			if (selected_roba_ids) {
				$('#JSGarancijaModal').foundation('reveal', 'open');
			}else{
				alertify.alert(translate('Nema selektovanih artikala!'));
			}
		});







		$("#JSObrisiArtikal").click(function(e){
			if(roba_ids){				
				alertify.confirm(translate("Da li ste sigurni da želite obrisati artikal/le?"),
			function(e){
	            if(e){
					$.post(base_url+'admin/products-delete-post',{roba_ids: roba_ids, check: 0},function (response){
						if(response != ''){
							alertify.confirm(response + "<br><strong>Ipak obriši</strong>", function(e){
						        if(e) {
									$.post(base_url+'admin/products-delete-post',{roba_ids: roba_ids, check: 1},function (response){
							        	window.location.reload(true);
								    });
						        }else{
						        	window.location.reload(true);
						        }
						    });
						}else{
							window.location.reload(true);
						}

					});
				}
           });
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		$("#JSRefresh").click(function(){
			location.reload(true);
		});
		$( "#JSPreracunajCene" ).click(function(){
			if(roba_ids[0]){			
		  		$.post(base_url+'admin/ajax/articles', {action:'preracunaj_cene', roba_ids: roba_ids}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  				
		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.web_cena').text(response_roba_ids[value]);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});

	// akcije preko inputa
	  	$( ".JSexecuteBtn" ).click(function(){

		  	var dataUpd = $(this).data('execute');
		  	var val = $(this).parent().find('.JSexecuteInput').val();

			if(val != ''){
			  	$.each(dataUpd, function( index, value ) {
					dataUpd[index].val = val;
				});

			  	if(roba_ids[0]){
			  		$.post(base_url+'admin/ajax/articles', {action:'execute', data: dataUpd, roba_ids: roba_ids}, function (response){
			  			alertify.success(translate('Data akcija je izvršena'));
			  			//custom
			  			if(dataUpd[0].column == 'grupa_pr_id'){
			  				location.reload(true);
			  			}
						if(dataUpd[0].table == 'roba'){
						  	$.each(selected_roba_ids, function( index, value ) {
						  		if ($("#selectable").find('[data-id="'+ value +'"]').find('.'+'flag_zakljucan').text() == 'NE') {					  			
								$("#selectable").find('[data-id="'+ value +'"]').find('.'+dataUpd[0].column).text(response);
						  		}
							});
						}
			  		});

			  	}
			  	else{
			  		alertify.alert(translate('Nema selektovanih artikala!'));
			  	}
			}
		  	else{
		  		alertify.alert(translate('Popunite sadržај polja!'));
		  	}
	  	});

	  	$( "#JSPreracunajWebCenuButton" ).click(function(){
			if(roba_ids[0]){			
		  		$.post(base_url+'admin/ajax/articles', {action:'preracunaj_webcenu', roba_ids: roba_ids}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  				
		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.wc-col').text(response_roba_ids[value]);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});


		$( "#JSPreracunajEndCenuButton" ).click(function(){
			if(roba_ids[0]){			
		  		$.post(base_url+'admin/ajax/articles', {action:'preracunaj_endcenu', roba_ids: roba_ids}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  				
		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.endcena').text(response_roba_ids[value]);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});



		$( "#JSZaokruziWebCenuButton" ).click(function(){
			var dec = $('#JSzaokruziweb').val();
			
			if(roba_ids[0]){
				if(dec<99){			
		  		$.post(base_url+'admin/ajax/articles', {action:'zaokruzi_webcenu', roba_ids: roba_ids, dec: dec}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  

		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.wc-col').text(response_roba_ids[value]);
					});
		  		});	
		  		}else{
		  			alertify.alert(translate('Broj ne sme prelaziti 99!'));	
		  		}		
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		$( "#JSZaokruziMpCenuButton" ).click(function(){
			var dec = $('#JSzaokruzimp').val();
			
			if(roba_ids[0]){
				if(dec<99){			
		  		$.post(base_url+'admin/ajax/articles', {action:'zaokruzi_mpcenu', roba_ids: roba_ids, dec: dec}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  

		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.mpcena').text(response_roba_ids[value]);
					});
		  		});	
		  		}else{
		  			alertify.alert(translate('Broj ne sme prelaziti 99!'));	
		  		}		
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});

		$( "#JSZaokruziEndCenuButton" ).click(function(){
			var dec = $('#JSzaokruziend').val();
			
			if(roba_ids[0]){
				if(dec<99){			
		  		$.post(base_url+'admin/ajax/articles', {action:'zaokruzi_endcenu', roba_ids: roba_ids, dec: dec}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  

		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.endcena').text(response_roba_ids[value]);
					});
		  		});	
		  		}else{
		  			alertify.alert(translate('Broj ne sme prelaziti 99!'));	
		  		}		
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
		

		$( "#JSPreracunajMPCenuButton" ).click(function(){
			if(roba_ids[0]){			
		  		$.post(base_url+'admin/ajax/articles', {action:'preracunaj_mpcenu', roba_ids: roba_ids}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));  				
		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
						$("#selectable").find('[data-id="'+ value +'"]').find('.mpcena').text(response_roba_ids[value]);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});
	
		$(function() {
			$( "#datum_akcije_od, #datum_akcije_do,#b2b_datum_akcije_od,#b2b_datum_akcije_do" ).datepicker();
		});
			$('#datum_akcije_od').keydown(false);
			$('#datum_akcije_do').keydown(false);
			$('#b2b_datum_akcije_od').keydown(false);
			$('#b2b_datum_akcije_do').keydown(false);
			$('#datum_od_delete').click(function(){
			$('#datum_akcije_od').val('');
		});
			$('#b2b_datum_od_delete').click(function(){
			$('#b2b_datum_akcije_od').val('');
		});
			$('#datum_do_delete').click(function(){
			$('#datum_akcije_do').val('');
		});
			$('#b2b_datum_do_delete').click(function(){
			$('#b2b_datum_akcije_do').val('');
		});

	//mass edit marza, rabat
		$( "#JSMassEditMarzaButton, #JSMassEditRabatButton" ).click(function(){

	  		var data = {
	  			roba_ids: roba_ids,
	  			cena_od: $( "#JSMassEditCenaOd" ).val(),
	  			cena_za: $( "#JSMassEditCenaZa" ).val()
	  		}
	  		if($(this).attr('id') == 'JSMassEditMarzaButton'){	  			
		  		var marza_check = 0;
		  		if($("#JSMassEditMarzaCheck").is(':checked')){
		  			marza_check = 1;
		  		}
	  			data.action = 'mass_edit_marza'; 
	  			data.marza = $( "#JSMassEditMarzaInput" ).val();
	  			data.marza_check = marza_check;
	  		}
	  		else if($(this).attr('id') == 'JSMassEditRabatButton'){
	  			data.action = 'mass_edit_rabat';
		  		data.rabat = $( "#JSMassEditRabatInput" ).val();
	  		}
	  		
		  	if(roba_ids[0] && ((data.marza >=null && $.isNumeric(data.marza)) || (data.rabat >=null && $.isNumeric(data.rabat)))){
		  		if((data.marza >= 0 && data.marza < 100) || (data.rabat >= 0 && data.rabat < 100)){		  			
			  		$.post(base_url+'admin/ajax/articles', data, function (response){
			  			alertify.success(translate('Data akcija je izvršena'));
			  			var response_roba_ids = $.parseJSON(response);
					  	$.each(response_roba_ids, function( index, value ) {
					  		$.each(value, function( ind, val ) {
					  			if(ind!='roba_id'){
					  				$("#selectable").find('[data-id="'+ value.roba_id +'"]').find('.'+ind).text(val);
					  			}
					  		});
						});		  			

			  		});
		  		}
			  	else{
			  		alertify.alert(translate('Marza ili rabat moraju biti izmedju 0 i 100!'));
			  	}
		  	}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala ili su marza i rabat nedozvoljenog formata!'));
		  	}

	  	});

	// dodela definisanih marzi
		$( "#JSdefMarza" ).click(function(){
  			$('.reveal-modal-bg').hide();

			if(roba_ids[0]){				
		  		$.post(base_url+'admin/ajax/articles', {action:'definisane_marze_primena', roba_ids: roba_ids}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			var result = $.parseJSON(response);

				  	$.each(result, function( index, value ) {
						var row_element = $("#selectable").find('[data-id="'+ value.roba_id +'"]');
						
						row_element.find('.web_marza').text(value.web_marza);
						row_element.find('.WebCena').find('.JSWebCenaVrednost').text(value.web_cena);
						row_element.find('.WebCena').find('.JSWebCenaInput').val(value.web_cena);
						row_element.find('.mp_marza').text(value.mp_marza);
						row_element.find('.mpcena').text(value.mpcena);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala!'));
		  	}
		});


	//Edit Texta
	   	$(" #JStextEditNaziv, #JStextEditOpis ").click(function(){
			if($(this).attr('id') == 'JStextEditOpis'){
	   			$("#JStextEditNazivInput").attr("placeholder", translate("Ovo je novi opis"));
	   		}else if($(this).attr('id') == 'JStextEditNaziv'){
	   			$("#JStextEditNazivInput").attr("placeholder", translate("Ovo je novi naziv"));
	   	}
		});

	   	$(" #JStextInsertButton, #JStextReplaceButton ").click(function(){
			var dugme=$(this).attr('id');
	   		if((dugme == 'JStextReplaceButton' && $("#JStextEditInput1").val() == '') || (dugme == 'JStextInsertButton' && $("#JStextEditNazivInput").val() == '')){
	   			$("#Results").text(translate("Niste popunili polje!"));
	   		}else{
	   			$("#Results").text("");
	   			
	   			var data = {
		  			roba_ids: roba_ids,
		  			insert: $("#JStextEditNazivInput").val(),
		  			replaceFrom: $("#JStextEditInput1").val(),
		  			replaceTo: $("#JStextEditInput2").val()
	  			}

	  			$("#JStextEditNazivInput").val('');
	  			$("#JStextEditInput1").val('');
	  			$("#JStextEditInput2").val('');

				var naziv_check = 1;
		  		if($("#JStextEditOpis").is(':checked')){
		  			naziv_check = 0;
		  		}
		  		var first = $("#JStextEditNaziv");
		  		first.prop("checked", true);

		  		var end_check = 0;
		  		if($("#JStextEditEnd").is(':checked')){
		  			end_check = 1;
		  		}
		  		var second = $("#JStextEditStart");
		  		second.prop("checked", true);

		  		var deoTeksta = 0;
		  		if($("#JStextEditDeo").is(':checked')){
		  			deoTeksta = 1;
		  		}
		  		var third = $("#JStextEditDeo");
		  		third.prop("checked", true);


				if(dugme == 'JStextInsertButton' && naziv_check == 1){
		  			if(data.insert.length > 500) //i got a problem with this one i think
		  			{
		  			alert(translate("Naziv ne sme biti duži od 500 karaktera."));
		  			return false;
		  			}
	  			}

	  			if(dugme == 'JStextInsertButton'){	  					  		
		  			data.action = 'text_edit'; 
		  			data.naziv_check = naziv_check;
		  			data.end_check = end_check;
	  			}else if(dugme == 'JStextReplaceButton'){
		  			data.action = 'text_replace';
			  		data.naziv_check = naziv_check;
			  		data.deoTeksta = deoTeksta;
	  			}

		  		$.post(base_url+'admin/ajax/articles', data, function (response){
		  				alertify.success(translate('Data akcija je izvršena'));
		  			if(naziv_check == 1 && dugme == 'JStextInsertButton' && end_check == 0){
		  		$.each(selected_roba_ids, function( index, value ) {
						var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.table-fixed-width1');	
						var obj1 = $("#selectable").find('[data-id="'+ value +'"]').find('.table-fixed-width1').text();	
					  	obj.html("<span>" + data.insert + ' ' + obj1+ "</span>");
						});
		  			}
		  			else if(naziv_check == 1 && dugme == 'JStextInsertButton' && end_check == 1){
		  				$.each(selected_roba_ids, function( index, value ) {
						var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.table-fixed-width1');	
						var obj1 = $("#selectable").find('[data-id="'+ value +'"]').find('.table-fixed-width1').text();	
					  	obj.html("<span>" + obj1 + ' ' + data.insert+ "</span>");
						});
		  			}
		  			else if(dugme == 'JStextReplaceButton'){
		  				var result = $.parseJSON(response);
					  	$("#Results").text("Izmenjeno " + result.counter);
					  	if(naziv_check == 1){
							$.each(result.roba_ids, function( index, value ) {
								var obj = $("#selectable").find('[data-id="'+ index +'"]').find('.table-fixed-width1');
							  	obj.html("<span>" + value + "</span>");
						 	});
						}	
				  	}
		  		});	
		  	}	  	
	  	});
		//Kraj editTexta

		$( "#JSWebCenaEditBtn" ).click(function(){
			var val = $('#JSWebCenaEdit').val();
			if(roba_ids[0] && val!=null && $.isNumeric(val)){			
		  		$.post(base_url+'admin/ajax/articles', {action:'promena_webcene', roba_ids: roba_ids, val: val}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			$.each(selected_roba_ids, function( index, value ) {
					var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.wc-col');			
				  	obj.text(val);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala !'));
		  	}
		});

		$( "#JSEndCenaEditBtn" ).click(function(){
			var val = $('#JSEndCenaEdit').val();
			if(roba_ids[0] && val!=null && $.isNumeric(val)){			
		  		$.post(base_url+'admin/ajax/articles', {action:'promena_endcene', roba_ids: roba_ids, val: val}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			$.each(selected_roba_ids, function( index, value ) {
					var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.endcena');			
				  	obj.text(val);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala !'));
		  	}
		});

		$( "#JSMPEditBtn" ).click(function(){
			var val = $('#JSMPEdit').val();
			if(roba_ids[0] && val!=null && $.isNumeric(val)){			
		  		$.post(base_url+'admin/ajax/articles', {action:'promena_mpcene', roba_ids: roba_ids, val: val}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			$.each(selected_roba_ids, function( index, value ) {
					var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.mpcena');			
				  	obj.text(val);
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala !'));
		  	}
		});

		$( "#JSPoreskaStopa" ).click(function(){
			var val = $('#JSValPoreskaStopa').val();
			var check = $('#JSCenaPoreskaStopa').val();
			if(roba_ids[0] && val){			
		  		$.post(base_url+'admin/ajax/articles', {action:'poreska_stopa', roba_ids: roba_ids, val: val, check: check}, function (response){
		  			alertify.success(translate('Data akcija je izvršena!'));
		  			var response_roba_ids = $.parseJSON(response);
				  	$.each(selected_roba_ids, function( index, value ) {
				  		$("#selectable").find('[data-id="'+ value +'"]').find('.tarifna_grupa').text(response_roba_ids['porez']);
			  			if(check==1){	  				
							$("#selectable").find('[data-id="'+ value +'"]').find('.web_cena').text(response_roba_ids['web_cena'][value]);	
			  			}
					});
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala ili nije izabrana poreska stopa!'));
		  	}
		});

		$( "#JSKolicinaBtn" ).click(function(){
			var val = $('#JSKolicinaAdd').val();
			var magacin_id = 0;
			if(criteria != null){
				magacin_id = $('.JSMagacinKolicina').val();
			}
			if(roba_ids[0] && val!=null && $.isNumeric(val)){			
		  		$.post(base_url+'admin/ajax/articles', {action:'promena_kolicine', roba_ids: roba_ids, val: val, magacin_id: magacin_id}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			if(magacin_id == criteria[5]){
					  	$.each(selected_roba_ids, function( index, value ) {
							var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.kolicina');			
					  			obj.text(val);
						});
					}
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala ili sadrzaj polja za kolicinu je neispravan!'));
		  	}
		});

		$( "#JSAkcijaBtn" ).click(function(){
			var val = $('#JSAkcijaAdd').val();
			var akc_od = $('#datum_akcije_od').val();
			var akc_do = $('#datum_akcije_do').val();

			var d = new Date();
			var curr_month = d.getMonth();
			var m = curr_month+1
			var time = d.getFullYear()+'-'+m+'-'+d.getDate();
			
		  		if(roba_ids[0] && val!=null && val >= 0 ){			
			  		$.post(base_url+'admin/ajax/articles', {action:'akcija', roba_ids: roba_ids, val: val, akc_od: akc_od, akc_do:akc_do}, function (response){
			  			alertify.success(translate('Data akcija je izvršena'));  					
						 $.each(selected_roba_ids, function( index, value ) { 
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+'akcija_flag_primeni').html('<span class="akcija-a">A</span>');
						}); 						  	
					});
				}
			  		else{
			  			alertify.alert(translate('Ispravno popunite sadržај polja. Akcijski popust mora biti veći od 0 !!!'));
			  	}		  	
		});
		
		$( "#JSAkcijaB2BBtn" ).click(function(){
			var val = $('#JSAkcijaB2BAdd').val();
			var akc_od = $('#b2b_datum_akcije_od').val();
			var akc_do = $('#b2b_datum_akcije_do').val();

			var d = new Date();
			var curr_month = d.getMonth();
			var m = curr_month+1
			var time = d.getFullYear()+'-'+m+'-'+d.getDate();
			
		  		if(roba_ids[0] && val!=null && val >= 0 ){			
			  		$.post(base_url+'admin/ajax/articles', {action:'akcijaB2B', roba_ids: roba_ids, val: val, akc_od: akc_od, akc_do:akc_do}, function (response){
			  			alertify.success(translate('Data akcija je izvršena'));  					
						 $.each(selected_roba_ids, function( index, value ) { 
							$("#selectable").find('[data-id="'+ value +'"]').find('.'+'akcija_flag_primeni').html('<span class="akcija-a" style="background-color: blue;">A</span>');
						}); 						  	
					});
				}
			  		else{
			  			alertify.alert(translate('Ispravno popunite sadržај polja. Akcijski popust mora biti veći od 0 !!!'));
			  	}		  	
		});
		$( "#JSTezinaBtn" ).click(function(){
			var val = $('#JSTezinaAdd').val();
			if(roba_ids[0] && val!=null && $.isNumeric(val)){			
		  		$.post(base_url+'admin/ajax/articles', {action:'promena_tezine', roba_ids: roba_ids, val: val}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
				 //  	$.each(selected_roba_ids, function( index, value ) {
					// 	var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.kolicina');			
				 //  			obj.text(val);
					// });
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala ili sadrzaj polja za težinu je neispravan!'));
		  	}
		});
		$( "#JSTagBtn" ).click(function(){
			var val = $('#JSTagAdd').val();
			if(roba_ids[0] && val!=null){			
		  		$.post(base_url+'admin/ajax/articles', {action:'promena_tag', roba_ids: roba_ids, val: val}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
				 //  	$.each(selected_roba_ids, function( index, value ) {
					// 	var obj = $("#selectable").find('[data-id="'+ value +'"]').find('.kolicina');			
				 //  			obj.text(val);
					// });
		  		});			
			}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala ili sadrzaj polja za dodelu taga je neispravan!'));
		  	}
		});
		// katalog
		$( ".JSKatalog" ).click(function(){
			var katalog_id = $('#katalog_select').val();
			var kind = $(this).data('kind');
			if(roba_ids[0] && katalog_id!=0){
				$('#wait').show();			
		  		$.post(base_url+'admin/ajax/articles', {action:'katalog', kind: kind, roba_ids: roba_ids, katalog_id: katalog_id}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			$('#wait').hide();
		  		});			
			}else{
				alertify.alert(translate('Niste selektovali artikle ili katalog!'));
			}
		});
		$( ".JSKatalogNo" ).click(function(){
	  		var katalog_no = '';
  			katalog_no = $("#katalog_select").val();
	  		if(katalog_no != ''){
	  			if($(this).data('check') == 1){
	  				katalog_no = katalog_no;
	  			}else{
	  				katalog_no = '-'+katalog_no;
	  			}
	  		}else{
	  			katalog_no = '0';
	  		}

	  		location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+katalog_no+'/'+criteria[9]+'/'+criteria[10]+'/'+criteria[11]+'/'+criteria[12];
	  });

		//export
		$( ".JSExport" ).click(function(){
			var export_id = $('#export_select').val();
			var kind = $(this).data('kind');
			if(roba_ids[0] && export_id!=0){
				$('#wait').show();			
		  		$.post(base_url+'admin/ajax/articles', {action:'exporti', kind: kind, roba_ids: roba_ids, export_id: export_id}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			$('#wait').hide();
		  		});			
			}else{
				alertify.alert(translate('Niste selektovali artikle ili eksport!'));
			}
		});

	   $( "#JSExportNo" ).click(function(){
	  		var exporti_no = '';
  			exporti_no = $("#export_select").val();
	  		if(exporti_no != ''){
	  			exporti_no = '-'+exporti_no;
	  		}else{
	  			exporti_no = '0';
	  		}

	  		location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+exporti_no+'/'+criteria[8]+'/'+criteria[9]+'/'+criteria[10]+'/'+criteria[11]+'/'+criteria[12];
	  });

	   $( "#JSExportExecute" ).click(function(){
			var export_value = $('#export_select').find(':selected').data('export');
			var export_kind = $('#JSExportKind').val();
			if(export_value){
				window.open(base_url+'admin/export-external/'+export_value+'/'+export_kind, '_blank');
			}
	  });

		//garancija save
		$( "#JSGarancijaSave" ).click(function(){
			var broj_meseci = $('#JSGarancijaMeseci').val();
			var produzena_garancija = 0;
			if($('#JSProduzenaGarancija').prop('checked')){
				produzena_garancija = 1;
			}
			if(!$.isNumeric(broj_meseci) || ($.isNumeric(broj_meseci) && broj_meseci < 0)){
				$('#JSGarancijaMeseci').addClass('error');
			}else{
				$('#JSGarancijaMeseci').removeClass('error');

				if(roba_ids[0]){
					$('#wait').show();			
			  		$.post(base_url+'admin/ajax/articles', {action:'garancija_save', roba_ids: roba_ids, broj_meseci: broj_meseci, produzena_garancija: produzena_garancija}, function (response){
			  			alertify.success(translate('Data akcija je izvršena'));
			  			$('#wait').hide();
			  		});			
				}else{
					alertify.alert(translate('Niste selektovali artikle!'));
				}
			}
		});
		//garancija delete
		$( "#JSGarancijaDelete" ).click(function(){
			var produzena_garancija = 0;
			if($('#JSProduzenaGarancija').prop('checked')){
				produzena_garancija = 1;
			}
			if(roba_ids[0]){
				$('#wait').show();			
		  		$.post(base_url+'admin/ajax/articles', {action:'garancija_delete', roba_ids: roba_ids, produzena_garancija: produzena_garancija}, function (response){
		  			alertify.success(translate('Data akcija je izvršena'));
		  			$('#wait').hide();
		  		});			
			}else{
				alertify.alert(translate('Niste selektovali artikle!'));
			}
		});

	  $( "#all" ).click(function(){
	  		$("#selectable").find("tr").addClass("ui-selected");
	  		 $( ".ui-selected", "#selectable" ).each(function() {

	  			var selectedItems = $('.ui-selected', this);
	        	$('#JSselektovano').html('Sve');

	          var id = $( this ).data("id");
	          roba_ids.push(id);
	        });

	        selected_roba_ids=roba_ids.filter(function(n){return n !== undefined}); 
	        roba_ids = all_ids;
	  });

	  $("#proizvodjac_select").select2({
	  	placeholder: translate('Izaberi proizvođače')
	  	});
	  $("#dobavljac_select").select2({
	  	placeholder: translate('Izaberi dobavljače')
	  	});
	  $("#tip_select").select2({
	  	placeholder: translate('Izaberi tip')
	  	});
	   $("#labela_select").select2({
	  	placeholder: translate('Izaberi labelu')
	  	});
	  $("#change-manufac").select2({
			placeholder: translate('Izaberi proizvođača')
		});
	   $("#change-group").select2({
			placeholder: translate('Izaberi grupu')
		});
	  $("#JSDodatneGrupe").select2({
	  	placeholder: translate('Izaberite dodatne grupe')
	  	});
	  	
		if(criteria != null){
			var filters = criteria[9].split("-");

			var i=1;
			$.each(filters, function( index, value ) {
				if(value == "dd"){
					$( ".row" ).find('[data-check="'+i+'"]').attr('checked', '');
					$( ".row" ).find('[data-check="'+(i+1)+'"]').attr('checked', '');
				}
				if(value == "1"){
					$( ".row" ).find('[data-check="'+i+'"]').attr('checked', '');
				}
				if(value == "0"){
					$( ".row" ).find('[data-check="'+(i+1)+'"]').attr('checked', '');
				}
				i=i+2;
			});	 
		}

	   $( ".filter-check" ).click(function(){
	   		var element = $(this);
	     		if(element.attr('checked')){
     				$.each(filters, function( index, value ) {
			     		if(element.data("check") == 2*index+1 && !element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "nn";
			     		}
			     		if(element.data("check") == 2*index+1 && element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}
			     		if(element.data("check") == 2*index+2 && element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+2 && !element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "nn";
			     		}
					});
		     	}else{
     				$.each(filters, function( index, value ) {
			     		if(element.data("check") == 2*index+1 && !element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+1 && element.parent().parent().find('[data-check="'+(2*index+2)+'"]').attr('checked')){
			     			filters[index] = "1";
			     		}
			     		if(element.data("check") == 2*index+2 && !element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}
			     		if(element.data("check") == 2*index+2 && element.parent().parent().find('[data-check="'+(2*index+1)+'"]').attr('checked')){
			     			filters[index] = "0";
			     		}			
					});
		     	}
		  		var filteri = '';
				$.each(filters, function( index, value ) {
					filteri += '-' + value;
				});
				var flag = '0';
		  		if($("#JSFlag").is(':checked')){
  					flag = '1';
  				}
  				
				location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+filteri.substr(1)+'/'+flag+'/'+criteria[11]+'/'+criteria[12];
		});

	   $("#JSFlag").click(function(){
	   		var flag_checked = 0;
	  		if($("#JSFlag").is(':checked')){
	  			flag_checked = 1;
			}	   	
			location.href = base_url+'admin/artikli/'+criteria[0]+'/'+criteria[1]+'/'+criteria[2]+'/'+criteria[3]+'/'+criteria[4]+'/'+criteria[5]+'/'+criteria[6]+'/'+criteria[7]+'/'+criteria[8]+'/'+criteria[9]+'/'+flag_checked+'/'+criteria[11]+'/'+criteria[12];
	   });


	   $( ".dobavljac_proizvodjac" ).click(function(){
	  		var proizvodjaci = '';
	  		$("#proizvodjac_select :selected").each(function(){
	  			proizvodjaci += '+' + $(this).val();
	  		});
	  		if(proizvodjaci == ''){
	  			proizvodjaci = '+0';
	  		}
	  		var dobavljaci = '';
	  		$("#dobavljac_select :selected").each(function(){
	  			dobavljaci += '-' + $(this).val();

	  		});
	  		if(dobavljaci == ''){
	  			dobavljaci = '-0';
	  		}

	  		var tipovi = '';
	  		$("#tip_select :selected").each(function(){
	  			tipovi += '-' + $(this).val();

	  		});
	  		if(tipovi == ''){
	  			tipovi = '-0';
	  		}

	  		var labela = '';
	  		$("#labela_select :selected").each(function(){
	  			labela += '-' + $(this).val();

	  		});
	  		if(labela == ''){
	  			labela = '-0';
	  		}

	  		var karakteristika = ''; 
	  		if($("#karakt_select :selected").val() != '' && $("#karakt_select :selected").val() != '0' ){
	  			if ($(this).data('kind')=='NE') {
		  			karakteristika += '-' + $("#karakt_select :selected").val();
		  		}else{
		  			karakteristika += $("#karakt_select :selected").val();
		  		}
		  	}else{
		  		karakteristika='0';
		  	}
		  	if(karakteristika == 'undefined'){
		  		karakteristika='0';
		  	} 		

	  		var magacin = '';
  			magacin = $("#magacin_select").val();
	  		if(magacin == ''){
	  			magacin = '0';
	  		}

	  		var exporti = '';
  			exporti = $("#export_select").val();
	  		if(exporti == '' || exporti==null){
	  			exporti = '0';
	  		}
	  		
	  		var katalog = '';
  			katalog = $("#katalog_select").val();
	  		if(katalog == '' || katalog==null){
	  			katalog = '0';
	  		}

	  		location.href = base_url+'admin/artikli/'+criteria[0]+'/'+proizvodjaci.substr(1)+'/'+dobavljaci.substr(1)+'/'+tipovi.substr(1)+'/'+labela.substr(1)+'/'+karakteristika+'/'+magacin+'/'+exporti+'/'+katalog+'/'+criteria[9]+'/'+criteria[10]+'/'+criteria[11]+'/'+criteria[12];
	  });
	  
	// add mass images on product list
	$( "#JSSelectGalleryImageAssign" ).click(function(){
		var images = [];
		$(".JSSelectedGalleryImage").each(function(index,value){
			images.push({image: $(value).data('id'), alt: $(value).parent().parent().find('.JSGalleryImageAlt').val()});
		});
		
	  	if(roba_ids.length > 0 && images.length > 0){
	  		$("#JSGalleryModal").foundation('reveal', 'close');
	  		$('#wait').show();
	  		$.post(base_url+'admin/ajax/articles', {action:'assign_selected_image', roba_ids: roba_ids, images: images}, function (response){
	  			alertify.success('Slika je dodeljena!');
			  	$.each(selected_roba_ids, function( index, value ) {
			  		var obj = $("#selectable").find('[data-id="'+ value +'"]');
					var obj_br = obj.find('.web_slika')
				  	var br = parseInt(obj_br.text()) + images.length;
				  	obj_br.text(br);

				  	if(filters[4] == 0){
				  		obj.remove();
				  	}
				});
				$("#JSSelectedGalleryImages").html('<span class="options-title-img">'+translate('Niste izabrali sliku.')+'</span>');

				$('#wait').hide();
	  		});
	  	}
	  	else{
	  		alertify.alert(translate('Slika nije izabrana!'));
	  	}
	});

	   //modal data generisane
		$('#JScharacteristics').on('click', function(){
			var grupa_pr_id = criteria[0];
		  	if(roba_ids[0] && grupa_pr_id != 0){
		  		$('#wait').show();
		  		$.post(base_url+'admin/ajax/articles', {action:'data-generisane', grupa_pr_id: grupa_pr_id}, function (response){
		  			var response = $.parseJSON(response);
		  			if(response['status']!='success'){
						alertify.alert('Niste odabrali grupu!');
					}else{
						$('#JSTableContentGenerisane').html(response['table_content']);
						$('#JScharacteristicsModal').foundation('reveal', 'open');
					}
					$('#wait').hide();
		  		});
		  	}
		  	else{
		  		alertify.alert(translate('Nema selektovanih artikala ili niste odabrali grupu!'));
		  	}
		});
	  //generisane karakteristike add
		$(document).on('click','.JSAddGenerisane',function(){
			var karakteristika_naziv_id = $(this).data('naziv_id');
			var karakteristika_vrednost_id = $(this).closest('tr').find('.JSGenerisaneVrednost').val();
	  		$('#wait').show();
	  		$.post(base_url+'admin/ajax/articles', {action:'add-generisane', roba_ids: roba_ids, karakteristika_naziv_id: karakteristika_naziv_id, karakteristika_vrednost_id: karakteristika_vrednost_id}, function (response){
	  			alertify.success(translate('Karakteristika je dodeljena!'));
				$('#wait').hide();
	  		});
		});
	  //generisane karakteristike delete
		$(document).on('click','.JSDeleteGenerisane',function(){
			var karakteristika_naziv_id = $(this).data('naziv_id');
	  		$('#wait').show();
	  		$.post(base_url+'admin/ajax/articles', {action:'delete-generisane', roba_ids: roba_ids, karakteristika_naziv_id: karakteristika_naziv_id}, function (response){
	  			alertify.success(translate('Karakteristika je obrisana!'));
				$('#wait').hide();
	  		});
		});

	  //product edit
		$('.JSArticleEdit').click(function(){
			var roba_id = $(this).closest('tr').data('id');
			var zakljucan = $(this).data('zakljucan');
			var short = $(this).data('short');
			if(zakljucan == '0'){
				if(short==0){
					window.open(base_url+'admin/product/'+roba_id, '_blank');
				}else{
					window.open(base_url+'admin/product-short/'+roba_id, '_blank');
				}
			}else{
				$('#JSArticleEditYes').data('zakljucan-id',roba_id);
				$('#JSArticleEditModal').foundation('reveal', 'open');
			}
		});
		$('#JSArticleEditYes').click(function(){
			var roba_id = $(this).data('zakljucan-id');
			var dataUpd = [{"table":"roba", "column":"flag_zakljucan", "val":"false"},{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"0"}];
			var roba_ids = [roba_id];
	  		$.post(base_url+'admin/ajax/articles', {action:'execute', data: dataUpd, roba_ids: roba_ids}, function (response){
	  			alertify.success(translate('Artikal je otključan!'));
				$("#selectable").find('[data-id="'+ roba_id +'"]').find('.JSArticleEdit').data('zakljucan','0');
				$("#selectable").find('[data-id="'+ roba_id +'"]').find('.'+dataUpd[0].column).text('NE');
				$('.close-reveal-modal').trigger('click');
				setTimeout(function(){
					window.open(base_url+'admin/product/'+roba_id, '_blank');
				}, 1500);
	  		});
		});
		$('#JSArticleEditNo').click(function(){
			$('.close-reveal-modal').trigger('click');
		});
			// brzi edit naziva kod artikla


		$('.JSnaziv1Input').hide();

		$('.JSEditNaziv1Btn').on('click', function(){
			$(this).parent().children('.JSnaziv1Input').show();
			$(this).parent().children('.JSnaziv1Input').focus();
		});

		$('.JSnaziv1Input').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSnaziv1Input').on("keyup", function(event) {
			var val = $(this).val();
			var art_id = $(this).closest('tr').data('id');

			if(event.keyCode == 13){
				$(this).closest('td').find('.JSnaziv1Vrednost').html(val);
		    	$(this).focusout();

		    	$.post(base_url+'admin/ajax/dc_articles', {action:'fast_edit_name1', art_id: art_id, val: val}, function (response){
		    		alertify.success(translate('Uspešno ste sačuvali naziv'))
		    	});
		   	}
		});
		//kraj

	// brzi edit kolicine kod artikla
		$('.JSkolicinaInput').hide();

		$('.JSEditkolicinaBtn').on('click', function(){
			$(this).parent().children('.JSkolicinaInput').show();
			$(this).parent().children('.JSkolicinaInput').focus();
		});

		$('.JSkolicinaInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSkolicinaInput').on("keyup", function(event) {
			var selected_element = $(this);
			var val = selected_element.val();
			var art_id = selected_element.closest('tr').data('id');
			var orgj_id = criteria[6];

			if(event.keyCode == 13){

		    	$.post(base_url+'admin/ajax/dc_articles', {action:'promena_kolicine_artikal', art_id: art_id, val: val, orgj_id: orgj_id}, function (response){
					selected_element.closest('.kolicina').children('.JSkolicinaVrednost').html(response);
			    	selected_element.focusout();
			    	if(orgj_id != 0){
			    		alertify.success(translate('Uspešno ste promenili količinu'));
			    	}else{
			    		alertify.success(translate('Uspešno ste promenili količinu')+' '+translate('glavnog magacina'));
			    	}
		    	});
		   	}
		});
		//kraj

	//brzi edit web cene

		$('.JSWebCenaInput').hide();

		$('.JSEditWebCenaBtn').on('click', function(){
			$(this).parent().children('.JSWebCenaInput').show();
			$(this).parent().children('.JSWebCenaInput').focus();
		});

		$('.JSWebCenaInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSWebCenaInput').on("keyup", function(event) {
			var val = $(this).val();
			var art_id = $(this).closest('tr').data('id');

			if(event.keyCode == 13){
				$(this).closest('.WebCena').children('.JSWebCenaVrednost').html(val);
		    	$(this).focusout();

		    	$.post(base_url+'admin/ajax/dc_articles', {action:'fast_edit_WebCena', art_id: art_id, val: val}, function (response){
			    	$("#selectable").find('[data-id="'+ art_id +'"]').find('.web_marza').text(response);	
			    		alertify.success(translate('Uspešno ste promenili Web Cenu i Web Maržu'))
		    	});
	  		}
		});


	//brzi edit end-cene
	$('.JSEndCenaInput').hide();

		$('.JSEditEndCenaBtn').on('click', function(){
			$(this).parent().children('.JSEndCenaInput').show();
			$(this).parent().children('.JSEndCenaInput').focus();
		});

		$('.JSEndCenaInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSEndCenaInput').on("keyup", function(event) {
			var val = $(this).val();
			var art_id = $(this).closest('tr').data('id');

			if(event.keyCode == 13){
				$(this).closest('.EndCena').children('.JSEndCenaVrednost').html(val);
		    	$(this).focusout();

		    	$.post(base_url+'admin/ajax/dc_articles', {action:'fast_edit_EndCena', art_id: art_id, val: val}, function (response){
			    	$("#selectable").find('[data-id="'+ art_id +'"]').find('.end_marza').text(response);	
			    		alertify.success(translate('Uspešno ste promenili End Cenu'))
		    	});
	  		}
		});







	// brzi edit sifre
		$('.JSCodeInput').hide();
		$('.JSEditCodeBtn').on('click', function(){
			$(this).parent().children('.JSCodeInput').show();
			$(this).parent().children('.JSCodeInput').focus();
		});
		$('.JSCodeInput').focusout(function(){
			$(this).css('display', 'none');
		});
		$('.JSCodeInput').on("keyup", function(event) {
			var selected_element = $(this);
			var val = selected_element.val();
			var art_id = selected_element.closest('tr').data('id');

			if(event.keyCode == 13){
		    	$.post(base_url+'admin/ajax/articles', {action:'promena_sifre_artikal', art_id: art_id, val: val}, function (response){
					selected_element.closest('.live-edit').children('.JSCodeValue').html(response.value);
			    	selected_element.focusout();
			    	alertify.success(translate(response.message));
		    	});
		   	}
		});

	// Dodatne grupe
		$('#JSDodatneGrupeBtn').on("click", function(event) {
			var grupe_ids = $('#JSDodatneGrupe').val();
			if(grupe_ids && grupe_ids.length > 0){
		    	$.post(base_url+'admin/ajax/articles', {action:'dodela_dodatnih_grupa',dodela_dodatnih_grupa_akcija:'dodela', roba_ids: roba_ids, grupe_ids: grupe_ids}, function (response){
		    		alertify.success(translate('Uspešno ste promenili grupu/e!'))
		    	 });
		    }else{
		    	alertify.error(translate('Nema selektovanih grupa!'));
		    }
		});
		$('#JSDodatneGrupeObrisiBtn').on("click", function(event) {
			var grupe_ids = $('#JSDodatneGrupe').val();
			if(grupe_ids && grupe_ids.length > 0){
		    	$.post(base_url+'admin/ajax/articles', {action:'dodela_dodatnih_grupa',dodela_dodatnih_grupa_akcija:'brisanje', roba_ids: roba_ids, grupe_ids: ((grupe_ids && grupe_ids.length > 0) ? grupe_ids : [])}, function (response){
		    		alertify.success(translate('Uspešno ste obrisali dodatnu grupu/e!'))
		    	 });
		    }else{
		    	alertify.error(translate('Nema selektovanih grupa!'));
		    }
		});
		$('#JSDodatneGrupeObrisiSveBtn').on("click", function(event) {
	    	$.post(base_url+'admin/ajax/articles', {action:'dodela_dodatnih_grupa',dodela_dodatnih_grupa_akcija:'obrisi_sve', roba_ids: roba_ids}, function (response){
	    		alertify.success(translate('Uspešno ste obrisali sve dodatne grupe!'))
	    	 });
		});
		// end brzi edit web cene

	// Right click menu
	var active = 0;
	$('.JSarticles-listing').on("contextmenu", function(e) {
		$('.custom-menu, .articles-mn').css('top', e.pageY + "px");
		$('.custom-menu, .articles-mn').css('left', e.pageX + "px" );

		if(active == 0) {
			$('.custom-menu').addClass('custom-menu-active');
			active = 1;
		} else if(active == 1) {
			$('.custom-menu').removeClass('custom-menu-active');
			active = 0;
		}
		
    	return false;
  	});

	$(document).on('click', function(){
		$('.custom-menu').removeClass('custom-menu-active');
		active = 0;
	});

	$('.custom-menu-item').on('click', function(){
		active = 0;
	});

$(function() {
     
    $('#JSKarakteristike').sortable({
        //observe the update event...
        update: function(event, ui) {
            //create the array that hold the positions...
            var order = []; 
            //loop trought each li...
            $('#JSKarakteristike li').each( function(e) {

            //add each li position to the array...     
            // the +1 is for make it start from 1 instead of 0
            order.push( $(this).attr('id') );                
            });
            $.ajax({
                type: "POST",
                url: base_url+'admin/position-nazivKarak',
                data: {order:order},
                success: function(msg) {
                   console.log('test');
                }
            });
        }
    });                    
    $( "#JSKarakteristike").disableSelection();
                        
});  

// $(function() {
     
//     $('#JSListChar').sortable({
//         //observe the update event...
//         update: function(event, ui) {
//             //create the array that hold the positions...
//             var order = []; 
//             //loop trought each li...
//             $('#JSListChar li').each( function(e) {

//             //add each li position to the array...     
//             // the +1 is for make it start from 1 instead of 0
//             order.push( $(this).attr('id') );                
//             });
//             $.ajax({
//                 type: "POST",
//                 url: base_url+'admin/position-char',
//                 data: {order:order},
//                 success: function(msg) {
//                    console.log('test');
//                 }
//             });
//         }
//     });                    
//     $( "#JSListChar").disableSelection();
                        
// });  

	$(document).keydown(function(e){
		if(e.which == 114){
			$('#all').trigger('click');
			return false;
		} else if(e.which == 113){
			window.open(base_url+'admin/product/0', '_blank');
			return false;
		} else if(e.which == 115){
			$('#JSIzmeni').trigger('click');
			return false;
		} else if(e.which == 118){
			$('#JSUrediOpis').trigger('click');
			return false;
		} else if(e.which == 119){
			$('#JSObrisiArtikal').trigger('click');
			return false;
		}

	});


	
	$('.custom-menu-sub').hover(function(){
		$('.custom-menu-sub-item').toggleClass('active-sub');
	});

	// Article images dropdown
	$('.JSimages-options').hide();
	$('.JSimages-head').on('click', function(){
		$('.JSimages-options').toggle();
	});

	/* JS TREE VIEW CATEGORIES */
	$(function () {
	    $('#jstree').jstree({
	       "core" : {
	        "animation" : 0,
	        "check_callback" : true,
	        "themes" : { "stripes" : true },
	      },
	      "types" : {
	        "#" : {
	          "max_children" : 1,
	          "max_depth" : 5,
	          "valid_children" : ["root"]
	        },
	        "root" : {
	          "icon" : "/static/3.3.3/assets/images/tree_icon.png",
	          "valid_children" : ["default"]
	        },
	        "default" : {
	          "valid_children" : ["default","file"]
	        },
	        "file" : {
	          "icon" : "glyphicon glyphicon-file",
	          "valid_children" : []
	        }
	      },
	      "plugins" : [
	            "dnd", "search", "types", "wholerow"
	        ]
	    })
	    .on("click", "a",
	        function() {
	            document.location.href = this;
	        }
	    );
	});

    $("#jstree li").on("click", "a",
        function() {
            document.location.href = this;
        }
    );

	$('#JSSimpleExportSelected').on('click', function(){
    	location.href = base_url+'admin/export_xls?roba_ids='+roba_ids.join('-');
    });

});

// $('#article-toggler').on('click', function(){
// 	$('#article-toggle').slideToggle('slow');
// });

/* CUSTOM MENU OPTIONS */

// $('#JSchooseGroup').on('click', function(){
// 	$('#JSchooseGroupModal').foundation('reveal', 'open');
// });

// $('#JSchooseBrand').on('click', function(){
// 	$('#JSchooseBrandModal').foundation('reveal', 'open');
// });

// $('#JSchooseTip').on('click', function(){
// 	$('#JSchooseTipModal').foundation('reveal', 'open');
// });

// $('#JSchoosePrice').on('click', function(){
// 	$('#JSchoosePriceModal').foundation('reveal', 'open');
// });

// $('#JSchooseQuantity').on('click', function(){
// 	$('#JSchooseQuantityModal').foundation('reveal', 'open');
// });

// $('#JSTezina').on('click', function(){
// 	$('#JSTezinaModal').foundation('reveal', 'open');
// });
// $('#JSTag').on('click', function(){
// 	$('#JSTagModal').foundation('reveal', 'open');
// });

// $('#JSchooseTax').on('click', function(){
// 	$('#JSchooseTaxModal').foundation('reveal', 'open');
// });
	
// $('#JSchoosePictures').on('click', function(){
// 	$('#JSchoosePicturesModal').foundation('reveal', 'open');
// });

// $('#JSchooseExport').on('click', function(){
// 	$('#JSchooseExportModal').foundation('reveal', 'open');
// });

// $('#JSmassEdit').on('click', function(){
// 	$('#JSmassEditModal').foundation('reveal', 'open');
// });

// $('#JSobradaKarakteristika').on('click', function(){
// 	$('#JSobradaKarakteristikaModal').foundation('reveal', 'open');
// });

// $('#JStextEdit').on('click', function(){
// 	$('#JStextEditModal').foundation('reveal', 'open');
// });

// $('#JSakcijaEdit').on('click', function(){
// 	$('#JSakcijaModal').foundation('reveal', 'open');
// });

// $('#JSdefMarza').on('click', function(){
// 	$('#JSdefMarzaModal').foundation('reveal', 'open');
// });

 