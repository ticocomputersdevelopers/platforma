$(document).ready(function(){

	$(function() {
		$( "#datum_do_din, #datum_od_din" ).datepicker();
	});

	$('#datum_do_din').keydown(false);
	$('#datum_od_din').keydown(false);

	//
	$('#datum_od_din').change(function() {
	var params = getUrlVars();
	params['datum_od_din'] = $(this).val();
	params['datum_do_din'] = $('#datum_do_din').val();

	if(datum_do_din != '' && datum_od_din != ''){
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	}

	});
	$('#datum_do_din').change(function() {
	var params = getUrlVars();
	params['datum_do_din'] = $(this).val();
	params['datum_od_din'] = $('#datum_od_din').val();

	if(datum_do_din != '' && datum_od_din != ''){
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	}
	});

	$('.JSStatusNarudzbine').click(function() {
	var params = getUrlVars();
	var status_list = params['status'] ? params['status'].split('-') : [];
	var this_status = $(this).data('status');
	var status_index = status_list.findIndex(function(item) { return item == this_status; });

	if($(this).is(":checked")){
		if(this_status == 'head'){
			status_list = ['nove','prihvacene','realizovane','stornirane'];
		}else{		
			if(status_index == -1){
				status_list.push($(this).data('status'));
			}
		}
	}else{		
		if(this_status == 'head'){
			status_list = [];
		}else{
			if(status_index > -1){
				delete status_list[status_index];
			}
		}
	}
	params['status'] = status_list.filter(function(item) { if(item){ return item; } }).join('-');

	if(status_list.length == 0){
		delete params['status'];
	}
	window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});


	// $('#pretraga_prihoda').on('click', function(){
	// 	var dt_od = $('#datum_od_din').val();
	// 	var dt_do = $('#datum_do_din').val();
	// 	console.log('clicked');
	// 	if(dt_od != '' && dt_do != ''){
	// 		window.location.href = base_url + 'admin/analitika/' + dt_od + '/' + dt_do;
	// 	}
	// });

	$('.JSAnalitikaGrupa').click(function() {
		var id = $(this).data('id');
		if($(this).data('isopen') == 'closed'){
			$(this).data('isopen','opened');
			$(".JSAnalitikaArtikliGrupe[data-id='"+id+"']").removeAttr('hidden');
		}else{
			$(this).data('isopen','closed');
			$(".JSAnalitikaArtikliGrupe[data-id='"+id+"']").attr('hidden','hidden');
		}
	});


	//ankete
	$('#JSAnketaKupacId').change(function() {
		var web_kupac_id = $(this).val();

		if(web_kupac_id != ''){
			var params = getUrlVars();
			delete params['web_b2c_narudzbina_id'];
			params['web_kupac_id'] = web_kupac_id;

			window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '')+'#poll_position';
		}
	});
	$('#JSAnketaNarudzbinaId').change(function() {
		var web_b2c_narudzbina_id = $(this).val();

		if(web_b2c_narudzbina_id != ''){
			var params = getUrlVars();
			params['web_b2c_narudzbina_id'] = web_b2c_narudzbina_id;

			window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '')+'#poll_position';
		}
	});

	$(function() {
		$( "#anketa_datum_do, #anketa_datum_od" ).datepicker();
	});
	$('#anketa_datum_do').keydown(false);
	$('#anketa_datum_od').keydown(false);
	//
	$('#anketa_datum_od').change(function() {
		var anketa_datum_od = $(this).val();

		if(anketa_datum_od != ''){
			var params = getUrlVars();
			params['anketa_datum_od'] = anketa_datum_od;

			window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '')+'#poll_position';
		}
	});
	$('#anketa_datum_do').change(function() {
		var anketa_datum_do = $(this).val();

		if(anketa_datum_do != ''){
			var params = getUrlVars();
			params['anketa_datum_do'] = anketa_datum_do;

			window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '')+'#poll_position';
		}

	});
	$('#JSAnketeDatumPonisti').click(function() {
		var params = getUrlVars();
		delete params['anketa_datum_do'];
		delete params['anketa_datum_od'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '')+'#poll_position';
	});


	function getUrlVars() {
	    var vars = {};
	    var parts = window.location.href.replace('#poll_position','').replace('#','').replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	}

  });