$(document).ready(function(){
	//predracuni
	$('.JSSort').click(function(){
		var params = getUrlVars();
		params['sort_column'] = $(this).data('sort_column');
		params['sort_direction'] = $(this).data('sort_direction');
		delete params['page'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('.JSVrstaDokumenta').click(function(){
		var redirect = '';
		if($(this).val() == 'ponuda'){
			redirect = "/dokumenti/ponude";
		}else if($(this).val() == 'racun'){
			redirect = "/dokumenti/racuni";
		}else if($(this).val() == 'predracun'){
			redirect = "/dokumenti/predracuni";
		}else{
			var params = { vrsta_dokumenta: $(this).val() };

			redirect = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
		}

		window.location.href = redirect;
	});
	$('#JSParnerSearch').change(function(){
		var params = getUrlVars();
		params['partner_id'] = $(this).val();
		delete params['page'];
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	$('#JSStatusSearch').change(function(){
		var params = getUrlVars();
		params['dokumenti_status_id'] = $(this).val();
		delete params['page'];
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	//predracun
	var kupciTime;
	$(document).on("keyup", '#JSPartnerNaziv', function() {
		clearTimeout(kupciTime);
		kupciTime = setTimeout(function(){
			$('#JSPartnerId').val('0');

			$('.JSPartnerSearchList').remove();
			var partner = $('#JSPartnerNaziv').val();
			if (partner != '' && partner.length > 2) {
				$.post(base_url + 'dokumenti/ajax/partner-pretraga', { partner: partner }, function (response){
					$('#JSPartnerSearchContent').html(response);
				});
			} 				
		}, 500);
	});
	$(document).on("click", ".JSPartnerSearchItem", function() {
		$('#JSPartnerNaziv').val($(this).find('.JSPartnerSearchNaziv').text());
		$('#JSPartnerId').val($(this).data('partner_id'));

		$.post(base_url + 'dokumenti/ajax/partner-podaci', { partner_id: $(this).data('partner_id') }, function (response){
			var result = $.parseJSON(response);
			$('#JSPartnerPib').val(result.pib.trim());
			$('#JSPartnerAdresa').val(result.adresa);
			$('#JSPartnerMesto').val(result.mesto);
			$('#JSPartnerKontaktOsoba').val(result.kontakt_osoba);
			$('#JSPartnerMail').val(result.mail);
			$('#JSPartnerTelefon').val(result.telefon);
		});

		$('.JSPartnerSearchList').remove();
	});
	$('html :not(.JSPartnerSearchList)').on("click", function() {
		$('.JSPartnerSearchList').remove();
	});


	$('input[name="datum_predracuna"]').datepicker({
		dateFormat: 'dd-mm-yy'
	});
	$('input[name="vazi_do"]').datepicker({
		dateFormat: 'dd-mm-yy'
	});
	$('input[name="rok_isporuke"]').datepicker({
		dateFormat: 'dd-mm-yy'
	});

	$('input[name="datum_predracuna"]').change(function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});
	$('input[name="vazi_do"]').change(function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});
	$('input[name="predpredracun"]').click(function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});
	$('input[name="predracun"]').click(function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});
	$('input[name="rok_isporuke"]').click(function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});
	$('input[name="nacin_placanja"]').keyup(function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});
	$('#JSPredracunStatus').on('change',function(){
		$('#JSPredracunSubmit').css('background-color','red');
	});

	//stavke
	$('.JSRobaId').select2({placeholder: 'Izaberi stavku'});
	$('.JSRobaId').change(function(){
		var roba_id = $(this).val();
		var stavka_row = $(this).closest('.JSStavkaRow');
		var predracun_stavka_id = stavka_row.attr('data-predracun_stavka_id');
		var data = { change: 'roba', predracun_stavka_id: predracun_stavka_id, roba_id: roba_id };
		if(predracun_stavka_id == 0){
			data['predracun_id'] = stavka_row.attr('data-predracun_id');
		}

		$.post(base_url + 'dokumenti/ajax/predracun-stavka-save', data, function (response){
			alertify.success('Stavka je sačuvana.');
			if(predracun_stavka_id == 0){
				setTimeout(function(){ window.location.href = window.location.pathname; },500);
			}
			var result = $.parseJSON(response);
			stavka_row.find('.JSRobaIdText').text(result.roba_id);
			stavka_row.find('.JSNsbCena').val(parseFloat(result.nab_cena).toFixed(2));
			stavka_row.find('.JSUkupno').val(parseFloat(result.nab_cena*(1+result.pdv/100)*result.kolicina).toFixed(2));
			$('#JSPredracunUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
		});

	});

	var stavkaNazivTime;
	$(document).on("keyup", '.JSStavkaNaziv', function() {
		var naziv_stavke = $(this).val();
		var stavka_row = $(this).closest('.JSStavkaRow');
		var predracun_stavka_id = stavka_row.attr('data-predracun_stavka_id');
		
		clearTimeout(stavkaNazivTime);
		stavkaNazivTime = setTimeout(function(){
			$.post(base_url + 'dokumenti/ajax/predracun-stavka-save', { change: 'naziv_stavke', predracun_stavka_id: predracun_stavka_id, naziv_stavke: naziv_stavke }, function (response){
				var result = $.parseJSON(response);
				stavka_row.find('.JSStavkaNaziv').text(result.naziv_stavke);
				$('#JSPredracunUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
				alertify.success('Stavka je sačuvana.');
			});			
		}, 500);
	});

	$('.JSKolicina').keyup(function(){
		var kolicina = $(this).val();
		if(!isNaN(kolicina) && kolicina > 0){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var predracun_stavka_id = stavka_row.attr('data-predracun_stavka_id');

			if(predracun_stavka_id > 0){
				$('.select2-container').css('border','');

				$.post(base_url + 'dokumenti/ajax/predracun-stavka-save', { change: 'kolicina', predracun_stavka_id: predracun_stavka_id, kolicina: kolicina }, function (response){
					var result = $.parseJSON(response);
					stavka_row.find('.JSKolicina').val(result.kolicina);
					stavka_row.find('.JSUkupno').val(parseFloat(result.nab_cena*(1+result.pdv/100)*result.kolicina).toFixed(2));
					$('#JSPredracunUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
					alertify.success('Stavka je sačuvana.');
				});
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			$(this).css('border-color', 'red');
		}
	});	
	$('.JSPdv').keyup(function(){
		var pdv = $(this).val();
		if(!isNaN(pdv)){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var predracun_stavka_id = stavka_row.attr('data-predracun_stavka_id');
			if(predracun_stavka_id > 0){
				$('.select2-container').css('border','');

				$.post(base_url + 'dokumenti/ajax/predracun-stavka-save', { change: 'pdv', predracun_stavka_id: predracun_stavka_id, pdv: pdv }, function (response){
					var result = $.parseJSON(response);
					stavka_row.find('.JSPdv').val(result.pdv);
					stavka_row.find('.JSUkupno').val(parseFloat(result.nab_cena*(1+result.pdv/100)*result.kolicina).toFixed(2));
					$('#JSPredracunUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
					alertify.success('Stavka je sačuvana.');
				});
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			stavka_row.find('.select2-container').css('border','1px solid red');
		}
	});

	var stavkaCenaTime;
	$('.JSNsbCena').keyup(function(){
		var nab_cena = $(this).val();
		if(!isNaN(nab_cena) && nab_cena > 0){
			$(this).css('border-color', '');
			var stavka_row = $(this).closest('.JSStavkaRow');
			var predracun_stavka_id = stavka_row.attr('data-predracun_stavka_id');
			if(predracun_stavka_id > 0){
				$('.select2-container').css('border','');

				clearTimeout(stavkaCenaTime);
				stavkaCenaTime = setTimeout(function(){
					$.post(base_url + 'dokumenti/ajax/predracun-stavka-save', { change: 'nab_cena', predracun_stavka_id: predracun_stavka_id, nab_cena: nab_cena }, function (response){
						var result = $.parseJSON(response);
						// stavka_row.find('.JSNsbCena').val(parseFloat(result.nab_cena).toFixed(2));
						stavka_row.find('.JSUkupno').val(parseFloat(result.nab_cena*(1+result.pdv/100)*result.kolicina).toFixed(2));
						$('#JSPredracunUkupanIznos').text(parseFloat(result.iznos).toFixed(2));
						alertify.success('Stavka je sačuvana.');
					});
				},500);
			}else{
				stavka_row.find('.select2-container').css('border','1px solid red');
			}
		}else{
			$(this).css('border-color', 'red');
		}
	});

	$(document).on('keyup', 'input.select2-search__field', function(e) {
		var code = e.keyCode || e.which;
		if(code == 13 && $(this).val() != ''){
			var naziv_stavke = $(this).val();
			// $('#JSStavkaNaziv').attr('type','text');
			// $('#JSStavkaNaziv').val(text);
			// $('.select2-dropdown').hide();
			// $('.JSRobaId[data-roba_id="0"]').closest('td').find('.select2-container').hide();
			var data = { change: 'naziv_stavke', predracun_stavka_id: 0, naziv_stavke: naziv_stavke, predracun_id: $('input[name="predracun_id"]').val()};

			$.post(base_url + 'dokumenti/ajax/predracun-stavka-save', data, function (response){
				alertify.success('Stavka je sačuvana.');
				setTimeout(function(){ window.location.href = window.location.pathname; },500);
			});
		}
	});	

	$('#JSPredracunSabloniLink').click(function(){
		if($('#JSPredracunSabloni').attr("hidden")){
			$('#JSPredracunSabloni').removeAttr('hidden');
		}else{
			$('#JSPredracunSabloni').attr('hidden','hidden');
		}
	});

});