
$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip(); 

// SCROLL TO TOP
$(window).scroll(function () {
	if ($(this).scrollTop() > 150) { $('.JSscroll-top').css('right', '20px'); }
	else { $('.JSscroll-top').css('right', '-70px'); }
}); 
$('.JSscroll-top').click(function () {
	$('body,html').animate({ scrollTop: 0 }, 600);
	return false;
});  
 
// SLICK SLIDER INCLUDE 
if ($('input[name="JSstart_page"]').length){
	$('#JSmain-slider').slick({
		autoplay: true
	}); 
}

// PRODUCT SLICK   
if ($('.JSproducts_slick')[0]){
	$('.JSproducts_slick').slick({ 
		autoplay: true,
		slidesToShow: 4,
		slidesToScroll: 1,  
		responsive: [
		{
			breakpoint: 1160,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 880,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});
}
// RELATED ARTICLES TITLE 
$('.slickTitle').each(function(i, tit){
	if($(tit).next('.JSproducts_slick').find('.slick-track').children('.product').length){
		$(tit).show();
	}
})
    
	// CATEGORIES - MOBILE
if ($(window).width() < 1024 ) {
	$('.JScategories .fa-bars').click(function () {
    	$('.JSlevel-1').slideToggle();
	});
}


$('.JSlevel-1 li').each(function(i, li){
	if($(li).children('ul').length > 0){
		$(li).append('<span class="JSsubcat-toggle"><i class="fas fa-chevron-down"></i></span>');
	}
}); 

$('.JSsubcat-toggle').on('click', function() {
 	$(this).toggleClass('rotate');
    $(this).siblings('ul').slideToggle();
	$(this).parent().siblings().children('ul').slideUp();
	$(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
}); 

 
// SUBCATEGORIES 
if ($(window).width() > 991 ) {
	$('.JSlevel-1 li').each(function () {
		var subitems = $(this).find(".JSlevel-2 > li");
		for(var i = 0; i < subitems.length; i+=2) {
			subitems.slice(i, i+2).wrapAll("<div class='clearfix'></div>");
		}
	});
}

// SELECT ARROW - FIREFOX FIX
$('select').wrap('<span class="select-wrapper"></span>');

var searchTime;
$('#search').on("keyup", function() {
	clearTimeout(searchTime);
	searchTime = setTimeout(timer, 500);
});
// $('html :not(.JSsearch_list)').on("click", function() {
// 	$('.JSsearch_list').remove();
// });

function timer(){  
	$('.search_list').remove();
	var search = $('#search').val();
	if (search != '') {
		$.post(base_url + 'livesearch', {search:search}, function (response){
			//$('#JSlive-search').append(response);
		});
	} 
} 

(function($){
	$("#currency_eur").on("click", function(event){
		var base_url= $('#base_url').val();
		event.preventDefault();
		$.get(base_url + 'b2b/valuta/2', function (response){
			location.reload(true);
		});
	});	
	$("#currency_rsd").on("click", function(event){
		var base_url= $('#base_url').val();
		event.preventDefault();
		$.get(base_url + 'b2b/valuta/1', function (response){
			location.reload(true);
		});
	});	
})(jQuery); 

(function($){
	var total_amount = $("#header_total_amount").val();
	$("#header_total_amount_show").html(total_amount);
})(jQuery);

var base_url= $('#base_url').val();
// $.ajax({
// 	type: "GET",
// 	url: base_url+'b2b/cart_content',
// 	success:function(res){
// 		$('#header-cart-content').html(res.cartContent);
// 	}
// });

$('.JSProductListCartLess').click(function(){
	var input_element = $(this).parent().find('.JSProductListCartAmount');
	if(!isNaN(input_element.val())){	
		var current_val = parseInt(input_element.val());
		if(current_val > 1){
			input_element.val(current_val - 1);
		}
	}
});
$('.JSProductListCartMore').click(function(){
	var input_element = $(this).parent().find('.JSProductListCartAmount');
	var current_val = parseInt(input_element.val());
	if(!isNaN(input_element.val())){
		input_element.val(current_val + 1);
	}
});

$('.add-to-cart-products').click(function(){
	var id = $(this).data('roba-id');
	var quantity =  $('#quantity-'+id).val();
	var max = $(this).data('max-quantity');

	if(isNaN(quantity)){
		$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
		$('.info-popup .popup-inner').html("<p class='p-info'>Tražena količina nije dostupna.</p>");	
	}else{
		if(quantity<1){
			$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
			$('#quantity-'+id).val(1);
		}
		if(quantity > max){
			$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+ max +" kom.</p>");
			$('#quantity-'+id).val(max);
		}
		else{
			var _this = $(this);
			$.ajax({
				type: "POST",
				url: base_url + 'b2b/cart_add',
				cache: false,
				data:{roba_id:id, status:2, quantity:quantity},
				success:function(res){
					$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
					$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
					$('#broj_cart').text(res.countItems);
					// $('#header-cart-content').html(res.cartContent);
					_this.data('max-quantity', res.cartAvailable);
					location.reload(true);
				}
			});
		}
	}
});
$('.JSadd-to-offer-products').click(function(){
	var id = $(this).data('roba-id');
	var quantity =  $('#quantity-'+id).val();

	if(!isNaN(quantity) && quantity > 0){
		var data = { roba_id: id, kolicina: quantity };
		$.post(base_url + 'b2b/dokumenti/ponuda-stavka-save', data, function (response){
			var result = $.parseJSON(response);
			if(result.success){
				$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
				$('.info-popup .popup-inner').html("<p class='p-info'>Stavka je dodata u ponudu.</p>");
			}else{
				$('.info-popup').fadeIn("fast").delay(1500).fadeOut();
				$('.info-popup .popup-inner').html("<p class='p-info'>Izaberite ponudu u kojoj hoćete da dodate stavku.</p>");				
			}
		});		
	}
});
$(document).on('click','.JSadd-to-offer-product',function() {
	// alert(base_url);
	var roba_id = $(this).data('roba_id');
	var kolicina = $('.JSProductListCartAmount').val();
	if(!isNaN(kolicina) && kolicina > 0){
		var data = { roba_id: roba_id, kolicina: kolicina };
		$.post(base_url + 'b2b/dokumenti/ponuda-stavka-save', data, function (response){
			var result = $.parseJSON(response);
			if(result.success){
				$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
				$('.info-popup .popup-inner').html("<p class='p-info'>Stavka je dodata u ponudu.</p>");
			}else{
				$('.info-popup').fadeIn("fast").delay(1500).fadeOut();
				$('.info-popup .popup-inner').html("<p class='p-info'>Izaberite ponudu u kojoj hoćete da dodate stavku.</p>");				
			}
		});		
	}
});

$('.filter-item').change(function (){
	var url_vars = getUrlVars();
	delete url_vars['page'];
	var name = $(this).data('name');
	var value = $(this).val();
	var old_value = $(this).data('old-value');
	var type = $(this).data('type');

	if(type=='add'){
		url_vars[name] = value;
	}else if(type=='remove'){
		if(old_value == value) {
			delete url_vars[name];
		}else {
			url_vars[name] = old_value.replace('-'+value,'').replace(value+'-','');
		}
	}
	window.location.href = window.location.pathname+(Object.keys(url_vars).length > 0 ? '?'+$.param(url_vars) : '');
});

/* ======== Togle filter =========== */
$('.filter-links').click(function() {
	$(this).next('.select-fields-content').slideToggle('fast'); 
}); 

 // RESPONSIVE NAVIGATON
 $(".resp-nav-btn").on("click", function(){  
 	$("#responsive-nav").toggleClass("openMe");		 
 });
 $(".JSclose-nav").on("click", function(){  
 	$("#responsive-nav").removeClass("openMe");		 
 });

 // ARTICLE MODAL
 $(function(){
 	var modal = $('#JSarticle-modal-container'),
 	articleImg = $('#JSarticle-img'),
 	modalImg = $("#JSarticle-modal-img"),
 	trigger = $('.product-view');
 	galeryImg = $('.elevatezoom-gallery');

 	trigger.click(function(){
 		if ((articleImg.attr('src').toLowerCase().indexOf('no-image') < 0) && ($(window).width() > 480)){ 
 			modal.show();
 			modalImg.src = this.src;
 		}
 	});  

 	galeryImg.on('mouseover', function(e){
 		e.preventDefault();
 		var miniImg = $(this).children('img').attr('src');
 		articleImg.attr('src', miniImg);
 		modalImg.attr('src', miniImg);
 	});

 	modal.on('click', function(){   
 		$(this).fadeOut('fast');
 	});   

 	$(".JSarticle-modal-close").click(function(){
 		modal.fadeOut('fast');
 	});  

 });

 // Main Content Height
 if ($(window).width() > 1024 ) {
 	$('.JScategories').on('mouseover' ,function(){
 		if ($('.main-content').height() < $('.JSlevel-1').height()) { 
 			$('.main-content').height($('.JSlevel-1').height());
 		}
 	}).on('mouseleave' ,function(){
 		$('.main-content').height('');
 	}); 
 } 

// POPUP BANER 
if ($(window).width() > 991 ) {
	var first_banner = $('.JSfirst-popup'),
	popup_img = $('.popup-img'),
	close_banner = $('.JSclose-me-please');

		if($('#JSmain-slider').length){ // START PAGE
			
			setTimeout(function(){ 
				first_banner.animate({ top: '50%' }, 700);
			}, 1500);   

		$(document).on('click', function(e){
			var target = $(e.target);
			if(!target.is(popup_img)) { 
				first_banner.hide();
			} 
		}); 
	} 
}

$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 

	 // LEFT & RIGHT BODY LINK
if ($(window).width() > 1024 ) {  
 	setTimeout(function(){ 
 		$('body').before($('.JSleft-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
 		$('body').after($('.JSright-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
 		$('.JSleft-body-link, .JSright-body-link').width(($(window).outerWidth() - $('.container').outerWidth())/2)
 		.height($('body').height()-$('footer').outerHeight()-$('header').outerHeight());   
 	}, 1000);
 } 

 // HIDE FILTERS IF EMPTY 
$('.select-fields-content').each(function(index, el){     
	if ( ! $(el).children().length) {  
		$(el).closest('li').hide(); 
	}
});     

// SHOW SELECTED FILTERS 
$('.select-fields-content label input').each(function(i, input){ 
	if ($(input).is(':checked')) {     
		$(input).parents('label').addClass('JSmarkedFilter').closest('div').show(); 
	}  
});     
if ($('.JSmarkedFilter').length) {
	$('.JShidden-if-no-filters').show();
} 
// FOOTER MAP
$('.JStoggMap').on('click', function(){
	$('.JSmap').slideToggle('fast');
});

//ACTIVE PAGE  
$('.pages li a').each(function(i, el){ 
	$(el).on('click', function(){   
		$(el).css('color', 'var(--orange)');   
	}); 
	if (location.pathname.indexOf(el.pathname) > -1) { 
		$(el).css('color', 'var(--orange)');  
	}
});

// ADDON TITLE
$('.addon-title').each(function(i, el){
	if ($(el).text().trim() != '') {
		// SHOW ALL
		$('.addon-title').show();
	}
});
 

// OVERLAPING CATEGORIES
 if ($('input[name="JSstart_page"]').length && $(window).width() > 991 ){
	
	$('.JSlevel-1').show();

	(function(){
	  	var categ = $('.JSlevel-1'),
	  		categ_offset_top = categ.offset().top,
	  		categ_offset_btn = categ_offset_top + categ.outerHeight(true), 
	  		slider_offset = $('.JSproducts_slick').eq(0).offset().top, 
	  		difference = (categ_offset_btn - slider_offset) + 10;

	  		if (categ_offset_btn > slider_offset) {
	  			$('.JSproducts_slick').eq(0).css('marginTop', difference + 'px');
	  		} 
	}()); 
}
 


}); // DOCUMENT READY END

// LOADER
document.onreadystatechange = function(){
	if (document.readyState === 'complete') {
		$('.JSspinner').hide();
	} 
}
