
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        $('.JSlazy_load').Lazy();
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg').addClass('img-responsive');
	});
   
	// Facebook
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '163422577814443',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.0'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
 

	// SLICK SLIDER INCLUDE
	if ($('.JSmain-slider').length) {
		if($('#admin_id').val()){

			$('.JSmain-slider').slick({
				autoplay: false,
				draggable: false
			});

		}else{
			$('.JSmain-slider').slick({
				autoplay: true
			});
		} 
	}
 
// PRODUCTS SLICK SLIDER 
if ($('.JSproducts_slick').length){ 
	$('.JSproducts_slick').slick({ 
		autoplay: true,
		slidesToShow: 3,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		]
	}); 
}
 
// RELATED ARTICLES TITLE 
	$('.slickTitle').each(function(i, tit){
		if($(tit).next('.JSproducts_slick').find('.slick-track').children('.JSproduct ').length){
			$(tit).show();
		}
	})
  
	// if ($('.JSBrandSlider')[0]){
	// 	$('.JSBrandSlider').slick({
	// 		autoplay: true,
	// 	    infinite: true,
	// 	    speed: 600,
	// 	    arrows: true,
	// 		slidesToShow: 5,
	// 		slidesToScroll: 3, 
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1100,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: false
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 800,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 480,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			}
	// 		]
	// 	});
	// }
	if ($('.JSblog-slick').length){

		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 3,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				  }
				}
			]
		});
	}
 
	// CATEGORIES - MOBILE
	if ($(window).width() < 991 ) {
		$('.JScategories .fa-bars').click(function () { 
	    	$('.JSlevel-1').slideToggle();
		});
	}

	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length > 0){
			$(li).append('<span class="JSsubcat-toggle"><i class="fas fa-chevron-down"></i></span>');
		}
	}); 

	$('.JSsubcat-toggle').on('click', function() {
	 	$(this).toggleClass('rotate');
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp();
		$(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
	}); 
 
 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 

// PRODUCT PREVIEW IMAGE
    if ($(".JSproduct-preview-image")[0]){
		jQuery(".JSzoom_03").elevateZoom({
			gallery:'gallery_01', 
			cursor: 'pointer',  
			imageCrossfade: true,  
			zoomType: 'lens',
			lensShape: 'round', 
			lensSize: 200, 
			borderSize: 1, 
			containLensZoom: true
		}); 
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
    } 

// GALLERY SLIDER
    (function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 1;


		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);
				$('.JSmodal').show();
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});

		$('.JSleft_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]); 
			 
			if (next_prev == img_gallery_img.length - 1) {
				next_prev = 0; 
			} else {
				next_prev++;
			}	  
		}); 

		$('.JSright_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]);
	 		next_prev--;

	 		if (next_prev < 0 ) {
	 			next_prev = img_gallery_img.length - 1;
	 		}   
		});  

		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());

// ======== SEARCH ============
 
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
  
	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("openMe");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("openMe");		 
	});
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").click(function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
	});

	if ($('.selected-filters li').length) {
		$('.JShidden-if-no-filters').show();
	} 
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');
	
 
 /*======= MAIN CONTENT HEIGHT ========*/
	if ($(window).width() > 1024 ) {
		$('.JScategories').on('mouseover' ,function(){
			if ($('.d-content').height() < $('.JSlevel-1').height()) { 
				$('.d-content').height($('.JSlevel-1').height());
			}
		}).on('mouseleave' ,function(){
			$('.d-content').height('');
		});
	 } 


// POPUP BANER 
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
		setTimeout(function(){ 
			$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		$(document).on('click', function(e){
			var target = $(e.target);
			if(!target.is($('.popup-img'))) { 
				$('.JSfirst-popup').hide();
			} 
		}); 
		if ($('.JSfirst-popup').length) {
			sessionStorage.setItem('popup','1');
		}
	}

	if ($(window).width() > 991 ) { 
		
// CATEGORIES WRAP
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=2) {
				subitems.slice(i, i+2).wrapAll("<div class='clearfix'></div>");
			}
		});

	// FOOTER COLS CLEARFIX
	  	(function(){
	  		var cols = $('.JSfooter-cols > div');
	  		for(var i = 0; i < cols.length; i+=4){
	  			cols.slice(i, i+4).wrapAll('<div class="clearfix JSfooter-secs"></div>');
	  		}
  		}()); 

	}

 // LEFT & RIGHT BODY LINK 
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 

 		}, 1000); 
  	} 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt();
// FAST LOOK 
	$(document).on('click', '.JSQuickViewButton', function() {
		$('#JSQuickView').modal('show');
		$('#JSQuickViewContent').html('<img class="gif-loader" src="'+base_url+'images/quick_view_loader.gif">');	
		$.post(base_url + 'quick-view', { roba_id: $(this).data('roba_id') }, function(response) {
			$('#JSQuickViewContent').html(response);
		});	
	});
	$(document).on('click', '#JSQuickViewCloseButton', function() { 
		$('#JSQuickView').modal('hide');
	});


 	// COOKIES
 	$(function(){
		function getCookie(cookieParamName){
			if(document.cookie.split('; ').find(row => row.startsWith(cookieParamName))) {
				return document.cookie
				  .split('; ')
				  .find(row => row.startsWith(cookieParamName))
				  .split('=')[1];
			}else{
				return undefined;
			}
		}
		function setCookie(cookieParamName,cookieParamValue,expireDays){
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*86400*expireDays;
			now.setTime(expireTime);
			document.cookie = cookieParamName+"="+cookieParamValue+";expires="+now.toUTCString()+"; path=/;";
		}

		$("#alertCookiePolicy").hide()
		if (getCookie('acceptedCookiesPolicy') === undefined){
			//console.log('accepted policy', chk);
			$("#alertCookiePolicy").show(); 
		}
		$('#btnAcceptCookiePolicy').on('click',function(){
			//console.log('btn: accept');
			setCookie('acceptedCookiesPolicy','yes',30);
		});
		$('#btnDeclineCookiePolicy').on('click',function(){
			//console.log('btn: decline');
			setCookie('acceptedCookiesPolicy','no',30);
		});
 	});


 	// COOKIES TEXT
 	(function(){
	 	var cookiesBtn = $('.JScookiesInfo_btn');
		cookiesBtn.text('Prikaži detalje');
		cookiesBtn.on('click', function(){
			cookiesBtn.next().slideToggle('fast');
			cookiesBtn.text(function(i, text){
		   		return text == 'Prikaži detalje' ? 'Sakrij detalje' : 'Prikaži detalje'
			}); 
		});
	}());

 	// SHOW COOKIES
	setTimeout(function(){ 
	 	$('.JScookies-part').animate({ bottom: '0' }, 700);
	}, 1500);   
 	
}); // DOCUMENT READY END
 

// ZOOMER 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		if($('body').is('#artical-page')){	
			$('.zoomContainer').css({
				width: $('.zoomWrapper').width(),
				height: $('.zoomWrapper').height()
			}); 
				
			var art_img =  $('#art-img').attr('src');
			if (art_img.toLowerCase().indexOf('no-image') >= 0){
				$('.zoomContainer').hide();
			} 
			// ARTICLE ZOOMER		
			$('.zoomContainer').each(function(i, el){
				if ( i != 0) {
					$(el).remove();
				}
			});
		}  
		$('.product-image').each(function(i, img){
			if ($(img).attr('src') != 'quick_view_loader.gif') {
				$(img).attr('src', '/images/no-image.jpg').addClass('img-responsive');
			}
		});  
	} 
}

 