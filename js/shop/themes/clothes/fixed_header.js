
/*======= FIXED HEADER ==========*/ 
$(window).scroll(function(){

	var header = $('header'),
		header_height = $('header').height(),
		offset_top = $('header').offset().top, 
		admin_menu = $('#admin-menu');
 
 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_top) {
	  		
	  		if(admin_menu.length){ $('.JSsticky_header').css('top', admin_menu.outerHeight() + 'px'); }
			
			$('#JSfixed_header').addClass('JSsticky_header');
			
			header.css('height', header_height + 'px');	        
	    }else {
			
			$('#JSfixed_header').removeClass('JSsticky_header');   
			
			header.css('height', '');
	    }
	}
});    
 
 