
/*======= FIXED HEADER ==========*/ 
var header = $('header'),
	header_height = $('header').height(),
	offset_top = $('header').offset().top,
	offset_bottom = offset_top + header.outerHeight(true),
	admin_menu = $('#admin-menu');


$(window).scroll(function(){
 
 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_bottom) {

	  		if(admin_menu.length){ $('.JSsticky_header').css('top', admin_menu.outerHeight() + 'px'); }

			$('#JSfixed_header').addClass('JSsticky_header');

			header.css('height', header_height + 'px');	        

	    }else {

			$('#JSfixed_header').removeClass('JSsticky_header');    
			
			header.css('height', '');
	    }
	}
});    
 
 