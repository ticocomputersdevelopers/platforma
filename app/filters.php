<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/
use Service\GeoLocation;


App::before(function($request)
{
	if(Options::gnrl_options(3013) == 1 && Options::gnrl_options(3022) == 0){
		header("Location: http://www.selltico.com");
		die();
	}

	if(count($jezikKods=DB::table('jezik')->where(array('aktivan'=>1))->get())==1 && $jezikKods[0]->kod!='sr'){
		Session::put('lang',$jezikKods[0]->kod);
	}
	
	if(false && !Session::has('valuta')){
		$currencyCode = GeoLocation::ipVisitorCurrencyCode();
		if(!is_null($currencyCode) && !is_null($valutaId = DB::table('valuta')->where(array('ukljuceno'=>1,'valuta_sl'=>$currencyCode))->pluck('valuta_id'))){
			Session::put('valuta',$valutaId);
		}
	}
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (! Session::has('b2b_user_'.Options::server()))
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

Route::filter('lang',function($route, $request){
	// if(Options::web_options(130) == 2){
	// 	return Redirect::to(Options::base_url());
	// }

	if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
		return Redirect::to('/');
	}
   $lang = $route->parameter('lang');
   if($lang && $lang!=Language::lang() && !is_null(DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>$lang))->first())){
   		Session::put('lang',$lang);
   }
});


Route::filter('b2b',function($route, $request){
	if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
		return Redirect::to('/');
	}
	if(!B2bOptions::checkB2B()){
		return Redirect::to(Options::base_url());
	}
	if(!Session::has('b2b_user_'.Options::server()) ){
		return Redirect::to(Options::base_url().'b2b/login');
	}

	if(B2bOptions::info_sys('infograf') && !(Session::has('b2b_user_'.B2bOptions::server().'_discounts') && is_array(Session::get('b2b_user_'.B2bOptions::server().'_discounts')))){
		return Redirect::to(Options::base_url().'b2b/login');
	}
});
Route::filter('b2b_user_data',function($route, $request){
    if(!is_null(B2bPartner::getPartnerUserObject())){
        return Redirect::to('b2b/');
    }
});

Route::filter('rma',function($route, $request){
	if(AdminOptions::gnrl_options(3040) == 0){
		return Redirect::to(Options::base_url());
	}

	$rmaUser = RmaOptions::user();
	if(is_null($rmaUser)){
		return Redirect::to(Options::base_url().'rma/login');
	}

});

Route::filter('user_filter',function($route, $request){
   if(Session::has('b2c_kupac')){
    //return Redirect::back();
	   
   }
   else {
	   return Redirect::to(Options::base_url());
   }
});

Route::filter('b2c_admin',function($route, $request){
   if(!Session::has('b2c_admin'.AdminOptions::server())){
		return Redirect::to(AdminOptions::base_url().'admin-login');
	}
});

Route::filter('hash_protect',function($route,$request){
	
	if($route->parameter('key') !== DB::table('options')->where('options_id',3002)->pluck('str_data') || DB::table('options')->where('options_id',3002)->pluck('int_data') == 0){
		return Redirect::to(AdminOptions::base_url());
	}

});
Route::filter('hash_protect_export',function($route,$request){
	if($route->parameter('key') !== DB::table('options')->where('options_id',3002)->pluck('str_data')){
		return Redirect::to(AdminOptions::base_url());
	}

});

Route::filter('b2b_admin',function($route, $request){
   if(!(Session::has('b2c_admin'.AdminOptions::server()) && AdminOptions::checkB2B())){
	   return Redirect::to('admin-login');
   }
});

Route::filter('wings',function($route, $request){
   if(!AdminB2BOptions::info_sys('wings')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('calculus',function($route, $request){
   if(!AdminB2BOptions::info_sys('calculus')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('infograf',function($route, $request){
   if(!AdminB2BOptions::info_sys('infograf')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('logik',function($route, $request){
   if(!AdminB2BOptions::info_sys('logik')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('roaming_is',function($route, $request){
   if(!AdminB2BOptions::info_sys('roaming')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('sbcs_is',function($route, $request){
   if(!AdminB2BOptions::info_sys('sbcs')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('skala',function($route, $request){
   if(!AdminB2BOptions::info_sys('skala')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('gsm',function($route, $request){
   if(!AdminB2BOptions::info_sys('gsm')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('promobi',function($route, $request){
   if(!AdminB2BOptions::info_sys('promobi')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('xml_is',function($route, $request){
   if(!AdminB2BOptions::info_sys('xml')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('softcom_is',function($route, $request){
   if(!AdminB2BOptions::info_sys('softcom')){
	   return Redirect::to('admin/b2b');
   }
});
Route::filter('panteon_is',function($route, $request){
   if(!AdminB2BOptions::info_sys('panteon')){
	   return Redirect::to('admin/b2b');
   }
});

Route::filter('dokumenti',function($route, $request){
	$dokumentiUser = DokumentiOptions::user();
	if(is_null($dokumentiUser)){
		return Redirect::to(Options::base_url().'dokumenti/login');
	}
});
Route::filter('b2b_dokumenti',function($route, $request){
	$dokumentiUser = DokumentiOptions::user();
	if(is_null($dokumentiUser)){
		return Redirect::to(Options::base_url().'dokumenti/login');
	}
});

Route::filter('api',function($route, $request){
	// $path = $request->path();
	// $method = $request->method();

	// $endpoints = [
	// 	['path' => 'api/v1/categories', 'method'=> 'GET'],
	// 	['path' => 'api/v1/articles', 'method'=> 'GET'],
	// 	['path' => 'api/v1/partners', 'method'=> 'GET'],
	// 	['path' => 'api/v1/b2b-orders', 'method'=> 'GET'],
	// 	['path' => 'api/v1/categories-save', 'method'=> 'POST'],
	// 	['path' => 'api/v1/articles-save', 'method'=> 'POST'],
	// 	['path' => 'api/v1/partners-save', 'method'=> 'POST'],
	// 	['path' => 'api/v1/partner-cards-save', 'method'=> 'POST'],
	// 	['path' => 'api/v1/b2b-orders-save', 'method'=> 'POST'],
	// 	['path' => 'api/v1/categories-delete', 'method'=> 'DELETE'],
	// 	['path' => 'api/v1/articles-delete', 'method'=> 'DELETE'],
	// 	['path' => 'api/v1/partners-delete', 'method'=> 'DELETE'],
	// 	['path' => 'api/v1/partner-cards-delete', 'method'=> 'DELETE'],
	// 	['path' => 'api/v1/b2b-orders-delete', 'method'=> 'DELETE'],
	// 	['path' => 'api/v1/b2b-order-items-delete', 'method'=> 'DELETE']
	// ];

	// $filtered = array_filter($endpoints,function($endpoint) use ($path,$method){
	// 	return $endpoint['path'] == $path && $endpoint['method'] == $method;
	// });
	// if(count($filtered) == 0){
	// 	return Response::make('Not found', 404);
	// }


    $authorization = $request->header('Authorization');
    $authorizationArray = $authorization ? explode(' ',$authorization) : array();
    if(!isset($authorizationArray[1])){
    	return Response::make('Unauthorized', 403);
    }

    $auth = base64_decode($authorizationArray[1]);
    $authArray = explode(':',$auth);
    $username = $authArray[0];
    $password = $authArray[1];

	$imenik = DB::table('imenik')->where(array('login'=>$username,'password'=>$password))->first();
	if(is_null($imenik)){
		return Response::make('Unauthorized', 403);
	}
});

Route::filter('parner_api',function($route, $request){
	$username = $request->get('korisnickoime');
	$password = $request->get('lozinka');

	$parnter = DB::table('partner')->where(array('api_username'=>$username,'api_password'=>$password,'aktivan_api'=>1))->whereNotNull('api_username')->whereNotNull('api_password')->first();
	if(is_null($parnter)){
		return Response::make('Unauthorized', 403);
	}
});

Route::filter('ticketing',function($route, $request){
   if(!Session::has('ticketing_user')){
	   return Redirect::to(Options::base_url().'ticketing/login');
   }
});
