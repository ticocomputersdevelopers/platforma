<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans;
}
ul { list-style-type: none; }

table {border-collapse: collapse; margin-bottom: 15px; width: 100%;}

table tr td, table tr th {
	border: 1px solid #cacaca; 
	text-align: left;
	padding: 3px 5px;
}
.no-padd{ padding: 0; }

.row::after {content: ""; clear: both; display: table;}
[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; }
.text-right{ text-align: right; }
.text-center{ text-align: center; } 
.relative { position: relative; }

p{
	font-size: 13px; 
}
img{
	max-width: 100%;
}
.header{
	border-bottom: 2px solid #000; 
	padding: 10px 0;
}
.header .logo {
	max-height: 80px;  
	margin: 20px;
} 
.main-content {
	padding: 15px;   
} 
.border-btm { border-bottom: 1px solid #000; }
</style>
</head><body>
	<!-- @include('rma/partials/radni_nalog_usluzni_servis_uput_part') -->

	@include('rma/partials/radni_nalog_usluzni_servis_uput_part')

	@if(false)
	<div class="container relative"> 
		<div class="row header">
			<div class="col-3">
				<img class="logo" src="{{ Options::base_url()}}{{Options::company_logo()}}" alt="logo">
			</div>
			<div class="col-4"> 
				<p>Firma: {{RmaOptions::company_name()}}</p>
				<p>Adresa: {{RmaOptions::company_adress()}}</p>
				<p>Telefon: {{RmaOptions::company_phone()}}</p>
				<p>Fax: {{RmaOptions::company_fax()}}</p>
				<p>PIB: {{RmaOptions::company_pib()}}</p>
				<p>E-mail: {{RmaOptions::company_email()}}</p> 
			</div>
			<div class="col-4"> 
				<p>Servis: {{$usluzni_servis->naziv}}</p>
				<p>Adresa: {{$usluzni_servis->adresa.', '.$usluzni_servis->mesto}}</p>
				<p>Telefon: {{$usluzni_servis->telefon}}</p>
				<p>Fax: {{$usluzni_servis->fax}}</p>
				<p>PIB: {{$usluzni_servis->pib}}</p>
				<p>E-mail: {{$usluzni_servis->mail}}</p> 
			</div>
		</div>

	  	<div class="row">
	  		<div class="col-12">  
				<p>Prijemnica sa servisa br: {{ $radni_nalog_trosak_prijem->radni_nalog_trosak_broj_dokumenta }}</p>
		  		<p">Datum: {{ !is_null($radni_nalog_trosak_prijem->datum_dokumenta) ? RMA::format_datuma($radni_nalog_trosak_prijem->datum_dokumenta) : '' }}</p> 

			</div>
		</div>
		<div class="row">
	  		<div class="col-9">
	  			<label>Uređaj:</label>
	  			<textarea rows="3" cols="50">{{ $radni_nalog_trosak_prijem->uredjaj }}</textarea>
			</div>
	  		<div class="col-3">
				<label>Serijski broj:</label>
	  			<input type="text" value="{{ $radni_nalog_trosak_prijem->serijski_broj }}" />
			</div>
		</div>
		<div class="row">
			<div class="col-12">
	  			<label>Napomena:</label>
	  			<textarea rows="3" cols="50">{{ $radni_nalog_trosak_prijem->napomena }}</textarea>
			</div>
	 	</div>
	 	<div class="row">
	  		<div class="col-6">
	  			Predao
	  		</div>
	  		<div class="col-6">
	  			Primio
	  		</div>
	  	</div>
	 	<div class="row">
	  		<div class="col-6">
	  			------------------MP
	  		</div>
	  		<div class="col-6">
	  			------------------MP
	  		</div>
	  	</div>
	</div>
	@endif 
</body></html>
