@extends('rma.templates.main')
@section('content')
<section id="main-content">
	<div class="row">
		@include('rma/partials/radni_nalog_tabs')
		<div class="flat-box">
			<div class="columns medium-12">
				<h1>{{ $radni_nalog->broj_naloga }} ({{RMA::getStatusTitle($radni_nalog->status_id)}})</h1>
			</div>
		</div>
	</div>
	
 	<div class="row">
 		<form method="POST" action="{{ RmaOptions::base_url() }}rma/radni-nalog-predaja-post">
	 		<div class="flat-box">
	 			<h3 class="title-med">Roba koja se iznosi sa servisa</h3> 
				<div class="row"> 
		            <div class="field-group columns medium-6">
						<label for="">Primljen uređaj</label>
						<input name="uredjaj" class="{{ $errors->first('uredjaj') ? 'error' : '' }}" type="text" value="{{ htmlentities(Input::old('uredjaj') ? Input::old('uredjaj') : $radni_nalog->uredjaj) }}">
	            	</div>
					<div class="field-group columns medium-6">
						<label for="">Predat uređaj</label>
						<input name="predat_uredjaj" class="datum-val has-tooltip {{ $errors->first('predat_uredjaj') ? 'error' : '' }}" type="text" value="{{ htmlentities(Input::old('predat_uredjaj') ? Input::old('predat_uredjaj') : (!empty($radni_nalog->predat_uredjaj) ? $radni_nalog->predat_uredjaj : $radni_nalog->uredjaj)) }}" >
					</div> 
				</div>
			</div>
	 		<div class="flat-box"> 
				<h3 class="title-med">Predaja</h3> 
				<div class="columns medium-8 no-padd">
					<div class="row"> 
						<label>Preuzeo</label>
			            <div class="columns medium-6">
							<label for="">Ime i Prezime</label>
							<input name="preuzeo_ime" class="{{ $errors->first('preuzeo_ime') ? 'error' : '' }}" type="text" value="{{ Input::old('preuzeo_ime') ? Input::old('preuzeo_ime') : $radni_nalog->preuzeo_ime }}">
		            	</div>
						<div class="columns medium-6">
							<label for="">Broj lične karte</label>
							<input name="preuzeo_brojlk" class="datum-val has-tooltip {{ $errors->first('preuzeo_brojlk') ? 'error' : '' }}" type="text" value="{{ Input::old('preuzeo_brojlk') ? Input::old('preuzeo_brojlk') : $radni_nalog->preuzeo_brojlk }}" >
						</div> 
			        </div>
					<div class="row"> 
						<label>Predao</label>
			            <div class="columns medium-6">
							<label for="">Ime i Prezime</label>
							<select name="predao_serviser_id" class="{{ $errors->first('predao_serviser_id') ? 'error' : '' }}">
								{{ RMA::serviseriSelect($radni_nalog->partner_id,(!is_null(Input::old('predao_serviser_id')) ? Input::old('predao_serviser_id') : $radni_nalog->predao_serviser_id)) }}
							</select>
		            	</div>
						<div class="columns medium-4">
							<label for="">Datum</label>
							<input name="datum_zavrsetka" class="datum-val has-tooltip {{ $errors->first('datum_zavrsetka') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_zavrsetka') ? Input::old('datum_zavrsetka') : (!is_null($radni_nalog->datum_zavrsetka) ? $radni_nalog->datum_zavrsetka : date('Y-m-d H:i')) }}" autocomplete="off">
						</div>
						<div class="columns medium-2">
							<label for="">Predato</label>
							<input name="check_predato" class="datum-val has-tooltip" type="checkbox" {{ (!is_null(Input::old('check_predato')) && Input::old('check_predato') == 'on') ? 'checked' : ($radni_nalog->status_id == 4 ? 'checked' : '') }} >
						</div> 
			        </div>
				</div>
				<input type="hidden" name="radni_nalog_id" value="{{ $radni_nalog->radni_nalog_id }}">
				<div class="columns medium-4">
					<label>&nbsp;</label>
					<label>&nbsp;</label>
					<table class="rma-predaja">
	<!-- 					<tr>
							<td>Rad:</td>
							<td>{{ RMA::format_cene($cene->cena_rada) }}</td>
						</tr>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<tr>
							<td>Rezervni delovi:</td>
							<td>{{ RMA::format_cene($cene->rezervni_delovi) }}</td>
						</tr>
						@endif
						@if(RmaOptions::gnrl_options(3041))
						<tr>
							<td>Troškovi:</td>
							<td>{{ RMA::format_cene($cene->troskovi) }}</td>
						</tr>
						@endif
						@if(RmaOptions::gnrl_options(3043) == 1)
						<tr>
							<td>UKUPNO:</td>
							<td>{{ RMA::format_cene($cene->ukupna_cena_rada) }}</td>
						</tr>
						@endif
						<tr>
							<td>PDV:</td>
							@if(RmaOptions::gnrl_options(3043) == 1)
							<td>{{ RMA::format_cene($cene->pdv_cena) }}</td>
							@else
							<td>{{ RMA::format_cene($cene->cena_rada*0.2) }}</td>
							@endif
						</tr> -->
						<tr>
							<td>SVEGA:</td>
							@if(RmaOptions::gnrl_options(3043) == 1)
							<td>{{ RMA::format_cene($cene->ukupno) }}</td>
							@else
							<td>{{ RMA::format_cene($cene->cena_rada) }}</td>
							@endif
						</tr>
					</table>
					<div class="btn-container center"> 
						<a href="#" id="JSRNRekapitulacija" class="btn btn-primary save-it-btn" data-id="{{$radni_nalog->radni_nalog_id}}">Rekapitulacija</a>
					</div>
					@if(RmaOptions::gnrl_options(3041))
					<input type="checkbox" id="JSRNRekapitulacijaITrosak"> Sa uslugom servisa
					@endif
				</div>
				<div class="columns medium-12 text-center">
					<div class="btn-container"> 
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
				</div> 
			</div>
		</form>
	</div>
</section>
@endsection