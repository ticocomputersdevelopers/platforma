<nav id="categories">

    <?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
    <h3 class="categories-titile">Kategorije </h3>
    
    <span class="toggler"><i class="fa fa-bars"></i></span>
    
    <!-- CATEGORIES LEVEL 1 -->

    <ul class="level-1">
        @if(B2bOptions::category_type()==1)
        @foreach ($query_category_first as $row1)
        
            @if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
            <li>
                <div class="category__link-group">
                    <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}" class="category__link-group__link">
                        <div class="category__link-group__link__text">

                            @if(B2bCommon::check_image_grupa($row1->putanja_slika))
                                <img class="cat-img-1" src="{{B2bOptions::base_url().$row1->putanja_slika}}" alt="">
                            @endif

                            {{ $row1->grupa  }}  
                        </div>
                       <!--  <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span> -->
                    </a>
                    
                        <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
                </div>
                <ul class="level-2 clearfix">
                
                    @foreach ($query_category_second->get() as $row2)
                        <li>
                            @if(B2bCommon::check_image_grupa($row2->putanja_slika))
                            <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}" class="">
                                <img class="cat-img-2" src="{{B2bOptions::base_url().$row2->putanja_slika}}" alt="">
                            </a>
                            @endif
                            <div class="category__link-group">
                                <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}" class="category__link-group__link">
                                    {{ $row2->grupa }} 
                                    <!-- <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row2->grupa_pr_id)}}</span> -->
                                </a>

                                @if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)

                                <span class="category__item__link__expend">
                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                </span>
                                @endif
                            </div>
                            
                            @if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                            
                            
                                <ul class="level-3 clearfix">
                                    @foreach($query_category_third as $row3)
                                     <li>
                                        <div class="category__link-group">
                                            <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}" class="test">
                                                {{ $row3->grupa }}
                                                <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row3->grupa_pr_id)}}</span>
                                            </a>

                                            @if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
                                            <span class="category__item__link__expend">
                                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </span>
                                            @endif
                                        </div>
                                        @if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
                                            <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                                                                        
                                                <ul class="level-4 clearfix">
                                                    @foreach($query_category_forth as $row4)
                                                     <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}/{{ B2bUrl::url_convert($row4->grupa) }}">{{ $row4->grupa }} <!-- <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row4->grupa_pr_id)}}</span> --></a>
                                                    @endforeach
                                                </ul>
                                        @endif                                  
                                    @endforeach
                                </ul>
                                @endif
                        </li>
                    @endforeach
                </ul>
            </li>
            @else
            <li>
                <div class="category__link-group">
                    <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}" class="category__link-group__link">
                        <div class="category__link-group__link__text">
                            {{ $row1->grupa  }} 
                        </div>
                        <!-- <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span> -->
                    </a>
                </div>
            </li>
            @endif
        @endforeach
        @else
        @foreach ($query_category_first as $row1)
        
            
                @if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
                <li>
                    <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}">
                        {{ $row1->grupa  }}
                        <!-- <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span> -->
                    </a>
                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
                <ul class="level-2-category clearfix level-3-open">
                
                    @foreach ($query_category_second->get() as $row2)
                         <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}">{{ $row2->grupa }} <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row2->grupa_pr_id)}}</span></a>
                         @if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                            
                            
                                <ul class="level-3 clearfix">
                                    @foreach($query_category_third as $row3)
                                     <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}">{{ $row3->grupa }} <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row3->grupa_pr_id)}}</span></a>
                                         @if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
                                            <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                                                                        
                                                <ul class="level-4 clearfix">
                                                    @foreach($query_category_forth as $row4)
                                                     <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}/{{ B2bUrl::url_convert($row4->grupa) }}">{{ $row4->grupa }} <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row4->grupa_pr_id)}}</span></a>
                                                    @endforeach
                                                </ul>
                                        @endif  
                                    @endforeach
                                </ul>
                                @endif
                         </li>
                    @endforeach
                </ul>
                @else
                <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}">{{ $row1->grupa  }} 
                   <!--  <span class="category__item__link__span">{{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}}</span></a> -->
                
                @endif
                
                </li>
        @endforeach             
        @endif

          <!-- BUSILICA -->
         <div class="under-categories-image">
              <img src="{{ B2bOptions::base_url() }}images/footerLogos/busilica.png">
            
              <figure> 
                    <img class="drill-image" src="{{ B2bOptions::base_url() }}images/footerLogos/Visual-Garantie-SRL-T3_238_110.jpg">
                     <h6 class="two-of-tree-title">Od 2 napravi 3</h6>
                    <figcaption>
                        <span class="drill-description">      
                         Ne samo kod električnih i mernih alata nego i kod kupovine baštenskog uređaja obezbedite produženo vreme garancije od tri umesto dve godine.  
                        </span>
                    </figcaption>
                </figure>
          </div>
     </ul> 

</nav>
