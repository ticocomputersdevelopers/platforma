<!DOCTYPE html>
<html lang="sr">
<head>
    <title>{{ $seo['title'] }} | {{B2bOptions::company_name() }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ $seo['description'] }}" />
    <meta name="author" content="{{$seo['keywords']}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- BOOTSTRAP CDN -->
<!--     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

    <!-- BOOTSTRAP LOCAL -->  
    <script src="{{ B2bOptions::base_url() }}js/3.3.1_jquery.min.js"></script> 
    <link href="{{ B2bOptions::base_url() }}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="{{B2bOptions::base_url() }}js/bootstrap.min.js"></script>
     
 
 <!--  STYLESHEETS  -->
    <link href="{{ B2bOptions::base_url()}}css/b2b/style.css" rel="stylesheet" type="text/css" />
 
 <!--  SLICK LIST  -->     
    <link href="{{ B2bOptions::base_url()}}css/slick.css" rel="stylesheet" type="text/css" />
 
 <!--  FONTS   -->  
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <link href="{{B2bOptions::domain()}}css/fontawesome-free-5.11.2-web/css/all.min.css" rel="stylesheet" type="text/css">

     <!--  FAVICON -->
    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">

    <script src="{{B2bOptions::base_url()}}js/bootbox.min.js"></script>

    @if($seo['title']=='Ponude' OR $seo['title']=='Ponuda')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="{{ B2bOptions::base_url() }}css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ B2bOptions::base_url()}}css/alertify.core.css">
    <link rel="stylesheet" href="{{ B2bOptions::base_url()}}css/alertify.default.css">
    <script src="{{ B2bOptions::base_url()}}js/alertify.js" type="text/javascript"></script>

    <link href="{{ B2bOptions::base_url()}}css/b2b/ponude.css" rel="stylesheet" type="text/css" />
    @endif

    @yield('head')
</head>

<body @if(B2bOptions::getBGimg() != null) style="background-image: url({{B2bOptions::base_url()}}{{B2bOptions::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" @endif>

 
<!-- LOADER -->
<div class="JSspinner">
    <span>
        <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
    </span> 
</div>
 
 
<!-- preheader -->
@include('b2b.partials.menu_top')


<!-- header -->
@include('b2b.partials.header')

<!-- main.blade -->
<div class="main-content container"> 

    @yield('content') 

</div> 
<!-- main.blade end -->


<!-- footer -->
@include('b2b.partials.footer')


@include('b2b.partials.popups')
 

<input type="hidden" id="base_url" value="{{ AdminB2BOptions::base_url() }}">
<script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript"></script>
<script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript"></script>
@if($seo['title']=='Ponude' OR $seo['title']=='Ponuda')
<script src="{{ B2bOptions::base_url()}}js/select2.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="{{ B2bOptions::base_url()}}js/jquery-ui.min.js"></script>
<script src="{{ B2bOptions::base_url()}}js/dokumenti/funkcije.js" type="text/javascript"></script>
<script src="{{ B2bOptions::base_url()}}js/dokumenti/ponude.js" type="text/javascript"></script>
    @if($seo['title']=='Ponuda')
        <script type="text/javascript" src="{{ AdminOptions::base_url()}}js/tinymce_5.1.3/tinymce.min.js"></script>
        <script type="text/javascript">  
            tinymce.init({
                selector: ".special-textareas", 
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",    
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste",
                    "autoresize"
                ],       
                // Advanced in insert->media_poster
                media_poster: false,
                media_alt_source: false, 

                extended_valid_elements : "script[language|type|async|src|charset]",
                end_container_on_empty_block: true,
 
                contextmenu: "image", //  ------->  COPY PASTE
                toolbar: "insertfile undo redo | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | antrfile | glosa | forecolor backcolor | gallery",  
                // setup: function (editor) { 
                //     editor.ui.registry.addButton('antrfile', {
                //         text: 'Antrfile',
                //             onAction: function () {   
                //                 var text = editor.selection.getContent({'format': 'html'});
                //                 if(text && text.length > 0) {
                //                     editor.execCommand('mceInsertContent', false, '<div style="background-color: #141140; padding: 40px 20px; margin: 10px 0; color: #fff; display: inline-block;">'+ text +'</div>');
                //                 }
                //             }
                //     });
                //     editor.ui.registry.addButton('glosa', {
                //         text: 'Glosa',
                //             onAction: function () {   
                //                 var text = editor.selection.getContent({'format': 'html'});
                //                 if(text && text.length > 0) {
                //                     editor.execCommand('mceInsertContent', false, '<p style="text-align: center; font-style: italic; color:#a6a6a6; font-size: 22px; line-height: 1.3; margin: 22px 0;">"'+ text +'"</p>');
                //                 }
                //             }
                //     });
                //     if ($('.JSfixed_tinumce_toolbar')[0]) { 
                //         editor.ui.registry.addButton('gallery', {
                //             text: 'Gallery',
                //                 onAction: function () {
                //                     if($('#JSGalerijaModal').length > 0){
                //                         $('#JSGalerijaModal').foundation('reveal', 'open');
                //                     }
                //                 }
                //         });
                //     }
                // }
            });
        </script>
    @endif
@endif

<input type="hidden" id="lat" value="{{ All::lat_long()[0] }}" />
<input type="hidden" id="long" value="{{ All::lat_long()[1] }}" />
<input type="hidden" id="isb2b" value="1" />
<input type="hidden" id="template_change" value="{{ Session::has('sablon_change') ? Session::get('sablon_change') : 0 }}" />
<input type="hidden" id="submit_order" value="{{ Session::has('submit_order') ? Session::get('submit_order') : 0 }}" />

<script> 
    @if(Session::has('message'))
        popupMessage("{{ Session::get('message') }}");
    @endif
</script> 

<!-- @if(Options::company_map() != '' && Options::company_map() != ' ') 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCf05l1NhASLAtI_hy9k4Z2EowvME7JyoM&callback=initMap"></script>
<script src="{{B2bOptions::base_url()}}js/shop/map_initialization.js"></script>
@endif -->

@if(Options::header_type()==1)
    <script src="{{ B2bOptions::base_url()}}js/b2b/fixed_header.js"></script>
@endif
<script>
    function deleteItemCart(cart_id){
        $.ajax({
            type: "POST",
            url:'{{route('b2b.cart_delete')}}',
            cache: false,
            data:{cart_id:cart_id},
            success:function(res){
                location.reload();
            }
        });
    }
</script>
</body>
</html>