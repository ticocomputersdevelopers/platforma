@foreach($items as $row)
<?php $rabatCena = B2bArticle::b2bRabatCene($row->roba_id); ?>
<li>
    <div class="header-cart-image-wrapper">
        <img src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika($row->roba_id) }}"  />
    </div>
    <a href="{{ B2bOptions::base_url() }}b2b/artikal/{{ B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id)) }}">{{B2bArticle::short_title($row->roba_id)}}</a>
    <span class="header-cart-amount">{{ $row->kolicina }}</span>
    <span class="header-cart-price">{{ $rabatCena->ukupna_cena }}</span>
    <a href="javascript:deleteItemCart({{ $row->web_b2b_korpa_stavka_id }})" title="Ukloni artikal" class="close" >X</a>
</li>
@endforeach
<?php
    $cartTotal = B2bBasket::total();
?>
<li><span class="header-cart-summary">Osnovica: <span>{{ B2bBasket::cena($cartTotal->cena_sa_rabatom) }}</span></span></li>
<li><span class="header-cart-summary">PDV: <span>{{ B2bBasket::cena($cartTotal->porez_cena) }}</span></span></li>
<li><span class="header-cart-summary">Ukupno: <span>{{ B2bBasket::cena($cartTotal->ukupna_cena) }}</span></span></li>