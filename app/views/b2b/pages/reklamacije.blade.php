@extends('b2b.templates.main')

@section('content')
<section id="main-content">
	<div class="row">
		<div class="col-xs-12 complaints">
			<h2 class="section-title"> {{ Language::trans('Reklamacije') }} </h2>
			<a href="{{RmaOptions::base_url()}}b2b/tiket" class="button">{{ Language::trans('Novi tiket') }}</a>
			<div class="table-responsive" style="overflow: auto;">
				<table class="table table-condensed table-striped table-hover">
					<thead> 
						<tr>
							<th>{{ Language::trans('Broj') }}.</th>
							<th>{{ Language::trans('Naziv') }}</th>
							<th>{{ Language::trans('Opis') }}</th>
							<th>{{ Language::trans('Grupa') }}</th>
							<th>{{ Language::trans('Serijski broj') }}</th>
							<th>{{ Language::trans('Broj računa') }}</th>
							<th>{{ Language::trans('Proizvođač') }}</th>
							<th>{{ Language::trans('Hitno') }}</th>
							<th>{{ Language::trans('Napomena') }}</th>
							<th>{{ Language::trans('Status') }}</th>
						</tr>
					</thead>	
					<!-- thus repeat -->
					<tbody> 
						@foreach($radniNalozi->items as $row)
						<tr>	
							<td>{{ $row->broj_naloga }}</td>
							<td>{{ $row->uredjaj }}</td>
							<td>{{ $row->opis_kvara }}</td>
							<td>{{ $row->roba == 1 ? 'Roba' : 'Usluga' }}</td>
							<td>{{ $row->serijski_broj }}</td>
							<td>{{ $row->broj_fiskalnog_racuna }}</td>
							<td>{{ $row->proizvodjac }}</td>
							<td>{{ $row->hitno == 1 ? 'Da' : 'Ne' }}</td>
							<td>{{ $row->napomena }}</td>
							<td>{{ ($row->pregledan == 1) ? $row->status : 'Poslat zahtev' }}</td>
						</tr>
						@endforeach
					</tbody>
					<!-- end repeat -->
				</table>
				{{ is_array($radniNalozi->items) ? Paginator::make($radniNalozi->items, $radniNalozi->count, $radniNalozi->limit)->links() : '' }}
			</div>
		</div>
	</div>
</section>
@endsection