<?php
$query_gr=AdminB2BSupport::query_gr_level(0);
?>

<style type="text/css">
	input[type="number"]::-webkit-outer-spin-button,
	input[type="number"]::-webkit-inner-spin-button {
	    -webkit-appearance: none;
	    margin: 0;
	}
	input[type="number"] {
	    -moz-appearance: textfield;
	}

	.li-item .active {
		background: #ccc !important;
	}

	.li-item__link .active {
		background: rgba(231, 76, 60, 0.65) !important;
	}
	.btn-w {
		width: 100%;
	}
</style>
<div class="flat-box"> 
	<a class="btn btn-primary btn-w" href="{{AdminB2BOptions::base_url()}}admin/b2b/article-list/0">Sve kategorije</a>
	<div class="btn-container"> 
		<input type="text" name="search" id="myInput" placeholder="Pretraga..">
	</div>
	<!-- LEVEL 1 -->
	<ul class="categories b2b-cat" id="myList">
		@foreach($query_gr as $row)
			<li class="li-item <?php if($grupa_pr_id == $row->grupa_pr_id){ echo "active"; } ?>">
				
				<div class="li-item__link <?php if($grupa_pr_id == $row->grupa_pr_id){ echo "active"; } ?>" data-id="{{ $row->grupa_pr_id }}" data-old="{{ $row->redni_broj }}">
					
					<a class="li-item__link__text" href="{{AdminB2BOptions::base_url()}}admin/b2b/article-list/{{ $row->grupa_pr_id }}">{{ $row->grupa }}</a>
					<!-- <a class="li-item__link__edit" title="Izmeni" href="{{AdminB2BOptions::base_url()}}admin/grupe/{{ $row->grupa_pr_id }}">
						<i class="fa fa-pencil-square" aria-hidden="true"></i>
					</a> -->
					<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" class="li-item__link__input rabat_edit"  value="{{ ($row->rabat > 0) ? $row->rabat : '' }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}>
				</div> <!-- end of .li-item__link -->
				
				<?php $query_gr1=AdminB2BSupport::query_gr_level($row->grupa_pr_id); ?>
				
				<!-- LEVEL 2 -->
				<ul class="ul-lvl ul-lvl--2" id="myList">
					@foreach($query_gr1 as $row1)
					<li class="li-item ">
						
						<div class="li-item__link <?php if($grupa_pr_id == $row1->grupa_pr_id){ echo "active"; } ?>" data-id="{{ $row1->grupa_pr_id }}" data-old="{{ $row1->redni_broj }}">
							
							<a class="li-item__link__text" href="{{AdminB2BOptions::base_url()}}admin/b2b/article-list/{{ $row1->grupa_pr_id }}">{{ $row1->grupa }}</a>
							<!-- <a class="li-item__link__edit" title="Izmeni" href="{{AdminB2BOptions::base_url()}}admin/grupe/{{ $row1->grupa_pr_id }}">
								<i class="fa fa-pencil-square" aria-hidden="true"></i>
							</a> -->
							<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" class="li-item__link__input rabat_edit"  value="{{ ($row1->rabat > 0) ? $row1->rabat : '' }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div> <!-- end of .li-item__link -->

						<?php $query_gr2=AdminB2BSupport::query_gr_level($row1->grupa_pr_id); ?>
						
						<!-- LEVEL 3 -->
						<ul class="ul-lvl ul-lvl--3" id="myList">
							@foreach($query_gr2 as $row2)
								<li class="li-item <?php if($grupa_pr_id == $row2->grupa_pr_id){ echo "active"; } ?>">
									<div class="li-item__link" data-id="{{ $row2->grupa_pr_id }}" data-old="{{ $row2->redni_broj }}">
										
										<a class="li-item__link__text" href="{{AdminB2BOptions::base_url()}}admin/b2b/article-list/{{ $row2->grupa_pr_id }}">{{ $row2->grupa }}</a>
										<!-- <a class="li-item__link__edit" title="Izmeni" href="{{AdminB2BOptions::base_url()}}admin/grupe/{{ $row2->grupa_pr_id }}">
											<i class="fa fa-pencil-square" aria-hidden="true"></i>
										</a> -->
										<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" class="li-item__link__input rabat_edit"  value="{{ ($row2->rabat > 0) ? $row2->rabat : '' }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}>
									</div> <!-- end of .li-item__link -->
								</li>
								<?php $query_gr3=AdminB2BSupport::query_gr_level($row2->grupa_pr_id); ?>
								
								<!-- LEVEL 4 -->
								<ul class="ul-lvl ul-lvl--4" id="myList">
									@foreach($query_gr3 as $row3)
										<li class="li-item <?php if($grupa_pr_id == $row3->grupa_pr_id){ echo "active"; } ?>">

											<div class="li-item__link" data-id="{{ $row3->grupa_pr_id }}" data-old="{{ $row3->redni_broj }}">
												
												<a class="li-item__link__text" href="{{AdminB2BOptions::base_url()}}admin/b2b/article-list/{{ $row3->grupa_pr_id }}">{{ $row3->grupa }}</a>
												<!-- <a class="li-item__link__edit" title="Izmeni" href="{{AdminB2BOptions::base_url()}}admin/grupe/{{ $row3->grupa_pr_id }}">
													<i class="fa fa-pencil-square" aria-hidden="true"></i>
												</a> -->
												<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" class="li-item__link__input rabat_edit"  value="{{ ($row3->rabat > 0) ? $row3->rabat : '' }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}>
											</div> <!-- end of .li-item__link -->
										</li>
									@endforeach
								</ul>
							@endforeach
						</ul>
					</li>
					@endforeach
				</ul>
			</li>
		@endforeach
	</ul>
</div>
<!-- <ul class="custom-menu articles-mn">
	<li>
		<a class="custom-menu-item" id="new-art" href="{{AdminB2BOptions::base_url()}}admin/product/0" target="_blank">Dodaj novi artikal (F2)</a>

	</li>
	<li>
		<button class="custom-menu-item" id="JSIzmeni">Izmeni artikal (F4)</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSUrediOpis">Uredi opis (F5)</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSKlonirajArtikal">Kloniraj artikal</button>
	</li>
	
	<li>
		<button class="custom-menu-item custom-menu-sub" id="">Postavi karakteristike <i class="fa fa-caret-right" aria-hidden="true"></i></button>
		<ul class="custom-menu-sub-item">
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"0"}]'>HTML кarakteristike</button>	
			</li>
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"1"}]'>Generisane</button>
			</li>
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"2"}]'>Od dobavljača</button>
			</li>	
		</ul>
	</li>	
	<li>
		<button class="custom-menu-item" id="JSObrisiArtikal">Obriši artikal (F8)</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSRefresh">Osvezi (F7)</button>
	</li>
</ul> -->