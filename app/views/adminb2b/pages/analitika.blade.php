@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content" class="orders-page row b2bAnaliticus">
	<div class="row">
		<div class="column large-12">
			<h1 class="h1-title">AnalitikaB2B</h1>
		</div>
	</div>
		<div class="row">
		<div class="column medium-6 small-12 large-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
				<!-- 	<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno porudžbina</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getUkupnoPorudzbina() }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Porudžbine na čekanju</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getNaCekanju() }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Obrađene porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getObradjene() }}</span>
					</li> -->


					@if(isset($nove))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Nove porudžbine</span>
						<span class="anly-ul__li__count">{{ $nove }} </span>						
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Nove porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getNew() }}</span>
					</li>
					@endif
					@if(isset($prihvaceno))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihvaćene porudžbine</span>
						<span class="anly-ul__li__count">{{ $prihvaceno }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihvaćene porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getPrihvacene() }}</span>
					</li>
					@endif
					@if(isset($realizovano))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Realizovane porudžbine</span>
						<span class="anly-ul__li__count">{{ $realizovano }} </span>						
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Realizovane porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getRealizovane() }}</span>
					</li>
					@endif
					@if(isset($stornirano))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Stornirane porudžbine</span>
						<span class="anly-ul__li__count">{{ $stornirano }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Stornirane porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getStornirane() }}</span>
					</li>
					@endif
					@if(isset($ukupno))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno porudžbina</span>
						<span class="anly-ul__li__count">{{ $ukupno }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno porudžbina</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::getUkupnoPorudzbina() }}</span>
					</li>
					@endif
					<div class="graph-box">
						<canvas height=140 id="graph5"></canvas>
					</div>  
				</ul>
			</div>

		</div>
		<div class="column medium-6 small-12 large-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Jučerašnji prihod</span>
						<span class="anly-ul__li__count">{{ number_format(AdminB2BAnalitika::jucerasnjiPrihod(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihod u proteklih mesec dana</span>
						<span class="anly-ul__li__count">{{ number_format(AdminB2BAnalitika::mesecniPrihod(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupan prihod</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminB2BAnalitika::ukupanPrihod(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupna Razlika u ceni</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminB2BAnalitika::ukupanRUC(), 2, ',', ' ')  }} din</span>
					</li>
				</ul>
			</div>
						<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<div class="">
							<div class="column medium-12 large-6">
								<label for="">Datum Od</label>
								<input id="datum_od_din" class="datum-val has-tooltip" name="datum_od_din" type="text" value="{{$od}}">
							</div>
							<div class="column medium-12 large-6">
								<label for="">Datum Do</label>
								<input id="datum_do_din" class="datum-val has-tooltip" name="datum_do_din" type="text" value="{{$do}}">
							</div>
							<div class="column medium-12">
								<button id="pretraga_prihoda" class="btn btn-primary fl" value="Pretraga">Pretraga</button>
								<a class="btn btn-danger fr" href="/admin/b2b/analitika">Poništi</a>
							</div>
						</div>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihod u odabranom periodu: </span>
						<span class="anly-ul__li__count">{{ number_format($prihod, 2, ',', ' ') }} din</span>
					</li>
			<!-- 		<li class="anly-ul__li">
						<span class="anly-ul__li__text tooltipz" aria-label="Razlika u ceni">RUC</span>
						<span class="anly-ul__li__count">{{ 
						number_format($razlika, 2, ',', ' ')  }} din</span>
					</li> -->

				</ul>
			</div>
		</div>
		<div class="column medium-6 small-12 large-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno artikala u prodavnici</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::ukupnoArtikala() }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno korisnika registrovano</span>
						<span class="anly-ul__li__count">{{ AdminB2BAnalitika::ukupnoKorisnika() }}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=150 id="graph"></canvas>
			</div>  
		</div>
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=150 id="graph2"></canvas>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=150 id="graph3"></canvas>
			</div>  
		</div>
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=150 id="graph4"></canvas>
			</div>
		</div>
	</div>
	<div class="row analitics-filter flat-box">
		 <div class="column medium-3">
			<label for="">Datum Od</label>
			<input id="datum_od" class="datum-val has-tooltip" name="datum_od" type="text" value="{{ $datum_od }}">
		</div>
		<div class="column medium-3">
			<label for="">Datum Do</label>
			<input id="datum_do" class="datum-val has-tooltip" name="datum_do" type="text" value="{{ $datum_do }}">
		</div>
		<div class="column medium-3">
			<a class="btn btn-danger btn-small" href="/admin/b2b/analitika">Poništi</a>
		</div>
	</div>

	<div class="row">
		<div class="flat-box">
		<div class="column medium-12 large-6">
				<table class="analitics-table">
					<tr>
						<th>Naziv</th>
						<th>Broj prodatih</th>
					</tr>
					@foreach(AdminB2BAnalitika::bestSeller() as $row)
					@if($row->count > 0)
					<tr>
						<td>
							{{ AdminB2BArticles::analitika_title($row->roba_id) }}
						</td>
						<td>
							{{ $row->count + 0  }}
						</td>
					</tr>
					@endif
					@endforeach
				</table>
		</div>
		<div class="column medium-12 large-6">
				<table class="analitics-table">
					<tr>
						<th>Naziv</th>
						<th>Broj pregleda</th>
					</tr>
					@foreach(AdminB2BAnalitika::mostPopularArticles() as $row)
					@if($row->pregledan_puta_b2b > 0)
					<tr>
						<td>
							{{ AdminB2BArticles::analitika_title($row->roba_id) }}
						</td>
						<td>
							{{ $row->pregledan_puta_b2b }}
						</td>
					</tr>
					@endif
					@endforeach
				</table>
		</div>
		<div class="columns medium-12 no-padd"> 
			<div class="column medium-12">
				<table class="analitics-table">
					<thead> 
						<tr>
							<th>&nbsp;Naziv (Grupe / Artikla)</th>
							<th>Broj prodatih</th>
							<th>Ukupna cena</th>
						</tr>
					</thead>
					<tbody> 
						@foreach($analitikaGrupa as $grupa)
						<tr class="JSAnalitikaGrupa" data-id="{{ $grupa->grupa_pr_id }}" data-isopen="closed">
							<td class="first-td">
							<a href="#!">{{$grupa->grupa}}</a>
							</td>
							<td>
								{{round($grupa->sum)}}
							</td>
							<td>
								{{sprintf("%.2f",$grupa->ukupno)}}
							</td>
						</tr>
								<?php $suma=0; ?>
						@foreach(AdminB2BAnalitika::prikazGrupa($grupa->grupa_pr_id) as $prikaz)
							@if(isset($prikaz->naziv))
							<tr class="JSAnalitikaArtikliGrupe sub-td" data-id="{{ $grupa->grupa_pr_id }}" hidden>
								<td class="colapsable-td">&nbsp;&nbsp; - {{$prikaz->naziv}} </td>
								<td> {{round($prikaz->sum)}} </td>
								<td> {{sprintf("%.2f",$prikaz->ukupno)}} </td>
							</tr>
							@endif
						@endforeach

						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
	
 

<div class="row">
	 <div class="flat-box"> 
	<div class="column medium-4">
		<h3 class="title-med">Najprodavanije grupe</h3>
		<div class="graph-box"><br>
			<canvas height=140 id="graph6"></canvas>
		</div>
	</div>
	<div class="column medium-4"> 
		<h3 class="title-med">Najposećenije grupe</h3>
		<div class="graph-box"><br>
		<canvas height=140 id="graph8"></canvas>
		</div>
	</div>
	<div class="column medium-4">
		<table class="analitics-table">
			<thead> 
			<tr>
				<th>Naziv grupe</th>
				<th>Broj poseta</th>
			</tr>
			</thead>
			<tbody> 
				@foreach($analitikaGrupaPregled as $grupa)
				<tr>
					<td>
						@if($datum_od && $datum_do)
						{{$grupa->grupa1}}
						@else
						{{$grupa->grupa1}}
						@endif
					</td>
					<td>
						{{$grupa->sum}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div> 

	</div>
</div>
<div class="row">
	<div class="flat-box">
		<h3 class="title-med">Prodaja po partnerima (<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/analitika/partneri">Detaljno</a>)</h3>
		<div class="column medium-6">
			<div class="graph-box"><br>
			<canvas height=140 id="graph9"></canvas>
			</div>
		</div> 
		<div class="column medium-6">
			<table class="analitics-table">
				<thead> 
					<tr>
						<th>Partner</th>
						<th>Promet</th>
					</tr>
				</thead>
				<tbody> 
					@foreach($partner as $partner1)
					<tr>
						<td>
							{{$partner1->naziv}}
						</td>
						<td>
							{{ number_format($partner1->promet,2,","," ") }}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="flat-box">
		<h3 class="title-med">Analitika partnera (<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/analitika/partner-logovi">Detaljno</a>)</h3>
		<div class="column medium-6">
			<div class="graph-box"><br>
			<canvas height=140 id="graph10"></canvas>
			</div>
		</div> 
		<div class="column medium-6">
	
		</div>
	</div>
	</div>
</div>
</section> <!-- end of #main-content -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.js"></script>

<script>
    Chart.plugins.register({
        afterDatasetsDraw: function(chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;
			if(['graph2'].includes(ctx.canvas.id)){
	            chartInstance.data.datasets.forEach(function (dataset, i) {
	                var meta = chartInstance.getDatasetMeta(i);
	                if (!meta.hidden) {
	                    meta.data.forEach(function(element, index) {
	                        // Draw the text in black, with the specified font
	                        ctx.fillStyle = 'rgb(0, 0, 0)';

	                        var fontSize = 16;
	                        var fontStyle = 'normal';
	                        var fontFamily = 'Helvetica Neue';
	                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

	                        // Just naively convert to string for now
	                        var dataString = dataset.data[index].toFixed(2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ').replace('.', ',');

	                        // Make sure alignment settings are correct
	                        ctx.textAlign = 'center';
	                        ctx.textBaseline = 'middle';

	                        var padding = 5;
	                        var position = element.tooltipPosition();
	                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
	                    });
	                }
	            });
	        }
        }
    });

var ctx = document.getElementById("graph");
var myChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
		datasets: [{
			lineTension: 0,
			label: '# narudžbine',
			data: [ {{ AdminB2BAnalitika::mesecnaAnalitika(1)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(2)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(3)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(4)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(5)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(6)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(7)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(8)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(9)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(10)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(11)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika(12)}}
					],
			backgroundColor: [
				'rgba(162, 162, 235, .5)'
			],
			borderColor: [
				'rgba(54, 162, 235, 1)'
			],
			borderWidth: 2
		}]
	},
	options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

var ctx2 = document.getElementById("graph2");
var myChart2 = new Chart(ctx2, {
	type: 'line',
	data: {
		labels: ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"],
		datasets: [{
			lineTension: 0,
			label: 'prihod',
			data: [ {{ AdminB2BAnalitika::mesecnaAnalitika2(1)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(2)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(3)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(4)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(5)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(6)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(7)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(8)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(9)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(10)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(11)}}, 
					{{ AdminB2BAnalitika::mesecnaAnalitika2(12)}}
					],
			backgroundColor: [
				'rgba(153, 102, 255, .5)',
				'rgba(75, 192, 192, .5)',
				'rgba(54, 162, 235, .5)',
				'rgba(255, 206, 86, .5)',
				'rgba(255, 99, 132, .5)',
				'rgba(255, 159, 64, .5)'
			],
			borderColor: [
				'rgba(153, 102, 255, 1)',
				'rgba(75, 192, 192, 1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)',
				'rgba(255,99,132,1)',
				'rgba(255, 159, 64, 1)'
			],
			borderWidth: 2
		}]
	},
	options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

var ctx3 = document.getElementById("graph3");
var myChart3 = new Chart(ctx3, {
	type: 'bar',
	data: {
		labels: [
			@foreach(AdminB2BAnalitika::bestSeller() as $row)
				<?php echo '"'.AdminB2BArticles::analitika_title($row->roba_id) .'",' ?>
			@endforeach
			],
		datasets: [{

			label: 'Najprodavaniji artikli',
			data: [ 
				@foreach(AdminB2BAnalitika::bestSeller() as $row)
					{{ "$row->count" }},
				@endforeach
				],
			backgroundColor: [
				'rgba(153, 102, 255, .5)',
				'rgba(75, 192, 192, .5)',
				'rgba(54, 162, 235, .5)',
				'rgba(255, 206, 86, .5)',
				'rgba(255, 99, 132, .5)',
				'rgba(255, 159, 64, .5)'
			],
			borderColor: [
				'rgba(153, 102, 255, 1)',
				'rgba(75, 192, 192, 1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)',
				'rgba(255,99,132,1)',
				'rgba(255, 159, 64, 1)'
			],
			borderWidth: 1
		}]
	},
	options: {
		// responsive: false,
		
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}],
			xAxes: [{display:false}]
		}
	}
});

var ctx4 = document.getElementById("graph4");
var myChart4 = new Chart(ctx4, {
	type: 'bar',
	data: {
		labels: [
			@foreach(AdminB2BAnalitika::mostPopularArticles() as $row)
				<?php echo "'".AdminB2BArticles::analitika_title($row->roba_id) ."'," ?>
			@endforeach
		],
		datasets: [{
			label: '# Artikli sa najvećim brojem pregleda',
			data: [ @foreach(AdminB2BAnalitika::mostPopularArticles() as $row)
						{{ "$row->pregledan_puta_b2b" }},
					@endforeach
					],
			backgroundColor: [
				'rgba(153, 102, 255, .5)',
				'rgba(75, 192, 192, .5)',
				'rgba(54, 162, 235, .5)',
				'rgba(255, 206, 86, .5)',
				'rgba(255, 99, 132, .5)',
				'rgba(255, 159, 64, .5)'
			],
			borderColor: [
				'rgba(153, 102, 255, 1)',
				'rgba(75, 192, 192, 1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)',
				'rgba(255,99,132,1)',
				'rgba(255, 159, 64, 1)'
			],
			borderWidth: 1
		}]
	},
	options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}],
			xAxes: [{display:false}]
		}
	}
});
var ctx5 = document.getElementById("graph5");
var myChart5 = new Chart(ctx5, {
	type: 'doughnut',
	data: {
		labels: [
			"Nove",
        	"Prihvaćene porudžbine",
			"Realizovane porudžbine",
        	"Stornirane"

		],
		datasets: [{
			label: '',
			data: [
				{{ $ukupno1 }},
				{{ $ukupno4 }},
				{{ $ukupno3 }}, 
				{{ $ukupno2 }} 
			],
			backgroundColor: [
   				"#FF6384", 
                "#59cd74",
   				"#a6a6a6",
				"#a370c2"
			],
			borderColor: [
				 "#FF6384",
                "#59cd74",
				"#a6a6a6",
                "#a370c2"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: false
        }
	}
});

var ctx6 = document.getElementById("graph6");
var myChart6 = new Chart(ctx6, {
	type: 'doughnut',
	data: {
		labels: [
			@foreach($analitikaSkraceno as $grupa)
			'{{ $grupa->grupa }}',
			@endforeach
			'Ostalo'
		],
		datasets: [{
			label: '',
			data:[
			@foreach($analitikaSkraceno as $grupa_count)
			'{{ sprintf("%.2f",$grupa_count->ukupno) }}',
			@endforeach
			'{{ sprintf("%.2f",$ostalo) }}'
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933",
   				"#8080ff"
			],
			borderColor: [
                "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933",
   				"#8080ff"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: true,
            position: 'bottom'
        }
	}
});



var ctx8 = document.getElementById("graph8");
var myChart8 = new Chart(ctx8, {
	type: 'doughnut',
	data: {
		labels: [
			@foreach($analitikaGrupaPregled as $grupa)
			'{{ $grupa->grupa1 }}',
			@endforeach
		],
		datasets: 
		[{
			label: '',
			data:[
			@foreach($analitikaGrupaPregled as $grupa_sum)
			'{{ $grupa_sum->sum }}',
			@endforeach
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933"

			],
			borderColor: [
                "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: true,
            position: 'bottom'
        }
	}
});


var ctx9 = document.getElementById("graph9");
var myChart9 = new Chart(ctx9, {
	type: 'doughnut',
	data: {
		labels: [
			@foreach($partner as $partner1)
			'{{ $partner1->naziv }}',
			@endforeach
		],
		datasets: 
		[{
			label: '',
			data:[
			@foreach($partner as $partner1)
			'{{ sprintf("%.2f",$partner1->promet) }}',
			@endforeach
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933"

			],
			borderColor: [
                "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: true,
            position: 'bottom'
        }
	}
});

var ctx10 = document.getElementById("graph10");
var myChart10 = new Chart(ctx10, {
	type: 'doughnut',
	data: {
		labels: [
			@foreach($visits as $visit)
			'{{ $visit->naziv }}',
			@endforeach
		],
		datasets: 
		[{
			label: '',
			data:[
			@foreach($visits as $visit)
			'{{ $visit->posecenost }}',
			@endforeach
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933"

			],
			borderColor: [
                "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: true,
            position: 'bottom'
        }
	}
});

</script>


@endsection