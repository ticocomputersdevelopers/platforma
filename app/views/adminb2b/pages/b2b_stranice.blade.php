@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content" class="pages-page row">
	<section class="medium-3 columns">
		<div class="flat-box pageran">
			<label class="text-center">Strane:</label>
			<ul class="page-list lista {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) ? 'JSPagesSortable' : '' }}" parent="0">
				<li class="new-page new-elem relative" id="0">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/0">Nova strana</a>
					<span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span>
				</li>
				@foreach(AdminB2BSupport::pages() as $row)
				<li class="ui-state-default @if($row->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row->web_b2b_seo_id}}" {{$row->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
					<a href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{$row->web_b2b_seo_id}}">{{$row->title}}</a>
					<ul class="page-list lista JSPagesSortable lvl-2-ul" parent="{{$row->web_b2b_seo_id}}">
						@foreach(AdminB2BSupport::pages($row->web_b2b_seo_id) as $row2)
						<li class="ui-state-default @if($row2->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row2->web_b2b_seo_id}}" {{$row2->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
							<a href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{$row2->web_b2b_seo_id}}">{{$row2->title}}</a>
							<ul class="page-list lista JSPagesSortable" parent="{{$row2->web_b2b_seo_id}}">
								@foreach(AdminB2BSupport::pages($row2->web_b2b_seo_id) as $row3)
								<li class="ui-state-default @if($row3->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row3->web_b2b_seo_id}}" {{$row3->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
									<a href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{$row3->web_b2b_seo_id}}">{{$row3->title}}</a>
								</li>

								@endforeach
							</ul>
						</li>
						@endforeach
					</ul>
				</li>
				@endforeach
			</ul>
		</div>
	</section>
	<section class="page-edit medium-9 columns">
		<div class="flat-box">
			<label class="text-center">Sadržaj:</label>
			<form name="form_pages" action="{{AdminB2BOptions::base_url()}}admin/b2b/pages" method="post">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">
				<section class="page-edit-top">
					<div class="row">
						<div class="medium-3 columns page-edit-name">
							<label>Naziv strane:</label>
							<input type="text" id="page_name" name="page_name" value="{{Input::old('page_name') ? Input::old('page_name') : $naziv}}" {{$disable==1?'disabled="disabled"':''}} onchange="check_fileds('page_name')" {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						@if($disable==0)
						<div class="medium-3 columns include-in-menu">
							<label>Listaj artikle grupe:</label>  
							<select name="grupa_pr_id" class="medium-12 columns seo-button-wrapper" {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="-1"></option>
								@foreach(DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get() as $grupa)
								@if(Input::old('grupa_pr_id') == $grupa->grupa_pr_id)
								<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
								@else
								@if($grupa_pr_id == $grupa->grupa_pr_id)
								<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
								@else
								<option value="{{ $grupa->grupa_pr_id }}">{{ $grupa->grupa }}</option>
								@endif
								@endif
								@endforeach
							</select>
						</div>
					<div class="medium-3 columns include-in-menu">
							<label>Listaj artikle akcije ili tipova:</label>  
							<select name="tip_artikla_id" class="medium-12 columns seo-button-wrapper">
							<option value="-1"></option>
							@if(!is_null(Input::old('tip_artikla_id')) AND Input::old('tip_artikla_id') == 0)
							<option value="0" selected>Akcija</option>
							@else
							@if($tip_artikla_id == 0)
							<option value="0" selected>Akcija</option>
							@else
							<option value="0">Akcija</option>
							@endif
							@endif
							@foreach(AdminB2BSupport::getTipoviB2B(1) as $tip)
							@if(Input::old('tip_artikla_id') == $tip->tip_artikla_id)
							<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
							@else
							@if($tip_artikla_id == $tip->tip_artikla_id)
							<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
							@else
							<option value="{{ $tip->tip_artikla_id }}">{{ $tip->naziv }}</option>
							@endif
							@endif
							@endforeach
							</select>
						</div>
						@endif
						<div class="medium-3 columns include-in-menu">
							<label>Osnovna stranica </label>  
							<select name="parent_id" class="medium-12 columns seo-button-wrapper" {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="0">Izaberite nad stranicu</option>
								@foreach(AdminB2BSupport::parent_pages(0,$web_b2b_seo_id) as $strana1)
									@if((1 + AdminB2BSupport::page_childs_depth(array($web_b2b_seo_id))) < 3)
									<option value="{{ $strana1->web_b2b_seo_id }}" {{($parent_id == $strana1->web_b2b_seo_id) ? 'selected' : ''}}>{{ $strana1->title }}</option>
										@foreach(AdminB2BSupport::parent_pages($strana1->web_b2b_seo_id,$web_b2b_seo_id) as $strana2)
											@if((2 + AdminB2BSupport::page_childs_depth(array($web_b2b_seo_id))) < 3)
											<option value="{{ $strana2->web_b2b_seo_id }}" {{($parent_id == $strana2->web_b2b_seo_id) ? 'selected' : ''}}>&nbsp;&nbsp;&nbsp;&nbsp; {{ $strana2->title }}</option>
											@endif
										@endforeach
									@endif
								@endforeach
							</select>
						</div>

						<div class="medium-6 columns include-in-menu">
							<br>
							<label class="checkbox-label medium-6 columns">
							<input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" {{ Input::old('b2b_header_menu') ? 'checked="checked"' : ($b2b_header ? 'checked="checked"' : '') }} {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}> 
							<span class="label-text">Header meniju</span>
							</label>
							<label class="checkbox-label medium-6 columns">
							<input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" {{ Input::old('b2b_footer_menu') ? 'checked="checked"' : ($b2b_footer ? 'checked="checked"' : '') }} {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<span class="label-text">Footer meniju</span>
							</label>  
						</div>
					</div>
				 
					<br>
					@if(Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')))
						<div class="row">
							@if(count($jezici) > 1)
							<div class="languages">
								<ul>	
								@foreach($jezici as $jezik)
								<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/{{ $web_b2b_seo_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
								</ul>
							</div>
							@endif
						</div>
					@endif
					<!-- SEO SETTINGS -->
					<!-- 						<section class="seo-settings row">
					<div class="medium-6 columns field-group {{ $errors->first('seo_title') ? 'error' : '' }}">
					<label>Title:</label>
					<input id="seo-title" type="text" value="{{Input::old('seo_title') ? Input::old('seo_title') : $seo_title}}"  name="seo_title" onkeydown=" return character_limit(event,60)">
					</div>
					<div class="medium-6 columns field-group {{ $errors->first('keywords') ? 'error' : '' }}">
					<label>Keywords:</label>
					<input id="seo-keywords" type="text" value="{{Input::old('keywords') ? Input::old('keywords') : $keywords}}" name="keywords" onkeydown=" return character_limit(event,159)">
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('description') ? 'error' : '' }}">
					<label>Description:</label>
					<input id="seo-description" type="text" value="{{Input::old('description') ? Input::old('description') : $desription}}" name="description" onkeydown=" return character_limit(event)">
					</div>
					</section> -->
				</section>
				@if($disable==0)
				<!-- PAGE EDIT TEXT EDITOR -->
				<div class="row"> 
					<textarea @if(Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE'))) class="special-textareas" @endif name="content" id="content"  style="width:100%" {{ Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					{{$content}}
					</textarea>
				</div>
				@endif
				<input type="hidden" name="web_b2b_seo_id" value="{{$web_b2b_seo_id}}" />
				<input type="hidden" name="status" value="{{$status}}" />
				<input type="hidden" name="flag_page" id="flag_page" value="{{$flag_page}}" />
				<input type="hidden" name="flag_b2b_show" id="flag_b2b_show" value="{{$flag_b2b}}" />
				@if(Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')))
					<div class="btn-container center">
						<a href="#" class="page-edit-save btn btn-primary save-it-btn">Sačuvaj</a>
					</div>
				@endif
			</form>
			@if($disable==0)
			@foreach($stranice as $row)
			<a href="{{ AdminB2BSupport::page_link_b2b($row->naziv_stranice) }}" target="_blank" class="btn btn-primary" value="{{$web_b2b_seo_id}}">Vidi stranicu</a>
			@endforeach
			<form class="pages-form" method="post" action="{{AdminB2BOptions::base_url()}}admin/b2b/delete_page">
				<input type="hidden" name="web_b2b_seo_id" value="{{$web_b2b_seo_id}}" />	
				@if(Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')))			
					<input type="submit" value="Obriši stranicu" class="btn btn-danger">
				@endif
			</form>
				<div class="image-upload-area clearfix">	
					@if(Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE')))
						<form method="post" enctype="multipart/form-data" action="{{AdminB2BOptions::base_url()}}admin/b2b/image_upload" >
							<div class="row"> 
								<div class="column"> 
									<label>Dodajte sliku na strani:</label>
									<div class="bg-image-file"> 
										<input type="file" id="img" name="img" class="file-input">
										<button class="file-upload btn btn-secondary btn-small" type="submit">Učitaj</button>
									</div>
								</div>
							</div>
						</form>
					@endif
						<div class="images_upload has-tooltip" @if(Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE'))) title='Prevucite sliku na željenu poziciju' @endif>
						{{AdminB2BSupport::upload_directory()}}
						</div>
				</div>
			@endif
		</div> 
	</section>
</section>
@endsection
