@extends('adminb2b.defaultlayout')
@section('content')
<?php
if($datum_od==0 and $datum_do==0){
	$dat1='';
	$dat2='';
}else 
if($datum_od!=0 and $datum_do==0){
	$dat1=$datum_od;
	$dat2='';
}else 
if($datum_od==0 and $datum_do!=0){
	$dat1='';
	$dat2=$datum_do;
}else 
{
	$dat1=$datum_od;
	$dat2=$datum_do;
}
?>

<section id="main-content" class="orders-page row b2b-orders">

	<div class="row bw flex">
		<div class="column medium-4"> 
			<h1 class="title-filters-h1">B2B Narudžbine</h1>
		</div> 

		<div class="orders-input column medium-4"> 
			<input type="text" class="search-field" id="search" autocomplete="on" placeholder="Pretraži narudžbine...">
			<button type="submit" id="search-btn" value="{{$search}}" class="m-input-and-button__btn btn btn-primary btn-radius">
				<i class="fa fa-search" aria-hidden="true"></i>
			</button>
		</div>
		@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
		<div class="columns medium-4 text-right"><a href="/admin/b2b/narudzbina/0" class="btn btn-create btn-small">Kreiraj narudžbinu</a></div>
		@endif
	</div>

	<!-- filteri narudzbina -->
	<ul class="o-filters row"> 
		<span class="o-filters-span-1 column medium-8"> 
			<label>Filteri narudžbina:</label> 
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
					<span class="label-text nova">Nove</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
					<span class="label-text prihvacena">Prihvaćene</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
					<span class="label-text">Realizovane</span>
				</label>
			</li>
			<li class="o-filters__li">
				<label class="checkbox-label">
					<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
					<span class="label-text stornirana">Stornirane</span>
				</label>
			</li>
		</span>
		<span class="o-filters-span-1 column medium-4">
			@if(AdminB2BOptions::checkB2B() == 1)
			<div class="column medium-9"> 
				<label>Filteri dodatnih statusa:</label>
				<select class="m-input-and-button__input import-select " id="narudzbina_status_id" >
					<option value="0">Svi dodatni statusi</option>
					@foreach(AdminB2BNarudzbina::dodatni_statusi() as $statusi)
					@if($statusi->narudzbina_status_id == $narudzbina_status_id )	
					<option value="{{ $statusi->narudzbina_status_id }}" selected>{{ $statusi->naziv }}</option>
					@else
					<option value="{{ $statusi->narudzbina_status_id }}">{{ $statusi->naziv }}</option>
					@endif				
					@endforeach
				</select>	
			</div>
			@endif

			<a href="#" class="m-input-and-button__btn btn btn-danger erase-btn">
				<span class="fa fa-times tooltipz-left" aria-label="Poništi filtere" id="JSFilterClear" aria-hidden="true"></span>
			</a> 
		</span>
	</ul>

	<div class="row o-filters-span-2">

		<div class="columns medium-5">
			<span>{{ AdminLanguage::transAdmin('Ukupno narudžbina') }}: <b>{{count($query)}}</b></span>
		</div>

		<div class="columns medium-7">  
			<span class="datepicker-col">
				<div class="datepicker-col__box clearfix">
					<label>Od:</label>
					<input id="datepicker-from" class="has-tooltip" data-datumdo="{{$datum_do}}" value="{{$dat1}}" type="text">
				</div>

				<div class="datepicker-col__box clearfix">
					<label>Do:</label>
					<input id="datepicker-to" class="has-tooltip" data-datumod="{{$datum_od}}" value="{{$dat2}}" type="text">
				</div>
			</span>
		</div>
	</div> 


	<div class="table-scroll" style="max-height: none;">
		<table class="order-table"> 
			<thead>
				<tr>
					<th>Firma</th>
					<th>Broj narudžbine</th>
					<th>Datum</th>
					<th>Artikli</th>
					<th>Iznos</th>
					<th>Telefon</th>
					<th>Mesto</th>
					<th>Način plaćanja</th>
					<th>Način isporuke</th>
					<th>Dodatni status</th>
					<th>Status</th>
					<th>Napomena</th>
				</tr>
			</thead>

			<tbody>
				@foreach($query as $row)  
				<tr class="JSMoreWiew {{AdminB2BNarudzbina::narudzbina_status_css($row->web_b2b_narudzbina_id)}}" data-id-narudzbina="{{$row->web_b2b_narudzbina_id}}">

					<td>{{AdminB2BNarudzbina::narudzbina_partner($row->web_b2b_narudzbina_id)}}</td>
					<td>{{$row->broj_dokumenta}}</td>
					<td>{{AdminB2BNarudzbina::datum_narudzbine($row->web_b2b_narudzbina_id)}}</td>

					<td>
						@foreach(AdminB2BNarudzbina::narudzbina_stavke($row->web_b2b_narudzbina_id) as $stavka) 
						<div class="counter text-left">
							<span class="ellipsis-text vert-align inline-block">{{ AdminB2BArticles::find($stavka->roba_id, 'naziv_web') }}</span>
						</div>
						@endforeach
					</td>

					<td>{{AdminB2BArticles::cena(AdminB2BNarudzbina::narudzbina_iznos_ukupno($row->web_b2b_narudzbina_id))}}</td>

					<td>{{AdminB2BNarudzbina::partner_telefon($row->web_b2b_narudzbina_id)}}</td>

					<td>{{AdminB2BNarudzbina::partner_mesto($row->web_b2b_narudzbina_id)}}</td>

					<td>{{AdminB2BNarudzbina::narudzbina_np($row->web_b2b_narudzbina_id)}}</td>

					<td>{{AdminB2BNarudzbina::narudzbina_ni($row->web_b2b_narudzbina_id)}}</td>

					<td>{{AdminB2BNarudzbina::status_narudzbine($row->web_b2b_narudzbina_id)}}</td>

					<td>
						<section class="order-status">
							<select class="order-status__select" {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option class="order-status-active">{{AdminB2BNarudzbina::narudzbina_status_active($row->web_b2b_narudzbina_id)}}</option>
								{{AdminB2BNarudzbina::narudzbina_status_posible(AdminB2BNarudzbina::narudzbina_status_active($row->web_b2b_narudzbina_id),$row->web_b2b_narudzbina_id)}}
							</select>
						</section>
					</td>

					<td>
						@if(AdminB2BNarudzbina::narudzbina_napomena($row->web_b2b_narudzbina_id))
						<div class="btn-container center no-margin"> 
							<span class="btn btn-secondary btn-circle small relative tooltipz" aria-label="{{ AdminB2BNarudzbina::narudzbina_napomena($row->web_b2b_narudzbina_id) }}"><i class="fa fa-info" aria-hidden="true"></i></span>  
						</div>
						@endif
					</td>

				</tr> 
				@endforeach

			</tbody>
		</table>
	</div>
 
	{{ Paginator::make($query, $count, $limit)->links() }}

</section> <!-- end of #main-content -->

<!-- MODAL ZA DETALJI PORUDJINE -->
<div id="ordersDitailsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<div class="content"></div>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- MODAL END -->

@endsection