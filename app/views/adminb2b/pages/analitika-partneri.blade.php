@extends('adminb2b.defaultlayout')
@section('content')

<div id="main-content">

	<div class="flat-box"> 
		<div class="row collapse"> 
			<form method="GET" action="{{AdminOptions::base_url()}}admin/b2b/analitika/partneri" class="columns medium-10 no-padd">
				<div class="columns medium-4">  
					<div class="flex">
						<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">

						<input class="btn btn-primary btn-small" value="Pretraga" type="submit">

						<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}admin/b2b/analitika/partneri">{{ AdminLanguage::transAdmin('Poništi') }}</a>
					</div> 
				</div> 
			</form>

			<div class="columns medium-2">
				<div class="text-right">
					<button id="JSExportAnalitikaExport" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Export') }}</button>
				</div>
			</div>
		</div>
	</div>

	<div class="flat-box">  
		
		<label class="no-margin">{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</label>

		<div class="table-scroll"> 
			<table>
				<thead> 
					<tr>
						<th class="JSSort" data-sort_column="sifra" data-sort_direction="{{ $sort_column == 'sifra' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Šifra') }}</th>
						<th class="JSSort" data-sort_column="naziv" data-sort_direction="{{ $sort_column == 'naziv' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Naziv') }}</th>
						<th class="JSSort" data-sort_column="br_logovanja" data-sort_direction="{{ $sort_column == 'br_logovanja' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Br. logovanja') }}</th>
						<th class="JSSort" data-sort_column="br_narudzbina" data-sort_direction="{{ $sort_column == 'br_narudzbina' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Br. narudžbina') }}</th>
						<th class="JSSort" data-sort_column="sum_cena" data-sort_direction="{{ $sort_column == 'sum_cena' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Vrednost narudžbina') }}</th>
						<th class="JSSort" data-sort_column="cena_prosek" data-sort_direction="{{ $sort_column == 'cena_prosek' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Prosečna vrednost') }}</th>
					</tr>
				</thead>

				<tbody> 
					@foreach($partneri as $row)
					<tr>
						<td>{{ $row->id_is }}</td>
						<td>{{ $row->naziv }}</td>
						<td>{{ $row->br_logovanja }}</td>
						<td>{{ $row->br_narudzbina }}</td>
						<td>{{ number_format($row->sum_cena,2,",",".") }}</td>
						<td>{{ number_format($row->cena_prosek,2,",",".") }}</td>
					</tr>
					@endforeach 
				</tbody>
			</table>
		</div>
		{{ Paginator::make($partneri,$count,$limit)->links() }} 
	</div>
</div>
@endsection