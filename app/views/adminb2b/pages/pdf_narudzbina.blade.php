<!DOCTYPE html>
<html>
<head>
	<!-- <meta charset="utf-8"> -->
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
	<?php /* header('Content-Type: text/html; charset=utf-8;'); */?> 
	<title>PDF</title>
	<style>
		* {
			margin: 0;
			font-family: DejaVu Sans;
		}
		body {
			font-size: 16px;
		}
		table {
			border-collapse: collapse;
			width: 100%;
			font-size: 12px;
			margin-bottom: 50px;
			border-radius: 2px;
		}
		td, th {
			border: 1px solid #e1e1e1;
			text-align: left;
			padding: 5px;
		}
		tr:nth-child(even) {
			background-color: #f5f6ff;
		}
		h2 {
			margin-bottom: 15px;
		}
		.divider {
			/*margin: 30px 0;*/
			padding: 15px 0;
		}
		.container {
			width: 90%;
			margin: 20px auto;
		}
		.logo {
			width: 100%;
			text-align: center;
			margin-bottom: 15px;
		}
		.logo img {
			max-width: 150px;
		}
		img {
			max-width: 100%;
		}
		.text-right {
			text-align: right;
		}
		.signature {
			padding-top: 50px;
			background: none;
			color: #808080;
		}
		.signature td, th {
			border: none;
			background: none;
		}
	</style>
</head>
<body>
	<section class="shopping-information">
		<div class="container">
			<div class="logo">
				<img src="{{ AdminOptions::base_url()}}{{Options::company_logo()}}" alt="logo">
			</div>

			<h2>B2B Narudžbina</h2>
			

			<p class="divider">Informacije o narudžbini</p>
			<table>
				<tr>
					<td>Broj porudžbine:</td>
					<td>{{AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'broj_dokumenta')}}</td>
				</tr>
				<tr>
					<td>Datum porudžbine:</td>
					<td>{{AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'datum_dokumenta')}}</td>
				</tr>
				<tr>
					<td>Način isporuke:</td>
					<td>{{AdminCommon::n_i_b2b($web_b2b_narudzbina_id)}}</td>
				</tr>
				<tr>
					<td>Način plaćanja:</td>
					<td>{{AdminCommon::n_p_b2b($web_b2b_narudzbina_id)}}</td>
				</tr>
				<tr>
					<td>Napomena:</td>
					<td>{{ AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'napomena') }}</td>
				</tr>
			
			</table>
				
			<p class="divider">Informacije o kupcu:</p>
			{{AdminB2BSupport::narudzbina_kupac_pdf($web_b2b_narudzbina_id)}}

			<p class="divider">Informacije o prodavcu:</p>
			<table>
				<tr>
					<td>Naziv prodavca:</td>
					<td>{{AdminOptions::company_name()}}</td>
				</tr>
				<tr>
					<td>Adresa:</td>
					<td>{{AdminOptions::company_adress()}}</td>
				</tr>
				<tr>
					<td>Telefon:</td>
					<td>{{AdminOptions::company_phone()}}</td>
				</tr>
				<tr>
					<td>Fax:</td>
					<td>{{AdminOptions::company_fax()}}</td>
				</tr>
				<tr>
					<td>PIB:</td>
					<td>{{AdminOptions::company_pib()}}</td>
				</tr>
				<tr>
					<td>Šifra delatnosti:</td>
					<td>{{AdminOptions::company_delatnost_sifra()}}</td>
				</tr>
				<tr>
					<td>Žiro račun:</td>
					<td>{{AdminOptions::company_ziro()}}</td>
				</tr>
				<tr>
					<td>E-mail:</td>
					<td>{{AdminOptions::company_email()}}</td>
				</tr>
			</table>
			
			<table>
	        <tr>
	            <td class="cell-product-name">Naziv proizvoda:</td>
	            <td class="cell">Cena :</td>
	            <td class="cell">Količina</td>
	            <td class="cell">Ukupna cena:</td>
	        </tr>
	        @foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row)
	        	<tr>
	                <td class="cell-product-name">{{AdminCommon::short_title($row->roba_id)}}</td>
	                <td class="cell">{{ number_format($row->jm_cena, 2, '.', '') }}</td>
	                <td class="cell">{{(int)$row->kolicina}}</td>
	                <td class="cell">{{ number_format(($row->jm_cena * $row->kolicina), 2, '.', '') }}</td>
	            </tr>
	        @endforeach
	        </table>
	        <table>
	        	<tr class="text-right">
	                <td class="summary text-right"><span class="sum-span">Iznos osnovice:</span>
	                 {{ number_format(floatval($ukupna_cena->cena_sa_rabatom),2,'.','') }}</td>
	            </tr>
	            <tr class="text-right">
	                <td class="summary text-right"><span class="sum-span">Iznos PDV-a:</span>
	                 {{ number_format(floatval($ukupna_cena->porez_cena),2,'.','') }}</td>
	            </tr>
	            @if($troskovi_isporuke > 0)
	            <tr class="text-right">
	                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
	                 {{ number_format(floatval($troskovi_isporuke),2,'.','') }}</td>
	            </tr>
	            @endif	   
	            <tr class="text-right">
	                <td class="summary text-right">Ukupno:{{ number_format(floatval($ukupna_cena->ukupna_cena+$troskovi_isporuke),2,'.','') }}</td>
	            </tr>
	        </table>

			<table class="signature">
				<tr>
					<td>___________________________</td>
					<td class="text-right">___________________________</td>
				</tr>
			</table>

		</div>
	</section> <!-- end .shopping-information -->
</body>
</html>
