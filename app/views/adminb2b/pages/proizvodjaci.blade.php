<div class="row small-gutter art-row">
	<div class="columns medium-6"> 
		<h2 class="title-med">Proizvođači</h2><br>
		  	
		<div class="btn-container box-sett"> 
			<input type="text" id="myInput" placeholder="Pretraga..">
		</div>
		<table id="myTable">
		  <tr class="header">
		    <th></th>
		    <th></th>
		  </tr>
		  	@if($partner_id >= -2)
			 	@foreach($proizvodjaci as $row)
					<tr class="proizvodjaci-b2b">
						<td>&nbsp; {{ $row->naziv }}</td>
					    <td><span><input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" class="JSrabat" data-id="{{ $row->proizvodjac_id }}" value="{{ ($row->rabat != 0) ? $row->rabat : '' }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}></span></td>
					</tr>
			  	@endforeach
			@endif
		</table> 
	</div>
	<div class="columns medium-6"> 
		<h2 class="title-med">Partneri</h2><br>
		  	
		<div class="btn-container box-sett"> 
		<select id="parner-change" name="Partner" class="admin-select m-input-and-button__input">
		  		<option value="-2" selected>Nedefinisan</option>
		  		@foreach($partneri as $row)
		  			@if($partner_id == $row->partner_id)
		  				<option value="{{$row->partner_id}}" selected>
		  				{{$row->naziv}}
		  				</option>
		  			@else
		  				<option value="{{$row->partner_id}}">{{$row->naziv}}</option>
		  			@endif
		  		@endforeach
		  	</select>
		</div>
	</div>	
</div>