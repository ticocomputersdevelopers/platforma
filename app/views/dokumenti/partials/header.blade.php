<header id="admin-header" class="row">
	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	<div class="logo-wrapper">
		<a class="logo" href="{{ DokumentiOptions::base_url()}}admin" title="Selltico">
			<img src="{{ DokumentiOptions::base_url()}}/images/admin/logo-selltico-white.png" alt="Selltico">
		</a>
	</div>

	<!-- <span class="main-menu-toggler"></span> -->
	<nav class="main-menu">

		<ul class="clearfix">
			<li class="menu-item">
				<a href="{{DokumentiOptions::base_url()}}dokumenti/ponude" class="menu-item__link  @if(in_array($strana,array('ponude','ponuda','racuni','racun'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">Dokumenti</span>
					</div>
					<div class="menu-item__text">Dokumenti</div>
				</a>
			</li>
			@if(!is_null(DokumentiOptions::user('saradnik')) && is_null(AdminB2BOptions::info_sys()))
			<li class="menu-item">
				<a href="{{DokumentiOptions::base_url()}}dokumenti/nalog" class="menu-item__link  @if(in_array($strana,array('nalog'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-user" aria-hidden="true"></i>
						<span class="menu-tooltip">Nalog</span>
					</div>
					<div class="menu-item__text">Nalog</div>
				</a>
			</li>
			@endif
			<li class="menu-item">
				<a href="{{DokumentiOptions::base_url()}}dokumenti/partneri" class="menu-item__link  @if(in_array($strana,array('partneri','partner'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">Kupci</span>
					</div>
					<div class="menu-item__text">Kupci</div>
				</a>
			</li>
			<li class="menu-item">
				<a href="{{DokumentiOptions::base_url()}}dokumenti/podesavanja" class="menu-item__link  @if(in_array($strana,array('podesavanja'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span class="menu-tooltip">Podešavanja</span>
					</div>
					<div class="menu-item__text">Podešavanja</div>
				</a>
			</li>
			<div class="menu-item">
				<a href="{{ AdminB2BOptions::base_url()}}b2b/dokumenti/ponude" class="menu-item__link">
					<div class="menu-item__icon">
						<img class="shop-logo" src="{{ AdminB2BOptions::base_url()}}images/admin/wbp-logo.png" alt="{{Options::company_name()}}" />
						<span class="menu-tooltip">B2B Ponude</span>
					</div>
					<div class="menu-item__text">B2B Ponude</div>
				</a>
			</div>
			@if(!is_null(DokumentiOptions::user('admin')) AND AdminOptions::checkB2B())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/b2b" class="menu-item__link">
					<div class="menu-item__icon">
						B2B
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('B2B Admin') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('B2B Admin') }}</div>
				</a>
			</li>
			@endif
		</ul>
	</nav>

	<div class="logout-wrapper">
		<div class="menu-item">
			<a href="{{ DokumentiOptions::base_url()}}dokumenti/logout" class="menu-item__link">
				<div class="menu-item__icon">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span class="menu-tooltip">Odjavi se</span>
				</div>
				<div class="menu-item__text">Odjavi se</div>
			</a>
		</div>
		<p class="footnote">TiCo &copy; {{ date('Y') }} - All rights reserved</p>
	</div>
</header>

