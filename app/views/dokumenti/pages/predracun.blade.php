@extends('dokumenti.templates.main')
@section('content')

<div id="main-content" class="kupci-page">
	<div class="row flat-box">
		<div class="columns medium-12">
			<h1>
				@if(!is_null($ponuda))
				<a href="{{DokumentiOptions::base_url()}}dokumenti/ponuda/{{$ponuda->ponuda_id}}">{{$ponuda->broj_dokumenta}} </a> 
				@endif
				{{ $predracun->broj_dokumenta }}
				@if(!is_null($racun))
				<a href="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$racun->racun_id}}">{{$racun->broj_dokumenta}} </a> 
				@endif
			</h1>
		</div>
	</div>

	<form method="POST" action="{{DokumentiOptions::base_url()}}dokumenti/predracun-post"> 
		<div class="row flat-box">
			<div class="field-group columns medium-6">
				<h2 class="title-med">Informacije o kupcu</h2>
				<div> 
					<label for="">Kupac</label>
					<input type="hidden" name="partner_id" id="JSPartnerId" value="{{ (Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id) }}">
					<input type="text" name="naziv" id="JSPartnerNaziv" value="{{ (Input::old('naziv') ? Input::old('naziv') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'naziv')) }}" class="{{ $errors->first('naziv') ? 'error' : '' }}" autocomplete="off">
					<div id="JSPartnerSearchContent" class="rma-custom-select"></div>
				</div>
				<div> 
					<label for="">PIB</label>
					<input type="text" name="pib" id="JSPartnerPib" value="{{ trim(Input::old('pib') ? Input::old('pib') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'pib')) }}" class="{{ $errors->first('pib') ? 'error' : '' }}" autocomplete="off">
				</div>
				<div> 
					<label for="">Ulica i broj</label>
					<input type="text" name="adresa" id="JSPartnerAdresa" value="{{ (Input::old('adresa') ? Input::old('adresa') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'adresa')) }}" class="{{ $errors->first('adresa') ? 'error' : '' }}" autocomplete="off">
				</div>
				<div> 
					<label for="">Mesto</label>
					<input type="text" name="mesto" id="JSPartnerMesto" value="{{ (Input::old('mesto') ? Input::old('mesto') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'mesto')) }}" class="{{ $errors->first('mesto') ? 'error' : '' }}" autocomplete="off">
				</div>
			</div>
			<div class="field-group columns medium-6">
				<h2 class="title-med">Kontakt informacije</h2>
				<div> 
					<label for="">Kontakt Osoba</label>
					<input type="text" name="kontakt_osoba" id="JSPartnerKontaktOsoba" value="{{ (Input::old('kontakt_osoba') ? Input::old('kontakt_osoba') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'kontakt_osoba')) }}" class="{{ $errors->first('kontakt_osoba') ? 'error' : '' }}" autocomplete="off">
				</div>
				<div> 
					<label for="">Email</label>
					<input type="text" name="mail" id="JSPartnerMail" value="{{ (Input::old('mail') ? Input::old('mail') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'mail')) }}" class="{{ $errors->first('mail') ? 'error' : '' }}" autocomplete="off">
				</div>
				<div> 
					<label for="">Telefon</label>
					<input type="text" name="telefon" id="JSPartnerTelefon" value="{{ (Input::old('telefon') ? Input::old('telefon') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'telefon')) }}" class="{{ $errors->first('telefon') ? 'error' : '' }}" autocomplete="off">
				</div>
			</div>
		

      	<div class="field-group columns no-padd medium-12">
			<div class="field-group columns medium-2">
				<label for="">Datum izdavanja</label>
				<input name="datum_predracuna" class="datum-val has-tooltip {{ $errors->first('datum_predracuna') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_predracuna') ? Input::old('datum_predracuna') : Dokumenti::dateInversion($predracun->datum_predracuna) }}" autocomplete="off">
			</div>
			<div class="field-group columns medium-2">
				<label for="">Važi do</label>
				<input name="vazi_do" class="datum-val has-tooltip {{ $errors->first('vazi_do') ? 'error' : '' }}" type="text" value="{{ Input::old('vazi_do') ? Input::old('vazi_do') : Dokumenti::dateInversion($predracun->vazi_do) }}" autocomplete="off">
			</div>
			<div class="field-group columns medium-2">
				<label for="">Rok isporuke</label>
				<input name="rok_isporuke" class="datum-val has-tooltip {{ $errors->first('rok_isporuke') ? 'error' : '' }}" type="text" value="{{ Input::old('rok_isporuke') ? Input::old('rok_isporuke') : Dokumenti::dateInversion($predracun->rok_isporuke) }}" autocomplete="off">
			</div>
			<div class="field-group columns medium-2">
				<label for="">Način plaćanja</label>
				<input name="nacin_placanja" class="datum-val has-tooltip {{ $errors->first('nacin_placanja') ? 'error' : '' }}" type="text" value="{{ Input::old('nacin_placanja') ? Input::old('nacin_placanja') : $predracun->nacin_placanja }}" autocomplete="off">
			</div>
			<div class="field-group columns medium-2">
				<label for="">Status</label>
				<select name="dokumenti_status_id" id="JSPredracunStatus">
					{{ Dokumenti::statusSelect((Input::old('dokumenti_status_id') ? Input::old('dokumenti_status_id') : $predracun->dokumenti_status_id)) }}
				</select>
			</div>
			@if(AdminOptions::gnrl_options(3050))
			<div class="field-group columns medium-1">
				<label for="">Račun</label>
				<input name="racun" class="datum-val has-tooltip {{ $errors->first('racun') ? 'error' : '' }}" type="checkbox" {{ ((count(Input::old()) > 0 ? (Input::old('racun')=="on" ? 1 : 0) : 0)==1) ? 'checked' : '' }}">
			</div>
			@endif
		</div>
        </div>

        <div class="flat-box row">
        <div id="JSPredracunSabloniLink" class="sabloni">Šabloni</div>
        <div id="JSPredracunSabloni" hidden="hidden">
			<div class="field-group columns medium-12">
				<label for="">Šablon</label>
				<textarea name="tekst" class="special-textareas">{{ $predracun->tekst }}</textarea>
			</div>
			<div class="field-group columns medium-12">
				<label for="">Footer</label>
				<textarea name="tekst_footer" class="special-textareas">{{ $predracun->tekst_footer }}</textarea>
			</div>
		</div>
		<input type="hidden" name="predracun_id" value="{{ $predracun->predracun_id }}">
		</div>
        @if($predracun->predracun_id > 0)
        <div class="row flat-box">
        	<h2 class="columns medium-12 title-med stavke">Stavke</h2>
        	<div class="field-group columns medium-12">
        		<table>
        			<thead>
        				<tr>
        					<td>ID</td>
        					<td>Naziv</td>
        					<td>Kolicina</td>
        					<td>Cena po jedinici</td>
        					<td>Iznos PDV-a (%)</td>
        					<td>Ukupno (din.)</td>
        					<td></td>
        				</tr>
        			</thead>
        			<tbody>
		            	@foreach($stavke as $stavka)
        				<tr class="JSStavkaRow" data-roba_id="{{$stavka->roba_id}}" data-predracun_stavka_id="{{$stavka->predracun_stavka_id}}" data-predracun_id="{{$predracun->predracun_id}}">
        					<td><span class="JSRobaIdText">{{$stavka->roba_id}}</span></td>
        					<td>
        						@if(!is_null($stavka->roba_id))
								<select class="JSRobaId">
									{{ Dokumenti::artikliSelect($stavka->roba_id) }}
								</select>
								@else
								<input type="text" class="JSStavkaNaziv" value="{{$stavka->naziv_stavke}}">    
								@endif
        					</td>
        					<td>
        						<input type="text" class="JSKolicina" value="{{ $stavka->kolicina }}">
        					</td>
        					<td>
        						<input type="text" class="JSNsbCena" value="{{round($stavka->nab_cena,2)}}">
        					</td>
        					<td><input type="text" class="JSPdv" value="{{$stavka->pdv}}"></td>
        					<td><input type="text" class="JSUkupno" value="{{ number_format(round((($stavka->nab_cena*$stavka->kolicina)*(1+$stavka->pdv/100)),2),2,'.','') }}" disabled></td>
        					@if(!$checkRacuni)
        					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/predracun-stavka/{{$stavka->predracun_stavka_id}}/delete">Ukloni</a></td>
        					@else
        					<td></td>
        					@endif
        				</tr>
						@endforeach
						@if($novaStavka)
        				<tr class="JSStavkaRow" data-predracun_stavka_id="0" data-predracun_id="{{$predracun->predracun_id}}">
        					<td><span class="JSRobaIdText">0</span></td>
        					<td>
								<select class="JSRobaId">
									{{ Dokumenti::artikliSelect(0,$roba_ids) }}
								</select>
        					</td>
        					<td>
        						<input type="text" class="JSKolicina" value="1">
        					</td>
        					<td>
        						<input type="text" class="JSNsbCena" value="0">
        					</td>
        					<td><input type="text" class="JSPdv" value="20"></td>
        					<td><input type="text" class="JSUkupno" value="0.00" disabled></td>
        					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/predracun/{{$predracun->predracun_id}}">Ukloni</a></td>
        				</tr>							
						@endif
        				<tr>
        					<td></td>
        					<td></td>
        					<td></td>
        					<td></td>
        					<td></td>
        					<td id="JSPredracunUkupanIznos">{{ round($stavkeUkupanIznos,2) }}</td>
        					<td></td>
        				</tr>
						@if(!$novaStavka AND !$checkRacuni)
        				<tr>
        					<td></td>
        					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/predracun/{{$predracun->predracun_id}}?nova_stavka=1">Dodaj novu stavku</a></td>
        					<td></td>
        					<td></td>
        					<td></td>
        					<td></td>
        					<td></td>
        				</tr>
        				@endif
        			</tbody>
        		</table>
			</div>      	
        </div>
        @endif
		<div class="row flat-box">
			<div class="columns medium-12 large-12 text-center">
				@if(!$checkRacuni)
				<div class="btn-container inline-block"> 
					<button type="submit" id="JSPredracunSubmit" class="btn btn-primary save-it-btn" style="{{(count(Input::old()) > 0) ? 'background-color: red;':''}}">Sačuvaj</button>
				</div>
				<div class="btn-container inline-block"> 
					<a class="btn btn-primary save-it-btn JSbtn-delete" data-link="{{DokumentiOptions::base_url()}}dokumenti/predracun/{{$predracun->predracun_id}}/delete">Obriši</a>
				</div>
				@endif
				<div class="btn-container inline-block"> 
					<a href="{{DokumentiOptions::base_url()}}dokumenti/predracun-pdf/{{$predracun->predracun_id}}" class="btn btn-primary save-it-btn">PDF</a>
				</div>				
			</div>
		</div>
    </form>	
</div>
@endsection