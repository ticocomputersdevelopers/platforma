@extends('dokumenti.templates.main')
@section('content')

<div id="main-content" class="kupci-page">
	<div class="row">
		<div class="flat-box">
			<div class="columns medium-12">
					<h1>
						@if(!is_null($ponuda))
						<a href="{{DokumentiOptions::base_url()}}dokumenti/ponuda/{{$ponuda->ponuda_id}}">{{$ponuda->broj_dokumenta}}</a>
						@endif
						@if(!is_null($predracun))
						<a href="{{DokumentiOptions::base_url()}}dokumenti/predracun/{{$predracun->predracun_id}}">{{$predracun->broj_dokumenta}}</a> 
						@endif
						{{ $racun->broj_dokumenta }}
					</h1>
			</div>
		</div>

		<form class="flat-box" method="POST" action="{{DokumentiOptions::base_url()}}dokumenti/racun-post"> 
			<div class="row">
				<div class="field-group columns medium-6">
					<h2>Informacije o kupcu</h2>
					<div> 
						<label for="">Kupac</label>
						<input type="hidden" name="partner_id" id="JSPartnerId" value="{{ (Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id) }}">
						<input type="text" name="naziv" id="JSPartnerNaziv" value="{{ (Input::old('naziv') ? Input::old('naziv') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'naziv')) }}" class="{{ $errors->first('naziv') ? 'error' : '' }}" autocomplete="off">
						<div id="JSPartnerSearchContent" class="rma-custom-select"></div>
					</div>
					<div> 
						<label for="">PIB</label>
						<input type="text" name="pib" id="JSPartnerPib" value="{{ trim(Input::old('pib') ? Input::old('pib') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'pib')) }}" class="{{ $errors->first('pib') ? 'error' : '' }}" autocomplete="off">
					</div>
					<div> 
						<label for="">Ulica i broj</label>
						<input type="text" name="adresa" id="JSPartnerAdresa" value="{{ (Input::old('adresa') ? Input::old('adresa') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'adresa')) }}" class="{{ $errors->first('adresa') ? 'error' : '' }}" autocomplete="off">
					</div>
					<div> 
						<label for="">Mesto</label>
						<input type="text" name="mesto" id="JSPartnerMesto" value="{{ (Input::old('mesto') ? Input::old('mesto') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'mesto')) }}" class="{{ $errors->first('mesto') ? 'error' : '' }}" autocomplete="off">
					</div>
				</div>
				<div class="field-group columns medium-6">
					<h2>Kontakt informacije</h2>
					<div> 
						<label for="">Kontakt Osoba</label>
						<input type="text" name="kontakt_osoba" id="JSPartnerKontaktOsoba" value="{{ (Input::old('kontakt_osoba') ? Input::old('kontakt_osoba') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'kontakt_osoba')) }}" class="{{ $errors->first('kontakt_osoba') ? 'error' : '' }}" autocomplete="off">
					</div>
					<div> 
						<label for="">Email</label>
						<input type="text" name="mail" id="JSPartnerMail" value="{{ (Input::old('mail') ? Input::old('mail') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'mail')) }}" class="{{ $errors->first('mail') ? 'error' : '' }}" autocomplete="off">
					</div>
					<div> 
						<label for="">Telefon</label>
						<input type="text" name="telefon" id="JSPartnerTelefon" value="{{ (Input::old('telefon') ? Input::old('telefon') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'telefon')) }}" class="{{ $errors->first('telefon') ? 'error' : '' }}" autocomplete="off">
					</div>
				</div>
			</div>

            <div class="row">
				<div class="field-group columns medium-2">
					<label for="">Datum izdavanja</label>
					<input name="datum_racuna" class="datum-val has-tooltip {{ $errors->first('datum_racuna') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_racuna') ? Input::old('datum_racuna') : $racun->datum_racuna }}" autocomplete="off">
				</div>
				<div class="field-group columns medium-2">
					<label for="">Rok isporuke</label>
					<input name="rok_isporuke" class="datum-val has-tooltip {{ $errors->first('rok_isporuke') ? 'error' : '' }}" type="text" value="{{ Input::old('rok_isporuke') ? Input::old('rok_isporuke') : $racun->rok_isporuke }}" autocomplete="off">
				</div>
				<div class="field-group columns medium-2">
					<label for="">Rok za plaćanje</label>
					<input name="rok_placanja" class="datum-val has-tooltip {{ $errors->first('rok_placanja') ? 'error' : '' }}" type="text" value="{{ Input::old('rok_placanja') ? Input::old('rok_placanja') : $racun->rok_placanja }}" autocomplete="off">
				</div>
				<div class="field-group columns medium-2">
					<label for="">Način plaćanja</label>
					<input name="nacin_placanja" class="datum-val has-tooltip {{ $errors->first('nacin_placanja') ? 'error' : '' }}" type="text" value="{{ Input::old('nacin_placanja') ? Input::old('nacin_placanja') : $racun->nacin_placanja }}" autocomplete="off">
				</div>
				<div class="field-group columns medium-2">
					<label for="">Status</label>
					<select name="dokumenti_status_id" id="JSRacunStatus">
						{{ Dokumenti::statusSelect((Input::old('dokumenti_status_id') ? Input::old('dokumenti_status_id') : $racun->dokumenti_status_id)) }}
					</select>
				</div>
            </div>
			<input type="hidden" name="racun_id" value="{{ $racun->racun_id }}">

	        @if($racun->racun_id > 0)
	        <div class="row">
	        	<h2>Stavke</h2>
	        	<div class="field-group columns medium-12">
	        		<table>
	        			<thead>
	        				<tr>
	        					<td>ID</td>
	        					<td>Naziv</td>
	        					<td>Kolicina</td>
	        					<td>Cena po jedinici</td>
	        					<td>Iznos PDV-a (%)</td>
	        					<td>Ukupno (din.)</td>
	        					<td></td>
	        				</tr>
	        			</thead>
	        			<tbody>
			            	@foreach($stavke as $stavka)
	        				<tr class="JSStavkaRow" data-roba_id="{{$stavka->roba_id}}" data-racun_stavka_id="{{$stavka->racun_stavka_id}}" data-racun_id="{{$racun->racun_id}}">
	        					<td><span class="JSRobaIdText">{{$stavka->roba_id}}</span></td>
	        					<td>
	        						@if(!is_null($stavka->roba_id))
									<select class="JSRobaId">
										{{ Dokumenti::artikliSelect($stavka->roba_id) }}
									</select>
									@else
									<input type="text" class="JSStavkaNaziv" value="{{$stavka->naziv_stavke}}">    
									@endif
	        					</td>
	        					<td>
	        						<input type="text" class="JSKolicina" value="{{ $stavka->kolicina }}">
	        					</td>
	        					<td>
	        						<input type="text" class="JSNsbCena" value="{{round($stavka->nab_cena,2)}}">
	        					</td>
	        					<td><input type="text" class="JSPdv" value="{{$stavka->pdv}}"></td>
	        					<td><input type="text" class="JSUkupno" value="{{ number_format(round((($stavka->nab_cena*$stavka->kolicina)*(1+$stavka->pdv/100)),2),2,'.','') }}" disabled></td>
	        					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/racun-stavka/{{$stavka->racun_stavka_id}}/delete">Ukloni</a></td>
	        				</tr>
							@endforeach
							@if($novaStavka)
	        				<tr class="JSStavkaRow" data-racun_stavka_id="0" data-racun_id="{{$racun->racun_id}}">
	        					<td><span class="JSRobaIdText">0</span></td>
	        					<td>
									<select class="JSRobaId">
										{{ Dokumenti::artikliSelect(0,$roba_ids) }}
									</select>
	        					</td>
	        					<td>
	        						<input type="text" class="JSKolicina" value="1">
	        					</td>
	        					<td>
	        						<input type="text" class="JSNsbCena" value="0">
	        					</td>
	        					<td><input type="text" class="JSPdv" value="20"></td>
	        					<td><input type="text" class="JSUkupno" value="0.00" disabled></td>
	        					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$racun->racun_id}}">Ukloni</a></td>
	        				</tr>							
							@endif
	        				<tr>
	        					<td></td>
	        					<td></td>
	        					<td></td>
	        					<td></td>
	        					<td></td>
	        					<td id="JSRacunUkupanIznos">{{ round($stavkeUkupanIznos,2) }}</td>
	        					<td></td>
	        				</tr>
							@if(!$novaStavka)
	        				<tr>
	        					<td></td>
	        					<td><a href="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$racun->racun_id}}?nova_stavka=1">Dodaj novu stavku</a></td>
	        					<td></td>
	        					<td></td>
	        					<td></td>
	        					<td></td>
	        					<td></td>
	        				</tr>
	        				@endif
	        			</tbody>
	        		</table>
				</div>      	
	        </div>
	        @endif
			<div class="row">
				<div class="columns medium-12 large-12 text-center">
					<div class="btn-container"> 
						<button type="submit" id="JSRacunSubmit" class="btn btn-primary save-it-btn" style="{{(count(Input::old()) > 0) ? 'background-color: red;':''}}">Sačuvaj</button>
					</div>
					<div class="btn-container"> 
						<a class="btn btn-primary save-it-btn JSbtn-delete" data-link="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$racun->racun_id}}/delete">Obriši</a>
					</div>
					<div class="btn-container"> 
						<a href="{{DokumentiOptions::base_url()}}dokumenti/racun-pdf/{{$racun->racun_id}}" class="btn btn-primary save-it-btn">PDF</a>
					</div>				
				</div>
			</div>
        </form>	
	</div>
</div>
@endsection