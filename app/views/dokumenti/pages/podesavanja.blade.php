@extends('dokumenti.templates.main')
@section('content')

<div id="main-content" class="orders-page row">
	<div class="columns medium-12 flat-box b2b-options">
		<form method="POST" action="{{ AdminB2BOptions::base_url() }}dokumenti/podesavanja-post" enctype="multipart/form-data" class="btn-container text-center">
			@if(!is_null(DokumentiOptions::user('saradnik')))
			<div class="row">
				<div class="columns medium-12">
					<h1>Logo</h1>
					<input type="file" name="logo" class="logo_inp">
					@if(is_null($podesavanja->logo))
					<h2>Postavite vaš logo</h2>
					@endif
				</div>
				<div class="columns medium-12">
					@if(!is_null($podesavanja->logo))
					<img src="{{ AdminB2BOptions::base_url() }}{{$podesavanja->logo}}" class="img_logo_settings">
					@endif
				</div>
			</div>
			@endif
			<div class="row"> 
				<h1>Šablon</h1>
				<textarea name="sablon" class="special-textareas">{{ $podesavanja->sablon }}</textarea>
			</div>
			<div class="row"> 
				<h1>Footer</h1>
				<textarea name="footer" class="special-textareas">{{ $podesavanja->footer }}</textarea>
			</div>
			<button type="submit" class="btn btn-secondary save-it-btn">Sačuvaj</button>
		</form>
	</div>

</div>
@endsection