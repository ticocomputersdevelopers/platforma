@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- REGISTRATION.blade -->

<h2><span class="page-title text-bold center-block text-center">{{ Language::trans('Registrujte se') }}</span></h2> 

<div class="login-title text-center">
	@if(Input::old('flag_vrsta_kupca') == 1)
		<span class="inline-block pointer private-user">{{ Language::trans('fizičko lice') }}</span>
		<span class="inline-block pointer active-user company-user">{{ Language::trans('pravno lice') }}</span>
	@else
		<span class="inline-block pointer active-user private-user">{{ Language::trans('fizičko lice') }}</span>
		<span class="inline-block pointer company-user">{{ Language::trans('pravno lice') }}</span>
	@endif
</div> 


<form action="{{ Options::base_url() }}registracija-post" method="post" class="registration-form margin-auto" autocomplete="off"> 

	<input type="hidden" name="flag_vrsta_kupca" value="0"> 

	<div>
		<label for="ime">{{ Language::trans('Ime') }}</label>
		<input id="ime" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
		</div>
	</div>

	<div>
		<label for="prezime">{{ Language::trans('Prezime') }}</label>
		<input id="prezime" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
	</div>

	<div>
		<label for="naziv">{{ Language::trans('Naziv firme') }}:</label>
		<input id="naziv" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
		</div>
	</div>

	<div>
		<label for="pib">{{ Language::trans('PIB') }}</label>
		<input id="pib" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
	</div>	

	<div>
		<label for="maticni_br">{{ Language::trans('Matični broj') }}</label>
		<input id="maticni_br" name="maticni_br" type="text" value="{{ Input::old('maticni_br') ? Input::old('maticni_br') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('maticni_br') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('maticni_br') : "" }}</div>
	</div>	  

	<div>
		<label for="email">{{ Language::trans('E-mail') }}</label>
		<input id="email" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
		</div>
	</div>	

	<div>
		<label for="lozinka">{{ Language::trans('Lozinka') }}</label>
		<input id="lozinka" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
		<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
	</div>

	<div>
		<label for="telefon">{{ Language::trans('Telefon') }}</label>
		<input id="telefon" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
		<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
	</div>

	<div>
		<label for="adresa">{{ Language::trans('Adresa') }}</label>
		<input id="adresa" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
	</div>

	<div>
		<label for="mesto"><span class="red-dot"></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }} </label>
		<input id="mesto" type="text" name="mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
		<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
	</div> 

	<div class="capcha text-center"> 
		{{ Captcha::img(5, 160, 50) }}<br>
		<span>{{ Language::trans('Unesite kod sa slike') }}</span>
		<input type="text" name="captcha-string" autocomplete="off">
		<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
	</div>

	<div class="text-center">   
		<button type="submit" class="button">{{ Language::trans('Registruj se') }}</button>
	</div> 
</form>


<script>
@if(Session::get('message'))
	$(document).ready(function(){     
 	
	bootboxDialog({ message: "<p>" + trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') + "!</p>", size: 'large', closeButton: true }, 6000); 
 
	});
@endif
</script>

<br>

<!-- REGISTRATION.blade -->

@endsection 