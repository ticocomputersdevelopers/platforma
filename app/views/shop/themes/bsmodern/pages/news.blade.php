@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<div class="row">
	@foreach(All::getNews() as $row) 

	<div class="col-md-12 col-sm-4 col-xs-12 ">

		<div class="news row">

			<div class="col-md-3 relative">

				<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>


				@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

				<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>

				@else

				<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

				@endif
			</div>


			<h2 class="news-title overflow-hdn col-md-9">{{ $row->naslov }}</h2>


			<div class="news-desc overflow-hdn col-md-9">{{ All::shortNewDesc($row->tekst) }}</div>

			<div class="news-meta col-md-6 text-left">

				<span><i class="far fa-clock">  01.01.2021</i></span>

			</div>

			<div class="text-right col-md-9">
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }}</a>
			</div> 

		</div>
	</div>

<!-- 	<div>
{{ All::getNews()->links() }}
</div>   -->

@endforeach
</div> 

@endsection