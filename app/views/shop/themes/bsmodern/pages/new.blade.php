@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<!-- NEW.blade -->

<div class="single-news row"> 

	<h2 class="news-title col-md-12 text-center">{{ $naslov }}</h2>

	<div class="news-meta col-md-12 text-right">
					<span><i class="fa fa-user"></i> {{ Language::trans('Postavio admin') }}</span>
					<span><i class="fa fa-comments"></i> {{ Language::trans('8 komentara') }}</span>
					<span><i class="far fa-clock"></i> {{ Language::trans('22. Maj') }}</span>
				</div>
 
    @if(in_array(Support::fileExtension($slika),array('jpg','png','jpeg','gif')))
	
		<img class="max-width col-md-12 news-image" src="{{ $slika }}" alt="{{ $naslov }}">
    
    @else
    
    	<iframe src="{{ $slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
    
    @endif
     

	

	{{ $sadrzaj }} 

  <!-- SHARE  -->

	<div class="share-content flex">
		<span>Podelite:</span>
		<div class="blog-icon ">	
			<a class="facebook-icon blog-share share-icons" href="https://www.facebook.com/sharer.php?u=https://www.selltico.com/sta-su-uslovi-koriscenja-sajta-i-zasto-su-neophodni-za-internet-prodavnice-" target="_blank" title="Facebook share" ><i class="fab fa-facebook-f"></i></a>

			<a class="twitter-icon blog-share share-icons" href="https://twitter.com/share?url=https://www.selltico.com/sta-su-uslovi-koriscenja-sajta-i-zasto-su-neophodni-za-internet-prodavnice-"  title="Twitter share"><i class="fab fa-twitter"></i></a>

			<a class="linkedin-icon blog-share share-icons" href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.selltico.com/sta-su-uslovi-koriscenja-sajta-i-zasto-su-neophodni-za-internet-prodavnice-"  title="Linkedin Share"><i class="fab fa-linkedin-in"></i></a>		
		</div>	
	</div>
  <!-- SHARE KRAJ -->

	<!-- POCETAK KOMENTARA -->
	
		

			<div class="col-xs-12 comment-box">

				<div class="col-xs-12 text-left padding-text"><h3>Komentari</h3></div>

				<div class="col-xs-12 text-left padding-text ">

					<div class="col-xs-12"><h4>Ime korisnika <span>01.01.2021</span></h4></div>
					<div class="col-xs-12 text-left">
						<p class="comment padding-text"><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</span></p>
					</div>


				</div>

			</div>


			<div class="col-xs-12">
				<div class="col-xs-12 text-left padding-text"><h3>Ostavite svoj komentar</h3></div>
				<div class="col-xs-12 text-left padding-text"><h4>Vaša email adresa neće biti objavljena. Obavezna polja su označena *</h4></div>
				<form class=" col-md-10 col-sm-10 col-xs-12">
					<div>
						<!-- <label for="name">Ime<em class="required">*</em></label> -->
						<input type="text" class="input-text" title="Name" value="" id="user" name="ime_osobe" placeholder="Ime">
					</div>


					<div>
						<!-- <label for="email">E-mail<em class="required">*</em></label> -->
						<input type="text" class="input-text validate-email" title="Email" value="test@gmail.com" id="email" name="email" placeholder="test@gmail.com" >
					</div>

					<div>
						<!-- <label for="website">Website<em class="required">*</em></label> -->
						<input type="text" class="input-text validate-website" title="web_site" value="" id="website" name="web_site" placeholder="Website">
					</div>

					<div>
						<!-- <label for="comment">Vaš komentar<em class="required">*</em></label> -->
						<textarea rows="5" cols="50" class="input-text" title="Comment" id="comment" name="komentar" placeholder="Komentar"></textarea>
					</div>

					<button type="submit" class="button">Postavi</button>
				</form>


			</div>
		
	
	<!-- KRAJ KOMENTARA -->

</div>  

<!-- NEW.blade END -->

@endsection