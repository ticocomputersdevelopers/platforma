@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<div class="table-responsive">
	<table class="konfigurator table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>{{ Language::trans('Grupa') }}</th> 
				<th></th>
				<th>{{ Language::trans('Slika') }}</th>
				<th>{{ Language::trans('Artikli') }}</th>
				<th></th>
				 
				<th>{{ Language::trans('Količina') }}</th>
				<th>{{ Language::trans('Cena') }}</th>
			</tr> 
		</thead> 
		<tbody> 
			@foreach($grupe as $row)
			<tr class="JSUsefulRow">
				<td class="konfigurator-grupa">{{ Language::trans($row->grupa) }}</td>
				<td @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="JSGrupaAdd" data-id="{{ $row->grupa_pr_id }}" @endif>{{ Language::trans('Dodaj') }}</td>
				<td>
					<span>
						<img class="JSSlika konf-img" src="">
					</span>
				</td>
				<td>	
					<select class="JSArtikal" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
						<option value="">{{ Language::trans('Odaberite') }}...</option>
						@foreach(All::getKonfiguratorArticles($row->grupa_pr_id) as $row2) 
						<?php $cena = Product::get_price($row2->roba_id); ?>
						<?php $slika = All::getSlika($row2->roba_id); ?>
						<option value="{{ $row2->roba_id }}" data-slika="{{ Options::domain().$slika }}" data-cena="{{ $cena }}">{{  $row2->naziv_web }} -> {{ $cena }}.din</option>
						@endforeach
					</select> 
					<span class="JSArtDetails">{{ Language::trans('Vidi') }}</span>
				</td> 
				<td>
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<span class="JSGrupaRemove fa fa-remove"></span>
					@endif
				</td>
				<td>
					<input type="text" class="JSKolicina" value="1" onkeypress="validate(event)" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
				</td>
				<td>
					<div class="JSCenaKonf">0.00</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row clearfix"> 
	<div class="pull-right col-md-5 col-sm-6 col-xs-12">
		<div class="ukupno-konf-div row">
			<p class="col-md-7 col-sm-7 col-xs-12">{{ Language::trans('Ukupno u konfiguratoru') }}</p>
			<p class="col-md-5 col-sm-5 col-xs-12" id="JSUkupnaCenaKonf">0.00</p>
		</div>
		<div class="ukupno-konf-div row">
			<p class="col-md-7 col-sm-7 col-xs-12">{{ Language::trans('Ukupno u korpi') }}</p>
			<p class="col-md-5 col-sm-5 col-xs-12" id="JSKorpaUkupnaCena">{{ $korpa_ukupno }}</p>
		</div>
		<div class="ukupno-konf-div row">		
			<p class="col-md-7 col-sm-7 col-xs-12">{{ Language::trans('Ukupno') }}</p>
			<p class="col-md-5 col-sm-5 col-xs-12" id="JSUkupno">{{ $korpa_ukupno }}</p>
		</div>
	</div> 
 </div>
 <div class="row text-right">
 	<button id="JSKonfAdd" class="dodaj-konf button">{{ Language::trans('Dodaj u korpu') }}</button>
 </div>
@endsection