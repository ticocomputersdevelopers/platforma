<!-- PRODUCT ON GRID -->
<div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding"> 
	<div class="shop-product-card relative"> 
		
		<!-- ACTION LABEL AND STICKER -->
		<div class="article_top_info">
			<div class="row">
				@if(All::provera_akcija($row->roba_id))
				<div class="col-xs-4 no-padding"> 
					<div class="sale-label inline-block">
						{{ Language::trans('Akcija') }}  <!-- {{ Product::getSale($row->roba_id) }}% --> 
					</div>
				</div>
				@endif

				<div class="{{ All::provera_akcija($row->roba_id) ? 'col-xs-8' : 'col-xs-12' }} no-padding">     
					<div class="product-sticker flex">
					    @if( Product::stiker_levo($row->roba_id) != null )
				            <a class="{{ All::provera_akcija($row->roba_id) ? 'hidden' : '' }} article-sticker-img">
				                <img class="img-responsive" src="{{ Options::domain() }}{{product::stiker_levo($row->roba_id) }}"  />
				            </a>
				        @endif 
				        
				        @if( Product::stiker_desno($row->roba_id) != null )
				            <a class="article-sticker-img clearfix">
				                <img class="img-responsive pull-right" src="{{ Options::domain() }}{{product::stiker_desno($row->roba_id) }}"  />
				            </a>
				        @endif 
					</div>
				</div>
			</div>
		</div>

		<div class="product-image-wrapper relative">

			<!-- ADMIN BUTTON -->
			@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
				<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
			@endif


			<a class="flex" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
				<img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />

				<span class="JSimg-placeholder" data-src="{{ Options::domain() }}{{ Product::web_slika_second($row->roba_id) }}" hidden></span>
			</a>
 

<!-- 			@if(All::provera_akcija($row->roba_id)) 
				@if(All::provera_datuma_i_tajmera($row->roba_id))
				<input type="hidden" class="JSrok_akcije" value="{{ All::action_deadline($row->roba_id) }}">
				<table class="table product-timer">
					<tbody>
						<tr>
							<td class="JSdays"></td>
							<td class="JShours"></td>
							<td class="JSminutes"></td>
							<td class="JSseconds"></td>
						</tr>
					</tbody> 
				</table>
				@endif 
			@endif -->

	

			<div class="add-to-cart-container text-center">  

				@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)

					@if(!Product::check_osobine($row->roba_id)) 

					<button class="buy-btn JSadd-to-cart" data-roba_id="{{$row->roba_id}}">{{ Language::trans('U korpu') }}</button>

					@else

					<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn">
						{{ Language::trans('Vidi artikal') }}
					</a>	 
					@endif

					@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
					<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
						<i class="fas fa-exchange-alt" aria-hidden="true"></i>
					</button>
					@endif		

					@else  	

					<button class="not-available">{{ Language::trans('Nije dostupno') }}</button>	               	 
					@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
					<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
						<i class="fas fa-exchange-alt" aria-hidden="true"></i>
					</button>
					@endif	

				@endif

				@else  
					<button class="buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>	

					@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
					<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
						<i class="fas fa-exchange-alt" aria-hidden="true"></i>
					</button>
					@endif	 

				@endif 	

				@if(Cart::kupac_id() > 0)
					<button class="like-it JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
				@else
			  		<button class="like-it JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
				@endif		
			</div>   
		</div>

		<div class="product-meta">  
				
			<h2 class="product-name">
				<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
					{{ Product::short_title($row->roba_id) }}  
				</a>
			</h2>
			<!-- <span class="review">{{ Product::getRating($row->roba_id) }}</span>  -->
		
			<div class="price-holder">
				@if(Product::getStatusArticlePrice($row->roba_id))
					<div> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
					@if(All::provera_akcija($row->roba_id))
					<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
					@endif     
				@endif 
			</div>			
		

			<!-- @if(AdminOptions::web_options(153)==1)
				<div class="generic_car">
					{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
				</div>
			@endif  -->

		</div>
	</div>
</div>



