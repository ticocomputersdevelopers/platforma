@if(Cart::broj_cart()>0)
 
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<div class="row mini-cart-item"> 

		<div class="col-xs-3"> 
			<img class="mini-cart-img img-responsive" src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
		</div>

		<div class="col-xs-9 no-padding"> 
 
			<a class="mini-cart-title inline-block line-h" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{Product::short_title($row->roba_id)}}
			</a> 

			<div class="text-bold clearfix"> 

				<div class="inline-block"> {{ Language::trans('Količina:') }} <span>{{round($row->kolicina)}}</span> </div>
				@if(AdminOptions::web_options(152)==1)
				<div class="inline-block pull-right">{{Cart::cena(ceil(($row->jm_cena)/10)*10 )}}</div>
				@else
				<div class="inline-block pull-right">{{Cart::cena($row->jm_cena)}}</div>
				@endif
			</div>

			<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="remove-cart-item JSdelete_cart_item" rel=”nofollow”><i class="fas fa-times"></i></a>

		</div>
	</div>
	@endforeach	


	<ul class="mini-cart-sum"> 
		@if(Options::checkTroskoskovi_isporuke() == 1 AND Cart::cart_ukupno() < Cart::cena_do()) 

			<li>{{ Language::trans('Cena artikala') }}: <i>{{Cart::cena(Cart::cart_ukupno())}}</i></li> 
			<li>{{ Language::trans('Troškovi isporuke') }}: <i>{{Cart::cena(Cart::cena_dostave())}}</i></li> 
			<li>{{ Language::trans('Ukupno') }}: <i>{{Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave())}}</i></li> 

		@else
			
	 		<li>{{ Language::trans('Ukupno') }}: <i>{{ Cart::cena(Cart::cart_ukupno()) }}</i></li> 

		@endif  
	</ul>

	<!-- <div class="text-right"> 
		<a class="inline-block go-to-cart-btn text-bold" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">{{Language::trans('Vidi korpu')}}</a>
	</div> -->


@endif
