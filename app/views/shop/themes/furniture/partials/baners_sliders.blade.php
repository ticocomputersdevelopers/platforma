    <!-- MAIN SLIDER -->  
    @if($firstSlider = Slider::getFirstSlider() AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
    <div id="JSmain-slider">
        @foreach($slajderStavke as $slajderStavka)
        <div class="relative"> 
            <a class="slider-link" href="{{ $slajderStavka->link }}"> </a>

            <div class="bg-img" style="background-image: url( '{{ Options::domain() }}{{ $slajderStavka->image_path }}' )"></div>

            <div class="sliderText"> 

                @if($slajderStavka->naslov != '')
                <div>
                    <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                        {{ $slajderStavka->naslov }}
                    </h2>
                </div>
                @endif

                @if($slajderStavka->sadrzaj != '')
                <div>
                    <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                        {{ $slajderStavka->sadrzaj }}
                    </div>
                </div>
                @endif

                @if($slajderStavka->naslov_dugme != '')
                <div>
                    <a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
                        {{ $slajderStavka->naslov_dugme }}
                    </a>
                </div>
                @endif
            </div>

        </div>
        @endforeach
    </div>
    @endif

    <!-- BANNERS -->
    @if($firstSlider = Slider::getFirstSlider('Baner') AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
    <div class="container">
        <div class="banners row">
            @foreach($slajderStavke as $slajderStavka)
            <div class="col-md-6 col-sm-6 col-xs-6 no-padding">
                <a href="{{ $slajderStavka->link }}" class="relative">
                    <img class="img-responsive" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
                </a>
            </div>
            @endforeach
        </div>  
    </div>
    @endif