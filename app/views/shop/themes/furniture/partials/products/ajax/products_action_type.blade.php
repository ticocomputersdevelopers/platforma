@foreach($articles as $row)
<div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
	<div class="shop-product-card relative"> 
	<!-- SALE PRICE -->
		@if(All::provera_akcija($row->roba_id))
 			<div class="label label-red sale-label"><span class="for-sale">{{ Language::trans('Akcija') }}</span><span class="for-sale-price">- {{ Product::getSale($row->roba_id) }} %</span></div>
 		@endif
 		 <div class="product-image-wrapper relative">
 	<!-- PRODUCT IMAGE -->
			<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
				<img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>
			<!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="article-details hidden-xs hidden-sm">
				<span class="fas fa-search-plus"></span> {{ Language::trans('Detaljnije') }}
			</a> -->
		</div>
		 
		<div class="product-meta text-center">
			
			<span class="review">
				{{ Product::getRating($row->roba_id) }}
			</span>
		
  	<!-- PRODUCT TITLE -->
  			<h2 class="product-name">
				<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{ Product::short_title($row->roba_id) }}</a>
			</h2>

			@if(AdminOptions::web_options(153)==1)
			<div class="generic_car">
				{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
			</div>
	 		@endif
			
  	<!-- PRODUCT PRICE -->
			<div class="price-holder">
				<div>{{ Cart::cena(Product::get_price($row->roba_id)) }}</div>
				@if(All::provera_akcija($row->roba_id))
				<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
				@endif	   
			</div> 
	 
	<!-- ADD TO CART BUTTON -->
	  		<div class="add-to-cart-container"> 
	  			@if(Cart::kupac_id() > 0)
	  			<div class="like-it"><button data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}" class="JSadd-to-wish far fa-heart wish-list"></button></div> 
	  			@else
	  			<div class="like-it JSnot_logged"><button data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}" class="far fa-heart wish-list"></button></div>
	  			@endif
				@if(Product::getStatusArticle($row->roba_id) == 1)
					@if(Cart::check_avaliable($row->roba_id) > 0)
						@if(!Product::check_osobine($row->roba_id))
						<div data-roba_id="{{$row->roba_id}}" class="JSadd-to-cart buy-btn button" title="{{ Language::trans('Dodaj u korpu') }}">
							 {{ Language::trans('U korpu') }}
						</div>
						@else
						<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn button" title="{{ Language::trans('Detalnije') }}">
							 {{ Language::trans('Vidi artikal') }}
						</a>
						@endif
					@else 
					<div class="not-available button" title="{{ Language::trans('Nije dostupno') }}">{{ Language::trans('Nije dostupno') }}</div>	
					@endif  
				@else
					<div class="not-available button">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</div>
				@endif
			</div>

	    </div> 
  	<!-- ADMIN BUTTON -->
	   @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
			<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
	   @endif
	</div>
</div>
@endforeach