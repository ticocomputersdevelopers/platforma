@if(Options::web_options(206) == 0)
<div class="row"> 
    @if(All::broj_akcije() > 0 AND Options::web_options(98) == 1)
        <div>
         
            <h2 class="section-heading"><span class="section-title JSInlineShort" data-target='{"action":"sale","id":"34"}'>{{Language::trans(Support::title_akcija())}}</span></h2>
           
            <div class="sale-list relative JSproduct-list row slider JSslide-articles">
                <div class="loader-wrapper wide js-tip-loader min-height js-loader-akcija">
                    <div class="loader"></div>
                </div>

                <button class="previous btn-slider slider-prev" id="JSakcija-previous" data-id="akcija" data-offset="1">
                   <i class="fas fa-chevron-left"></i>
                </button>

                <button class="next btn-slider slider-next" id="JSakcija-next" data-id="akcija" data-offset="1">
                    <i class="fas fa-chevron-right"></i>
                </button>

                <div class="ajax-content" id="JSakcija-content" data-tipid="akcija">
                    <!-- Product ajax content -->
                </div>
            </div>
        </div>
    @endif
    
    <?php $tipovi = DB::table('tip_artikla')->where('tip_artikla_id','<>',-1)->where('active',1)->where('prikaz','!=',1)->where('prikaz','!=',3)->orderBy('rbr','asc')->get(); ?>

    @if(count($tipovi) > 0) 
    @foreach($tipovi as $tip)
        @if(All::provera_tipa($tip->tip_artikla_id)) 
            
            <h2 class="section-heading"><span class="section-title JSInlineShort" data-target='{"action":"type","id":"{{$tip->tip_artikla_id}}"}'>{{ Language::trans($tip->naziv) }}</span></h2>       
       
            <div class="tip-list relative JSproduct-list row slider JSslide-articles">
                <div class="loader-wrapper wide js-tip-loader min-height js-loader-{{ $tip->tip_artikla_id }}">
                    <div class="loader"></div>
                </div>

                <button class="previous btn-slider slider-prev" id="JS{{ $tip->tip_artikla_id.'-' }}previous" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                    <i class="fas fa-chevron-left"></i>
                </button>
                
                <button class="next btn-slider slider-next" id="JS{{ $tip->tip_artikla_id.'-' }}next" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                   <i class="fas fa-chevron-right"></i>
                </button>

                <div class="ajax-content" id="JS{{ $tip->tip_artikla_id.'-' }}content" data-tipid="{{ $tip->tip_artikla_id }}">
                    <!-- Product ajax content -->
                </div>
            </div>
        @endif
    @endforeach
    @endif
</div>

<!-- SLICK -->
@else
    @if(All::broj_akcije() > 0 AND Options::web_options(98) == 1) 
       
        <h2><span class="section-title JSInlineShort" data-target='{"action":"sale","id":"34"}'>{{Language::trans(Support::title_akcija())}}</span></h2>
   
        <div class="JSproducts_slick"> 
            @foreach(Articles::akcija(null,20) as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </div> 
    @endif

    <?php $tipovi = DB::table('tip_artikla')->where('tip_artikla_id','<>',-1)->where('prikaz','!=',1)->where('active',1)->orderBy('rbr','asc')->get(); ?>
    @if(count($tipovi) > 0) 
    @foreach($tipovi as $tip)
        @if(All::provera_tipa($tip->tip_artikla_id))  
          
            <h2><span class="section-title JSInlineShort" data-target='{"action":"type","id":"{{$tip->tip_artikla_id}}"}'>{{ Language::trans($tip->naziv) }}</span></h2>
          
            <div class="JSproducts_slick"> 
                @foreach(Articles::artikli_tip($tip->tip_artikla_id,20) as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>      
        @endif
    @endforeach
    @endif
@endif