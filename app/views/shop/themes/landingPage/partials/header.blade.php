@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
            <span>{{ Language::trans('Prijavljeni ste kao administrator') }}</span> |

            <a target="_blank" href="{{ Options::domain() }}admin">{{ Language::trans('Admin Panel') }}</a> |
            
            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
            <a href="#!" id="JSShortAdminSave">{{ Language::trans('Sačuvaj izmene') }}</a> |
            @endif
 
            <a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a>
        </div>
    </div>
</div>  
@endif

<div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }} header" {{ Options::web_options(321, 'str_data') != '' ? 'style=background-color:' . Options::web_options(321, 'str_data') : '' }}>
    <div class="row flex">

        <div class="col-md-3 col-sm-3 col-xs-6"> 
            
            <h1 class="seo">{{ Options::company_name() }}</h1>

            <a class="logo inline-block v-align" href="/" title="{{Options::company_name()}}">
                <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
            </a>

        </div>


        <div class="col-md-9 col-sm-9 col-xs-6"> 
            <div class="menu"> 

                <div class="close-nav hidden-md hidden-lg"><i class="fas fa-times"></i></div>

                <ul> 
                    @foreach(All::header_menu_pages() as $row)
                    <li>
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                        <ul class="drop-2">
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach 
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach

                    @if(Options::checkB2B())
                    <li>
                        <a href="{{Options::domain()}}b2b/login" class="inline-block">B2B</a> 
                    </li>
                    @endif 
                </ul> 
            </div>

            <div class="menu-toggle text-right hidden-md hidden-lg"><i class="fas fa-bars"></i></div>

        </div>
    </div>
</div>


