
<div class="section-contact"> 

	<h2 class="text-white"><a class="section-title text-center center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('kontakt') }}">{{ Language::trans('Kontaktirajte nas') }}</a>

	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">  
			<div class="form-group">
				<input id="JScontact_name" class="JSvalidate" name="name" type="text" placeholder="{{ Language::trans('Vaše ime') }}*" />
			</div>

			<div class="form-group">
				<input id="JScontact_email" class="JSvalidate" name="email" type="text" 
				placeholder="{{ Language::trans('Vaša e-mail adresa') }}*">
			</div>		

			<div class="form-group">	
				<textarea rows="5" id="message" placeholder="{{ Language::trans('Vaša poruka') }}*"></textarea>
			</div>

			<div class="text-right"> 
				<button class="button" onclick="meil_send()">{{ Language::trans('Pošalji') }}</button>
			</div> 
		</div>
	</div>

</div> 



