@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
@include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection 

@section('page')

<div class="container">  

	<!-- <div class="row text-center below-slide">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="" class="img-responsive" alt="image">
			<h3>Schedule Appointments</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="" class="img-responsive" alt="image">
			<h3>Schedule Appointments</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="" class="img-responsive" alt="image">
			<h3>Schedule Appointments</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		</div>
	</div> -->

	<div class="main-desc text-center">
		{{ Support::front_admin_content(5) }}
	</div>
 
	@include('shop/themes/'.Support::theme_path().'partials/section-news')
</div>
 

<div class="home-contact"> 
	<div class="container"> 
		@include('shop/themes/'.Support::theme_path().'partials/section-contact')
	</div>
</div>


<!-- BANERS -->
@if($firstSlider = Slider::getFirstSlider('Baner') AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
<div class="container bw">  
	<div class="row"> 
		@foreach($slajderStavke as $slajderStavka)
		<div class="col-md-6 col-sm-6 col-xs-12"> 
			<div class="banners relative"> 

				<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

				<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">

					<div class="banner-desc text-center">

						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
						@endif
					</div> 
				</div>
			</div> 
		</div>
		@endforeach
	</div> 
</div>
@endif

<!-- NEWSLATTER -->
@if(Options::newsletter()==1)
<div class="newsletter text-white"> 
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h5 class="">{{ Language::trans('Prijavite se za naš newsletter') }}</h5> 
				<p>{{ Language::trans('Za najnovije informacije o našem sajtu prijavite se na našu e-mail listu') }}.</p>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="relative">              
					<input type="text" placeholder="{{Language::trans('Unesite Vašu e-mail adresu')}}" id="newsletter" /> 
					<button onclick="newsletter()" class="text-uppercase">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>
		</div> 
	</div>
</div>
@endif

@endsection