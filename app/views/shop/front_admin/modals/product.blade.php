<div class="centered-modal"> 
<div class="modal fade" id="FAProductModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-scroller">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-heading">
                    <h4 class="modal-title text-center">{{ Language::trans('Artikal') }}</h4>
                </div>
            </div>
            <hr>
            <div class="modal-body">
                <div id="JSFAProductContent"></div>
            </div>
        </div>   
    </div>
</div>
</div>

