
@foreach($products as $product)

<li class="JSFAProductOnGrid row flexed" data-roba_id="{{$product->roba_id}}">
	<div class="col-md-2 col-sm-2 col-xs-4 text-center"> 
		<input type="checkbox" class="JSRobaSelected" data-roba_id="{{$product->roba_id}}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
		<img class="JSFAProductModalCall" data-roba_id="{{$product->roba_id}}" width="65" height="65" src="{{ Options::domain() }}{{ Product::web_slika($product->roba_id) }}" alt="{{ Product::seo_title($product->roba_id) }}" />
	</div>

	<div class="col-md-1 col-sm-1 col-xs-3"> 
		<span class="JSFAflag_prikazi_u_cenovniku">{{$product->flag_prikazi_u_cenovniku==1 ? Language::trans('DA') : Language::trans('NE')}}</span>
	</div>

	<div class="col-md-2 col-sm-2 col-xs-5 text-center">
		<span class="JSFAkolicina">{{ round(Cart::kolicina_lager($product->roba_id)) }}</span>
		<span>{{Product::jedinica_mere($product->roba_id)->naziv}}</span>
	</div>

	<div class="col-md-7 col-sm-6 col-xs-12"> 
		<h3 class="JSFAProductModalCall" data-roba_id="{{$product->roba_id}}"><span class="JSFANaziv">{{ Product::seo_title($product->roba_id) }}</span></h3>
	</div>

	<!-- <a href="{{ Options::domain() }}admin/product/{{ $product->roba_id }}" target="_blank">
		{{ Language::trans('Detaljnije') }}
	</a> -->
</li>
@endforeach      