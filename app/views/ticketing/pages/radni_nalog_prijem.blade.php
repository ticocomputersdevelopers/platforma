@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<section id="main-content">
		<div class="row">
	        <div class="medium-6 medium-centered columns">
				<form class="flat-box front-page-servis" method="POST" action="{{RmaOptions::base_url()}}ticketing/radni-nalog-prijem-post" style="padding: 15px;"> 
	        	<h1 class="center ticket-title"> {{ 'Novi tiket' }} </h1>
	        	<div class="text-right"><a href="{{TicketingOptions::base_url()}}ticketing/reklamacije">{{ Language::trans('Svi tiketi') }}</a></div>
					<div class="field-group"> 
						<label for="">Uređaj</label>
						<div>
							<input name="uredjaj" class="{{ $errors->first('uredjaj') ? 'error' : '' }}" type="text" value="{{ Input::old('uredjaj') ? Input::old('uredjaj') : $radni_nalog->uredjaj }}" autocomplete="off">
						</div>
					<!-- 	<select name="roba_id" id="JSRoba" class="{{ $errors->first('roba_id') ? 'error' : '' }}">
							{{ RMA::uredjajiSelect(array(Input::old('roba_id') ? Input::old('roba_id') : $radni_nalog->roba_id),false) }}
						</select> -->
					</div>
					<div class="field-group">
						<label for="">Opis kvara</label>
						<textarea name="opis_kvara" rows="2" class="{{ $errors->first('opis_kvara') ? 'error' : '' }}">{{ Input::old('opis_kvara') ? Input::old('opis_kvara') : $radni_nalog->opis_kvara }}</textarea>
					</div>

					@if($user = TicketingOptions::user())
					<div class="field-group"> 
						<label for="">Klijent</label>
						<div>
							<input type="text" value="{{  $user->flag_vrsta_kupca == 0 ? $user->ime.' '.$user->prezime : $user->naziv.' '.$user->pib }}" disabled>
						</div>
					</div>
					@endif

					<div class="field-group">
						<label for="">Grupa</label>
						<select name="roba" class="{{ $errors->first('roba') ? 'error' : '' }}">
							<option value="1">Roba</option>
							<option value="0" {{ (Input::old('roba')=='0') ? 'selected' : '' }}>Usluga</option>
						</select>
					</div> 
					<div class="field-group"> 
						<label for="">Serijski broj</label>
						<div>
							<input name="serijski_broj" class="{{ $errors->first('serijski_broj') ? 'error' : '' }}" type="text" value="{{ Input::old('serijski_broj') ? Input::old('serijski_broj') : $radni_nalog->serijski_broj }}" autocomplete="off">
						</div>
					</div>
					<div class="field-group"> 
						<label for="">Broj računa</label>
						<div>
							<input name="broj_fiskalnog_racuna" class="{{ $errors->first('broj_fiskalnog_racuna') ? 'error' : '' }}" type="text" value="{{ Input::old('broj_fiskalnog_racuna') ? Input::old('broj_fiskalnog_racuna') : $radni_nalog->broj_fiskalnog_racuna }}" autocomplete="off">
						</div>
					</div>
					<div class="field-group"> 
						<label for="">Proizvođač</label>
						<div>
							<input name="proizvodjac" class="{{ $errors->first('proizvodjac') ? 'error' : '' }}" type="text" value="{{ Input::old('proizvodjac') ? Input::old('proizvodjac') : $radni_nalog->proizvodjac }}" autocomplete="off">
						</div>
					</div>
					<div class="field-group ">
						<label for="">Hitno</label>
						<select name="hitno" class="{{ $errors->first('hitno') ? 'error' : '' }}">
							<option value="0">Ne</option>
							<option value="1" {{ (Input::old('hitno')==1) ? 'selected' : '' }}>Da</option>
						</select>
					</div> 

					<div class="field-group ">
						<label for="">Napomena</label>
						<textarea name="napomena" rows="2" class="{{ $errors->first('napomena') ? 'error' : '' }}">{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $radni_nalog->napomena }}</textarea>
					</div>

					<input type="hidden" name="check_serijski_broj" value="{{ !is_null(Input::old('check_serijski_broj')) ? Input::old('check_serijski_broj') : 1 }}">
					<input type="hidden" name="radni_nalog_id" value="{{ $radni_nalog->radni_nalog_id }}">

					<div class="btn-container"> 
						<button type="submit" class="btn btn-primary save-it-btn left">Pošalji</button>
					</div>
					
					@if(Session::get('message'))
					<div class="ticket-title text-right" style="padding: 4px;">{{ Language::trans('Vaši podaci su uspešno poslati') }}!</div>
					@endif
				</form>
			</div>	
		</div>
</section>
@endsection