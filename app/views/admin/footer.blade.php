<?php $input_futer_sekcija_tip_id = (!is_null(Input::old('futer_sekcija_tip_id')) ? intval(Input::old('futer_sekcija_tip_id')) : $item->futer_sekcija_tip_id); ?>
<section id="main-content">

	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	<section class="medium-3 columns">
		<div class="flat-box"> 

			<label class="text-center">{{ AdminLanguage::transAdmin('Sekcije') }}</label>

			<ul @if(Admin_model::check_admin(array('FUTER_AZURIRANJE'))) class="banner-list JSListFooterSections" @endif data-table="2">
				@if(Admin_model::check_admin(array('FUTER_AZURIRANJE')))
				<li id="0" class="new-banner new-elem relative">
					<a href="{{AdminOptions::base_url()}}admin/futer/0/1">{{ AdminLanguage::transAdmin('Nova sekcija') }}</a>
					<span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span>
				</li>
				@endif
				@foreach($sections as $section)
				<li class="ui-state-default relative @if($section->futer_sekcija_id == $id) active @endif" id="{{$section->futer_sekcija_id}}">
					<a href="{{AdminOptions::base_url()}}admin/futer/{{$section->futer_sekcija_id}}/1">{{AdminSupport::footer_section_name($section->futer_sekcija_id)}}</a>
					@if(Admin_model::check_admin(array('FUTER_AZURIRANJE')))
					<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/futer-delete/{{$section->futer_sekcija_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>
		</div>   
	</section>  
	
	<!-- BANNER EDIT -->
	<section class="medium-9 columns">
		<div class="flat-box"> 
			<form action="{{AdminOptions::base_url()}}admin/futer-edit" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
				<label class="text-center">{{ AdminLanguage::transAdmin('Sekcija') }}</label>

				@if(Admin_model::check_admin(array('FUTER_AZURIRANJE')))
				@if($id != 0 && count($jezici) > 1)
				<div class="row">
					<div class="columns"> 
						<div class="languages">
							<ul>	
								@foreach($jezici as $jezik)
								<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/futer/{{ $id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif 
				@endif

				<section class="row">
					<div class="medium-4 columns field-group">
						<label>{{ AdminLanguage::transAdmin('Naslov') }}</label>
						<input id="naslov" value="{{ Input::old('naslov') ? Input::old('naslov') : trim($lang_data->naslov) }}" type="text" name="naslov" {{ Admin_model::check_admin(array('FUTER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('naslov') ? $errors->first('naslov') : '' }}</div>
					</div>

					<div class="medium-4 columns field-group">
						<label>{{ AdminLanguage::transAdmin('Tip') }}</label>
						<select name="futer_sekcija_tip_id" id="JSFuterSekcijaTip" {{ Admin_model::check_admin(array('FUTER_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@foreach(DB::table('futer_sekcija_tip')->where('aktivan',1)->get() as $footer_section_type)
							<option value="{{$footer_section_type->futer_sekcija_tip_id}}" 
								{{ ($footer_section_type->futer_sekcija_tip_id == intval(Input::old('futer_sekcija_tip_id')) ? 
								'selected' : 
								((is_null(Input::old('futer_sekcija_tip_id')) AND 
								$item->futer_sekcija_tip_id == $footer_section_type->futer_sekcija_tip_id) ? 'selected' : '')) }}
								>{{ $footer_section_type->naziv }}</option>
								@endforeach
							</select>
						</div>

						<div class="medium-2 columns field-group">
							<label>{{ AdminLanguage::transAdmin('Aktivna sekcija') }}</label>
							<select name="aktivan" {{ Admin_model::check_admin(array('FUTER_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" {{ (Input::old('aktivan') == '0') ? 'selected' : (($item->aktivan == 0) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('NE') }}</option>
							</select>
						</div>

						<div id="JSFooterSectionContent" class="medium-12 columns" {{ in_array($input_futer_sekcija_tip_id,array(1,2)) ? '' : 'hidden'}}>
							<label>{{ AdminLanguage::transAdmin('Sadržaj') }}</label>
							<textarea @if(Admin_model::check_admin(array('FUTER_AZURIRANJE'))) class="special-textareas" @endif name="sadrzaj" {{ Admin_model::check_admin(array('FUTER_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('sadrzaj') ? Input::old('sadrzaj') : trim($lang_data->sadrzaj) }}</textarea>
						</div>
					</section>

					<section id="JSFooterSectionLinks" class="banner-edit-top row" {{ ($input_futer_sekcija_tip_id == 3) ? '' : 'hidden' }}>
						<div class="medium-4 columns">
							<select name="web_b2c_seo_id" {{ Admin_model::check_admin(array('FUTER_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="0">{{ AdminLanguage::transAdmin('Izaberi') }}</option>
								@foreach($pages as $page)
								<option value="{{ $page->web_b2c_seo_id }}">{{ $page->title }}</option>
								@endforeach
							</select>
						</div>

						<div class="medium-4 columns footer-page">
							<ul>
								@foreach($linked_pages as $linked_page)
								<li data-id="{{ $linked_page->web_b2c_seo_id }}">{{ $linked_page->title }}
									@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
									<a class="right tooltipz" aria-label="Obriši" href="{{AdminOptions::base_url()}}admin/futer-page-delete/{{$item->futer_sekcija_id}}/{{$linked_page->web_b2c_seo_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i>
									</a>
									@endif
								</li>
								@endforeach
							</ul>
						</div>
					</section>


					<section id="JSFooterSectionImage" class="row" {{ ($input_futer_sekcija_tip_id == 1) ? '' : 'hidden' }}>

						<div class="medium-12 columns">
							<img class="banner-preview" src="{{AdminOptions::base_url()}}{{$item->slika}}" alt="{{$lang_data->naslov}}" />
						</div>

					</section>

					<br>

					<input type="hidden" name="futer_sekcija_id" value="{{$item->futer_sekcija_id}}" />
					<input type="hidden" name="jezik_id" value="{{$jezik_id}}" />

					@if(Admin_model::check_admin(array('FUTER_AZURIRANJE')))
					<div class="btn-container center">
						<button class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
					@endif

				</form>
			</div>
		</section>
	</section>

