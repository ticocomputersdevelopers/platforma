<section class="nivoi-sec" id="main-content">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/permissions-tabs')
	<div class="row grupa-modula-cont">
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">
				<h1 class="title-med">{{ AdminLanguage::transAdmin('Nivoi pristupa') }}</h1>
				<ul class="name-ul">
					<a href="{{ AdminOptions::base_url() }}admin/grupa-modula" class="admin-users center">
						<li class="name-ul__li"{{$ac_group_id == null ? ' style="background-color:#ddd"' : ''}}>
							<div class="name-ul__li__name">Dodaj novi</div>
						</li>
					</a>
					 
					@foreach($grupe_modula as $row)
					<a href="{{ AdminOptions::base_url() }}admin/grupa-modula/{{ $row->ac_group_id }}" class="admin-users">
						<li class="name-ul__li"{{$row->ac_group_id == $ac_group_id && $ac_group_id != null ? ' style="background-color:#ddd"' : ''}}>
							<div class="name-ul__li__name">{{ $row->naziv }}</div>
						</li>
				    </a>
					@endforeach
				</ul>				
			</div>
		</section>
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">
				<h1 class="title-med">{{ $title }}</h1>

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/modul-grupa-edit" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="ac_group_id" value="{{ $ac_group_id }}"> 
						<div class="columns medium-12 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="">{{ AdminLanguage::transAdmin('Naziv grupe modula') }}</label>
							<input type="text" name="naziv" data-id="" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}"> 
						</div>
						<div class="columns medium-12 field-group">
							<label for="">{{ AdminLanguage::transAdmin('Opis') }}</label>
							<textarea name="opis">{{ Input::old('opis') ? Input::old('opis') : $opis }}</textarea>
						</div>
						<div class="btn-container center column medium-12">
							<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							@if(!in_array($ac_group_id,array(null,0,32)))
							<a class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/modul-grupa-delete/{{ $ac_group_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</a>
							@endif
						</div>
					</div>
				</form>
				 
			</div>
		</section>
		@if($ac_group_id != null)
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">
				<h4 class="title-med">{{ AdminLanguage::transAdmin('Glavni moduli') }}
				</h4>
				@if(count(AdminSupport::aktivniAdminModuli()) > 0)
					<label> Uključi sve <input type="checkbox" data-general-module = "{{$ac_group_id}}" class="JSGlavniUkljuci"> Isključi sve <input type="checkbox" data-general-module= "{{$ac_group_id}}" class="JSGlavniIskljuci"></label>
				@endif
				<ul class="name-ul">
					
					@foreach( AdminSupport::aktivniAdminModuli() as $row)
						<li class="name-ul__li"{{$row->ac_module_id == $glavni_modul_id ? ' style="background-color:#ddd"' : ''}}>
							<div class="name-ul__li__name">{{ $row->opis }}</div>
							
							@if(count(AdminSupport::aktivniAdminModuli($row->ac_module_id)) > 0)
								<a href="{{ AdminOptions::base_url() }}admin/grupa-modula/{{ $ac_group_id }}/{{ $row->ac_module_id }}" class="name-ul__li__right-arrow button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sporedni moduli') }}">
								<i class="fa fa-arrow-right" aria-hidden="true"></i>
								</a>
							@endif

							<input type="checkbox" class="JSGlavni name-ul__li__checkbox button-option nivoi-chk" data-general-module='{"group":"{{$ac_group_id}}", "module_id":"{{$row->ac_module_id}}", "tip":"0"}' @if(in_array($row->ac_module_id,$moduli)) checked @endif>

						</li>
					@endforeach
				</ul>
 			</div> <!-- end of .flat-box -->
 			
		</section>

		@if(isset($glavni_modul_id))
		<section class="small-12 medium-12 large-3 columns JSSporednaKolona">
			<div class="flat-box">
				<h1 class="title-med">{{ AdminLanguage::transAdmin('Sporedni moduli') }}</h1>
				<ul class="name-ul">
					
					@foreach(AdminSupport::aktivniAdminModuli($glavni_modul_id) as $row)
					<li class="name-ul__li">
						<div class="name-ul__li__name">{{ $row->opis }}</div>
						
						<input type="checkbox" class="JSSporedni name-ul__li__checkbox button-option" data-general-module='{"group":"{{$ac_group_id}}", "module_id":"{{$row->ac_module_id}}", "tip":"1"}' @if(in_array($row->ac_module_id,$moduli)) checked @endif>
					</li>
					@endforeach
				</ul>
 			</div> <!-- end of .flat-box -->
		</section>
		@endif
		@endif


	
	</div>
  <!-- </form> -->
</section>