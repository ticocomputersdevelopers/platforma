@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<div id="main-content" class="images-edit">
	@include('admin/partials/product-tabs')

	@if($parent_id > 0)
	<div class="row"> 
		<div class="large-2 medium-3 small-12 columns">
			<a class="m-subnav__link JS-nazad" href="{{AdminOptions::base_url()}}admin/product_slike/{{ $roba_id }}">
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					{{ AdminLanguage::transAdmin('Nazad') }}
				</div>
			</a>
		</div>
	</div><br />
	<center>
		<div class="row firma-slike-row"> 
		    <img src="{{AdminOptions::base_url().$web_slika_parent->putanja}}" width="300">
		</div>
	</center>
    @endif

	<div class="row firma-slike-row"> 
		<div id="JSUploadImages" class="big-image medium-8 columns medium-centered no-padd"></div>
    </div>
	<div class="row slike-form-row">
	 	<form class="column medium-8 medium-centered no-padd" method="POST" action="{{AdminOptions::base_url()}}admin/slika-edit" enctype="multipart/form-data">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}"> 
			<input type="hidden" name="parent_id" value="{{ $parent_id }}"> 
			<div class="btn-container row">
				@if($parent_id == 0)
			    <h2 class="title-big center">{{ AdminLanguage::transAdmin('Slike') }}</h2>
			    @else
			    <h2 class="title-big center">{{ AdminLanguage::transAdmin('Dodatne slike') }}</h2>
			    @endif
				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
					<div class="columns medium-5 no-padd"> 
						<p>{{ AdminLanguage::transAdmin('Dodaj nove slike') }}</p>
							<input type="file" name="slika[]" multiple> 
					</div>
					<div class="columns medium-7"> 
						<input type="submit" class="btn btn-primary save-it-btn" value="{{ AdminLanguage::transAdmin('SAČUVAJ SLIKE') }}">
					</div>
				@endif
				</div>
				<div class="error">{{ Session::has('error') ? Session::get('error') : '' }}</div>
		</form>
		<div class="column medium-2 medium-offset-2 no-padd margin-bottom-buttons">
			<button class="btn btn-primary" id="JSGallery">{{ AdminLanguage::transAdmin('Galerija') }}</button>
		</div>
	</div> 

	<div class="row firma-slike-row">
		@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
			<div class="column medium-8 small-12 medium-centered no-padd">
				<div class="row imges-heading"> 
					<div class="columns medium-6"> 
						<input type="checkbox" id="JSSlikaSelected"> 
						<label>{{ AdminLanguage::transAdmin('Označi sve') }}</label>
					</div>
					<div class="columns medium-6 text-right"> 
						<label>{{ AdminLanguage::transAdmin('Izaberi akciju') }}</label>
						<select id="JSSlikaExecute" class="inline-block">
							<option class="JSSlikaExecuteOption" value=""></option>
							<option class="JSSlikaExecuteOption" value="active">{{ AdminLanguage::transAdmin('Prikaži u prodavnici') }}</option>
							<option class="JSSlikaExecuteOption" value="unactive">{{ AdminLanguage::transAdmin('Skloni iz prodavnice') }}</option>
							<option class="JSSlikaExecuteOption" value="delete">{{ AdminLanguage::transAdmin('Obriši') }}</option>
						</select>
					</div>
				</div>
			</div>
		@endif
		<div class="no-float"> 
		@if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)   
			<div class="column medium-8 small-12 col-slika medium-centered"> 
				<div class="radio-button-field row">  
					<div class="columns medium-4">
						<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
					</div>
				</div> 
			</div> 
			@if(Product::pitchPrintImageExist($roba_id) == TRUE) 
			<div class="column medium-8 small-12 col-slika medium-centered"> 
				<div class="radio-button-field row">  
					<div class="columns medium-4">
						<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">
					</div>
				</div> 
			</div>                        
			@endif
        @else
		@foreach($slike as $row)
		<div class="column medium-8 small-12 col-slika medium-centered {{ $row->flag_prikazi == 1 ? 'active' : '' }}">
			<div class="radio-button-field row">  
				<div class="columns medium-4">
					<input type="checkbox" class="JSSlikaSelected img-input" data-id="{{ $row->web_slika_id }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
					<img src="{{ AdminOptions::base_url().$row->putanja }}">
				</div> 

				<div class="columns medium-5">
					<textarea class="JSSlikaAlt" data-id="{{ $row->web_slika_id }}" placeholder="{{ AdminLanguage::transAdmin('Naziv slike (alt tag)') }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{$row->alt}}</textarea>
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<button class="JSSlikaAltSave invisible-btn" data-id="{{ $row->web_slika_id }}" hidden="hidden">{{ AdminLanguage::transAdmin('SAČUVAJ') }}</button> 
					@endif 
				</div>

				@if($parent_id == 0) 
				    <div class="columns medium-3 text-right"> 
				    	<div> 
							<input type="radio" class="JSAkcija" data-robaid="{{ $roba_id }}" data-id="{{ $row->web_slika_id }}" @if($row->akcija) checked @endif {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<label class="no-margin">{{ AdminLanguage::transAdmin('Glavna slika') }} </label> 
						</div>
						<br>
						@if(AdminOptions::web_options(310) == 1)
						<div>  
							<a class="btn btn-secondary btn-small" href="{{AdminOptions::base_url()}}admin/product_slike/{{ $roba_id }}/{{ $row->web_slika_id }}">{{ AdminLanguage::transAdmin('Dodatne slike') }}</a>
						</div>
						@endif
					</div>  
				@endif
				<div class="columns medium-12 addon-article-images"> 
					@if(AdminOptions::web_options(310) == 1) 
						@foreach(AdminArticles::getDodatneSlike($row->web_slika_id) as $dodatna_slika)
							<div class="inline-block">
								<img src=" {{ AdminOptions::base_url().$dodatna_slika->putanja }} " alt="image">
							</div>
						@endforeach
					@endif 
				</div>
			</div>
		</div>
		@endforeach 
		@endif
		</div>
	</div>
