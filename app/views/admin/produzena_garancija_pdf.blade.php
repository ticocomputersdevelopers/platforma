
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php /* header('Content-Type: text/html; charset=utf-8;'); */?> 
<title>PDF</title>
<style>

.row::after {content: ""; clear: both; display: table;}

[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

* { 
	font-family: 'DejaVu Sans'; 
	box-sizing: border-box;   
} /*DejaVu Sans; */

body {font-size: 14px;}   
h3, h2 {
	font-weight: normal;
	margin: 15px 0;  
}
.container {   
	width: 100%;
    max-width: 800px;
    margin: 0 auto;
    padding: 0 10px;
}  
/*.form-like > div {  
    border-bottom: 1px solid #ccc;
    margin: 6px 0;
    padding: 3px 0;
}
 .form-like > div span{ font-weight: 600; }*/
.text-right {text-align: right;} 
.text-center {text-align: center;} 
.inline-block { display: inline-block; } 

p{
	margin: 0;
	font-size: 13px; 
} 
header{
	background-color: #0F2147;
	padding: 0 15px;
	font-size: 18px;
	color: #fff;
}
.centered{
	max-width: 90%;
	margin: 0 auto;
}
table{
	width: 100%;
	border-collapse: collapse;
}
table tr:nth-child(even){
	background-color: #f5f6ff;
}
table tr td{
	padding: 2px 10px;
	font-size: 13px;
}
</style>
</head>
<body>
	 
<!-- 	<div class="container"> 
		<div>
			<h3 class="text-center">OBRAZAC PRIJAVE</h3>
			<br>
			<p>Kako biste ostvarili pravo na dodatnu garanciju (2 godine zakonske saobraznosti plus 3 godine dodatne garancije) potrebno je da izvršite registraciju na jedan od sledećih nacina:</p>

			<h3>PUTEM POŠTE:</h3>
			<p>- uz popunjeni obrazac priložiti i fotokopiju fiskalnog računa koju ćete poslati poštom najkasnije u roku od 30 dana od dana kupovine. Potvrda registracije će vam biti poslata na kućnu adresu.</p>

			<h3>PUTEM ELEKTRONSKE POŠTE</h3>
			<p>- poslati popunjeni obrazac i kopiju fiskalnog računa elektronskom poštom, na e-mail: <b>garancija@nitom.rs</b> najkasnije u roku od 30 dana od dana kupovine. Potvrda registracije će vam biti poslata elektronskom poštom.</p>

			<h3>PUTEM WEB PORTALA:</h3>
			<p>- putem našeg web portala <b>www.nitom.rs/garancija</b> najkasnije u roku od 30 dana od dana kupovine. Potrebno je odštampati ili sacuvati u elektronskom (PDF) formatu potvrdu o registraciji.</p>

			<p>Za ispravnost unetih podataka je odgovorno lice koje unosi podatke. Ukoliko podaci nisu verodostojni smatraće se da registracija nije izvršena.</p>

			<p><h3 class="inline-block">NAPOMENA:</h3> <b>Akcija produžene garancije na KONČAR šporete važi za uređaje kupljene u periodu od 15.07.2018. do 31.06. 2019. godine.</b></p>
			<br>
		</div> 
		<div class="form-like"> 
			<br>
			<div><span>{{ Language::trans('Ime i prezime') }}:</span> {{ $garancija->ime }} {{ $garancija->prezime }}</div> 
			<div><span>{{ Language::trans('Adresa') }}:</span> {{ $garancija->adresa }}</div> 
			<div><span>{{ Language::trans('Grad') }}:</span> {{ $garancija->grad }}</div> 
			<div><span>{{ Language::trans('Email') }}:</span> {{ $garancija->email }}</div> 
			<div><span>{{ Language::trans('Telefon') }}:</span> {{ $garancija->telefon }}</div> 
			<div><span>{{ Language::trans('Proizvod') }}:</span> {{ DB::table('roba')->where('roba_id',$garancija->roba_id)->pluck('naziv_web') }}</div> 
			<div><span>{{ Language::trans('Serijski broj') }}(SN):</span> {{ $garancija->serijski_broj }}</div> 
			<div><span>{{ Language::trans('Prodavac') }}:</span> {{ $garancija->maloprodaja }}</div> 
			<div><span>{{ Language::trans('Mesto prodavca') }}:</span> {{ $garancija->prodavnica_mesto }}</div> 
			<div><span>{{ Language::trans('Broj fiskalnog računa') }}(BI):</span> {{ $garancija->fiskalni }}</div> 
			<div><span>{{ Language::trans('Datum izdavanja fiskalnog računa') }}:</span> {{ $garancija->datum_izdavanja }}</div> 
		</div>	
	</div>  -->

	<div class="container"> 
		<header>
			<h2>{{ AdminOptions::company_name() }} - {{ AdminLanguage::transAdmin('produženje garancije') }}</h2>
		</header>

		<h3>{{ AdminLanguage::transAdmin('Podaci o korisniku produžene garancije') }}</h3>

		<div class="centered"> 
			<table>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Ime i prezime') }}:</td>
					<td>{{ $garancija->ime }} {{ $garancija->prezime }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Adresa') }}:</td>
					<td>{{ $garancija->adresa }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Grad') }}:</td>
					<td>{{ $garancija->grad }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Email') }}:</td>
					<td>{{ $garancija->email }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Telefon') }}:</td>
					<td>{{ $garancija->telefon }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Proizvod') }}:</td>
					<td>{{ DB::table('roba')->where('roba_id',$garancija->roba_id)->pluck('naziv_web') }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Serijski broj') }}:</td>
					<td>{{ $garancija->serijski_broj }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Prodavac') }}:</td>
					<td>{{ $garancija->maloprodaja }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Mesto prodavca') }}:</td>
					<td>{{ $garancija->prodavnica_mesto }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Broj fiskalnog računa') }}:</td>
					<td>{{ $garancija->fiskalni }}</td>
				</tr>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Datum izdavanja fiskalnog računa') }}:</td>
					<td>{{ $garancija->datum_izdavanja }}</td>
				</tr>
			</table> 
		</div>
		<br>
		<div class="text-right">
			<p>{{ AdminLanguage::transAdmin('Datum i vreme registracije') }}: {{ date_format(date_create($garancija->final_parent_created),"d.m.Y H:i:s")  }}</p>
			<p>{{ AdminLanguage::transAdmin('Zavedeno pod brojem') }}: {{ $garancija->produzene_garancije_id }}</p>
		</div>
		<br>
		<h4><i>{{ AdminLanguage::transAdmin('Uspešno ste izvršili registraciju i ostvarili pravo na dodatnu garanciju (2 godine zakonske saobraznosti plus tri godine)') }}.</i></h4>
	</div>   
 
</body>
</html>
