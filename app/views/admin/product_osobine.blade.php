<div id="main-content" class="article-edit">
	
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@if(Session::has('alert'))
	<script>
		alertify.error('{{ Session::get('alert') }}');
	</script>
	@endif


	@include('admin/partials/product-tabs')

	<div class="row">
		<div class="columns medium-6">
			<div class="flat-box">
				<h1 class="title-med">{{ AdminLanguage::transAdmin('Osobine artikla') }} - {{ AdminLanguage::transAdmin('Kombinacije') }}</h1> 
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/product-osobine/{{$roba_id}}/0/save">
					<div class="row property">
						<div class="columns medium-8">

							<span class="inline-block margin-right">
								<label>{{ AdminLanguage::transAdmin('Nova') }} {{ AdminLanguage::transAdmin('Kombinacija') }}</label>
							</span>
							
							&nbsp;&nbsp;&nbsp;

							<span class="inline-block margin-right">
								<label>{{ AdminLanguage::transAdmin('Količina') }}</label>
								<input class="ordered-number" name="kolicina" type="text" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</span>
			<!-- 				<span class="columns medium-5 no-padd">
								<label>{{ AdminLanguage::transAdmin('Cena') }}</label>
								<input class="ordered-number" name="cena" type="text" value="{{ $web_cena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</span> -->
							
							&nbsp;&nbsp;&nbsp;

							<span class="inline-block">
								<input name="aktivna" type="checkbox" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivno') }}
							</span>

						</div>

						@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<div class="columns medium-4 btn-container text-right">
							<button type="submit" class="btn btn-primary save-it-btn"> {{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						</div>
						@endif
					</div>
				</form> 
			</div>

			<div class="flat-box">

				@foreach($kombinacije as $key => $row)
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/product-osobine/{{$roba_id}}/{{$row->osobina_kombinacija_id}}/save">
					<div class="row property" style="{{ ($row->osobina_kombinacija_id == $osobina_kombinacija_id) ? 'background: #ddd' : '' }}">
						<div class="columns medium-7"> 

							<span class="inline-block margin-right">
								<label>{{ AdminLanguage::transAdmin('Kombinacija') }} {{ $key+1 }}</label>
							</span>
							
							&nbsp;&nbsp;&nbsp;

							<span class="inline-block margin-right">
								<label>{{ AdminLanguage::transAdmin('Količina') }}</label>
								<input class="ordered-number" name="kolicina" type="text" value="{{ $row->kolicina }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</span>
			<!-- 			<span class="columns medium-5 no-padd">
							<label>{{ AdminLanguage::transAdmin('Cena') }}</label>
							<input class="ordered-number" name="cena" type="text" value="{{ $row->cena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</span> -->

							&nbsp;&nbsp;&nbsp;

							<span class="inline-block">
								<input name="aktivna" type="checkbox" @if($row->aktivna == 1) checked @endif {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivno') }}
							</span>
						</div>

					<div class="columns medium-5 btn-container text-right">
						
						@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<button type="submit" class="btn btn-primary save-it-btn"> {{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						<a href="{{ AdminOptions::base_url() }}admin/product-osobine/{{ $roba_id }}/{{ $row->osobina_kombinacija_id }}/delete" class="btn btn-danger"> {{ AdminLanguage::transAdmin('Obriši') }}</a>
						@endif

						<a href="{{ AdminOptions::base_url() }}admin/product-osobine/{{ $roba_id }}/{{ $row->osobina_kombinacija_id }}" class="name-ul__li__right-arrow button-option tooltipz" aria-label="Dodaj vrednost">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</a>						
					</div>
				</div>
			</form>
			@endforeach
			
		</div> 
	</div>

	@if(!is_null($osobina_kombinacija_id) AND $osobina_kombinacija_id > 0)
	<div class="columns medium-6">
		<div class="flat-box">
			<h1 class="title-med">{{ AdminLanguage::transAdmin('Vrednosti') }}</h1>
			@if(count($osobine_nazivi) > 0) 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/product-osobine-vrednost/{{ $roba_id }}/{{ $osobina_kombinacija_id }}/save">
					<input type="hidden" name="old_osobina_vrednost_id" value="0">
					<div class="row property">
						<div class="columns medium-8">

							<div class="row">  
								<span class="columns medium-6">
									<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
									<select class="ordered-number" name="osobina_naziv_id" id="JSOsobinaKombinacijaNaziv" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
										@foreach($osobine_nazivi as $osobina_naziv)
										<option value="{{ $osobina_naziv->osobina_naziv_id }}">{{ $osobina_naziv->naziv }}</option>
										@endforeach
									</select>
								</span>

								<span class="columns medium-6">
									<label>{{ AdminLanguage::transAdmin('Vrednost') }}</label>
									<select class="ordered-number" name="osobina_vrednost_id" id="JSOsobinaKombinacijaVrednosti" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
										@foreach($osobina_vrednosti as $osobina_vrednost)
										<option value="{{ $osobina_vrednost->osobina_vrednost_id }}">{{ $osobina_vrednost->vrednost }}</option>
										@endforeach
									</select>
								</span>
							</div>

						</div>

						@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<div class="columns medium-4 text-right btn-container no-margin">
							<label>&nbsp;</label>
							<div> 
								<button type="submit" class="btn btn-primary save-it-btn"> {{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							</div>
						</div>
						@endif
					</div>
				</form>	

			@endif
		</div>

		<div class="flat-box">
			@foreach($kombinacija_vrednosti as $kombinacija_vrednost)
			
			<form method="POST" action="{{ AdminOptions::base_url() }}admin/product-osobine-vrednost/{{ $roba_id }}/{{ $osobina_kombinacija_id }}/save">
				<input type="hidden" name="old_osobina_vrednost_id" value="{{ $kombinacija_vrednost->osobina_vrednost_id }}">

				<div class="row property">
					<div class="columns medium-8"> 

						<div class="row"> 
							<span class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
								<input type="text" name="osobina_naziv_id" disabled value="{{ $kombinacija_vrednost->naziv }}">
							</span>

							<span class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Vrednost') }}</label>
								<select class="ordered-number" name="osobina_vrednost_id" id="JSOsobinaKombinacijaVrednosti" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
									@foreach(AdminOsobine::getPropertyValues($kombinacija_vrednost->osobina_naziv_id) as $osobina_vrednost)
									<option value="{{ $osobina_vrednost->osobina_vrednost_id }}"
										{{ ($kombinacija_vrednost->osobina_vrednost_id == $osobina_vrednost->osobina_vrednost_id) ? 'selected' : '' }}
										>
										{{ $osobina_vrednost->vrednost }}
									</option>
									@endforeach
								</select>
							</span>
						</div>
					</div>

					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
					<div class="columns medium-4 text-right btn-container">
						<div>&nbsp;</div>

						<button type="submit" class="btn btn-primary save-it-btn"> {{ AdminLanguage::transAdmin('Sačuvaj') }}</button>

						<a href="{{ AdminOptions::base_url() }}admin/product-osobine-vrednost/{{ $roba_id }}/{{ $osobina_kombinacija_id }}/{{ $kombinacija_vrednost->osobina_vrednost_id }}/delete" class="btn btn-danger"> {{ AdminLanguage::transAdmin('Obriši') }}</a>
					</div>
					@endif
				</div>
			</form>

			@endforeach
		</div>				

	</div>
	@endif

</div>
</div>