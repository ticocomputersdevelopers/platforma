<section id="main-content" class="banners">
	
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	<section class="medium-3 columns">
		<div class="flat-box">
			
			<!-- BANNER LIST -->
			<label class="text-center">{{ AdminLanguage::transAdmin('Baneri') }}</label>
			<ul @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list JSListaBaner" id="banner-sortable" @endif data-table="2">
				@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/1">{{ AdminLanguage::transAdmin('Novi baner') }}</a>
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/1"><span class="icon"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
				</li>
				@endif
				@foreach($banners as $banner)
				<li id="{{$banner->baneri_id}}" class="{{ ( (AdminSupport::trajanje_banera($banner->baneri_id) == 'NULL' ) && ( AdminSupport::trajanje_banera($banner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($banner->baneri_id == $id) active @endif" id="{{$banner->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$banner->baneri_id}}/1">
						{{$banner->naziv}} 
						<img class="slide-xs-img" src="{{ AdminOptions::base_url().$banner->img }}" alt="{{ AdminOptions::base_url().$banner->img }}">
					</a>
					@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
					<a class="icon tooltipz JSbtn-delete" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{AdminOptions::base_url()}}admin/baneri-delete/{{$banner->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>
			
			<!-- SLIDE LIST -->		
			<label class="text-center">{{ AdminLanguage::transAdmin('Slajderi') }}</label>
			<ul @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list JSListaSlider" id="slide-sortable" @endif data-table="2">
				@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
				<li id="0" class="new-slide new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/2">{{ AdminLanguage::transAdmin('Novi slajd') }}
					</a>
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/2"><span class="icon"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
				</li>
				@endif
				@foreach($sliders as $slide)
				<li id="{{$slide->baneri_id}}" class="{{ ( (AdminSupport::trajanje_banera($slide->baneri_id) == 'NULL' ) && ( AdminSupport::trajanje_banera($slide->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($slide->baneri_id == $id) active @endif" id="{{$slide->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$slide->baneri_id}}/2">
						{{$slide->naziv}} 
						<img class="slide-xs-img" src="{{ AdminOptions::base_url().$slide->img }}" alt="{{ AdminOptions::base_url().$slide->img }}">
					</a>
					@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
					<a class="icon tooltipz JSbtn-delete" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{AdminOptions::base_url()}}admin/baneri-delete/{{$slide->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>

			<label class="text-center">{{ AdminLanguage::transAdmin('Popup baner') }}</label>
			<ul @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list JSListaBaner" id="banner-sortable" @endif data-table="2">
				@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/9">{{ AdminLanguage::transAdmin('Novi popup baner') }}</a>
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/9"><span class="icon"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
				</li>
				@endif
				@foreach($popup as $popbanner)
				<li id="{{$popbanner->baneri_id}}" class="{{ ( (AdminSupport::trajanje_banera($popbanner->baneri_id) == 'NULL' ) && ( AdminSupport::trajanje_banera($popbanner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($popbanner->baneri_id == $id) active @endif" id="{{$popbanner->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$popbanner->baneri_id}}/9">{{$popbanner->naziv}}</a>
					@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
					<a class="icon tooltipz JSbtn-delete" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{AdminOptions::base_url()}}admin/baneri-delete/{{$popbanner->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>

		</div> <!-- end of .flat-box -->
		
		<!-- BACKGROUND IMAGE -->
		@if(in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(5,6,7,10)))
		<div class="flat-box">
			<label class="text-center">{{ AdminLanguage::transAdmin('Pozadinska slika') }}</label>
			<ul class="">
				<form action="{{AdminOptions::base_url()}}admin/bg-img-edit" method="POST" enctype="multipart/form-data">
					@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
					<input type="file" name="bgImg" value="Izaberi sliku" class="no-margin">
					@endif
					
					@if(AdminSupport::getBGimg() == null)
					<img src="{{AdminOptions::base_url()}}/images/no-image.jpg" alt="" class="bg-img">
					@else
					<img src="{{AdminOptions::base_url()}}{{ AdminSupport::getBGimg() }}" alt="" class="bg-img">
					@endif
					<label class="text-center">{{ AdminLanguage::transAdmin('Preporučene dimenzije slike') }}: 1920x1080</label>

					<!-- <label for="aktivna">
						<input type="checkbox" name="aktivna" value=""> Aktivna slika
					</label> -->
					<div> 
						<label class="text-center">{{ AdminLanguage::transAdmin('Link leve strane') }}</label>
						<input id="link" value="{{AdminSupport::getlink()}}" type="text" name="link" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<label class="text-center">{{ AdminLanguage::transAdmin('Link desne strane') }}</label>
						<input id="link2" value="{{AdminSupport::getlink2()}}" type="text" name="link2" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div><br>
					<input type="hidden" value="3" name="flag">
					@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
					<div class="text-center btn-container">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						<a class="btn btn-danger" href="{{AdminOptions::base_url()}}admin/bg-img-delete">{{ AdminLanguage::transAdmin('Obriši') }}</a>
					</div>
					@endif
				</form>
			</ul>
		</div>
		@endif

	</section> <!-- end of .medium-3 .columns -->
	
	<!-- BANNER EDIT -->
	<section class="page-edit medium-3 columns">
		<div class="flat-box banner-edit-top"> 
			<form action="{{AdminOptions::base_url()}}admin/banner-edit" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
				@if($type == 1)
				<label class="text-center">{{ AdminLanguage::transAdmin('Izmeni baner') }}</label>
				@elseif($type == 2)
				<label class="text-center">{{ AdminLanguage::transAdmin('Izmeni slajder') }}</label>
				@elseif($type == 9)
				<label class="text-center">{{ AdminLanguage::transAdmin('Izmeni popup baner') }}</label>
				@endif
				<!-- BANNER EDIT TOP -->
				<section class="row">
					<div class="medium-5 columns">
						@if($type == 1)
						<label>{{ AdminLanguage::transAdmin('Naziv banera') }}</label>
						@elseif($type == 2)
						<label>{{ AdminLanguage::transAdmin('Naziv slajdera') }}</label>
						@elseif($type == 9)
						<label>{{ AdminLanguage::transAdmin('Naziv popup banera') }}</label>
						@endif
						<div class="relative"> 

							<input id="naziv" class="input-bann" value="{{ Input::old('naziv') ? Input::old('naziv') : trim($item->naziv) }}" type="text" name="naziv" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
							<span class="banner-uploads">
								<input type="file" name="img">
								<span>{{ AdminLanguage::transAdmin('Dodaj sliku') }}</span>
							</span>
							@endif
						</div>
						<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div> 
						<div class="error">{{ $errors->first('img') ? ($errors->first('img') == 'Niste popunili polje.' ? 'Niste izabrali sliku.' : $errors->first('img')) : '' }}</div>
					</div>

					<div class="medium-6 columns">
						@if($type == 1)
						<label>{{ AdminLanguage::transAdmin('Link banera') }}</label>
						@elseif($type == 2)
						<label>{{ AdminLanguage::transAdmin('Link slajdera') }}</label>
						@elseif($type == 9)
						<label>{{ AdminLanguage::transAdmin('Link popup banera') }}</label>
						@endif
						<input id="link" value="{{ Input::old('link') ? Input::old('link') : trim($lang_data->link) }}" type="text" name="link" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('link') ? $errors->first('link') : '' }}</div>
					</div>
				</section>

				<div class="row"> 
					<div class="medium-2 columns"> 
						<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
						<select name="aktivan" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" {{ (Input::old('aktivan') == '0') ? 'selected' : (($item->aktivan == 0) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('NE') }}</option>
						</select>
					</div>
					@if($type == 1)
					<div class="column medium-3 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Baner postavljen') }}:</label>
						<input class="akcija-input" id="datum_od" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<span id="datum_od_delete"><i class="fa fa-close"></i></span>
						@endif
					</div>
					@elseif($type == 2)
					<div class="column medium-3 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Slajder postavljen') }}:</label>
						<input class="akcija-input" id="datum_od" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<span id="datum_od_delete"><i class="fa fa-close"></i></span>
						@endif
					</div>
					@elseif($type == 9)
					<div class="column medium-3 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Pop-Up baner postavljen') }}:</label>
						<input class="akcija-input" id="datum_od" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<span id="datum_od_delete"><i class="fa fa-close"></i></span>
						@endif
					</div>
					@endif
					<div class="column medium-3 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Traje Do') }}:</label>
						<input class="akcija-input" id="datum_do" name="datum_do" autocomplete="off" type="text" value="{{ Input::old('datum_do') ? Input::old('datum_do') : $datum_do }}" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<span id="datum_do_delete"><i class="fa fa-close"></i></span>
						@endif
					</div>
					@if(!empty($datum_do) && $type == 1) 
					<div class="column medium-3 baners-date">
						<label>&nbsp;</label>
						<span>{{ AdminLanguage::transAdmin('Baner ističe za') }}<input type="text" class="time-limit center" value="{{ AdminSupport::trajanje_banera($id) }}" disabled {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}> {{ AdminLanguage::transAdmin('dana') }}.</span>
					</div> 
					@elseif(!empty($datum_do) && $type ==2)
					<div class="column medium-3 baners-date">
						<label>&nbsp;</label>
						<span>{{ AdminLanguage::transAdmin('Slajder ističe za') }}<input type="text" class="time-limit center" value="{{ AdminSupport::trajanje_banera($id) }}" disabled {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}> {{ AdminLanguage::transAdmin('dana') }}.</span>
					</div>
					@elseif(!empty($datum_do) && $type ==9)
					<div class="column medium-3 baners-date">
						<label>&nbsp;</label>
						<span>{{ AdminLanguage::transAdmin('PopUp baner ističe za' ) }}<input type="text" class="time-limit center" value="{{ AdminSupport::trajanje_banera($id) }}" disabled {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}> {{ AdminLanguage::transAdmin('dana') }}.</span>
					</div>
					@endif
				</div>


				<!-- BANNER PREVIEW -->
				<section class="banner-preview-area">
					<div class="row">
						<img class="banner-preview" src="{{AdminOptions::base_url()}}{{$item->img}}" alt="{{$item->naziv}}" />
					</div>
					<div class="row">
						<div class="medium-4 columns medium-centered large-centered">
							<label>{{ AdminLanguage::transAdmin('Alt') }}</label>
							<input id="alt" value="{{ Input::old('alt') ? Input::old('alt') : trim($lang_data->alt) }}" type="text" name="alt" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('alt') ? $errors->first('alt') : '' }}</div>
						</div>
					</div>
				</section>


				@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
				@if($item->baneri_id != 0 AND count($jezici) > 1)
				<div class="row"> 
					<div class="languages">
						<ul>	
							@foreach($jezici as $jezik)
							<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{ $id }}/{{ $type }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
							@endforeach
						</ul>
					</div>
				</div>
				@endif 
				@endif

				@if($type != 9)
				<section class="banner-edit-top row">
					<div class="medium-4 columns">
						<label>{{ AdminLanguage::transAdmin('Naslov') }}</label>
						<input id="naslov" value="{{ Input::old('naslov') ? Input::old('naslov') : trim($lang_data->naslov) }}" type="text" name="naslov" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('naslov') ? $errors->first('naslov') : '' }}</div>
					</div>

					<div class="medium-4 columns">
						<label>{{ AdminLanguage::transAdmin('Nadnaslov') }}</label>
						<input id="nadnaslov" value="{{ Input::old('nadnaslov') ? Input::old('nadnaslov') : trim($lang_data->nadnaslov) }}" type="text" name="nadnaslov" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('nadnaslov') ? $errors->first('nadnaslov') : '' }}</div>
					</div>

					<div class="medium-4 columns">
						<label>{{ AdminLanguage::transAdmin('Naslov dugmeta') }}</label>
						<input id="naslov_dugme" value="{{ Input::old('naslov_dugme') ? Input::old('naslov_dugme') : trim($lang_data->naslov_dugme) }}" type="text" name="naslov_dugme" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('naslov_dugme') ? $errors->first('naslov_dugme') : '' }}</div>
					</div>
					@if($type == 1)
					<input class="ordered-number" type="hidden" name="redni_broj" value="{{ !is_null(Input::old('redni_broj')) ? Input::old('redni_broj') : $redni_broj }}" >
					@endif
					<div class="medium-12 columns">
						<label>{{ AdminLanguage::transAdmin('Sadržaj') }}</label>
						<textarea @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) class="special-textareas" @endif name="sadrzaj" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('sadrzaj') ? Input::old('sadrzaj') : trim($lang_data->sadrzaj) }}</textarea>
						<div class="error">{{ $errors->first('sadrzaj') ? $errors->first('sadrzaj') : '' }}</div>
					</div>
				</section>
				@endif

				<!-- BANNER DISPLAY PAGES -->	
				@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))		
				<div>
					<input type="hidden" name="baneri_id" value="{{$item->baneri_id}}" />
					<input type="hidden" name="tip_prikaza" value="{{$type}}" />
					<input type="hidden" name="jezik_id" value="{{$jezik_id}}" />
					<div class="btn-container center">
						<button class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</div>
				@endif
			</form>
		</div>

		<!-- ====================== -->
		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="banners-manual">{{ AdminLanguage::transAdmin('Uputstvo') }}<i class="fa fa-film"></i></a>
			<div id="banners-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
					<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Baneri i slajder') }}</span></p> 
					<iframe src="https://player.vimeo.com/video/271254080" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
			</div>
		</div>
		<!-- ====================== -->
	</section>
</section>

