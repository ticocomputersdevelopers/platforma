<section class="nivoi-sec" id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/permissions-tabs')

	<div class="row logs">
		<section class="small-12 medium-7 columns">
			<div class="flat-box">
				<div class="row"> 
					<div class="column medium-3">
						<label for="">{{ AdminLanguage::transAdmin('Datum Od') }}</label>
						<input id="datepicker_from" class="datum-val has-tooltip" name="datepicker_from" type="text" value="{{$od}}">
					</div>
					
					<div class="column medium-3">
						<label for="">{{ AdminLanguage::transAdmin('Datum Do') }}</label>
						<input id="datepicker_to" class="datum-val has-tooltip" name="datepicker_to" type="text" value="{{$do}}">
					</div>

					<div class="column medium-2">
						<label for="">&nbsp;</label>
						<a class="btn btn-danger btn-block btn-small no-margin" href="/admin/logovi/0/0/SVI/SVE">{{ AdminLanguage::transAdmin('Poništi') }}</a>
					</div> 
				</div>

				<div class="row"> 
					<div class="column medium-3">
						<label>{{ AdminLanguage::transAdmin('Korisnici') }}</label>
						<select id="adminUser">
							<option value="SVI" {{ ($korisnik_id != NULL && $korisnik_id == 'SVI') ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Svi') }}</option>
							@foreach($korisnici as $row)
							<option value="{{$row->imenik_id}}" {{ ($korisnik_id != NULL && $korisnik_id != 'SVI' && ($row->imenik_id == $korisnik_id)) ? 'selected' : '' }}>{{ $row->ime }} {{ $row->prezime }}</option>
							@endforeach
						</select>			
					</div>

					<div class="column medium-3">
						<label>{{ AdminLanguage::transAdmin('Logovanja') }}</label>
						<select id="loging">
							<option>{{ AdminLanguage::transAdmin('Izaberi') }}</option>
							@foreach($logovanja as $row)
							<option value='{"id":"{{$row->korisnik_id}}","dateFrom":"{{ $row->start }}", "dateTo":"{{ $row->finish }}" }'>{{ $row->ime }} {{ $row->prezime }} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp {{ date_format(date_create($row->start),"d.m.y H:i:s") }} &nbsp &nbsp &nbsp  -  &nbsp &nbsp &nbsp {{ date_format(date_create($row->finish),"d.m.y H:i:s")}}</option>
							@endforeach
						</select>
					</div>

					<div class="column medium-3">
						<label>{{ AdminLanguage::transAdmin('Glavne akcije') }}</label>
						<select id="mainAction">
							<option {{ ($akcija != NULL && ($akcija == 'SVE')) ? 'selected' : '' }} value="SVE">{{ AdminLanguage::transAdmin('Sve') }}</option>
							<option {{ ($akcija != NULL && ($akcija == 'DODAJ')) ? 'selected' : '' }} value="DODAJ">{{ AdminLanguage::transAdmin('Dodaj') }}</option>
							<option {{ ($akcija != NULL && ($akcija == 'IZMENI')) ? 'selected' : '' }} value="IZMENI">{{ AdminLanguage::transAdmin('Izmeni') }}</option>
							<option {{ ($akcija != NULL && ($akcija == 'OBRISI')) ? 'selected' : '' }} value="OBRISI">{{ AdminLanguage::transAdmin('Obriši') }}</option>
						</select> 
					</div>
				</div>
				
				<br>

				<div class="row"> 
					<div class="column medium-12">  
						<div class="table-scroll">
							<table><tr><th>{{ AdminLanguage::transAdmin('Korisnik') }}</th><th>{{ AdminLanguage::transAdmin('Akcija') }}</th><th>{{ AdminLanguage::transAdmin('Datum') }}</th><th>{{ AdminLanguage::transAdmin('Vreme') }}</th></tr>
								@foreach($logovi as $log)
								<tr class="LogovanjaNaziv" data-id="{{ $log->datum }}" data-isopen="closed">
									<td class="first-td">{{$log->ime}} {{$log->prezime}}</td> 
									@if($log->naziv != 'Logovanje' && $log->naziv != 'Log out')
									<td><a href="#!">{{$log->naziv}}</a></td> 
									@else
									<td>{{$log->naziv}}</td>
									@endif
									<td>{{ date_format(date_create($log->datum),"d.m.y")}}</td> <td>{{ date_format(date_create($log->datum),"H:i:s")}}</td>
								</tr>
								<tr class="LogovanjaProizvodi" data-id="{{ $log->datum }}" hidden>
									<td class="colapsable-td"></td>
									<td>
										@foreach(AdminSupport::logItemsNames($log->naziv_proizvoda) as $item)
										{{$item->id}} - {{$item->naziv}}
										@if(isset($item->additional))
										-> {{ $item->additional->addnaziv }} - {{$item->additional->naziv}}
										@endif
										@if(isset($item->additionalKoji))
										-> {{ $item->additionalKoji }} - {{$item->additionalKojiNaziv}}
										@endif
										@if(isset($item->additionalKojiKoji))
										-> {{ $item->additionalKojiKoji }} - {{$item->additionalKojiKojiNaziv}}
										@endif
										<br>
										@endforeach
									</td>
									<td></td>
									<td></td>
								</tr>
								@endforeach
							</table>
						</div>
						{{ Paginator::make($logovi, $count, $limit)->links() }}
					</div>
				</div> 
			</section>

			<div class="small-12 medium-5 columns">
				<div class="flat-box">
					<br>
					<select class="statistika">
						<option value=-1>{{ AdminLanguage::transAdmin('Izaberi statistiku') }}</option>
						<option {{ $statistics == 1 ? 'selected' : '' }} value="1">{{ AdminLanguage::transAdmin('Dodavanje artikala') }}</option>
						<option {{ $statistics == 2 ? 'selected' : '' }} value="2">{{ AdminLanguage::transAdmin('Izmena artikala') }}</option>
						<option {{ $statistics == 3 ? 'selected' : '' }} value="3">{{ AdminLanguage::transAdmin('Brisanje artikala') }}</option>
					</select>
					
					<br><br>

					<div class="table-scroll">
						<table>
							<tr><th>{{ AdminLanguage::transAdmin('ime') }}</th><th>{{ AdminLanguage::transAdmin('broj aartikala') }}</th></tr>
							@if($statistics == '1')
							@foreach($statisticsInsert as $insert)
							<tr class="Akcija" data-id="{{ $insert->ime }}" data-isopen="closed">
								<td>{{$insert->ime}}</td>
								<td>{{$insert->proizvodi ? count(AdminSupport::parseGroupProizvodi($insert->proizvodi)) : 0}}</td>
							</tr>

							@foreach(AdminSupport::parseGroupProizvodi($insert->proizvodi) as $roba)
							<tr class="AkcijaData" data-id="{{ $insert->ime }}" hidden>
								<td>{{$roba->id}}</td>
								<td>{{$roba->naziv}}</td>
								<td class="colapsable-td"></td>
							</tr>
							@endforeach						
							@endforeach
							@elseif($statistics == '2')
							@foreach($statisticsUpdate as $update)
							<tr class="Akcija" data-id="{{ $update->ime }}" data-isopen="closed">
								<td>{{$update->ime}}</td>
								<td>{{$update->proizvodi ? count(AdminSupport::parseGroupProizvodi($update->proizvodi)) : 0}}</td>
							</tr>

							@foreach(AdminSupport::parseGroupProizvodi($update->proizvodi) as $roba)
							<tr class="AkcijaData" data-id="{{ $update->ime }}" hidden>
								<td>{{$roba->id}}</td>
								<td>{{$roba->naziv}}</td>
								<td class="colapsable-td"></td>
							</tr>
							@endforeach
							@endforeach
							@elseif($statistics == '3')
							@foreach($statisticsDelete as $delete)
							<tr class="Akcija" data-id="{{ $delete->ime }}" data-isopen="closed">
								<td>{{$delete->ime}}</td>
								<td>{{$delete->proizvodi ? count(AdminSupport::parseGroupProizvodi($delete->proizvodi)) : 0}}</td>
							</tr>

							@foreach(AdminSupport::parseGroupProizvodi($delete->proizvodi) as $roba)
							<tr class="AkcijaData" data-id="{{ $delete->ime }}" hidden>
								<td>{{$roba->id}}</td>
								<td>{{$roba->naziv}}</td>
								<td class="colapsable-td"></td>
							</tr>
							@endforeach
							@endforeach
							@endif

						</table>
					</div>
				</div>
			</div>
		</div>
	</section>