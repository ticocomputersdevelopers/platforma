<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod plaćanja - poziv na broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</p>
		<p><strong>{{ AdminLanguage::transAdmin('Napomena o poreskom oslobađanju') }}:</strong></p>
		<p>{{substr(AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena'),0,220)}}</p>
	</div>
</div>

<div class="row"> 
	<table class="signature">
		<tr>
			<td class=""></td>
			<td class="text-right"><span class="robu_izdao">{{ AdminLanguage::transAdmin('Predračun izdao') }}</span> ___________________________</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style='width: 60%;'>
				<p style="border-top: 1px solid #666; font-size:12px;"> {{ AdminLanguage::transAdmin('Predračun je izrađen na računaru i punovažan je bez pečata i potipisa') }}</p>
			</td>
			<td class=""></td>
		</tr>
	</table>
</div>