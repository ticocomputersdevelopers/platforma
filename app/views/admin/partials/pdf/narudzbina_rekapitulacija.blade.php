        <div class="row"> 
        	<br>
			<h4>{{ AdminLanguage::transAdmin('Rekapitulacija') }}</h4>
		</div>

		<div class="row"> 
			<div class="col-7"> 
				<table class="rekapitulacija">
					<thead>
						<tr>
							<td></td>
							<td>{{ AdminLanguage::transAdmin('Osnovica') }}</td>
							<td>{{ AdminLanguage::transAdmin('PDV') }}</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Oslobodjeno poreza') }}</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Po posebnoj stopi') }}</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Po opstoj stopi') }}</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
						</tr>
						<tr>
							<td><strong>{{ AdminLanguage::transAdmin('Ukupno') }}</strong></td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
						</tr>
					</tbody>
				</table>
		 	</div>

		 	<div class="col-4"> 
		        <table class="summary">
		        	<tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos osnovice') }}:</span>
		                 {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		            </tr>
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos PDV-a') }}:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		            </tr>
		           
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
		            </tr>
		          
		        </table>
	        </div>
		</div>
