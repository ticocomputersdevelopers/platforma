	<div class="m-tabs clearfix">
		@if(Admin_model::check_admin(array('KORISNICI')))
		<div class="m-tabs__tab{{ $strana=='administratori' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/administratori">{{ AdminLanguage::transAdmin('Korisnici administracije') }}</a></div>
		@endif
		@if(Admin_model::check_admin(array('KORISNICI')))
		<div class="m-tabs__tab{{ $strana=='grupa_modula' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/grupa-modula">{{ AdminLanguage::transAdmin('Nivoi pristupa i moduli') }}</a></div>
		@endif
		@if(Admin_model::check_admin(array('KORISNICI')))
		<div class="m-tabs__tab{{ $strana=='logovi' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/logovi/0/0/SVI/SVE">{{ AdminLanguage::transAdmin('Logovi') }}</a></div>
		@endif
	</div>