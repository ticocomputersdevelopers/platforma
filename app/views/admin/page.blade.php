<!DOCTYPE html>
<html>
    <head>
        <title>{{$title}}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msapplication-tap-highlight" content="no"/> 

        <link rel="icon" type="image/png" href="{{ AdminOptions::base_url()}}favicon.ico">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.core.css">
        <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.default.css">
        <link href="{{ AdminOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />
        <script src="{{ AdminOptions::base_url()}}js/alertify.js" type="text/javascript"></script>
  
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122633081-1"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-122633081-1');
        </script>
    </head>
<body class="{{ Session::get('adminHeaderWidth') }}">

    @include('admin.partials.aop_trial')
   
    <div>            
        <!-- HEADER -->
        <!-- MAIN MENU -->
        @if(AdminB2BOptions::web_options(130) == 1 AND in_array($strana,array('partneri','partner')))
            @include('adminb2b.partials.header')
        @elseif((AdminB2BOptions::web_options(130) == 2 OR (AdminB2BOptions::web_options(130) == 1 AND !AdminOptions::is_shop())) AND in_array($strana,array('artikli','web_import','partneri','grupe','proizvodjac','mesto','tip_artikla','jedinica_mere','poreske_stope','stanje_artikla','status_narudzbine','kurirska_sluzba','konfigurator','osobine','nacin_placanja','nacin_isporuke','troskovi_isporuke','kurs','partner','katalog','product_short','product','labela_artikla','bodovi','definisane_marze','vauceri','kupovina_na_rate')))
            @include('adminb2b.partials.header')
        @else
            @include('admin.partials.header')
        @endif
        
        
        <!-- ORDER FILTERS -->
        @if($strana=='pocetna' AND Admin_model::check_admin(array('NARUDZBINE')) AND Admin_model::check_admin(array('NARUDZBINE_PREGLED')))
            @include('admin/porudzbine')
        @elseif($strana=='stranice' AND Admin_model::check_admin(array('STRANICE')) AND Admin_model::check_admin(array('STRANICE_PREGLED')))
            @include('admin/pages')           
        @elseif($strana=='kontakt' AND Admin_model::check_admin(array('KONTAKT_PODACI')) AND Admin_model::check_admin(array('KONTAKT_PODACI_PREGLED')))
            @include('admin/kontakt')
        @elseif($strana=='slajder' AND Admin_model::check_admin(array('BANERI_I_SLAJDER')) AND Admin_model::check_admin(array('BANERI_I_SLAJDER_PREGLED')))
            @include('admin/slajder')
        @elseif($strana=='footer' AND Admin_model::check_admin(array('FUTER')) AND Admin_model::check_admin(array('FUTER_PREGLED')))
            @include('admin/footer')
        @elseif($strana=='podesavanja' AND Admin_model::check_admin(array('PODESAVANJA')))
            @include('admin/podesavanja')
        @elseif($strana=='css')
            @include('admin/css')
        @elseif($strana=='artikli' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/artikli')
        @elseif($strana=='product' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/product')
        @elseif($strana=='product_short' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/product_short')
        @elseif($strana=='web_import' AND Admin_model::check_admin(array('WEB_IMPORT')) AND Admin_model::check_admin(array('WEB_IMPORT_PREGLED')))
            @include('admin/web_import') 
        @elseif($strana=='import_podaci_grupa' AND Admin_model::check_admin(array('WEB_IMPORT')) AND Admin_model::check_admin(array('WEB_IMPORT_PREGLED')))
            @include('admin/import_podaci_grupa')
        @elseif($strana=='grupe' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/grupe')
        @elseif($strana=='ankete' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/ankete')
        @elseif($strana=='slike' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/slike')
        @elseif($strana=='opis' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/opis')
        @elseif($strana=='seo' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/seo')
        @elseif($strana=='karakteristike' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/karakteristike')
        @elseif($strana=='vezani_artikli' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/vezani_artikli')
        @elseif($strana=='srodni_artikli' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/srodni_artikli')
        @elseif($strana=='product_akcija' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/akcija')
        @elseif($strana=='product_akcija_b2b' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/product_akcija_b2b')
        @elseif($strana=='dodatni_fajlovi' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/dodatni_fajlovi')
        @elseif($strana=='generisane' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/generisane')
        @elseif($strana=='dobavljac_karakteristike' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/dobavljac_karak')
        @elseif($strana=='proizvodjac' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/proizvodjac')
        @elseif($strana=='mesto' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/mesto')
        @elseif($strana=='tip_artikla' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/tipovi')
        @elseif($strana=='kupovina_na_rate' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/kupovina_na_rate')
        @elseif($strana=='labela_artikla' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/labela_artikla')
        @elseif($strana=='katalog' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/katalog')
        @elseif($strana=='bodovi' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/bodovi')
        @elseif($strana=='jedinica_mere' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/jedinice_mere')
        @elseif($strana=='poreske_stope' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/poreske_stope')
        @elseif($strana=='stanje_artikla' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/stanje_artikla')
        @elseif($strana=='status_narudzbine' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/status_narudzbine')
        @elseif($strana=='kurirska_sluzba' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/kurirska_sluzba')
        @elseif($strana=='konfigurator' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/konfigurator')
        @elseif($strana=='upload_image' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/upload_image')
        @elseif($strana=='analitika' AND Admin_model::check_admin(array('ANALITIKA')))
            @include('admin/analitika')
        @elseif($strana=='narudzbina' AND Admin_model::check_admin(array('NARUDZBINE')) AND Admin_model::check_admin(array('NARUDZBINE_PREGLED')))
            @include('admin/narudzbina')
        @elseif($strana=='narudzbina_search' AND Admin_model::check_admin(array('NARUDZBINE')) AND Admin_model::check_admin(array('NARUDZBINE_PREGLED')))
            @include('admin/narudzbina_search')
        @elseif($strana=='narudzbina_stavka' AND Admin_model::check_admin(array('NARUDZBINE')) AND Admin_model::check_admin(array('NARUDZBINE_PREGLED')))
            @include('admin/narudzbina_stavka')  
        @elseif($strana=='narudzbina_stavka_city' AND Admin_model::check_admin(array('NARUDZBINE')) AND Admin_model::check_admin(array('NARUDZBINE_PREGLED')))
            @include('admin/narudzbina_stavka_city')
        @elseif($strana=='vesti' AND Admin_model::check_admin(array('VESTI')) AND Admin_model::check_admin(array('VESTI_PREGLED')))
            @include('admin/vesti')
        @elseif($strana=='footer_setting' AND Admin_model::check_admin(array('FUTER')) AND Admin_model::check_admin(array('FUTER_PREGLED')))
            @include('admin/footer_setting')
        @elseif($strana=='vest' AND Admin_model::check_admin(array('VESTI')) AND Admin_model::check_admin(array('VESTI_PREGLED')))
            @include('admin/vest_single')
        @elseif($strana=='kupci_partneri' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/kupci_partneri')
        @elseif($strana=='kupci' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/kupci')
        @elseif($strana=='partneri' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/partneri')
        @elseif($strana=='kupac' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/kupac')
        @elseif($strana=='partner' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/partner')
        @elseif($strana=='korisnici_partnera' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/partner_users')
        @elseif($strana=='korisnik_partnera' AND Admin_model::check_admin(array('KUPCI_I_PARTNERI')) AND Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
            @include('admin/partner_user')
        @elseif($strana=='administratori' AND Admin_model::check_admin(array('KORISNICI')))
            @include('admin/administratori')
        @elseif($strana=='grupa_modula' AND Admin_model::check_admin(array('KORISNICI')))
            @include('admin/grupa_modula')
        @elseif($strana=='logovi' AND Admin_model::check_admin(array('KORISNICI')))
            @include('admin/logovi')    
        @elseif($strana=='crm')
            @include('admin/crm_zadaci')
        @elseif($strana=='komentari' AND Admin_model::check_admin(array('KOMENTARI')) AND Admin_model::check_admin(array('KOMENTARI_PREGLED')))
            @include('admin/komentari')
        @elseif($strana=='komentar' AND Admin_model::check_admin(array('KOMENTARI')) AND Admin_model::check_admin(array('KOMENTARI_PREGLED')))
            @include('admin/komentar')
        @elseif($strana=='osobine' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/osobine')
        @elseif($strana=='product_osobine' AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
            @include('admin/product_osobine')
        @elseif($strana=='nacin_placanja' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/nacin_placanja')
        @elseif($strana=='nacin_isporuke' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/nacin_isporuke')
        @elseif($strana=='troskovi_isporuke' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/troskovi_isporuke')
        @elseif($strana=='kurs' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/kurs')
        @elseif($strana=='file_upload' AND Admin_model::check_admin(array('WEB_IMPORT')) AND Admin_model::check_admin(array('WEB_IMPORT_PREGLED')))
            @include('admin/file_upload')
        @elseif($strana=='import_partner_proizvodjac' AND Admin_model::check_admin(array('WEB_IMPORT')) AND Admin_model::check_admin(array('WEB_IMPORT_PREGLED')))
            @include('admin/import_partner_proizvodjac')
        @elseif($strana=='definisane_marze' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/definisane_marze')
        @elseif($strana=='vauceri' AND Admin_model::check_admin(array('SIFARNICI')) AND Admin_model::check_admin(array('SIFARNICI_PREGLED')))
            @include('admin/vauceri')
        @elseif($strana=='garancije' && Admin_model::check_admin(array('GARANCIJE')) AND Admin_model::check_admin(array('GARANCIJE_PREGLED')))
            @include('admin/garancije')
        @elseif($strana=='garancija' && Admin_model::check_admin(array('GARANCIJE')) AND Admin_model::check_admin(array('GARANCIJE_PREGLED')))
            @include('admin/garancija')
        @elseif($strana=='rma' && Admin_model::check_admin(array('RMA')) AND Admin_model::check_admin(array('RMA_PREGLED')))
            @include('admin/rma')
        @elseif($strana=='operacije' && Admin_model::check_admin(array('RMA')) AND Admin_model::check_admin(array('RMA_PREGLED')))
            @include('admin/operacije')
        @elseif($strana=='radni_nalog_prijem' && Admin_model::check_admin(array('RMA')) AND Admin_model::check_admin(array('RMA_PREGLED')))
            @include('admin/radni_nalog_prijem')
        @elseif($strana=='radni_nalog_opis_rada' && Admin_model::check_admin(array('RMA')) AND Admin_model::check_admin(array('RMA_PREGLED')))
            @include('admin/radni_nalog_opis_rada')
        @elseif($strana=='radni_nalog_dokumenti' && Admin_model::check_admin(array('RMA')) AND Admin_model::check_admin(array('RMA_PREGLED')))
            @include('admin/radni_nalog_dokumenti')
        @elseif($strana=='radni_nalog_predaja' && Admin_model::check_admin(array('RMA')) AND Admin_model::check_admin(array('RMA_PREGLED')))
            @include('admin/radni_nalog_predaja')
        @elseif($strana=='sekcija_stranice' && Admin_model::check_admin(array('SEKCIJA_STRANICE')) AND Admin_model::check_admin(array('SEKCIJA_STRANICE_PREGLED')))
            @include('admin/sekcija_stranice')        
        @endif
        @include('admin.partials.files')
        @include('admin.partials.gallery')
    </div> <!-- end of flex-wrapper -->

    <!--  articles criteria -->   
    <?php if(isset($criteria)){ ?><script> var criteria = new Array("{{ $criteria['grupa_pr_id'] ? $criteria['grupa_pr_id'] : 0 }}", "{{ isset($criteria['proizvodjac']) ? $criteria['proizvodjac'] : 0 }}", 
    "{{ isset($criteria['dobavljac']) ? $criteria['dobavljac'] : 0 }}", "{{ isset($criteria['tip']) ? $criteria['tip'] : 0 }}", "{{ isset($criteria['labela']) ? $criteria['labela'] : 0 }}","{{ isset($criteria['karakteristika']) ? $criteria['karakteristika'] : 0 }}", "{{ isset($criteria['magacin']) ? $criteria['magacin'] : 0 }}", "{{ isset($criteria['exporti']) ? $criteria['exporti'] : 0 }}","{{ isset($criteria['katalog']) ? $criteria['katalog'] : 0 }}", "{{ isset($criteria['filteri']) ? $criteria['filteri'] : 'nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteria['flag']) ? $criteria['flag'] : 0 }}", "{{ isset($criteria['search']) ? $criteria['search'] : 0 }}", "{{ isset($criteria['nabavna']) ? $criteria['nabavna'] : 'nn-nn' }}"); </script><?php }else{ ?><script> var criteria; </script><?php } ?> 

    <!-- import criteria -->
     <?php if(isset($criteriaImport)){ ?><script> var criteriaImport = new Array("{{ $criteriaImport['grupa_pr_id'] ? $criteriaImport['grupa_pr_id'] : 0 }}", "{{ isset($criteriaImport['proizvodjac']) ? $criteriaImport['proizvodjac'] : 0 }}", 
     "{{ isset($criteriaImport['dobavljac']) ? $criteriaImport['dobavljac'] : 0 }}", "{{ isset($criteriaImport['filteri']) ? $criteriaImport['filteri'] : 'nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteriaImport['search']) ? $criteriaImport['search'] : 0 }}", "{{ isset($criteriaImport['nabavna']) ? $criteriaImport['nabavna'] : 'nn-nn' }}");</script><?php }else{ ?><script> var criteriaImport; </script><?php } ?>
    <script> var all_ids = {{ isset($all_ids) ? $all_ids : 'null' }}; var fast_insert_option = '{{ AdminOptions:: gnrl_options(3030) }}'; </script>


    <input type="hidden" id="base_url" value="{{AdminOptions::base_url()}}" />
    <input type="hidden" id="live_base_url" value="{{AdminOptions::live_base_url()}}" />
    <script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/jquery-ui.min.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_funkcije.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
   
    @if(in_array($strana, array('stranice','vest','opis','karakteristike','product_short','grupe','operacije','slajder','footer','sekcija_stranice')))
        <script type="text/javascript" src="{{ AdminOptions::base_url()}}js/tinymce_5.1.3/tinymce.min.js"></script>
        <script type="text/javascript">  
            tinymce.init({
                selector: ".special-textareas", 
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",    
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste",
                    "autoresize"
                ],       
                //IMAGE
                paste_data_images: true,  
                // Advanced in insert->media_poster
                media_poster: false,
                media_alt_source: false, 

                extended_valid_elements : "script[language|type|async|src|charset]",
                end_container_on_empty_block: true,
 
                contextmenu: "image",
                toolbar: "insertfile undo redo | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | antrfile | glosa | forecolor backcolor | gallery | files",  
                setup: function (editor) {
                    editor.ui.registry.addButton('gallery', {
                        text: 'Gallery',
                            onAction: function () {
                                if($('#JSGalleryModal').length > 0){
                                    $("#JSSelectedGalleryImages").html('<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">'+translate('Niste izabrali sliku.')+'</span>');
                                    upload_image_page = 1;
                                     $.ajax({
                                         type: "POST",
                                         url: base_url + 'admin/ajax/gallery-content',
                                         data:  {page: upload_image_page},
                                         success: function(response) {
                                            upload_image_max_page = response.max_page;
                                            $('#JSGalleryModal').find('.JSContent').html(response.content);
                                            $('#JSGalleryModal').foundation('reveal', 'open');
                                         }
                                     });
                                }
                            }
                    }),
                    editor.ui.registry.addButton('files', {
                        text: 'Files',
                            onAction: function () {
                                if($('#JSFilesModal').length > 0){
                                    $("#JSSelectedFiles").html('<span id="JSNotSelectFile" class="options-title-img column medium-12">'+translate('Niste izabrali fajl.')+'</span>');
                                    upload_file_page = 1;
                                     $.ajax({
                                         type: "POST",
                                         url: base_url + 'admin/ajax/files-content',
                                         data:  {page: upload_file_page},
                                         success: function(response) {
                                            upload_image_max_page = response.max_page;
                                            $('#JSFilesModal').find('.JSFileContent').html(response.content);
                                            $('#JSFilesModal').foundation('reveal', 'open');
                                         }
                                     });
                                }
                            }
                    });
                }
            });
        </script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_sekcija_stranice.js" type="text/javascript"></script>
    @endif

    @if($strana=='product' OR $strana=='product_short' OR $strana=='generisane' OR $strana=='dobavljac_karakteristike' OR $strana=='vezani_artikli' OR $strana=='srodni_artikli' OR $strana=='upload_image' OR $strana=='dodatni_fajlovi')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_product.js" type="text/javascript"></script>
    @endif
    @if($strana=='slajder') 
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_slajderi.js" type="text/javascript"></script>
    @elseif($strana=='footer') 
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_footer.js" type="text/javascript"></script>
    @elseif($strana=='artikli')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_articles.js" type="text/javascript"></script>
    @elseif($strana=='grupe') 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_grupe.js" type="text/javascript"></script>    @elseif($strana=='rma-operacije') 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_grupe.js" type="text/javascript"></script>
    @elseif($strana=='product_akcija' OR $strana=='product_akcija_b2b') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_artikal_akcija.js" type="text/javascript"></script>
    @elseif($strana=='slike') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_slike.js" type="text/javascript"></script>
    @elseif($strana=='web_import') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_import.js" type="text/javascript"></script>
    @elseif($strana=='proizvodjac' OR $strana=='tip_artikla' OR $strana=='kupovina_na_rate' OR $strana=='jedinica_mere' OR $strana=='poreske_stope' OR $strana=='stanje_artikla' OR $strana=='status_narudzbine' OR $strana=='kurirska_sluzba' OR $strana=='kurirska_sluzba' OR $strana=='konfigurator' OR $strana=='mesto' OR $strana=='bodovi' OR $strana=='labela_artikla' OR $strana=='katalog' OR $strana=='ankete')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_support.js" type="text/javascript"></script> 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_katalog.js" type="text/javascript"></script> 
    @elseif($strana=='pocetna' OR $strana=='narudzbina') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_narudzbine.js" type="text/javascript"></script>
    @elseif($strana=='grupa_modula' OR $strana=='administratori' OR $strana=='logovi') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_permission.js" type="text/javascript"></script>
    @elseif($strana=='analitika') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_analitika.js" type="text/javascript"></script>    
    @elseif($strana=='osobine' OR $strana=='product_osobine') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_osobine.js" type="text/javascript"></script> 
    @elseif($strana=='nacin_placanja') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_nacin_placanja.js" type="text/javascript"></script> 
    @elseif($strana=='nacin_isporuke') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_nacin_isporuke.js" type="text/javascript"></script>     
    @elseif($strana=='kupci' OR $strana=='kupac') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_kupci.js" type="text/javascript"></script>     
    @elseif($strana=='partneri') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_partneri.js" type="text/javascript"></script>
    @elseif($strana=='kurs')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_kurs.js" type="text/javascript"></script>
    @elseif($strana=='narudzbina_stavka')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_narudzbina_stavka.js" type="text/javascript"></script>
    @elseif($strana=='narudzbina_stavka_city')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_narudzbina_stavka.js" type="text/javascript"></script>
    @elseif($strana=='kontakt' OR $strana=='partner')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_kontakt.js" type="text/javascript"></script>
    @elseif($strana=='import_partner_proizvodjac' OR $strana=='import_podaci_grupa')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_import_data.js" type="text/javascript"></script>
    @elseif($strana=='vesti' OR $strana=='vest')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_vesti.js" type="text/javascript"></script>
    @elseif(in_array($strana,array('radni_nalog_prijem','radni_nalog_opis_rada','radni_nalog_dokumenti','radni_nalog_predaja')))
        <link href="{{ AdminOptions::base_url()}}css/jquery.datetimepicker.min.css" rel="stylesheet">
        <script src="{{ AdminOptions::base_url()}}js/jquery.datetimepicker.full.min.js"></script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_rma.js" type="text/javascript"></script>
    @elseif($strana=='operacije')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_operacije.js" type="text/javascript"></script>
    @elseif($strana=='garancije' OR $strana=='garancija')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_garancije.js" type="text/javascript"></script>
    @elseif($strana=='css')
        <script src="{{ Options::domain() }}js/ace/ace.js" type="text/javascript"></script>
        <script>
            // var editor = ace.edit("editor");
            // editor.session.setMode("ace/mode/css");
            $(function () {
                $('.ace_editor').each(function () {
                    var textarea = $(this);

                    var mode = textarea.data('editor');

                    var editDiv = $('<div>', {
                        position: 'absolute',
                        // width: textarea.width(),
                        // height: textarea.height(),
                        // 'class': textarea.attr('class')
                    }).insertBefore(textarea);

                    textarea.hide();

                    var editor = ace.edit(editDiv[0]);
                    editor.getSession().setValue(textarea.val());
                    editor.getSession().setMode('ace/mode/css');
                    
                    // copy back to textarea on form submit
                    textarea.closest('form').submit(function () {
                        textarea.val(editor.getSession().getValue());
                    })

                });
            });
        </script>
    @endif
    
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_gallery.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_files.js" type="text/javascript"></script>

    <section class="popup more-popup">
        <div class="popup-wrapper">
            <section class="popup-inner">
                <span class="popup-close">X</span>
                <h3>{{ AdminLanguage::transAdmin('Detalji porudžbine') }}</h3>
                <section class="popup-inner2"></section>
            </section>
        </div>
    </section>

    <section class="popup info-confirm-popup info-popup">
        <div class="popup-wrapper">
            <section class="popup-inner">
            </section>
        </div>
    </section>

</body>
</html>