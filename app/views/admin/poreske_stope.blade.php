<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">
		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi poresku stopu') }}</h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/poreske-stope">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
					@foreach(AdminSupport::getPoreskeGrupe(false) as $row)
					<option value="{{ AdminOptions::base_url() }}admin/poreske-stope/{{ $row->tarifna_grupa_id }}" {{ ($tarifna_grupa_id != NULL && ($row->tarifna_grupa_id == $tarifna_grupa_id)) ? 'selected' : '' }} >({{ $row->tarifna_grupa_id }}) {{ $row->naziv }} -> {{$row->porez}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<section class="medium-5 columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/poreske-stope-edit" enctype="multipart/form-data">
					<input type="hidden" name="tarifna_grupa_id" value="{{ $tarifna_grupa_id }}"> 

					<div class="row">
						<div class="columns {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv poreske stope') }}</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>
					</div>

					<div class="row">
						<div class="columns medium-6 {{ $errors->first('porez') ? ' error' : '' }}">
							<label for="porez">{{ AdminLanguage::transAdmin('Porez') }} %</label>
							<input type="text" name="porez" value="{{ Input::old('porez') ? Input::old('porez') : $porez }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>

						<div class="columns medium-6 {{ $errors->first('tip') ? ' error' : '' }}">
							<label for="tip">{{ AdminLanguage::transAdmin('Tip') }}</label>
							<input type="text" name="tip" value="{{ Input::old('tip') ? Input::old('tip') : $tip }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>

						<div class="columns medium-12">
							<label class="inline-block"> 
								<input name="active" type="checkbox" @if($active == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
							<label class="inline-block">
								<input name="default_tarifna_grupa" type="checkbox"  @if($default_tarifna_grupa == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Default') }}
							</label>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($tarifna_grupa_id != null)
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/poreske-stope-delete/{{ $tarifna_grupa_id }}">{{ AdminLanguage::transAdmin('Obriši') }}
						</button>
						@endif
					</div> 
					@endif
				</form>
			</div>
		</section>
	</div>  
</div>  
