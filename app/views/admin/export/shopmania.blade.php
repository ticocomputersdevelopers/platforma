<html>
	<head>
		<title>{{ AdminLanguage::transAdmin('Export Shopmania') }}</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
       
        <style>

* { box-sizing: border-box; }

.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.text-center { text-align: center; }

header { background-color: #f2f2f2; padding: 30px 0; }

.custom-col { padding: 20px 0; }

.custom-btn { background: #a6a6a6; padding: 10px 15px; }

.select2-container--default .select2-search--inline .select2-search__field { margin: 0; }

.select2-results__option {  padding: 0 5px !important; font-size: 14px; }

        </style>
	</head>
	<body>

		<form method="POST" action="{{AdminOptions::base_url()}}export/shopmania-config/{{ $key }}">
			<input type="hidden" name="export_id" value="{{ $export_id }}">
			<input type="hidden" name="kind" value="{{ $kind }}">

			<header class="text-center">
			  	<h1>{{ AdminLanguage::transAdmin('Export - Shopmania') }}</h1>
			</header>

			<div class="row">
			 
				 
				<div class="col-5 custom-col">
						<div><input name="name" type="checkbox" checked> {{ AdminLanguage::transAdmin('Naziv') }}</div>
						<div><input name="category" type="checkbox" checked> {{ AdminLanguage::transAdmin('Kategorije') }}</div>
						<div><input name="category_photo" type="checkbox"> {{ AdminLanguage::transAdmin('Slika kategorije') }}</div>
						<div><input name="manufacturer" type="checkbox" checked> {{ AdminLanguage::transAdmin('Proizvođač') }}</div>
						<div><input name="description_meta" type="checkbox" checked> {{ AdminLanguage::transAdmin('Meta opis') }}</div>
						<div><input name="keywords" type="checkbox" checked> {{ AdminLanguage::transAdmin('Ključne reči') }}</div>
						<div><input name="price" type="checkbox" checked> {{ AdminLanguage::transAdmin('Web Cena') }}</div>
						<div><input name="mp_price" type="checkbox" checked> {{ AdminLanguage::transAdmin('MP Cena') }}</div>
						<div><input name="currency" type="checkbox" checked> {{ AdminLanguage::transAdmin('Valuta') }}</div>
					</div>

				 <div class="col-5 custom-col">
						<div><input name="photo" type="checkbox" checked> {{ AdminLanguage::transAdmin('Slika') }}</div>
						<div><input name="url" type="checkbox" checked> {{ AdminLanguage::transAdmin('URL proizvoda') }}</div>
						<div><input name="availability" type="checkbox" checked> {{ AdminLanguage::transAdmin('Dostupnost') }}</div>
						<div><input name="stock" type="checkbox" checked> {{ AdminLanguage::transAdmin('Zalihe') }}</div>
						<div><input name="weight" type="checkbox" checked> {{ AdminLanguage::transAdmin('Tezina') }}</div>
						<div><input name="tags" type="checkbox" checked> {{ AdminLanguage::transAdmin('Tagovi') }}</div>
						<div><input name="additional_file" type="checkbox"> {{ AdminLanguage::transAdmin('Dodatni fajl') }}</div>
						<div><input name="product_video" type="checkbox"> {{ AdminLanguage::transAdmin('Video proizvoda') }}</div>
						<div><input name="description" type="checkbox" checked> {{ AdminLanguage::transAdmin('Opis') }}</div>
						<div>			
						<select id="grupa_pr_ids" name="grupa_pr_ids[]" multiple="multiple">						
							{{ AdminSupport::select2Groups() }}
						</select>
						</div>
						 
						<input type="checkbox" name="auto_export"> {{ AdminLanguage::transAdmin('Auto') }}
					</div>

				 	<div class="col-12 text-center">
				 		<button class="custom-btn" type="submit">{{ AdminLanguage::transAdmin('Eksportuj') }}</button>
				 	</div>
				 
			</div>
			
		</form>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>
	</body>
	<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
	<script type="text/javascript">
	  	$("#grupa_pr_ids").select2({
	  		placeholder: {{ AdminLanguage::transAdmin('Izaberi grupe') }}
	  	});
	</script>
</html>