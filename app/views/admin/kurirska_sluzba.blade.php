<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">
		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi kurirsku službu') }}  <i class="fa fa-truck"></i></h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/kurirska_sluzba">{{ AdminLanguage::transAdmin('Dodaj novu') }}</option>
					@foreach(AdminSupport::getKurirskaSluzba(true) as $row)
					<option value="{{ AdminOptions::base_url() }}admin/kurirska_sluzba/{{ $row->posta_slanje_id }}" @if($row->posta_slanje_id == $posta_slanje_id) {{ 'selected' }} @endif>{{ $row->naziv }} </option>
					@endforeach
				</select>
			</div>
		</div>

		<section class="medium-5 columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }} {{ $difolt == 1 ? AdminLanguage::transAdmin('Podrazumevani') : '' }}</h1> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/kurirska_sluzba_edit" enctype="multipart/form-data">
					<input type="hidden" name="posta_slanje_id" value="{{ $posta_slanje_id }}"> 
					<div class="row">
						<div class="columns medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Kurirska služba') }}</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>
						<div class="columns medium-6">
							<label>{{ AdminLanguage::transAdmin('Aktivna') }}</label>
							<select name="aktivna" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::aktivnaCheck(Input::old('aktivna') ? Input::old('aktivna') : $aktivna) }}
							</select>
						</div>
					</div>

					<div class="row">
						<div class="columns medium-4">
							<label for="nasa_sifra">{{ AdminLanguage::transAdmin('Naša šifra') }}</label>
							<input type="text" name="nasa_sifra" value="{{ htmlentities(Input::old('nasa_sifra') ? Input::old('nasa_sifra') : $nasa_sifra) }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>
						<div class="columns medium-8"><br>
							<label class="inline-block"> 
								<input name="difolt" type="checkbox"  @if($difolt == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Podrazumevan') }}
							</label>
							
							&nbsp;

							<label class="inline-block">
								<input name="api_aktivna" type="checkbox"  @if($api_aktivna == 1) checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Api podrška') }}
							</label>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($posta_slanje_id != 0)	
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/kurirska_sluzba_delete/{{ $posta_slanje_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
						@endif
					</div> 
					@endif
				</form>
			</div>   
		</section>
	</div> 
</div>
