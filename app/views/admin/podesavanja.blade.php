<section class="settings" id="main-content">

	@if(Admin_model::check_admin())
	<div class="row">  
		<div class="flat-box">
			<label class="text-center">{{ AdminLanguage::transAdmin('Konfigurisanje SWIFT mailer-a') }}</label>

			<label>
				&nbsp; 
				<input class="check_swift" type="checkbox" {{ AdminOptions::gnrl_options(3003) ? 'checked' : '' }}>
				{{ AdminLanguage::transAdmin('Aktivan') }} 
			</label>

			<form action="save_email_options" method="post" class="">
				@if(Session::has('error_msg'))
				{{ Session::get('error_msg') }}
				@endif

				<div class="row">
					<div class="medium-2 columns">
						<label for="email_host">{{ AdminLanguage::transAdmin('Host') }}:</label>
						<input type="text" name="email_host" id="email_host" class="JSMailInput" value="{{ AdminOptions::email_server() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
					</div>

					<div class="medium-2 columns">
						<label for="email_port">{{ AdminLanguage::transAdmin('Port') }}:</label>
						<input type="text" name="email_port" id="email_port" class="JSMailInput" value="{{ AdminOptions::port() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
					</div>

					<div class="medium-2 columns">
						<label for="email_username">{{ AdminLanguage::transAdmin('Username') }}:</label>
						<input type="text" name="email_username" id="email_username" class="JSMailInput" value="{{ AdminOptions::username() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
					</div>

					<div class="medium-2 columns">
						<label for="email_password">{{ AdminLanguage::transAdmin('Password') }}:</label>
						<input type="password" name="email_password" id="email_password" class="JSMailInput" value="{{ AdminOptions::password() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
					</div>

					<div class="medium-2 columns">
						<label for="email_name">{{ AdminLanguage::transAdmin('Name') }}:</label>
						<input type="text" name="email_name" id="email_name" class="JSMailInput" value="{{ AdminOptions::name() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
					</div>

					<div class="medium-2 columns">
						<label for="email_subject">{{ AdminLanguage::transAdmin('Subject') }}:</label>
						<input type="text" name="email_subject" id="email_subject" class="JSMailInput" value="{{ AdminOptions::subject() }}" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>
					</div>
				</div>
				<div class="btn-container center">
					<button class="JSWebOptions setting-button active btn btn-primary JSMailInput" type="submit" {{ AdminOptions::gnrl_options(3003) ? '' : 'disabled' }}>{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
				</div>
			</form>
		</div> 
	</div>
	@endif

<!-- *********** -->

	@if(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id == 5)
	<div class="row">
		<div class="flat-box columns medium-12">
			<div class="text-center" style="margin: 5px 0;">
				<button class="JStoggle_btn btn btn-secondary" data-section_id="2">{{ AdminLanguage::transAdmin('Boje prodavnice') }} <i class="fa fa-chevron-down" style="font-size: 11px;"></i></button>
			</div>

			<div class="JStoggle_content hide" data-section_id="2"> 
				<form action="save-color" method="post">
					<label for="active"> 
						<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3021) == 1 ? 'checked="checked"' : '' }}>
						{{ AdminLanguage::transAdmin('Aktivan') }}
					</label>

					<div class="row"> 
						@foreach(DB::table('prodavnica_boje')->where('aktivan',1)->get() as $boja)
						<div class="custom_colors medium-2 columns">
							<label class="small-10 medium-10 large-10 columns manual-padding" >{{ $boja->title }}:</label>
							<div class="medium-2 small-2 columns manual-padding">	 
								<input type="color" name="{{ $boja->prodavnica_boje_id }}" value="{{ isset($boja->kod) ? $boja->kod : $boja->default_kod }}">
							</div>
						</div>
						@endforeach
					</div>

					<div class="btn-container center">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endif

<!-- *********** -->

	@if(Admin_model::check_admin())
	@if(DB::table('prodavnica_stil')->where('zakljucana',1)->count() == 0 && AdminB2BOptions::web_options(130) != 2)
	<div class="row">
		<div class="columns flat-box">

			<label class="text-center">{{ AdminLanguage::transAdmin('Izbor teme') }}:</label> 

			<ul class="flex flex-stretch">
				@foreach(DB::table('prodavnica_tema')->where('aktivna',1)->orderBy('prodavnica_tema_id','asc')->get() as $row1)
				<li class="theme_choose">
					<div class="theme-box bsh"> 
						<div class="field-group text-center">{{ $row1->naziv }}</div>

						@foreach(DB::table('prodavnica_stil')->where('prodavnica_tema_id',$row1->prodavnica_tema_id)->where('aktivna',1)->orderBy('prodavnica_stil_id','asc')->get() as $row2)
						<div class="row collapse">
							<div class="columns small-10"> 
								<label>{{ $row2->naziv }}</label>
							</div>
							<div class="columns small-2"> 
								<input type="radio" name="JSTheme" class="JSTheme" data-id="{{ $row2->prodavnica_stil_id }}" {{ $row2->izabrana == 1 ? 'checked' : '' }}> 	
							</div>	
						</div>
						@endforeach 
					</div>
				</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif
	@endif

<!-- *********** -->

	@if(AdminOptions::is_shop() or (Session::get('b2c_admin'.AdminOptions::server()) == 0))
	<div class="row">
		<div class="flat-box">
			<div class="text-center" style="margin: 5px 0;">
				<button class="JStoggle_btn btn btn-secondary" data-section_id="3">{{ AdminLanguage::transAdmin('Podešavanja') }} <i class="fa fa-chevron-down" style="font-size: 11px;"></i></button>
			</div> 
			
			<div class="JStoggle_content hide" data-section_id="3"> 
				<ul class="row">
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Dozvoli registraciju korisnika') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::user_registration()==1)
							<button class="JSWebOptions setting-button active" data-id="0" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="0" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="0" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="0" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div> 		
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Dozvoli poručivanje neregistrovanim korisnicima') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::neregistrovani_korisnici()==1)
							<button class="JSWebOptions setting-button active" data-id="1" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="1" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="1" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="1" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Uključi potvrdu registracije korisnika') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::user_cnfirm_registration()==1)
							<button class="JSWebOptions setting-button active" data-id="100" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="100" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="100" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="100" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('PDV u računu') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(311)==1)
							<button class="JSWebOptions setting-button active" data-id="311" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="311" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="311" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="311" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Kupovina na gram') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::kupovina_na_gram()==1)
							<button class="JSWebOptions setting-button active" data-id="320" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="320" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="320" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="320" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Vodjenje lagera') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::vodjenje_lagera()==1)
							<button class="JSWebOptions setting-button active" data-id="103" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="103" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="103" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="103" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>
					@endif

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							@if(AdminOptions::checkB2B())
							<label>{{ AdminLanguage::transAdmin('Vodjenje lagera B2B') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminB2BOptions::vodjenje_lageraB2B()==1)
							<button class="JSWebOptions setting-button active" data-id="210" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="210" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="210" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="210" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
						@endif
					</li>
					@endif

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz akcije na početnoj') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(98)==1)
							<button class="JSWebOptions setting-button active" data-id="98" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="98" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="98" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="98" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz blogova na početnoj') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(99)==1)
							<button class="JSWebOptions setting-button active" data-id="99" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="99" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="99" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="99" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz gen.karakteristika') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(153)==1)
							<button class="JSWebOptions setting-button active" data-id="153" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="153" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="153" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="153" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz sifre na webu') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::prikaz_sifre()==1)

							<button class="JSWebOptions setting-button active" data-id="203" data-status="1" id="JSPrikazDa">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="203" data-status="0" id="JSPrikazNe">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="203" data-status="1" id="JSPrikazDa">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="203" data-status="0" id="JSPrikazNe">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					@if(Admin_model::check_admin()) 
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Vrsta prikaza liste proizvoda') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::product_view()==1)
							<button class="JSWebOptions setting-button" data-id="106" data-status="2">{{ AdminLanguage::transAdmin('Grid') }}</button>
							<button class="JSWebOptions setting-button" data-id="106" data-status="3">{{ AdminLanguage::transAdmin('List') }}</button>
							<button class="JSWebOptions setting-button active" data-id="106" data-status="1">{{ AdminLanguage::transAdmin('Oba prikaza') }}</button>
							@elseif(AdminOptions::product_view()==2)
							<button class="JSWebOptions setting-button active" data-id="106" data-status="2">{{ AdminLanguage::transAdmin('Grid') }}</button>
							<button class="JSWebOptions setting-button" data-id="106" data-status="3">{{ AdminLanguage::transAdmin('List') }}</button>
							<button class="JSWebOptions setting-button " data-id="106" data-status="1">{{ AdminLanguage::transAdmin('Oba prikaza') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="106" data-status="2">{{ AdminLanguage::transAdmin('Grid') }}</button>
							<button class="JSWebOptions setting-button active" data-id="106" data-status="3">{{ AdminLanguage::transAdmin('List') }}</button>
							<button class="JSWebOptions setting-button" data-id="106" data-status="1">{{ AdminLanguage::transAdmin('Oba prikaza') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz kolone u tabeli Artikli i u Narudžbinama(ROBA_ID,SIFRA IS, ŠIFRA DOB, SKU)') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::sifra_view()==1)
							<button class="JSWebOptions setting-button" data-id="200" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions setting-button active" data-id="200" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view()==2)
							<button class="JSWebOptions setting-button active" data-id="200" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions setting-button " data-id="200" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view()==3)
							<button class="JSWebOptions setting-button" data-id="200" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions setting-button active" data-id="200" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view()==4)
							<button class="JSWebOptions setting-button" data-id="200" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions setting-button" data-id="200" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions setting-button active" data-id="200" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@endif
						</div>
					</li>
					@endif

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Uključi Newsletter') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::newsletter()==1)
							<button class="JSWebOptions setting-button active" data-id="104" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="104" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="104" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="104" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz vrste sifre na webu') }} :</label>
						</div>

						@if(AdminOptions::prikaz_sifre()==1)
						<div class="option-col__option">
							@if(AdminOptions::sifra_view_web()==1)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view_web()==2)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button " data-id="201" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view_web()==3)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view_web()==4)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@endif
						</div>
						@else
						<div class="option-col__option" >
							@if(AdminOptions::sifra_view_web()==1)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="2" >{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view_web()==2)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button " data-id="201" disabled data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view_web()==3)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@elseif(AdminOptions::sifra_view_web()==4)
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="2">{{ AdminLanguage::transAdmin('Šifra IS') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="3">{{ AdminLanguage::transAdmin('SKU') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button" data-id="201" disabled data-status="1">{{ AdminLanguage::transAdmin('Roba_id') }}</button>
							<button class="JSWebOptions JSOptionPrikazSifra setting-button active" data-id="201" disabled data-status="4">{{ AdminLanguage::transAdmin('Šifra Dob') }}</button>
							@endif
						</div>
						@endif
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Uključi opciju broj proizvoda po strani') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::product_number()==1)
							<button class="JSWebOptions setting-button active" data-id="107" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="107" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="107" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="107" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Sortiranje proizvoda po min - max ceni , najnoviji artikli i po nazivu artikla') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::product_sort()==1)
							<button class="JSWebOptions setting-button active" data-id="108" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="108" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="108" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="108" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Uključi opciju promena valute') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::product_currency()==1)
							<button class="JSWebOptions setting-button active" data-id="109" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="109" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="109" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="109" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Uključi opciju filtriranja artikala') }} :</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::enable_filters()==1)
							<button class="JSWebOptions setting-button active" data-id="110" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="110" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="110" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="110" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Filtriranje atikla, sužavajući filter') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::filters_type()==1)
							<button class="JSWebOptions setting-button active" data-id="111" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="111" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="111" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="111" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif
						</div>
					</li>
					@endif

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Fiksiran Heder') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::header_type()==1)
							<button class="JSWebOptions setting-button active" data-id="114" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="114" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="114" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="114" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li> 

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Stranica sa svim kategorijama') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::all_category()==1)
							<button class="JSWebOptions setting-button active" data-id="116" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="116" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="116" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="116" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Upoređivanje artikala') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::compare()==1)
							<button class="JSWebOptions setting-button active" data-id="117" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="117" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="117" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="117" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz vezanih artikala') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(118)==1)
							<button class="JSWebOptions setting-button active" data-id="118" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="118" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="118" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="118" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li> 
					@endif

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz dodatnih fajlova') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(120)==1)
							<button class="JSWebOptions setting-button active" data-id="120" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="120" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="120" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="120" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Konfigurator') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(121)==1)
							<button class="JSWebOptions setting-button active" data-id="121" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="121" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="121" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="121" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					@if(Admin_model::check_admin())
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Cena u prodavnici') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(132)==1)
							<button class="JSWebOptions setting-button active" data-id="132" data-status="1">{{ AdminLanguage::transAdmin('Web Cena') }}</button>
							<button class="JSWebOptions setting-button" data-id="132" data-status="0">{{ AdminLanguage::transAdmin('MP cena') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="132" data-status="1">{{ AdminLanguage::transAdmin('Web Cena') }}</button>
							<button class="JSWebOptions setting-button active" data-id="132" data-status="0">{{ AdminLanguage::transAdmin('MP cena') }}</button>
							@endif	
						</div>
					</li>
					@endif

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Zaokruživanje cena na front-panelu') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(152)==1)
							<button class="JSWebOptions setting-button active" data-id="152" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="152" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="152" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="152" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Troškovi isporuke po težini') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(133)==1)
							<button class="JSWebOptions setting-button active" data-id="133" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="133" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="133" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="133" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Dodatni statusi za narudžbinu') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(135)==1)
							<button class="JSWebOptions setting-button active" data-id="135" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="135" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="135" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="135" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					@if(Admin_model::check_admin(array('PODESAVANJA')))
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Troškovi isporuke po vrednosti') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(150)==1)
							<button class="JSWebOptions setting-button active JSCena" data-id="150" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button JSCena" data-id="150" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button JSCena" data-id="150" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active JSCena" data-id="150" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Tagovi') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(134)==1)
							<button class="JSWebOptions setting-button active" data-id="134" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="134" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="134" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="134" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Kupovina na rate') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(313)==1)
							<button class="JSWebOptions setting-button active" data-id="313" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="313" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="313" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="313" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz svih artikala na početnoj') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(136)==1)
							<button class="JSWebOptions setting-button active" data-id="136" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="136" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="136" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="136" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>

					<li class="medium-6 columns option-col">
						<div class="option-col__label"> 
							<label>{{ AdminLanguage::transAdmin('Prikaz najnovijih, najpopularnijih i najprodavanijih artikala na početnoj') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(208)==1)
							<button class="JSWebOptions setting-button active" data-id="208" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="208" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="208" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="208" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div> 
					</li>

					@if(AdminOptions::checkB2B())
					<li class="medium-6 columns option-col">
						<div class="option-col__label">
							<label>{{ AdminLanguage::transAdmin('Prikaz najnovijih, najpopularnijih i najprodavanijih artikala na B2B-u') }}:</label>
						</div>
						<div class="option-col__option">
							@if(AdminOptions::web_options(151)==1)
							<button class="JSWebOptions setting-button active" data-id="151" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="151" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="151" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="151" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endif

					@foreach(DB::table('options')->whereIn('options_id',array(3029,3033,3035) )->orderBy('options_id','asc')->get() as $row)
					<li class="medium-6 columns option-col">
						<div class="option-col__label">  
							<label>{{ $row->naziv }}:</label>
						</div>
						<div class="option-col__option">
							@if($row->int_data == 1 )
							<button class="JSOptions setting-button active" data-id="{{$row->options_id}}" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSOptions setting-button" data-id="{{$row->options_id}}" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSOptions setting-button" data-id="{{$row->options_id}}" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSOptions setting-button active" data-id="{{$row->options_id}}" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endforeach 
					@if(Admin_model::check_admin())
					@foreach(DB::table('options')->whereIn('options_id',array(3063,3064))->orderBy('options_id','asc')->get() as $row)
					<li class="medium-6 columns option-col">
						<div class="option-col__label">  
							<label>{{ $row->naziv }}:</label>
						</div>
						<div class="option-col__option">
							@if($row->int_data == 1 )
							<button class="JSOptions setting-button active" data-id="{{$row->options_id}}" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSOptions setting-button" data-id="{{$row->options_id}}" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSOptions setting-button" data-id="{{$row->options_id}}" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSOptions setting-button active" data-id="{{$row->options_id}}" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif	
						</div>
					</li>
					@endforeach 
					@endif


					@foreach(DB::table('web_options')->whereIn('web_options_id',(Admin_model::check_admin() ? array(314,318,321,322,323) : array(321,322)))->orderBy('web_options_id','asc')->get() as $row)
					<li class="medium-6 columns option-col">
						<div class="option-col__label">  
							<label>{{ $row->naziv }}:</label>
						</div>
						
						<div class="option-col__option"> 
							@if(in_array($row->web_options_id,[321,322]))
								<input type="color" value="{{ $row->str_data }}" data-id="{{ $row->web_options_id }}" class="no-margin JSOptionsChangeStrData">
							@endif

							@if($row->int_data == 1 )
							<button class="JSWebOptions setting-button active" data-id="{{$row->web_options_id}}" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button" data-id="{{$row->web_options_id}}" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@else
							<button class="JSWebOptions setting-button" data-id="{{$row->web_options_id}}" data-status="1">{{ AdminLanguage::transAdmin('Da') }}</button>
							<button class="JSWebOptions setting-button active" data-id="{{$row->web_options_id}}" data-status="0">{{ AdminLanguage::transAdmin('Ne') }}</button>
							@endif 
						</div>
					</li>
					@endforeach
				</ul>  
			</div>
		</div>  
	</div>
	@endif

<!-- *********** -->

	<div class="flat-box"> 
		<div class="row flex flex-stretch">

			<div class="medium-2 columns bsh text-center"> 
				<div>{{ AdminLanguage::transAdmin('Google analitika') }}</div>

				<form action="save-google-analytics" method="post">
					<div class="row collapse">

						<div class="medium-8 columns">
							<label for="google_id">{{ AdminLanguage::transAdmin('ID') }}:</label>
							<input type="text" name="google_id" id="google_id" value="{{ AdminOptions::gnrl_options(3019,'str_data') }}">
							<div class="error">{{ $errors->first('google_id') ? $errors->first('google_id') : '' }}</div>
						</div>

						<div class="medium-4 columns">
							<label for="active"> 
								<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3019) == 1 ? 'checked="checked"' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
						</div> 
					</div>

					<div class="btn-container">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>  


			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Facebook pixel') }}</div>

				<form action="save-facebook-pixel" method="post">
					<div class="row collapse">

						<div class="medium-8 columns">
							<label for="facebook_id">{{ AdminLanguage::transAdmin('Key') }}:</label>
							<input type="text" name="facebook_id" id="facebook_id" value="{{ AdminOptions::gnrl_options(3036,'str_data') }}">
							<div class="error">{{ $errors->first('facebook_id') ? $errors->first('facebook_id') : '' }}</div>
						</div>

						<div class="medium-4 columns">
							<label for="active"> 
								<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3036) == 1 ? 'checked="checked"' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
						</div>
					</div>

					<div class="btn-container">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>


			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Loadbee') }}</div>

				<form action="save-loadbee" method="post">
					<div class="row collapse">

						<div class="medium-8 columns">
							<label for="loadbee_id">{{ AdminLanguage::transAdmin('Key') }}:</label>
							<input type="text" name="loadbee_id" id="loadbee_id" value="{{ AdminOptions::gnrl_options(3058,'str_data') }}">
							<div class="error">{{ $errors->first('loadbee_id') ? $errors->first('loadbee_id') : '' }}</div>
						</div>

						<div class="medium-4 columns">
							<label for="active"> 
								<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3058) == 1 ? 'checked="checked"' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
						</div> 
					</div>

					<div class="btn-container">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>


			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Flixmedia') }}</div>

				<form action="save-flixmedia" method="post">
					<div class="row collapse">
						<div class="medium-8 columns">
							<label for="flixmedia_id">{{ AdminLanguage::transAdmin('Key') }}:</label>
							<input type="text" name="flixmedia_id" id="flixmedia_id" value="{{ AdminOptions::gnrl_options(3059,'str_data') }}">
							<div class="error">{{ $errors->first('flixmedia_id') ? $errors->first('flixmedia_id') : '' }}</div>
						</div>

						<div class="medium-4 columns">
							<label for="active"> 
								<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3059) == 1 ? 'checked="checked"' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
						</div>
					</div>

					<div class="btn-container">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>

			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('PitchPrint') }}</div>

				<form action="save-pitchprint" method="post">
					<div class="row collapse">
						<div class="medium-8 columns">
							<label for="pitchprint_id">{{ AdminLanguage::transAdmin('Key') }}:</label>
							<input type="text" name="pitchprint_id" id="pitchprint_id" value="{{ AdminOptions::gnrl_options(3062,'str_data') }}">
							<div class="error">{{ $errors->first('pitchprint_id') ? $errors->first('pitchprint_id') : '' }}</div>
						</div>
						
						<div class="medium-4 columns">
							<label for="active">
								<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3062) == 1 ? 'checked="checked"' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}:
							</label>
						</div>
					</div>
						<div class="btn-container"> 
							<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						</div>
				</form>
			</div>

			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Čet') }}</div>

				<form action="save-chat" method="post">
					<div class="row collapse">

						<div class="medium-8 columns">
							<label for="key">{{ AdminLanguage::transAdmin('Link') }}:</label>
							<input type="text" name="key" id="key" value="{{ AdminOptions::gnrl_options(3020,'str_data') }}">
							<div>{{ $errors->first('key') ? $errors->first('key') : '' }}</div>
						</div>

						<div class="medium-4 columns">
							<label for="active"> 
								<input type="checkbox" name="active" id="active" {{ AdminOptions::gnrl_options(3020) == 1 ? 'checked="checked"' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
						</div>
					</div>

					<div class="btn-container">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>


			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Sitemap') }}</div>

				<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}admin/sitemap-generate">{{ AdminLanguage::transAdmin('Generiši') }}</a> 
			</div>  


			@if(Admin_model::check_admin() && AdminOptions::is_shop())
			<div class="medium-3 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Intesa podešavanja') }}</div>

				<form action="save-intesa-keys" method="post">
					<div class="row collapse">

						<div class="medium-7 columns">
							<label for="client_id">{{ AdminLanguage::transAdmin('Klijent ID') }}:</label>
							<input type="text" name="client_id" value="{{ AdminOptions::gnrl_options(3023,'str_data') }}">
							<div>{{ $errors->first('client_id') ? $errors->first('client_id') : '' }}</div>
						</div>

						<div class="medium-4 columns medium-offset-1">
							<label for="store_key">{{ AdminLanguage::transAdmin('Store Key') }}</label>
							<input type="text" name="store_key" value="{{ AdminOptions::gnrl_options(3024,'str_data') }}">
							<div>{{ $errors->first('store_key') ? $errors->first('store_key') : '' }}</div>
						</div>
					</div>

					<div class="btn-container">
						<button class="JSWebOptions setting-button active btn btn-primary" type="submit">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
				</form>
			</div>  

			<div class="medium-2 columns bsh text-center">
				<div>{{ AdminLanguage::transAdmin('Backup') }}</div> 

				<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}backup/articles/xml/{{ AdminOptions::gnrl_options(3002,'str_data') }}">{{ AdminLanguage::transAdmin('Artikli') }}</a>
				<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}backup/images/zip/{{ AdminOptions::gnrl_options(3002,'str_data') }}">{{ AdminLanguage::transAdmin('Slike artikala') }}</a> 
			</div> 

			@endif
		</div>
	</div>

<!-- *********** -->

	@if(Admin_model::check_admin()) 
	<div class="flat-box"> 
		<div class="row flex flex-stretch">
			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('B2C i B2B portal') }}:</div>


				<div class="row collapse">
					<div class="columns small-9"> 
						<label class="no-margin">{{ AdminLanguage::transAdmin('B2C') }}</label>
					</div>	

					<div class="columns small-3 text-right"> 
						<input name="JSPortal" value="0" data-id="130" type="radio" {{ !AdminOptions::checkB2B() && (AdminOptions::web_options(130) == 0) ? 'checked' : '' }}> 
					</div> 
				</div>


				<div class="row collapse">
					<div class="columns small-9"> 
						<label class="no-margin">{{ AdminLanguage::transAdmin('B2C i B2B') }} </label>
					</div>

					<div class="columns small-3 text-right"> 
						<input name="JSPortal" value="1" data-id="130" type="radio" {{ AdminOptions::checkB2B() && (AdminOptions::web_options(130) == 1) ? 'checked' : '' }}> 
					</div>
				</div>


				<div class="row collapse">
					<div class="columns small-9"> 
						<label class="no-margin">{{ AdminLanguage::transAdmin('B2B') }} </label>
					</div>

					<div class="columns small-3 text-right"> 
						<input name="JSPortal" value="2" data-id="130" type="radio" {{ AdminOptions::checkB2B() && (AdminOptions::web_options(130) == 2) ? 'checked' : '' }}> 
					</div> 
				</div>
			</div>


			<div class="medium-2 columns text-center bsh">
				<div class="field-group">{{ AdminLanguage::transAdmin('Izbor teme') }}:</div>

				@if(DB::table('prodavnica_stil')->where('zakljucana',1)->count() == 0)
				<button id="JSThemeConfirm" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Zaključaj temu') }}</button>
				@else
				<?php $stil = DB::table('prodavnica_stil')->where('zakljucana',1)->first(); ?>
				<div>Izabrana tema: {{ DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('naziv') }} {{ $stil->naziv }}</div>
				@endif
			</div>


			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Prikaz artikala na web-u') }}:</div>

				<div class="row collapse">
					<div class="columns small-8">
						<label class="no-margin">{{ AdminLanguage::transAdmin('Bez cene') }}</label>
					</div>

					<div class="columns small-4 text-right">	
						<input class="options_active" data-id="1307" type="checkbox" {{ AdminOptions::gnrl_options(1307) == 1 ? 'checked' : '' }}>
					</div> 
					<!-- ********* --> 
					<div class="columns small-8">
						<label class="no-margin">{{ AdminLanguage::transAdmin('Bez slike') }}</label>
					</div>

					<div class="columns small-4 text-right">	
						<input class="options_active" data-id="1308" type="checkbox" {{ AdminOptions::gnrl_options(1308) == 1 ? 'checked' : '' }}>
					</div> 
					<!-- ********* --> 
					<div class="columns small-8">
						<label class="no-margin">{{ AdminLanguage::transAdmin('Bez opisa') }}</label>
					</div>

					<div class="columns small-4 text-right">	
						<input class="options_active" data-id="1305" type="checkbox" {{ AdminOptions::gnrl_options(1305) == 1 ? 'checked' : '' }}>
					</div> 
					<!-- ********* --> 
					<div class="columns small-8">
						<label class="no-margin">{{ AdminLanguage::transAdmin('Bez karakteristika') }}</label>
					</div>

					<div class="columns small-4 text-right">	
						<input class="options_active" data-id="1306" type="checkbox" {{ AdminOptions::gnrl_options(1306) == 1 ? 'checked' : '' }}> 
					</div> 
					<!-- ********* -->
					<div class="columns small-8">
						<label class="no-margin">{{ AdminLanguage::transAdmin('Bez količine') }}</label>
					</div>

					<div class="columns small-4 text-right">	
						<input class="options_active" data-id="3057" type="checkbox" {{ AdminOptions::gnrl_options(3056) == 1 ? 'checked' : '' }}> 
					</div>
				</div>
			</div> 


			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Jezici') }}:</div>  
				@foreach(DB::table('jezik')->orderBy('jezik_id','asc')->get() as $row)
				<div class="row collapse">
					<div class="columns small-8">
						<label class="no-margin">{{ $row->naziv }}</label>
					</div>

					<div class="columns small-4 text-right">
						<input class="JSJezikActive" data-id="{{ $row->jezik_id }}" type="checkbox" {{ $row->aktivan == 1 ? 'checked' : '' }} {{ $row->izabrani == 1 ? 'disabled' : '' }}>
						<input name="jezik_primary" class="JSJezikPrimary" data-id="{{ $row->jezik_id }}" type="radio" {{ $row->izabrani == 1 ? 'checked' : '' }} {{ $row->aktivan == 1 ? '' : 'disabled' }}>	
					</div>	
				</div>
				@endforeach 
			</div>
		</div> 
	</div>

<!-- *********** -->

	<div class="flat-box"> 
		<div class="row flex flex-stretch">
			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Vrste cena') }}:</div>
	 
				@foreach(DB::table('vrsta_cena')->where('vrsta_cena_id','<>',-1)->orderBy('vrsta_cena_id','asc')->get() as $row)
				<div class="row collapse"> 
					<div class="columns small-4"><label class="no-margin">{{ $row->naziv }}</label></div>

					<div class="columns small-6"> 
						<select class="JSVrstaCena" data-id="{{ $row->vrsta_cena_id }}">
							@foreach(DB::table('valuta')->where('ukljuceno',1)->where('valuta_id','<>',-1)->orderBy('valuta_id','asc')->get() as $row2)
							@if($row->valuta_id == $row2->valuta_id)
							<option value="{{ $row2->valuta_id }}" selected>{{ $row2->valuta_sl }}</option>
							@else
							<option value="{{ $row2->valuta_id }}">{{ $row2->valuta_sl }}</option>
							@endif
							@endforeach
						</select>
					</div>

					<div class="columns small-2"> 
						<input class="JSVrstaCenaSelect" data-id="{{ $row->vrsta_cena_id }}" type="checkbox" {{ $row->selected == 1 ? 'checked' : '' }}>
					</div>
				</div>
				@endforeach 
			</div>


			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Magacini') }}:</div>
 
				@foreach(DB::table('orgj')->where('orgj_id','<>',-1)->orderBy('orgj_id','asc')->get() as $row)
				<div class="row collapse"> 
					<div class="columns small-8"><label class="no-margin">{{ $row->naziv }}</label></div>

					<div class="columns small-4 text-right">
						<input class="JSOrgjEnable" data-id="{{ $row->orgj_id }}" type="checkbox" {{ $row->b2c == 1 ? 'checked' : '' }} {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>10,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'disabled' : '' }}>

						<input name="orgj_primary" class="JSOrgjPrimary" data-id="{{ $row->orgj_id }}" type="radio" {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>10,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'checked' : '' }} {{ $row->b2c == 1 ? '' : 'disabled' }}>	
					</div>	
				</div>
				@endforeach 
			</div>


			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Eksporti') }}:</div>
 
				@foreach(DB::table('export')->orderBy('export_id','asc')->get() as $row)
				<div class="row collapse">
					<div class="columns small-10">
						<label class="no-margin">{{ $row->naziv }}</label>
					</div>
					
					<div class="columns small-2 text-right">
						<input class="JSExportEnable" data-id="{{ $row->export_id }}" type="checkbox" {{ $row->dozvoljen == 1 ? 'checked' : '' }}>
					</div>
				</div>
				@endforeach 
			</div>


			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Automatski import') }}:</div>
  
				<label class="text-center"> 
					<input class="auto_import_active" data-id="3002" type="checkbox" {{ AdminOptions::gnrl_options(3002) ? 'checked' : '' }}>
					{{ AdminLanguage::transAdmin('Aktivan') }} 
				</label>
			 
				<label>
					<a href="{{AdminOptions::base_url()}}auto-short-import/{{AdminOptions::sifra()}}">{{ AdminLanguage::transAdmin('Kratki import lagera i cena') }}</a>
				</label>

				<label>
					<a href="{{AdminOptions::base_url()}}auto-short-import/{{AdminOptions::sifra()}}/cene">{{ AdminLanguage::transAdmin('Kratki import cena') }}</a>
				</label>

				<label>
					<a href="{{AdminOptions::base_url()}}auto-short-import/{{AdminOptions::sifra()}}/lager">{{ AdminLanguage::transAdmin('Kratki import lagera') }}</a>
				</label>

				<label>
					<a href="{{AdminOptions::base_url()}}auto-full-import/{{AdminOptions::sifra()}}">{{ AdminLanguage::transAdmin('Kompletni import') }}</a>
				</label> 
			</div>

			<div class="medium-2 columns bsh">
				<div class="field-group text-center">{{ AdminLanguage::transAdmin('Auto import report') }}</div>
 
				<div class="text-center">
					<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}files/logs/full_import.txt">{{ AdminLanguage::transAdmin('Kompletni import') }}</a>
 
					<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}files/logs/short_import.txt">{{ AdminLanguage::transAdmin('Kratki import') }}</a>
				</div> 
			</div>
		</div> 
	</div>

<!-- *********** -->

</div>

@endif
</section>