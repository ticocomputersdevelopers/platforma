<?php 

class TicketingOptions {
	public static function base_url(){
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
	}

	public static function user(){
		$user = null;

		if(Session::has('ticketing_user')){
			$user = DB::table('web_kupac')->where('web_kupac_id',Session::get('ticketing_user'))->first();

		}
		return $user;
	}

	public static function company_name(){
		return DB::table('preduzece')->pluck('naziv');
	}

	public static function company_adress(){
		return DB::table('preduzece')->pluck('adresa');
	}
	public static function company_mesto(){
		return DB::table('preduzece')->pluck('mesto');
	}
	public static function company_phone(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->telefon;
			}
	}
	public static function company_fax(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->fax;
			}
			
	}

	public static function company_ziro(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->ziro;
			}
	}
	public static function company_pib(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->pib;
			}
	}
	public static function company_email(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->email;
			}
	}
	public static function company_delatnost_sifra(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->delatnost_sifra;
			}
	}

	public static function gnrl_options($options_id,$kind='int_data'){
		return DB::table('options')->where('options_id',$options_id)->pluck($kind);
	}

}
