<?php


class Ticketing
{
	
	function radniNalozi($page=1,$limit=20)
	{
		$offset = ($page-1)*$limit;
		$queryString = "SELECT broj_naloga, uredjaj, rn.radni_nalog_id as radni_nalog_id, (SELECT naziv FROM radni_nalog_status WHERE radni_nalog_status_id = status_id) as status, pregledan, roba, hitno, opis_kvara, napomena, broj_fiskalnog_racuna, serijski_broj, proizvodjac FROM radni_nalog rn LEFT JOIN radni_nalog_operacije rno ON rno.radni_nalog_id = rn.radni_nalog_id WHERE kupac_id = ".Session::get('ticketing_user')." GROUP BY rn.radni_nalog_id";

		$data = array();
    	$data['items'] = DB::select($queryString." ORDER BY rn.radni_nalog_id DESC OFFSET ".strval($offset)." LIMIT ".strval($limit)."");
    	$data['count'] = count(DB::select($queryString));
    	$data['limit'] = $limit;
    	return (object) $data;
	}

	function b2bRadniNalozi($page=1,$limit=20)
	{
		$offset = ($page-1)*$limit;
		$queryString = "SELECT broj_naloga, uredjaj, rn.radni_nalog_id as radni_nalog_id, (SELECT naziv FROM radni_nalog_status WHERE radni_nalog_status_id = status_id) as status, pregledan, roba, hitno, opis_kvara, napomena, broj_fiskalnog_racuna, serijski_broj, proizvodjac FROM radni_nalog rn LEFT JOIN radni_nalog_operacije rno ON rno.radni_nalog_id = rn.radni_nalog_id WHERE b2b_partner_id = ".Session::get('b2b_user_'.B2bOptions::server())." GROUP BY rn.radni_nalog_id";

		$data = array();
    	$data['items'] = DB::select($queryString." ORDER BY rn.radni_nalog_id DESC OFFSET ".strval($offset)." LIMIT ".strval($limit)."");
    	$data['count'] = count(DB::select($queryString));
    	$data['limit'] = $limit;
    	return (object) $data;
	}

}