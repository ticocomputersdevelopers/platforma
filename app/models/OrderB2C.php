<?php

class OrderB2C extends Eloquent
{
    protected $table = 'web_b2c_narudzbina';

    protected $primaryKey = 'web_b2c_narudzbina_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'web_b2c_narudzbina_id',
        'web_kupac_id',
        'orgj_id',
        'poslovna_godina_id',
        'vrsta_dokumenta_id',
        'broj_dokumenta',
        'datum_dokumenta',
        'valuta_id',
        'kurs',
        'web_nacin_placanja_id',
        'web_nacin_isporuke_id',
        'iznos',
        'prihvaceno',
        'stornirano',
        'realizovano',
        'napomena',
        'ip_adresa',
        'payment_id',
        'trans_id',
        'track_id',
        'post_date',
        'result_code',
        'auth_code',
        'posta_slanje_id',
        'posta_slanje_poslato',
        'posta_slanje_broj_posiljke',
        'promena',
        'flag_promena_connect',
        'sifra_connect',
        'narudzbina_status_id',
        'popust',
        'trosak_isporuke',
        'poslednja_izmena'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_b2c_narudzbina_id']) ? $this->attributes['web_b2c_narudzbina_id'] : null;
	}
    public function getCustomerIdAttribute()
    {
        return isset($this->attributes['web_kupac_id']) ? $this->attributes['web_kupac_id'] : null;
    }
    public function getDocumentNumberAttribute()
    {
        return isset($this->attributes['broj_dokumenta']) ? $this->attributes['broj_dokumenta'] : null;
    }
    public function getDateAttribute()
    {
        return isset($this->attributes['datum_dokumenta']) ? $this->attributes['datum_dokumenta'] : null;
    }
    public function getStatusAttribute()
    {
        if($this->attributes['prihvaceno'] == 0 && $this->attributes['realizovano'] == 0 && $this->attributes['stornirano'] == 0){
            return 'NEW';
        }else if($this->attributes['prihvaceno'] == 1 && $this->attributes['realizovano'] == 0 && $this->attributes['stornirano'] == 0){
            return 'ACCEPTED';
        }else if($this->attributes['realizovano'] == 1 && $this->attributes['stornirano'] == 0){
            return 'REALIZED';
        }else if($this->attributes['stornirano'] == 1){
            return 'CANCELED';
        }else{
            return null;
        }
    }
    public function getNoteAttribute()
    {
        return isset($this->attributes['napomena']) ? $this->attributes['napomena'] : null;
    }
    public function getUpdatedAtAttribute()
    {
        return isset($this->attributes['poslednja_izmena']) ? $this->attributes['poslednja_izmena'] : null;
    }
      

	protected $appends = array(
    	'id',
        'document_number',
        'date',
        'status',
        'note',
        'updated_at'
    	);

    public function customer(){
        return $this->belongsTo('Customer','web_kupac_id');
    }

    public function items(){
        return $this->hasMany('OrderB2CItem', 'web_b2c_narudzbina_id', 'web_b2c_narudzbina_id');
    }

}