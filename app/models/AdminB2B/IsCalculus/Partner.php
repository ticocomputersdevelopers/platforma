<?php
namespace ISCalculus;
use AdminB2BIS;
use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
			if(isset($partner->pravnofizicko) && $partner->pravnofizicko == 'P'){
		    	$partner_id++;

	            $id_is=$partner->ID;
	            $sifra= isset($partner->sifra) ? $partner->sifra : null;
	            $naziv=isset($partner->interninaziv) ? pg_escape_string(substr($partner->interninaziv,0,255)) : null;
	            $mesto=isset($partner->mesto) ? pg_escape_string(substr($partner->mesto,0,255)) : null;
	            $adresa=isset($partner->adresa) ? pg_escape_string(substr($partner->adresa,0,200) . ' ' . (isset($partner->postanskibroj) ? substr($partner->postanskibroj,0,19) : '')) : (isset($partner->postanskibroj) ? pg_escape_string(substr($partner->postanskibroj,0,80)) : null);
	            $telefon=isset($partner->tel) ?  pg_escape_string(substr($partner->tel,0,255)) : null;
	            $pib=isset($partner->pib) ?  pg_escape_string(substr($partner->pib,0,100)) : null;
	            $mail=isset($partner->email) ? pg_escape_string(substr($partner->email,0,255)) : null;
	            $broj_maticni=isset($partner->matbroj) ? pg_escape_string(substr($partner->matbroj,0,29)) : null;
	            $racun=isset($partner->tekuciracun) ? pg_escape_string(substr($partner->tekuciracun,0,29)) : null;
	            $rabat= isset($partner->rabatkupcu) ? $partner->rabatkupcu : 0;
	            $status="P";

		        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".trim($naziv)."',0,0,1,1,0,NULL,NULL,'".strval($racun)."',NULL,0,NULL,'".trim($status)."',NULL,'".trim($mail)."',0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::integer)";
		    }
		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}