<?php
namespace IsGSM;
use DB;

class Stock {

	public static function table_body($articles,$mapped_articles,$magacin=1){

		$result_arr = array();
		foreach($articles as $article) {
			$sifra = $article->SIFRA;
			$roba_id = isset($mapped_articles[trim(strval($sifra))]) ? $mapped_articles[trim(strval($sifra))] : null;

			if(!is_null($roba_id) && isset($article->KOLICINA) && is_numeric(floatval($article->KOLICINA))){
				$kolicina = floatval($article->KOLICINA);
				if($kolicina < 0){
					$kolicina = 0;
				}

				$result_arr[] = "(".strval($roba_id).",".intval($magacin).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($sifra)."')";
			}

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}

		DB::statement("UPDATE lager t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=lager_temp.sifra_is::varchar AND t.orgj_id=lager_temp.orgj_id");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (sifra_is, orgj_id) NOT IN (SELECT sifra_is, orgj_id FROM lager)";
		}
		//insert
		DB::statement("INSERT INTO lager (SELECT * FROM ".$table_temp.$where.")");

	}

}