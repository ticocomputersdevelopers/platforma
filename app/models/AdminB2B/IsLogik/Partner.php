<?php
namespace IsLogik;

use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();
		$ids_is = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
	    	$partner_id++;

            $id_is=$partner->sifra_kupca_logik;
            $sifra= isset($partner->sifra_kupca_logik) ? $partner->sifra_kupca_logik : null;
            $naziv=!empty($partner->naziv_firme) ? pg_escape_string(substr(Support::convert($partner->naziv_firme),0,250)) : (!empty($partner->ime) ? pg_escape_string(substr(Support::convert($partner->ime),0,250)) : null);
            $mesto=isset($partner->mesto) ? pg_escape_string(substr(Support::convert($partner->mesto),0,250)) : null;
            $postanski_broj=isset($partner->postanski_broj) ? pg_escape_string(substr($partner->postanski_broj,0,100)) : '';
            $adresa=isset($partner->adresa) ? substr(pg_escape_string(Support::convert($partner->adresa)),0,250).' '.$postanski_broj : null;
            $telefon=isset($partner->telefon) ? pg_escape_string(substr($partner->telefon,0,250)) : null;
            $pib=isset($partner->pib) ? pg_escape_string(substr($partner->pib,0,100)) : null;
            $mail=isset($partner->email) ? pg_escape_string(substr($partner->email,0,100)) : null;
            $komercijalista=isset($partner->komercijalista) ? pg_escape_string(substr($partner->komercijalista,0,100)) : null;
            $rabat= isset($partner->rabat) ? $partner->rabat : 0;
            $limit= isset($partner->limit) ? $partner->limit : 0;
            $valuta_placanja= isset($partner->valuta_placanja) ? $partner->valuta_placanja : 0;
            $fax=isset($partner->fax) ? pg_escape_string(substr($partner->fax,0,100)) : '';

            $login=isset($partner->korisnicko_ime) ? pg_escape_string(substr(Support::convert($partner->korisnicko_ime),0,250)) : null;
            $password=isset($partner->b2b_lozinka) ? pg_escape_string(substr($partner->b2b_lozinka,0,250)) : null;

	        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."','".trim($fax)."','".strval($pib)."',NULL,NULL,NULL,NULL,NULL,".strval($rabat).",".$limit.",".$valuta_placanja.",'".trim($naziv)."',0,0,1,1,0,'".$login."','".$password."',NULL,NULL,0,NULL,NULL,NULL,'".trim($mail)."',0,1,0,0,'".$komercijalista."',1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::integer,NULL,NULL,0)";

	        $ids_is[] = $id_is;
		}
		return (object) array("body"=>implode(",",$result_arr),"ids_is" => $ids_is);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}