<?php
namespace IsSpSoft;

use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
			if(isset($partner->id)){
		    	$partner_id++;
	            $id_is=$partner->id;
	            $sifra= $id_is;
	            $naziv_puni=isset($partner->punOpis) ? substr(pg_escape_string($partner->punOpis),0,200) : null;
	            $naziv = $naziv_puni;
	            // $mesto=isset($partner->mesto) ? pg_escape_string(substr($partner->mesto,0,250)) : null;
	            $mesto=isset($partner->mesto) ? substr(pg_escape_string($partner->mesto),0,190).(isset($partner->pttBroj) ? ' '.substr(pg_escape_string($partner->pttBroj),0,9) : '') : null;
	            $adresa=isset($partner->adresa) ? substr(pg_escape_string($partner->adresa),0,250) : null;
	            $telefon=isset($partner->tel) ? pg_escape_string(substr($partner->tel,0,250)) : null;
	            $pib=isset($partner->pib) && is_numeric(intval($partner->pib)) ? pg_escape_string(substr($partner->pib,0,100)) : null;
	            $broj_maticni=isset($partner->mbr) ? substr($partner->mbr,0,30) : null;
	            $mail=isset($partner->eposta) ? substr($partner->eposta,0,100) : null;
	            // $rabat= isset($partner->anRebate) && is_numeric($partner->anRebate) ? floatval($partner->anRebate) : 0;
	            $rabat = 0;

		        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".trim($naziv_puni)."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'".$mail."',0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::INTEGER,NULL,NULL,0,(NULL)::INTEGER,NULL,(NULL)::date)";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		$where = "";
		if(count(DB::select("SELECT id_is FROM partner where id_is is not null and id_is <> ''")) > 0){
			$where = " WHERE partner_temp.id_is NOT IN (SELECT id_is FROM partner where id_is is not null and id_is <> '')";
		}
		//insert
		DB::statement("INSERT INTO partner(id_is,sifra,naziv_puni,naziv,mesto,adresa,telefon,pib,broj_maticni,mail,rabat,drzava_id) SELECT id_is,sifra,naziv_puni,naziv,mesto,adresa,telefon,pib,broj_maticni,mail,rabat,drzava_id FROM ".$table_temp.$where);

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM partner WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}