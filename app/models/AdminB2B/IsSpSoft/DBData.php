<?php
namespace IsSpSoft;

use DB;


class DBData {

	public static function groups(){
		return DB::connection('ortopedija')->select("SELECT * FROM grupaweb");
	}
	public static function subgroups(){
		return DB::connection('ortopedija')->select("SELECT * FROM podgrupaweb");
	}

	public static function articles(){
		return DB::connection('ortopedija')->select("SELECT a.*, (select opis from grupaweb where id=a.idGrupa) as grupa, (select opis from podgrupaweb where id=a.idPodGrupa) as podgrupa FROM artweb a");
	}

	public static function partners(){
		return DB::connection('ortopedija')->select("SELECT * FROM pp2Web");
	}

	public static function partnersCards(){
		return DB::connection('ortopedija')->select("SELECT * FROM fkartdokweb");
	}

	public static function partnerGroupRabat(){
		return DB::connection('ortopedija')->select("SELECT * FROM view_b2bRebateCateg");
	}
	
}