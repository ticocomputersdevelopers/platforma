<?php
namespace IsSpSoft;
use DB;
use Options;

class Article {

	public static function table_body($articles,$mappedGroups){
		$kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
		$defaultGroupId = Support::getNewGrupaId('SP SOFT GUPE');

		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(isset($article->id)){
				$id_is = $article->id;
				$roba_id++;
				$sifra_k++;

				$sifra_is = $id_is;
				$naziv = isset($article->opis) ? Support::convert(substr($article->opis,0,300)) : '';
				// $web_opis = isset($article->acDescr) ? pg_escape_string($article->acDescr) : '';
				$web_opis = '';
				$jedinica_mere_id = isset($article->jm) && $article->jm != '' ? Support::getJedinicaMereId(strtolower($article->jm)) : 1;
				// $proizvodjac_id = isset($article->proizvodjac) && trim($article->proizvodjac) != '' ? Support::getProizvodjacId(pg_escape_string($article->proizvodjac)) : -1;
				$proizvodjac_id = -1;
				$vrednost_tarifne_grupe = isset($article->procPdv) && is_numeric($article->procPdv) ? $article->procPdv : 20;
				$tarifna_grupa_id = Support::getTarifnaGrupaId(strval($vrednost_tarifne_grupe),$vrednost_tarifne_grupe);
				$grupa_pr_id = isset($article->idPodGrupa) && $article->idPodGrupa != '' ? (isset($mappedGroups[$article->idPodGrupa]) ? $mappedGroups[$article->idPodGrupa] : (isset($article->idGrupa) && $article->idGrupa != '' ? (isset($mappedGroups[$article->idGrupa]) ? $mappedGroups[$article->idGrupa] : $defaultGroupId) : $defaultGroupId)) : $defaultGroupId;

				// $barkod = !is_null($article->acCode) && $article->acCode != '' ? "'".pg_escape_string(strval($article->acCode))."'" : "NULL";
				$barkod = "NULL";
				// $grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				// $kursValuta = trim($article->acCurrency) != 'EUR' ? 1 : $kurs;
				$kursValuta = 1;

				$racunska_cena_nc = isset($article->vpCena) && is_numeric(floatval($article->vpCena)) ? (floatval($article->vpCena) * $kursValuta) : 0;
				$mpcena = isset($article->webCena) && is_numeric(floatval($article->webCena)) ? (floatval($article->webCena) * $kursValuta) : 0;
				$web_cena = $mpcena;
				$racunska_cena_a = $racunska_cena_nc;
				$racunska_cena_end = $racunska_cena_nc;

				// $roba_cene = Support::roba_cene($id_is);
				// $stara_cena = !is_null($roba_cene) ? ($roba_cene->racunska_cena_end != $racunska_cena_end ? $roba_cene->racunska_cena_end : $roba_cene->stara_cena) : $racunska_cena_end;
				$stara_cena = 0.00;

				$marza = 0;
				$flag_aktivan = '1';
				// $flag_cenovnik = $racunska_cena_nc > 0 ? '1' : '0';
				$flag_cenovnik = $flag_aktivan;
				$sku = "NULL";
				// $garancija = isset($article->Warranty) && is_numeric($article->Warranty) ? $article->Warranty : "0";
				$garancija = 0;

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,".strval($garancija).",1,".$flag_cenovnik.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_a).",".strval($racunska_cena_end).",".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".$naziv."',".$flag_cenovnik.",NULL,('".date('Y-m-d H:i:s')."')::timestamp,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,".$barkod.",".strval($stara_cena).",0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,".$sku.",(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL)";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		if(count($updated_columns) > 0){
			DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> ''");	
		}

		$id_iss = DB::select("SELECT id_is FROM ".$table_temp." WHERE id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");

		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");

		if(isset($updated_columns['naziv']) || isset($updated_columns['naziv_web'])){
			Support::entityDecode();
		}else{
			$id_iss = array_map(function($item){ return "'".strval($item->id_is)."'"; },$id_iss);
			Support::entityDecode($id_iss);
		}
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM roba WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}