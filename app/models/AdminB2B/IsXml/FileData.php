<?php
namespace IsXml;

use DB;
use File;

use All;


class FileData {

	public static function articles(){
		$file_path = "files/IS/xml/roba.xml";
		$products = array();
		$partner_categories = array();

		if(!File::exists($file_path)){
			return $products;
		}

		$xml_products = simplexml_load_file($file_path);
		File::delete($file_path);
		return $xml_products;
	}

	public static function partners(){
		$file_path = "files/IS/xml/partner.xml";

		$partners = array();
		if(!File::exists($file_path)){
			return $partners;
		}

		$xml_partners = simplexml_load_file($file_path);
		File::delete($file_path);
		return $xml_partners;
	}


}