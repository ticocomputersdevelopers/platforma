<?php
namespace IsRoaming;

use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
			if(isset($partner->id_s)){
		    	$partner_id++;
	            $id_is=$partner->id_s;
	            $sifra= $id_is;
	            $naziv=isset($partner->naziv) && $partner->naziv!='' ? pg_escape_string(substr(Support::convert($partner->naziv),0,250)) : null;
	            $mesto=isset($partner->mesto) && $partner->mesto!='' ? pg_escape_string(substr(Support::convert($partner->mesto),0,250)) : null;
	            $adresa=isset($partner->adresa) && $partner->adresa!='' ? pg_escape_string(substr(Support::convert($partner->adresa),0,250)) : null;
	            $telefon=isset($partner->telefon) && $partner->telefon!='' ? pg_escape_string(substr($partner->telefon,0,250)) : null;
	            $pib=isset($partner->pib) && is_numeric(intval($partner->pib)) ? pg_escape_string(substr($partner->pib,0,100)) : null;
	            $mail=isset($partner->email) && $partner->email!='' ? pg_escape_string(substr($partner->email,0,100)) : null;
	            $komercijalista=isset($partner->komercijalista) && $partner->komercijalista!='' ? pg_escape_string(substr($partner->komercijalista,0,100)) : null;
	            $kontakt_osoba = '';
	            $rabat= isset($partner->rabat) && is_numeric(floatval($partner->rabat)) ? $partner->rabat : 0;
	            $broj_maticni= isset($partner->maticni_broj) && is_numeric(intval($partner->maticni_broj)) ? pg_escape_string(substr($partner->maticni_broj,0,30)) : null;
	            $racun=isset($partner->ziro_racun) && $partner->ziro_racun!='' ? pg_escape_string(substr($partner->ziro_racun,0,200)) : null;

	            $login=$mail;
	            $password=isset($partner->lozinka) && $partner->lozinka!='' ? pg_escape_string(substr($partner->lozinka,0,250)) : null;

	            $id_kategorije = (isset($partner->kategorija_cena) && $partner->kategorija_cena != '') ? Support::getPartnerCategoryId($partner->kategorija_cena) : "(NULL)::integer";
	            $naredni_dug7 = (isset($partner->dospeva7) && is_numeric(floatval($partner->dospeva7))) ? floatval($partner->dospeva7) : 0.00;

		        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".trim($naziv)."',0,0,1,1,0,'".$login."','".$password."','".$racun."',NULL,0,NULL,NULL,NULL,'".trim($mail)."',0,1,0,0,'".$komercijalista."',1,'".$id_is."',(NULL)::integer,NULL,'".$kontakt_osoba."',".$id_kategorije.",".strval($naredni_dug7).")";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}

		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}