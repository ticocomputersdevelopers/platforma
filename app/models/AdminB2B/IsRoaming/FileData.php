<?php
namespace IsRoaming;

use DB;
use File;


class FileData {

	public static function articles(){
		$file_path = "files/IS/xml/roba.xml";
		$products = array();
		// $partner_categories = array();

		if(!File::exists($file_path)){
			return (object) array('articles' => $products);
		}

		$xml_products = simplexml_load_file($file_path);
		// foreach($xml_products as $xml_product){
		// 	$xml_product_arr = (array) $xml_product;
		// 	$xml_product_arr['cena_nc'] = strval($xml_product->cena);
		// 	$products[] = (object) $xml_product_arr;


			// $partner_categories[] = (object) array('sifra_artikla' => $xml_product->sifra_artikla, 'kategorija_partnera' => 
			// 	array(
			// 		(object) array('naziv' => 'KA', 'cena' => $xml_product->KA),
			// 		(object) array('naziv' => 'LKA', 'cena' => $xml_product->LKA),
			// 		(object) array('naziv' => 'TT', 'cena' => $xml_product->TT),
			// 		(object) array('naziv' => 'RT', 'cena' => $xml_product->RT)
			// 	)
			// );
			
		// }
		$products = $xml_products;

		self::make_backup($file_path);
		return (object) array('articles' => $products);
	}

	public static function articles_prices(){
		$file_path = "files/IS/xml/roba_cene.xml";
		$products_prices = array();

		if(!File::exists($file_path)){
			return (object) array('articles_prices' => $products_prices);
		}
		$xml_products = simplexml_load_file($file_path);

		foreach($xml_products as $xml_product){
			if(!isset($products_prices[strval($xml_product->sifra_artikla)])){
				$products_prices[strval($xml_product->sifra_artikla)] = array();
			}
			array_push($products_prices[strval($xml_product->sifra_artikla)],((object) array('naziv' => strval($xml_product->kategorija_cena), 'cena' => floatval($xml_product->cena), 'popust' => intval($xml_product->popust))));
		}

		self::make_backup($file_path);
		return (object) array('articles_prices' => $products_prices);
	}

	public static function partners(){
		$file_path = "files/IS/xml/partner.xml";

		$partners = array();
		if(!File::exists($file_path)){
			return $partners;
		}

		$partners = simplexml_load_file($file_path);

		self::make_backup($file_path);
		return $partners;
	}

	public static function partners_cards(){
		$file_path = "files/IS/xml/partner_kartica.xml";

		$partners_cards = array();
		if(!File::exists($file_path)){
			return $partners_cards;
		}

		$partners_cards = simplexml_load_file($file_path);

		self::make_backup($file_path);
		return $partners_cards;
	}

	public static function partners_card_items(){
		$file_path = "files/IS/xml/partner_kartica_item.xml";

		$partners_card_items = array();
		if(!File::exists($file_path)){
			return $partners_card_items;
		}

		$partners_card_items = simplexml_load_file($file_path);

		self::make_backup($file_path);
		return $partners_card_items;
	}
	
	public static function address(){
		$file_path = "files/IS/xml/poslovnice.xml";

		$address = array();
		if(!File::exists($file_path)){
			return $address;
		}

		$address = simplexml_load_file($file_path);

		self::make_backup($file_path);
		return $address;
	}

	public static function make_backup($filePath){
		if(File::exists($filePath)){
			$dirPath = 'files/IS/backup/'.date('Y-m-d');
			File::makeDirectory($dirPath,0777,true,true);

			$fileArr = explode('/',$filePath);
			$fileName = $fileArr[count($fileArr)-1];

			File::copy($filePath,$dirPath.'/'.date('H:i:s').'-'.$fileName);
			File::delete($filePath);
		}
	}

}