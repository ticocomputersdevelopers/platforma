<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;

class PametnoRS {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, datum_akcije_do,
			(SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, 
			(SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = 
			(SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = 
			(SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina,(
			CASE WHEN akcijska_cena > 0 THEN akcijska_cena ELSE 0 END) as akcijska FROM roba WHERE roba_id IN 
			(SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='xml'){
			self::xml_exe($export_products);
		}
		elseif($kind=='xls'){
			self::xls_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("artikali");
		$xml->appendChild($root);

		date_default_timezone_get();
		$date = date('Y-m-d');
		foreach($products as $article){

			$product   = $xml->createElement("artikal");

		    Support::xml_node($xml,"sifra",$article->roba_id,$product);
		    Support::xml_node($xml,"kategorija",$article->grupa,$product);
		    Support::xml_node($xml,"naziv_artikla",$article->naziv_web,$product);
		    if ($article->akcijska>0 && $article->datum_akcije_do > $date) {
		    	Support::xml_node($xml,"cena",$article->akcijska,$product);
		    }else{
		    	Support::xml_node($xml,"cena",$article->web_cena,$product);
		    }
		    Support::xml_node($xml,"raspolozivost",$article->kolicina,$product);
		    Support::xml_node($xml,"link_na_artikal",Support::product_link($article->roba_id),$product);
		    Support::xml_node($xml,"link_na_sliku_artikla",Support::major_image($article->roba_id),$product);
			// Support::xml_node($xml,"detaljan_opis",$article->web_opis."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);

			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/pametno-rs/artikli.xml';
		$xml->save($store_path) or die("Error");

		header('Content-type: text/xml');
		// header('Content-Disposition: attachment; filename="artikli.xml"');
		echo file_get_contents($store_path); die;
	}

	public static function xls_exe($products){
        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=export.xls");
       
        echo 'sifra'."\t" . 'kategorija'. "\t" . 'naziv_artikla'. "\t" . 'cena'."\t" . 'raspolozivost'."\t" . 'link_na_artikal'."\t" . 'link_na_sliku_artikla'."\t" . 'detaljan_opis'."\t\n";

        $export_products = '';
		foreach($products as $article){
			$export_products .= $article->roba_id ."\t". Support::encodeTo1250($article->grupa) . "\t" . Support::encodeTo1250($article->naziv_web) . "\t" . $article->web_cena . "\t" . $article->kolicina ."\t". Support::encodeTo1250(Support::product_link($article->roba_id)) ."\t". Support::encodeTo1250(Support::major_image($article->roba_id)) ."\t". Support::encodeTo1250($article->web_opis." ".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike)) ."\t\n";

		}
		echo $export_products;		
	}
}