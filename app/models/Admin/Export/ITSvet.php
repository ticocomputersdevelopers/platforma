<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;

class ITSvet {

	public static function execute($export_id,$kind,$short=false){
		// if(AdminOptions::vodjenje_lagera()==1){
			// $export_products = DB::select("SELECT roba_id, web_cena, (SELECT REPLACE(REPLACE(naziv,';',' '),'\n',',') FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, REPLACE(REPLACE(naziv_web,';',' '),'\n',',') AS itsvetnaziv, web_flag_karakteristike, REPLACE(REPLACE(web_karakteristike,';',' '),'\n',',') AS itsvetkarakteristike, REPLACE(REPLACE(web_opis,';',' '),'\n',',') AS itsvetopis, (SELECT kolicina FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND web_cena > 0");
		// }else{
		// 	$export_products = DB::select("SELECT roba_id, web_cena, (SELECT REPLACE(REPLACE(naziv,';',' '),'\n',',') FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, REPLACE(REPLACE(naziv_web,';',' '),'\n',',') AS itsvetnaziv, web_flag_karakteristike, REPLACE(REPLACE(web_karakteristike,';',' '),'\n',',') AS itsvetkarakteristike, REPLACE(REPLACE(web_opis,';',' '),'\n',',') AS itsvetopis FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND web_cena > 0");
		// }
		$export_products = DB::select("SELECT roba_id, akcija_flag_primeni, akcijska_cena, web_cena, (SELECT REPLACE(REPLACE(naziv,';',' '),'\n',',') FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, REPLACE(REPLACE(naziv_web,';',' '),'\n',',') AS itsvetnaziv, web_flag_karakteristike, REPLACE(REPLACE(web_karakteristike,';',' '),'\n',',') AS itsvetkarakteristike, REPLACE(REPLACE(web_opis,';',' '),'\n',',') AS itsvetopis, (SELECT kolicina FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = 2) AS kolicina FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='csv'){
			self::csv_exe($export_products,$short);
		}
		else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function csv_exe($products,$short=false){
		header('Content-type: text/plain; charset=utf-8');

		foreach($products as $product){
			$sifra = (string) $product->roba_id;
			$akcija = $product->akcija_flag_primeni;
			$akcijska_cena = $product->akcijska_cena;

			if($akcija != 1){
			$web_cena = (string) round($product->web_cena);
			}else{
			$web_cena = (string) round($akcijska_cena);
			}

			// $lager = '1';
			// if(AdminOptions::vodjenje_lagera()==1){				
				// if(isset($product->kolicina) && $product->kolicina != 0){
				// 	$lager = '1';
				// }else{
				// 	$lager = '0';
				// }
			// }
			$lager = '0';
			
			if(!$short){
				$proizvodjac = $product->proizvodjac;
				$model = $product->itsvetnaziv;

				// $opis = strip_tags($product->itsvetopis);
				// $opis .= self::characteristics($product->roba_id,$product->web_flag_karakteristike,$product->itsvetkarakteristike);
				$opis = "";
				$link = AdminOptions::base_url().'artikal/'.AdminOptions::slug_trans(AdminCommon::seo_title($product->roba_id));
				if(strpos($link,';') !== false){
					$link = AdminOptions::base_url();
				}
				echo $sifra."; ".$web_cena."; ".$proizvodjac." ".$model." ".$opis."; ".$lager."; ".$link."\n";
			}else{
				echo $sifra."; ".$web_cena."; ".$lager."\n";
			}
		}
		die;
	}

	public static function characteristics($roba_id,$web_flag_karakteristike,$karakteristike){
		if($web_flag_karakteristike == 0){
			return $karakteristike;
		}
		elseif($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT REPLACE(REPLACE(naziv,';',' '),'\n',',') FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, REPLACE(REPLACE(vrednost,';',' '),'\n',',') AS itsvetvrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$generisane_karakteristike = '';
			foreach($generisane as $row){
				$generisane_karakteristike .= $row->naziv.': '.$row->itsvetvrednost.', ';
			}
			return substr($generisane_karakteristike,0,-2);
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT REPLACE(REPLACE(karakteristika_naziv,';',' '),'\n',',') as itsvetnaziv, REPLACE(REPLACE(karakteristika_vrednost,';',' '),'\n',',') AS itsvetvrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$dobavljac_karakteristike = '';
			foreach($dobavljac as $row){
				$dobavljac_karakteristike .= $row->itsvetnaziv.': '.$row->itsvetvrednost.', ';
			}
			return substr($dobavljac_karakteristike,0,-2);
		}		
	}
}