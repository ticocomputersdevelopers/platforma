<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Gume {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/gume/gume_excel/gume.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
			for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();

				if(!empty($sifra)){
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . addslashes($sifra) . "',";
					//$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . $sifra." ".$worksheet->getCell('B'.$row)->getValue() . "',";//ruske
					$naziv = '';
					if(!empty($worksheet->getCell('F'.$row)->getValue())){
						$naziv .= $worksheet->getCell('F'.$row)->getValue();
					}
					if(!empty($worksheet->getCell('B'.$row)->getValue())){
						$naziv .= " ". $worksheet->getCell('B'.$row)->getValue();
					}
					if(!empty($worksheet->getCell('C'.$row)->getValue())){
						$naziv .= " ". $worksheet->getCell('C'.$row)->getValue();
					}
					if(!empty($worksheet->getCell('D'.$row)->getValue())){
						$naziv .= " ". $worksheet->getCell('D'.$row)->getValue();
					}
					$naziv .= " ( ". $sifra." )";
					if(!empty($worksheet->getCell('G'.$row)->getValue())){
						$naziv .= " ". $worksheet->getCell('G'.$row)->getValue();
					}

					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv)) . "',";
					
					if(!empty($worksheet->getCell('F'.$row)->getValue())){
						$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($worksheet->getCell('F'.$row)->getValue())) . "',";
					}
					if(!empty($worksheet->getCell('G'.$row)->getValue())){
						$sPolja .= " grupa,";				$sVrednosti .= "'" . addslashes(Support::encodeTo1250($worksheet->getCell('G'.$row)->getValue())) . "',";
					}
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($worksheet->getCell('E'.$row)->getValue()*0.75,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}

			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/gume/gume_excel/gume.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

			for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();

				if(!empty($sifra)){
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . addslashes($sifra) . "',";
					//$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . $sifra." ".$worksheet->getCell('B'.$row)->getValue() . "',";//ruske

					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($worksheet->getCell('E'.$row)->getValue()*0.75,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}

			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}