<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class PinComputers {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/pincomputers/pincomputers_xml/pincomputers.xml');
			$products_file = "files/pincomputers/pincomputers_xml/pincomputers.xml";
			$continue = false;
        if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);

			foreach ($products as $product):
				$sifra=$product->attributes();
				$pmp_cena = $product->priceC;
				if(isset($sifra) && isset($product->name) && isset($product->priceA) && $product->priceA>0){
					
					$sPolja = '';
					$sVrednosti = '';
					
					if($product->onStock=="true"){
						$kolicina=1.00;
					}else{
						$kolicina=0.00;
					}
					if(!isset($pmp_cena) || $pmp_cena<=0){
						$pmp_cena=0.00;
					}
					$naziv = pg_escape_string($product->name);
					$grupa = pg_escape_string($product->group);

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv)."',";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($product->group) . "',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($product->producer)) . "',";
					$sPolja .= " model,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($product->model)) . "',";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$product->priceA, 2, '.', ''),1,$kurs,$valuta_id_nc). ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$pmp_cena, 2, '.', ''),1,$kurs,$valuta_id_nc) . ",";
					$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/pincomputers/pincomputers_xml/pincomputers.xml');
			$products_file = "files/pincomputers/pincomputers_xml/pincomputers.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);

			foreach ($products as $product):
				$sifra=$product->attributes();
				$pmp_cena = $product->priceC;
				if(isset($sifra) && isset($product->name) && isset($product->priceA) && $product->priceA>0){
					$sPolja = '';
					$sVrednosti = '';
					
					if($product->onStock=="true"){
						$kolicina=1.00;
					}else{
						$kolicina=0.00;
					}
					if(!isset($pmp_cena) || $pmp_cena<=0){
						$pmp_cena=0.00;
					}

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$product->priceA, 2, '.', ''),1,$kurs,$valuta_id_nc). ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$pmp_cena, 2, '.', ''),1,$kurs,$valuta_id_nc) . ",";
					$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}