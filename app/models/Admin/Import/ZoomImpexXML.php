<?php
namespace Import;
use Import\Support;
use DB;
use File;

class ZoomImpexXML {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/zoomimpexxml/zoomimpexxml.xml');
			$products_file = "files/zoomimpexxml/zoomimpexxml.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
	        
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$sifra=$product->Šifra;
				$naziv=$product->Naziv;	
				$opis=$product->Opis;
				$kolicina=$product->Količina;
				$cena=$product->Cena;		
				$grupa=$product->Kategorija;		
				$images = $product->Slika;
				$barkod = $product->Bar_kod;

				$images = $product->xpath('Slika');
				$flag_slika_postoji = "0";
				$i=0;
				foreach ($images as $slika):
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".$slika."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".$slika."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
				endforeach;		
			

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . $sifra . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($grupa) . "',";
				$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250($opis) . "',";
				if(isset($opis)){
				$sPolja .= " flag_opis_postoji,";		$sVrednosti .= " 1,";	
				}else{
				$sPolja .= " flag_opis_postoji,";		$sVrednosti .= " 0,";	
				}
				$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " '" . $flag_slika_postoji . "',";
				$sPolja .= " cena_nc,";		     		$sVrednosti .= "" . number_format(floatval($cena), 2, '.', '') . ",";
				$sPolja .= " web_cena,";		   		$sVrednosti .= "" . number_format(floatval($cena), 2, '.', '') . ",";
				if($kolicina == 'in stock'){
				$sPolja .= " kolicina";					$sVrednosti .= "1";
				}else{
				$sPolja .= " kolicina";					$sVrednosti .= "0";
				}
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/zoomimpexxml/zoomimpexxml.xml');
			$products_file = "files/zoomimpexxml/zoomimpexxml.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			}
		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$sifra=$product->Šifra;
				$naziv=$product->Naziv;	
				$opis=$product->Opis;
				$kolicina=$product->Količina;
				$cena=$product->Cena;		

				$cena= $cena*$kurs;

				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . $sifra . "',";			
				$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250($opis) . "',";
				$sPolja .= " cena_nc,";		     		$sVrednosti .= "" . number_format(floatval($cena), 2, '.', '') . ",";
				$sPolja .= " web_cena,";		   		$sVrednosti .= "" . number_format(floatval($cena), 2, '.', '') . ",";
				if($kolicina == 'in stock'){
				$sPolja .= " kolicina";					$sVrednosti .= "1";
				}else{
				$sPolja .= " kolicina";					$sVrednosti .= "0";
				}
				
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}


}