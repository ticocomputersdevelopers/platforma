<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Keller {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/keller/keller_excel/keller.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        

	        $podnaziv = '';

	        for ($row = 1; $row <= $lastRow; $row++) {

	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $pmpcena = $worksheet->getCell('I'.$row)->getValue();
	            $cena = $worksheet->getCell('Q'.$row)->getFormattedValue();
	            $c = $worksheet->getCell('C'.$row)->getValue();
	             			            
	            $kolicina=1;
	            $proizvodjac='Karcher';


            	if(empty($sifra) && empty($pmpcena) && !empty($c) && $sifra!='RS' ) {
            		$podnaziv = $c;
            	}else

	            if(!empty($sifra) && is_numeric($sifra) && $sifra!='RS' ){
                	$naziv = $c;
            		$naziv=str_replace("'","", $naziv); 
   	
         	 
	            if(is_numeric($sifra) && isset($sifra) && !empty($cena) && $cena !='l'){           

					$sPolja = '';
					$sVrednosti = '';


					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" .$proizvodjac. " ".Support::encodeTo1250($podnaziv) . " ".Support::encodeTo1250($naziv) . " (".Support::encodeTo1250($sifra) . ")',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" .$proizvodjac. " ',";
					$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmpcena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " kolicina";					$sVrednosti .= " " . number_format($kolicina,2,'.','') . "";

					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
			}	
			}
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/keller/keller_excel/keller.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {

	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $pmpcena = $worksheet->getCell('I'.$row)->getValue();
	            $cena = $worksheet->getCell('Q'.$row)->getFormattedValue();
	            $c = $worksheet->getCell('C'.$row)->getValue();
	             			            
	            $kolicina=1;
	            $proizvodjac='Karcher';


            	if(empty($sifra) && empty($pmpcena) && !empty($c) && $sifra!='RS' ) {
            		$podnaziv = $c;
            	}else

	            if(!empty($sifra) && is_numeric($sifra) && $sifra!='RS' ){
                	$naziv = $c;
            		$naziv=str_replace("'","", $naziv); 
   	
         	 
	            if(is_numeric($sifra) && isset($sifra) && !empty($cena) && $cena !='l'){           

					$sPolja = '';
					$sVrednosti = '';


					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" .$proizvodjac. " ".Support::encodeTo1250($podnaziv) . " ".Support::encodeTo1250($naziv) . " (".Support::encodeTo1250($sifra) . ")',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" .$proizvodjac. " ',";
					$sPolja .= " web_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmpcena,1,$kurs,$valuta_id_nc),2, '.', ''). ",";
					$sPolja .= " kolicina";					$sVrednosti .= " " . number_format($kolicina,2,'.','') . "";

					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
			}	
			}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


