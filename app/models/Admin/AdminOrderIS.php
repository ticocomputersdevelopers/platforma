<?php

class AdminOrderIS {

    public static function orderInfograf($partner_id,$napomena=''){
        $sql = "INSERT INTO narudzbina (partner_id,napomena) VALUES (".strval($partner_id).",'".strval($napomena)."')";
        DB::connection('infograf')->statement($sql);
        $last_order_id = DB::connection('infograf')->select("SELECT MAX(id) as last_order_id FROM narudzbina")[0]->last_order_id;
        return $last_order_id;
    }
    public static function addOrderStavkaInfograf($web_order_id,$article_id,$quantity,$amount,$discount){
        $sql = "INSERT INTO narudzbina_stavka (narudzbina_id,artikal_id,kolicina,cena,rabat) VALUES (".strval($web_order_id).",".strval($article_id).",".strval($quantity).",".strval($amount).",".strval($discount).")";
        DB::connection('infograf')->statement($sql);
    }
    public static function orderConfirmInfograf($order_id){
        $sql = "CALL web_order(".strval($order_id).")";
        $result = DB::connection('infograf')->select($sql);
        if(isset($result[0])){
            return $result[0]->dok;
        }
        return 0;
    }
    public static function createOrderInfograf($orderDetails){
        $partner_id_is = !is_null(AdminOptions::info_sys('infograf')->mp_kupac_id_is) ? AdminOptions::info_sys('infograf')->mp_kupac_id_is : '100';
        $success = false;
        $order_id = 0;

        $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('web');

        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();

        if(!is_null($customer)){
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = strval($orderDetails['web_b2c_narudzbina_id']) .';'. ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'. $delivery .';'. $payment .';'. $orderDetails['napomena'];

            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$orderDetails['web_b2c_narudzbina_id'])->get();

            $order_id = self::orderInfograf($partner_id_is,$note);

            foreach($cartItems as $stavka){
                $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                if(!is_null($roba->id_is) && $roba->id_is != ''){
                    self::addOrderStavkaInfograf($order_id,$roba->id_is,$stavka->kolicina,(($roba->web_cena / 1.2) / $kurs),0);
                }
            }
            
            $order_id = self::orderConfirmInfograf($order_id);
            $success = true;
        }

        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }

    public static function createOrderInfografById($web_order_id){
        $web_order = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_order_id)->first();
        $orderDetails = array(
            'web_b2c_narudzbina_id' => $web_order_id,
            'web_nacin_isporuke_id' => $web_order->web_nacin_isporuke_id,
            'web_nacin_placanja_id' => $web_order->web_nacin_placanja_id,
            'napomena' => $web_order->napomena,
            'web_kupac_id' => $web_order->web_kupac_id
        );
        self::createOrderInfograf($orderDetails);
    }
}
