<?php
namespace DirectImport;
use DB;
use Image;

class Images {

	public static function table_body($articles){

		$result_arr = array();
		$sirina_slike = DB::table('options')->where('options_id',1331)->pluck('int_data');

		foreach($articles as $article) {
    		$roba = DB::table('roba')->where('sifra_is',$article->code)->first();

    		if(!is_null($roba)){
    			$roba_id = $roba->roba_id;
				$images = $article->images;
				$img_count = DB::table('web_slika')->where('roba_id',$roba_id)->count();

				$check_url = false;
				if(count($images) > 0 && $images[0]){
					$itc_headers = @get_headers($images[0]);
					if(!(!$itc_headers || $itc_headers[0] == 'HTTP/1.1 404 Not Found')) {
					    $check_url = true;
					}
				}

				if($check_url){
					foreach($images as $key => $image){
						if($key > ($img_count-1)){
			                try { 
			                    $web_slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
			                    $putanja = $web_slika_id.'.jpg';
			                    $destination = 'images/products/big/'.$putanja;
		                        $content = file_get_contents($image);
		                        file_put_contents($destination,$content);

			                    Image::make($destination)->resize($sirina_slike, null, function ($constraint){ $constraint->aspectRatio(); })->save();

								$result_arr[] = "(".strval($web_slika_id).",".strval($roba_id).",(NULL)::bytea,".strval($key==0?1:0).",-1,NULL,(NULL)::integer,1,'".$destination."',NULL,0,NULL,(NULL)::integer)";		 
			                }
			                catch (Exception $e) {
			                }
						}
					}
				}else{
					foreach($images as $key => $image){
						if($key > ($img_count-1)){
							$image_array = explode('.',$image);
							if(isset($image_array[1]) && in_array($image_array[1], array('jpg','jpeg','png')) && is_numeric($image_array[0]) && DB::table('web_slika')->where('web_slika_id',intval($image_array[0]))->count() == 0){
								$result_arr[] = "(".strval($image_array[0]).",".strval($roba_id).",(NULL)::bytea,".strval($key==0?1:0).",-1,NULL,(NULL)::integer,1,'images/products/big/".$image."',NULL,0,NULL,(NULL)::integer)";
							}
						}
					}
				}

    		}

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_slika'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_slika_temp(".implode(',',$columns).")";

		//insert
		DB::statement("INSERT INTO web_slika (SELECT * FROM ".$table_temp.")");
		DB::statement("SELECT setval('web_slika_web_slika_id_seq', (SELECT MAX(web_slika_id)+1 FROM web_slika), FALSE)");
	}

}