<?php

class AdminSupport {
    public static function getLoggedUser($role=null){
        if(!Session::has('b2c_admin'.AdminOptions::server())){
            return null;
        }

        if(is_null($role)){
            return DB::table('imenik')->where(array('flag_aktivan'=>1, 'imenik_id'=>Session::get('b2c_admin'.AdminOptions::server())))->first();
        }

        if($role=='ADMIN'){
            return DB::table('imenik')->where(array('kvota'=>0.00,'flag_aktivan'=>1, 'imenik_id'=>Session::get('b2c_admin'.AdminOptions::server())))->first();
        }else if($role=='KOMERCIJALISTA'){
            return DB::table('imenik')->where(array('kvota'=>32.00,'flag_aktivan'=>1, 'imenik_id'=>Session::get('b2c_admin'.AdminOptions::server())))->first();
        }
        return null;
    }

    public static function regex(){
        return '/(^[A-Za-z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\'\"\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ\а\б\в\г\д\ђ\е\ж\з\и\ј\к\л\љ\м\н\њ\о\п\р\с\т\ћ\у\ф\х\ц\ч\џ\ш\А\Б\В\Г\Д\Ђ\Е\Ж\З\И\Ј\К\Л\Љ\М\Н\Њ\О\П\Р\С\Т\Ћ\У\Ф\Х\Ц\Ч\Џ\Ш]+$)+/';
    }
    public function one_digit($field, $value, $parameters){
        // if (preg_match("/(?<!\d)\d{1}(?!\d)/",$value)) {
        if (preg_match("/(?i)^(?=.*[a-z])(?=.*\d)/",$value)) {
            return true;
        }else{
            return false;
        }
    }
     public static function pages($parent_id=0){
      if($parent_id==0){
        return DB::table('web_b2c_seo')->where('parrent_id',0)->orWhereNull('parrent_id')->orderBy('rb_strane','asc')->get();
      }else{
        return DB::table('web_b2c_seo')->where('parrent_id',$parent_id)->orderBy('rb_strane','asc')->get();
      }
    }
    public static function parent_pages($parent_id=0,$page_id){
        return DB::table('web_b2c_seo')->where('parrent_id',$parent_id)->where('web_b2c_seo_id','!=',$page_id)->orderBy('rb_strane','asc')->get();
    }
    // public static function page_level($page_id,$level=1){
    //   $page = DB::table('web_b2c_seo')->where('web_b2c_seo_id',$page_id)->first();
    //   if($page->parrent_id == 0){
    //     return $level;
    //   }else{
    //     return self::page_level($page->parrent_id,($level+1));
    //   }
    // }
    public static function page_childs_depth($pages_ids,$count=0){
      $pages = DB::table('web_b2c_seo')->select('web_b2c_seo_id')->whereIn('parrent_id',$pages_ids)->get();
      if(count($pages) == 0){
        return $count;
      }else{
          return self::page_childs_depth(array_map('current',$pages),($count+1));
      }
    }
    public static function potpis(){
        $potpis=DB::table('preduzece')->where('preduzece_id',1)->pluck('potpis');
        if($potpis !=null or $potpis !=''){
            
           return AdminOptions::base_url()."".$potpis;
            
        }
     
    }
    // public static function upload_directory(){
    //     $dir    = "./images/upload";

    //     //return $dir;
 
    //     $files = scandir($dir);
    //         $slike="<ul>";
    //     $br=0;
    //    foreach ($files as $row) {
    //    if($br>1 && $row != 'b2b'){
       
    //        $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='javascript:void(0)' title='Obriši' data-url='/images/upload/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
    //        }
           
        
    //         $br+=1;
    //     }
    //     $slike.="</ul>";
    //     return $slike;
     
    // }
    
    public static function searchQueryString($search_arr,$column){
        $string_query = "";
        foreach($search_arr as $wrd){
            $string_query .= " ".$column." ILIKE '%".$wrd."%' AND";
        }
        return substr($string_query,0,-4);
    }
    
    public static function cena_round($value,$decimale=null)
    {
        is_null($decimale) ? $decimale = AdminOptions::web_options(209) : $decimale = 0;

        // $add_str = '';
        // if($decimale == 0){
        //     $add_str = '.00';
        // }
        // return round($value,$decimale,PHP_ROUND_HALF_UP).$add_str;

        return number_format(round($value, $decimale),$decimale,'.','');
    }

    public static function query_gr_level($grupa_pr_id)
    {
    return DB::select("SELECT grupa_pr_id, parrent_grupa_pr_id, grupa, redni_broj FROM grupa_pr WHERE parrent_grupa_pr_id = ".$grupa_pr_id." ORDER BY redni_broj ASC");
    }

    public static function dobavljac($id) 
    {
      return DB::table('partner')->where('partner_id', $id)->pluck('naziv');
    }
    // public static function tekst_pocetna() 
    // {
    //   return DB::table('web_b2c_seo')->where('web_b2c_seo_id', 1)->pluck('tekst');
    // }

    public static function dobavljac_ime() {
        return DB::table('dobavljac_cenovnik_kolone')->join('partner', 'dobavljac_cenovnik_kolone.partner_id', '=', 'partner.partner_id')->select('dobavljac_cenovnik_kolone.partner_id', 'partner.naziv')->where('dobavljac_cenovnik_kolone.partner_id','!=','-1')->get();
    }
    public static function trajanje_banera($id){ 
        
        foreach(DB::table('baneri')->where('baneri_id',$id)->whereNotNull('datum_do')->whereNotNull('datum_od')->get() as $row){            
            
           
            $do = date_create($row->datum_do);
            $danasnji_dan = date_create();

            $trajanje_banera = date_diff($do, $danasnji_dan);
//var_dump($trajanje_banera);die;
            if($trajanje_banera->invert == 1){
            return $trajanje_banera->days+1;
            }else{
            return 0;     
            }
        }         
    }
    
    // public static function parrent_strana($id){ 

    //  foreach (DB::table('web_b2c_seo')->where('web_b2c_seo_id',$id)->where('parrent_id','>',0)->get() as $row){
    //     $parrent =$row->parrent_id;
    //     if ($parrent > 0) {
    //         return 1;
    //     }else{
    //         return 0;
    //     }
    //     }
    // }

    public static function getProizvodjaci()
    {
        return DB::table('proizvodjac')->select('proizvodjac_id', 'naziv')->where('proizvodjac_id', '<>', -1)->where('proizvodjac_id', '<>', 0)->orderBy('naziv', 'ASC')->get();
    }
    
    public static function getDobavljaci()
    {
        return DB::table('partner')->select('partner_id', 'naziv')->where('partner_id', '<>', -1)->orderBy('naziv', 'ASC')->get();
    }

    public static function getKarakNaziv($grupa_pr_id)
    {   
       
        $childs= DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id))->count() >0;
        if(!$childs){
        return DB::table('grupa_pr_naziv')->where('grupa_pr_id', $grupa_pr_id )->orderBy('rbr', 'ASC')->get();  
        }else{
        return array();
        }
       
    }

    // public static function getKarak()
    // {   
    //     return DB::table('grupa_pr_naziv')->select('grupa_pr_naziv_id', 'naziv')->orderBy('rbr', 'ASC')->get();
       
    // }

    public static function getLevelGroups($grupa_pr_id){
        return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
    }
    public static function selectGroups($grupa_pr_id=null,$disabled=false){
        $render = '
        <option value="0" '.(($grupa_pr_id == 0 || is_null($grupa_pr_id) || $grupa_pr_id == 'null') ? 'selected' : '').'>Izaberite grupu</option>
        <option value="-1" '.($grupa_pr_id == -1 ? 'selected' : '').'>Nedefinisana</option>';

        foreach(self::getLevelGroups(0) as $row_gr){
            $level1 = self::getLevelGroups($row_gr->grupa_pr_id);
            $level1Count = count($level1) > 0 && $disabled;
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected '.($level1Count?'disabled':'').'>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" '.($level1Count?'disabled':'').'>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach($level1 as $row_gr1){
                $level2 = self::getLevelGroups($row_gr1->grupa_pr_id);
                $level2Count = count($level2) > 0 && $disabled;
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected '.($level2Count?'disabled':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" '.($level2Count?'disabled':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach($level2 as $row_gr2){
                    $level3 = self::getLevelGroups($row_gr2->grupa_pr_id);
                    $level3Count = count($level3) > 0 && $disabled;
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected '.($level3Count?'disabled':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" '.($level3Count?'disabled':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    foreach($level3 as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }
    public static function selectProizvodjaci($proizvodjaci=null){
        $proizvodjac_ids = (!is_null($proizvodjaci) && $proizvodjaci != '') ? explode("+",$proizvodjaci) : array();
        $render = '<option value="-1" '.( in_array(-1,$proizvodjac_ids) ? 'selected' : '' ).'>Nedefinisan</option>';
        foreach(AdminSupport::getProizvodjaci() as $proizvodjac){
            if( in_array($proizvodjac->proizvodjac_id ,$proizvodjac_ids) ){
                $render .= '<option value="'. $proizvodjac->proizvodjac_id .'" selected>'. $proizvodjac->naziv .'</option>';
            }else{
                $render .= '<option value="'. $proizvodjac->proizvodjac_id .'">'. $proizvodjac->naziv .'</option>';
            }
        }
        return $render; 
    }

    public static function listGroups(){
        $render = '<ul id="JSListaGrupe" class="fast-add-group" hidden="hidden">';
        foreach(self::getLevelGroups(0) as $row_gr){                
            $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr->grupa_pr_id.'" data-grupa="'.$row_gr->grupa.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</li>';
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr1->grupa_pr_id.'" data-grupa="'.$row_gr1->grupa.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</li>';
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr2->grupa_pr_id.'" data-grupa="'.$row_gr2->grupa.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</li>';
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr3->grupa_pr_id.'" data-grupa="'.$row_gr3->grupa.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</li>';
                    }
                }
            }
        }
        return $render.'<ul>';
    }

    public static function select2Groups($grupa_pr_ids=array()){
        $render = '<option value="-1">Izaberi grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if(in_array($row_gr->grupa_pr_id,$grupa_pr_ids)){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if(in_array($row_gr1->grupa_pr_id,$grupa_pr_ids)){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if(in_array($row_gr2->grupa_pr_id,$grupa_pr_ids)){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        if(in_array($row_gr3->grupa_pr_id,$grupa_pr_ids)){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

   public static function selectParentGroups($grupa_pr_id, $current_group_id=0){
        $current_group_ids = array();
        if($current_group_id > 0){
            AdminCommon::allGroups($current_group_ids,$current_group_id);
        }
        $render = '<option value="0">Izaberite grupu</option>';
        foreach(self::getLevelGroups(0) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.'</option>';
            }else{
                if(!in_array($row_gr->grupa_pr_id,$current_group_ids))  {
                    $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.' ('.$row_gr->web_b2c_prikazi.')</option>';
                }
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.'</option>';
                }else{
                    if(!in_array($row_gr1->grupa_pr_id,$current_group_ids))  {
                        $render .= '<option value="'.$row_gr1->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' ('.$row_gr1->web_b2c_prikazi.')</option>';
                    }
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.'</option>';
                    }else{
                        if(!in_array($row_gr2->grupa_pr_id,$current_group_ids))  {
                            $render .= '<option value="'.$row_gr2->grupa_pr_id.'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' ('.$row_gr2->web_b2c_prikazi.')</option>';
                        }
                    }

                }
            }
        }
        return $render;
    }

    public static function getPoreskeGrupe($active=true)
    {
        $query=DB::table('tarifna_grupa')->where('tarifna_grupa_id','!=',-1);
        if($active){
            $query = $query->where('active', 1);
        }
        return $query->get();
    }

    
    // public static function getPorez($roba_id)
    // {
    //     $tg= DB::table('roba')->where('roba_id',$roba_id)->pluck('tarifna_grupa_id');
    //     $tg1= DB::table('tarifna_grupa')->where('tarifna_grupa',$tg)->pluck('porez');
    //     return $tg1;
    // }
    public static function getTags($roba_id)
    {
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv');
    }
    
    public static function getFlagCene($all=false)
    {
        if($all){
            return DB::table('roba_flag_cene')->select('roba_flag_cene_id', 'naziv')->where('roba_flag_cene_id', '<>', -1)->orderBy('roba_flag_cene_id','ASC')->get();
        }else{
            return DB::table('roba_flag_cene')->select('roba_flag_cene_id', 'naziv')->where('selected',1)->where('roba_flag_cene_id', '<>', -1)->orderBy('roba_flag_cene_id','ASC')->get();
        }
    }
    public static function getFlagStatus($all=false)
    {
        if($all){
            return DB::table('narudzbina_status')->select('narudzbina_status_id', 'naziv')->where('narudzbina_status_id', '<>', -1)->orderBy('narudzbina_status_id','ASC')->get();
        }else{
            return DB::table('narudzbina_status')->select('narudzbina_status_id', 'naziv')->where('aktivan',1)->where('narudzbina_status_id', '<>', -1)->orderBy('narudzbina_status_id','ASC')->get();
        }
    }
    public static function getKurirskaSluzba($all=false)
    {
        if($all){
            return DB::table('posta_slanje')->select('posta_slanje_id', 'naziv')->where('posta_slanje_id', '<>', -1)->orderBy('posta_slanje_id','ASC')->get();
        }else{
            return DB::table('posta_slanje')->select('posta_slanje_id', 'naziv')->where('aktivna',1)->where('posta_slanje_id', '<>', -1)->orderBy('posta_slanje_id','ASC')->get();
        }
    }
    public static function getGrupaKarak($id)
    {
        return DB::table('grupa_pr_naziv')->select('grupa_pr_id', 'naziv', 'active', 'grupa_pr_naziv_id', 'rbr')->where(array('grupa_pr_id'=>$id, 'active'=>1))->orderBy('rbr', 'ASC')->get();
    }

    public static function getVrednostKarak($id,$active=null)
    {
        isset($active) ? $cond = array('grupa_pr_naziv_id'=>$id) : $cond = array('grupa_pr_naziv_id'=>$id, 'active'=>1);
        return DB::table('grupa_pr_vrednost')->select('grupa_pr_naziv_id', 'grupa_pr_vrednost_id', 'rbr', 'naziv', 'active')->where($cond)->orderBy('rbr', 'ASC')->get();
    }
    public static function naziv($roba_id) {
        
        $roba_id=DB::table('web_b2b_narudzbina_stavka')->where('roba_id',$roba_id)->pluck('roba_id');
       
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv');;
        
    }

    public static function getGenerisane($roba_id)
    {
        return DB::table('web_roba_karakteristike')->leftJoin('grupa_pr_naziv','web_roba_karakteristike.grupa_pr_naziv_id','=','grupa_pr_naziv.grupa_pr_naziv_id')
        ->select('grupa_pr_naziv.naziv', 'web_roba_karakteristike.grupa_pr_naziv_id', 'web_roba_karakteristike.grupa_pr_vrednost_id', 'web_roba_karakteristike.rbr', 'web_roba_karakteristike.roba_id', 'web_roba_karakteristike.web_roba_karakteristike_id')->where('roba_id', $roba_id)->orderBy('web_roba_karakteristike.rbr', 'ASC')->get();
    } 

    public static function getTipovi($active=null)
    {
        if($active){
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }else{
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('active',1)->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }
    }
    public static function getTipoviB2C($active=null)
    {
        if($active){
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('prikaz','<>',1)->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }else{
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('prikaz','<>',1)->where('active',1)->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }  
    }
    public static function tip_naziv($tip_id)
    {
        return DB::table('tip_artikla')->where('tip_artikla_id', $tip_id)->pluck('naziv');
    } 
    public static function getJediniceMere()
    {
        return DB::table('jedinica_mere')->select('jedinica_mere_id','naziv')->where('jedinica_mere_id','<>',-1)->where('jedinica_mere_id','<>',0)->orderBy('jedinica_mere','asc')->get();

    }

    public static function find_proizvodjac($proizvodjac_id, $column)
    {
        return DB::table('proizvodjac')->select($column)->where('proizvodjac_id', $proizvodjac_id)->pluck($column);
    }

    public static function find_tip_artikla($tip_artikla_id, $column)
    {
        return DB::table('tip_artikla')->select($column)->where('tip_artikla_id', $tip_artikla_id)->pluck($column);
    }
    public static function find_jedinica_mere($tip_artikla_id, $column)
    {
        return DB::table('jedinica_mere')->select($column)->where('jedinica_mere_id', $tip_artikla_id)->pluck($column);
    }
    public static function find_tarifna_grupa($tarifna_grupa_id, $column)
    {
        return DB::table('tarifna_grupa')->select($column)->where('tarifna_grupa_id', $tarifna_grupa_id)->pluck($column);
    }

  

    public static function find_flag_cene($roba_flag_cene_id, $column)
    {
        return DB::table('roba_flag_cene')->select($column)->where('roba_flag_cene_id', $roba_flag_cene_id)->pluck($column);
    }
    public static function getStatusArticle($roba_id)
    {
        return DB::select("SELECT roba_flag_cene_id FROM roba r WHERE roba_id=".$roba_id."")[0]->roba_flag_cene_id;
    }
    public static function getDobavljKarak($roba_id,$kind=null,$karakteristika_grupa=null)
    {
        return DB::table('dobavljac_cenovnik_karakteristike')->select('dobavljac_cenovnik_karakteristike_id','karakteristika_naziv','karakteristika_vrednost')->where('roba_id', $roba_id)->orderBy('redni_br_grupe','asc')->orderBy('dobavljac_cenovnik_karakteristike_id','asc')->get();
    }

    public static function getGrupeKarak($roba_id)
    {
        $karakteristika_grupe = DB::table('dobavljac_cenovnik_karakteristike')->distinct()->select('karakteristika_grupa','redni_br_grupe')->where('roba_id',$roba_id)->whereNotNull('karakteristika_grupa')->orderBy('redni_br_grupe','asc')->get();

        return array_unique(array_map('current', $karakteristika_grupe));

    }


    public static function getGrupeNazivKarak($roba_id,$karakteristika_grupa)
    {
        return DB::table('dobavljac_cenovnik_karakteristike')->select('dobavljac_cenovnik_karakteristike_id','karakteristika_naziv','karakteristika_vrednost','redni_br_grupe')->where(array('roba_id'=>$roba_id,'karakteristika_grupa'=>$karakteristika_grupa))->orderBy('redni_br_grupe','asc')->get();
    }

    public static function lager($roba_id){
        $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
        $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
        $kolicina = 0;
        if($lager){
            $kolicina += $lager->kolicina;
            if(AdminOptions::web_options(131) == 1){
                $kolicina -= $lager->rezervisano;
            }
        }
        return round($kolicina);
    }

    public static function selectCheck($selected = null){
        $string = '';
        if($selected == 1){
            $string .= '<option value="1" selected>DA</option>'.
                       '<option value="0" >NE</option>';
        }else{
            $string .= '<option value="1" >DA</option>'.
                       '<option value="0" selected>NE</option>';
        } 
        echo $string;      
    }

    public static function aktivnaCheck($aktivna = null){
        $string = '';
        if($aktivna == 1){
            $string .= '<option value="1" selected>DA</option>'.
                       '<option value="0" >NE</option>';
        }else{
            $string .= '<option value="1" >DA</option>'.
                       '<option value="0" selected>NE</option>';
        } 
        echo $string;      
    }
    public static function prikaz($prikaz = null){
        $string = '';
        if($prikaz == 1){
            $string .= '<option value="0">B2C</option>'.
                        '<option value="1" selected>B2C i B2B</option>'.
                        '<option value="2" >B2B</option>'.
                       '<option value="3">Ne prikazuj</option>';
        }elseif($prikaz == 2){
            $string .= 
                      
                       '<option value="2" selected>B2B</option>'.
                       '<option value="3">Ne prikazuj</option>';
        }elseif($prikaz == 0){
            $string .= '<option value="0" selected>B2C</option>'.
                       '<option value="1">B2C i B2B</option>'.
                       '<option value="2">B2B</option>'.
                       '<option value="3">Ne prikazuj</option>';
        }elseif($prikaz == 3){
            $string .= '<option value="0">B2C</option>'.
                       '<option value="1">B2C i B2B</option>'.
                       '<option value="2">B2B</option>'.
                       '<option value="3" selected>Ne prikazuj</option>';
        } 
        echo $string;      
    }

    // public static function rbrCheck($rbr = null){
    //     $string = '';
    //     if($rbr == 1){
    //         $string .= '<option value="1" selected>DA</option>'.
    //                    '<option value="0" >NE</option>';
    //     }else{
    //         $string .= '<option value="1" >DA</option>'.
    //                    '<option value="0" selected>NE</option>';
    //     } 
    //     echo $string;      
    // }

    // public static function lagerDob($roba_id){
    //     $kol_fr = DB::table('dobavljac_cenovnik')->select('kolicina')->where('roba_id',$roba_id)->orderBy('kolicina','desc')->orderBy('web_cena','asc')->first();
    //     if($kol_fr != null){
    //         return intval($kol_fr->kolicina);
    //     }else{
    //         return '';
    //     }
    // }

    public static function nameOfCharact($web_flag_karakteristike){
        if($web_flag_karakteristike == 2){
            return 'Dob.';
        }
        elseif($web_flag_karakteristike == 1){
            return 'Gen.';
        }
        else{
            return 'HTML';
        }
    }

    // public static function statusKarak($roba_id) {
    //     $data = DB::table('roba')->where('roba_id', $roba_id)->pluck('web_karakteristike');
    //     $generisane = DB::table('web_roba_karakteristike')->where('roba_id', $roba_id)->count();
    //     $dobavljaca = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id', $roba_id)->count();
        
    //     if($data != null || $generisane > 0 || $dobavljaca > 0) {
    //         return 'DA';
    //     } else {
    //         return 'NE';
    //     }
        
    // }

    public static function nivoPristupa($nivo_id){
        return DB::table('ac_group')->where(array('ac_group_id'=>intval($nivo_id)))->pluck('naziv');
    }

//uzima log_akcija_id iz tabele log_akcija za zadatu sifru, a ako ne postoji onda upisuje i vraca upisanu
    public static function getLogAkcijaId($sifra_akcije){
        $log_akcija = DB::table('log_akcija')->where('sifra', $sifra_akcije)->pluck('log_akcija_id');
        if(is_null($log_akcija)){
            $naziv= str_replace('_',' ',strtolower($sifra_akcije));
            DB::table('log_akcija')->insert(['sifra' => $sifra_akcije, 'log_akcija_id'=> DB::table('log_akcija')->max('log_akcija_id')+1,'naziv'=>$naziv]);
            DB::statement("SELECT setval('log_akcija_log_akcija_id_seq', (SELECT MAX(log_akcija_id) FROM log_akcija), FALSE)");
            $log_akcija = DB::table('log_akcija')->where('sifra', $sifra_akcije)->pluck('log_akcija_id');
            return $log_akcija;
        }else{
            return $log_akcija;
        }
    }
    public static function saveLog($sifra_akcije,array $ids=array()){
       if(Session::has('b2c_admin'.AdminOptions::server())){
            
            // foreach ($ids as $id) {
            //     $naziv = DB::table('roba')->where('roba_id', $id)->pluck('naziv');
            //     if(is_null($naziv)){
            //         $nazivi[] = 'Nema naziv';
            //     }else{
            //         $nazivi[] = $naziv;
            //     }
            // }
            $nazivi = self::logItemsNamesJson($sifra_akcije, $ids);
           


            $log_akcija_id = self::getLogAkcijaId($sifra_akcije);
            DB::table('log_korisnik')->insert(array('korisnik_id'=>Session::get('b2c_admin'.AdminOptions::server()),'log_akcija_id'=>$log_akcija_id, 'datum'=>date('Y-m-d H:i:s'), 'ids'=>implode(',', $ids), 'naziv_proizvoda'=>$nazivi));
            
            // $filename = "files/logs/users/administrators.txt";
            // $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();        
            // $myfile = fopen($filename, "a");
            // $line = date("d-m-Y H:i:s").' - Korisnik: '.$user->ime.' '.$user->prezime.' ('.$user->imenik_id.') - '.trim($report)."\r\n";
            // fwrite($myfile, $line);
            // fclose($myfile);        
        }
    }

    public static function logItemsNames($data){
        $rows = json_decode($data);
        if(count($rows) > 0){
            return $rows;
        }
        return array();
    }

    public static function logItemsNamesJson($logCode, array $ids = array()){
        $results = array();
        $ids = array_map('intval',$ids);

        if (in_array($logCode, 
            array(
                'ARTIKLI_IZMENI',
                'ARTIKLI_DODAJ',
                'ARTIKLI_OBRISI',
                'FRONT_ARTIKLI_STAVI_NA_WEB',
                'FRONT_ARTIKLI_SKINI_SA_WEBA',
                'FRONT_ARTIKLI_STAVI_NA_PRODAJU',
                'FRONT_ARTIKLI_SKINI_SA_PRODAJE',
                'FRONT_AKCIJA_ARTIKLI_OBRISI',
                'ARTIKLI_DODAJ_TEKST_NAZIV',
                'ARTIKLI_DODAJ_TEKST_OPIS',
                'ARTIKLI_IZMENI_TEKST_NAZIV',
                'ARTIKLI_IZMENI_TEKST_OPIS',
                'ARTIKLI_IMPORT',
                'ARTIKLI_OPIS',
                'ARTIKLI_IZMENA_SEO',
                'ARTIKLI_KARAKTERISTIKE_IZMENI',
                'PRODUCT_OSOBINA_ROBA_DODAJ',
                'PRODUCT_OSOBINA_OBRISI_SVE',
                'ARTIKLI_AKCIJA_IZMENI',
                'ARTIKLI_AKCIJA',
                'ARTIKLI_AKCIJA_POPUST',
                'ARTIKLI_PREBACI_HTML_KARAKTERISTIKE',
                'ARTIKLI_PRIKAZI_NA_WEBU',
                'ARTIKLI_SKLONI_SA_WEBA',
                'ARTIKLI_PRIKAZI_NA_B2B',
                'ARTIKLI_SKLONI_SA_B2B',
                'ARTIKLI_OTKLJUCAJ',
                'ARTIKLI_ZAKLJUCAJ',
                'ARTIKLI_SKLONI',
                'ARTIKLI_AKTIVNO',
                'ARTIKLI_NEAKTIVNO',
                'ARTIKLI_OBRISI_KARAKTERISTIKE',
                'ARTIKLI_DODAJ_KARAKTERISTIKE',
                'ARTIKLI_SLIKE_DODAJ',
                'ARTIKLI_PRERACUNAJ_WEB_CENU',
                'ARTIKLI_PRERACUNAJ_MP_CENU',
                'ARTIKLI_IZMENI_CENE_MARZE',
                'ARTIKLI_IZMENI_CENE_RABATA',
                'ARTIKLI_IZMENI_WEB_CENA',
                'ARTIKLI_IZMENI_MP_CENA',
                'ARTIKLI_OBRISI_SLIKE_VISE',
                'ARTIKLI_OBRISI_SLIKE_GET',
                'ARTIKLI_DODAJ_SLIKE',
                'ARTIKLI_OBRISI_G_KARAKTERISTIKE',
                'ARTIKLI_DODAJ_G_KARAKTERISTIKE',
                'ARTIKLI_FAJLOVI_DODAJ',
                'ARTIKLI_OBRISI_FAJLOVI',
                'ARTIKLI_EKSPORT_DODELA',
                'ARTIKLI_EKSPORT_SKLONI',
                'ARTIKLI_SLIKE_PRIKAZI_PRODAVNICA',
                'ARTIKLI_SLIKE_SKLONI_PRODAVNICA',
                'ARTIKLI_SLIKA_GLAVNA',
                'ARTIKLI_SLIKA_OPIS',
                'ARTIKLI_NAZIV_BRZA_IZMENA',
                'ARTIKLI_OBRISI_SLIKE',
                'ARTIKLI_DEFINISANE_MARZE_DODELI',
                'ARTIKLI_PRODUZENE_GARANCIJE_DODAJ',
                'ARTIKLI_PRODUZENE_GARANCIJE_OBRISI',
                'ARTIKLI_DODATNE_GRUPE_DODELI',
                'ARTIKLI_DODATNE_GRUPE_OBRISI',
                'ARTIKLI_DODATNE_GRUPE_OBRISI_SVE'               
            )
        )) {
            $results = DB::table('roba')->select('naziv_web AS naziv','roba_id AS id')->whereIn('roba_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(            
                'B2B_RABAT_GRUPA_IZMENI'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, b2b_max_rabat from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'maksimalni rabat', 'naziv' => $item->b2b_max_rabat);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'B2B_RABAT_AKCIJSKA_GRUPA_IZMENI'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, b2b_akcijski_rabat from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'maksimalni rabat', 'naziv' => $item->b2b_akcijski_rabat);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_DODAJ_TAG'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, tags from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'tag', 'naziv' => $item->tags);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_TEZINU'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, tezinski_faktor from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'težina', 'naziv' => sprintf("%.0f",$item->tezinski_faktor));

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_PORESKA_STOPA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, tarifna_grupa_id from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $poreska_stopa = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$item->tarifna_grupa_id)->first();

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'poreska stopa', 'naziv' => $poreska_stopa->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_KOLICINA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, tip_cene from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $kolicina = DB::table('lager')->where('roba_id',$item->id)->first();

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'količina', 'naziv' => sprintf("%.0f", $kolicina->kolicina));

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_VRSTU_CENE'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, roba_flag_cene_id from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $vrsta_cene = DB::table('roba_flag_cene')->where('roba_flag_cene_id',$item->roba_flag_cene_id)->pluck('naziv');

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'vrsta cene', 'naziv' => $vrsta_cene);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_TIP'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, tip_cene from roba WHERE roba_id in ('.implode(',', $ids).') AND flag_zakljucan=false');
           $results = array();
            foreach ($pre_results as $item) {

                $tip = DB::table('tip_artikla')->where('tip_artikla_id',$item->tip_cene)->pluck('naziv');

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'tip', 'naziv' => $tip);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_PROIZVODJAC',
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, proizvodjac_id from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id',$item->proizvodjac_id)->pluck('naziv');

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'proizvođač', 'naziv' => $proizvodjac);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(            
                'ARTIKLI_IZMENI_GRUPU',
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, grupa_pr_id from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $grupa = DB::table('grupa_pr')->where('grupa_pr_id',$item->grupa_pr_id)->pluck('grupa');

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'grupa', 'naziv' => $grupa);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_KOLICINA_BRZA_IZMENA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('lager')->where('roba_id',$item->id)->first();

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'količina', 'naziv' => sprintf("%.0f", $addon->kolicina));

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_WEB_CENA_BRZA_IZMENA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv_web as naziv, web_cena from roba WHERE roba_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);
                $record['additional'] = (object) array('addnaziv' => 'web cena', 'naziv' => $item->web_cena);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PRODUCT_OSOBINA_IZMENI',
                'PRODUCT_OSOBINA_OBRISI'
            )
        )){
           
            $item = DB::table('roba')->where('roba_id', $ids['id'])->first();
            $results = array();
            $record = array('id' => $item->roba_id, 'naziv' => $item->naziv_web);
            $osobina_naziv_id = DB::table('osobina_vrednost')->where('osobina_vrednost_id',$ids['vrednost'])->pluck('osobina_naziv_id');
            $vrednost_osobine = DB::table('osobina_vrednost')->where('osobina_vrednost_id',$ids['vrednost'])->pluck('vrednost');
            $addon = DB::table('osobina_naziv')->where('osobina_naziv_id',$osobina_naziv_id)->pluck('naziv');
            $record['additional'] = (object) array('addnaziv' => 'naziv osobine', 'naziv' => $addon);
            $record['additionalKoji'] = 'vrednost osobine';
            $record['additionalKojiNaziv'] = $vrednost_osobine;

            $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'PRODUCT_OSOBINA_OBRISI_GRUPA',
                'PRODUCT_OSOBINA_DODAJ_SVE'
            )
        )){
           
            $item = DB::table('roba')->where('roba_id', $ids['id'])->first();
            $results = array();
            $record = array('id' => $item->roba_id, 'naziv' => $item->naziv_web);
            $addon = DB::table('osobina_naziv')->where('osobina_naziv_id', $ids['naziv'])->first();
            $record['additional'] = (object) array('addnaziv' => 'naziv osobine', 'naziv' => $addon->naziv);

            $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_VEZI',
                'VEZANI_ARTIKLI_OBRISI',
                'VEZANI_ARTIKLI_FLAG_CENA_IZMENI'
            )
        )){
           $pre_results = DB::select('SELECT vezani_roba_id as id, roba_id, (SELECT naziv_web from roba where roba_id = va.vezani_roba_id limit 1) as naziv from vezani_artikli va WHERE vezani_roba_id in ('.implode(',', $ids).') limit 1');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('roba')->where('roba_id',$item->roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $addon->naziv_web);
                $record['additional'] = (object) array('addnaziv' => 'vezan sa artiklom', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'VEZANI_ARTIKLI_IZMENI_CENA'
            )
        )){
            $item = DB::table('vezani_artikli')->where('roba_id', $ids['roba_id'])->where('vezani_roba_id', $ids['vezani_roba_id'])->first();
           $results = array();

                $addon = DB::table('roba')->where('roba_id',$item->roba_id)->first();
                $vezan = DB::table('roba')->where('roba_id',$item->vezani_roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $addon->naziv_web);
                $record['additional'] = (object) array('addnaziv' => 'srodan sa artiklom', 'naziv' => $vezan->naziv_web);
                $record['additionalKoji'] =  'vrednost karakteristike';
                $record['additionalKojiNaziv'] =  $item->cena;

                $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'SRODNI_ARTIKLI_DODAJ',
                'SRODNI_ARTIKLI_OBRISI',
                'SRODNI_ARTIKLI_FLAG_CENA_IZMENI'
            )
        )){
           $pre_results = DB::select('SELECT srodni_roba_id as id, roba_id, (SELECT naziv_web from roba where roba_id = va.srodni_roba_id limit 1) as naziv from srodni_artikli va WHERE srodni_roba_id in ('.implode(',', $ids).') limit 1');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('roba')->where('roba_id',$item->roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $addon->naziv_web);
                $record['additional'] = (object) array('addnaziv' => 'srodan sa artiklom', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'SRODNI_ARTIKLI_IZMENI_KARAK'
            )
        )){
           
           $item = DB::table('srodni_artikli')->where('roba_id', $ids['roba_id'])->where('srodni_roba_id', $ids['srodni_roba_id'])->first();
           $results = array();

                $addon = DB::table('roba')->where('roba_id',$item->roba_id)->first();
                $srodan = DB::table('roba')->where('roba_id',$item->srodni_roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $addon->naziv_web);
                $record['additional'] = (object) array('addnaziv' => 'srodan sa artiklom', 'naziv' => $srodan->naziv_web);
                $record['additionalKoji'] =  'vrednost karakteristike';
                $karak = DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id', $item->grupa_pr_vrednost_id)->pluck('naziv');
                $record['additionalKojiNaziv'] =  $karak;

                $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'ARTIKAL_KARAKTERISTIKE_POZICIJA',
                'ARTIKLI_KARAKTERISTIKA_OBRISI',
                'ARTIKLI_KARAKTERISTIKA_IZMENI',
                'ARTIKLI_KARAKTERISTIKA_DODAJ'
            )
        )){
           $pre_results = DB::select('SELECT dobavljac_cenovnik_karakteristike_id as id, roba_id, karakteristika_grupa as naziv from dobavljac_cenovnik_karakteristike  WHERE dobavljac_cenovnik_karakteristike_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('roba')->where('roba_id',$item->roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $addon->naziv_web);
                $record['additional'] = (object) array('addnaziv' => 'naziv karakteristike', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKAL_KARAKTERISTIKE_NAZIV_POZICIJA',
                'DOBAVLJAC_KARAKTERISTIKE_DODAJ',
                'DOBAVLJAC_KARAKTERISTIKE_IZMENI',
                'DOBAVLJAC_KARAKTERISTIKE_OBRISI'
            )
        )){

           $pre_results = DB::select('SELECT dobavljac_cenovnik_karakteristike_id as id, roba_id, karakteristika_vrednost as naziv, karakteristika_naziv, karakteristika_grupa from dobavljac_cenovnik_karakteristike  WHERE dobavljac_cenovnik_karakteristike_id in ('.implode(',', $ids).') limit 1');
           $results = array();
            foreach ($pre_results as $item) {
                $newAddon = DB::table('roba')->where('roba_id',$item->roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $newAddon->naziv_web);

                $record['additional'] = (object) array('addnaziv' => 'karakteristika', 'naziv' => $item->karakteristika_grupa);

                $record['additionalKoji'] = 'naziv karakteristike';
                $record['additionalKojiNaziv'] = $item->karakteristika_naziv;

                $record['additionalKojiKoji'] = 'vrednost karakteristike';
                $record['additionalKojiKojiNaziv'] = $item->naziv;
                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_POZICIJA',
                'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_DODAJ',
                'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_IZMENI',
                'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_AKTIVNA_IZMENI',
                'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_OBRISI'
            )
        )){

           $pre_results = DB::select('SELECT grupa_pr_vrednost_id as id, grupa_pr_naziv_id, (SELECT naziv from grupa_pr_naziv where grupa_pr_naziv_id = gpv.grupa_pr_naziv_id limit 1) as karakteristika, naziv as naziv from grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id in ('.implode(',', $ids).') limit 1');

           $results = array();
            foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $grupa_pr_naziv_id =  DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$item->grupa_pr_naziv_id)->pluck('grupa_pr_id');
                $newAddon = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_naziv_id)->first();
                $record = array('id' => 'grupa', 'naziv' => $newAddon->grupa);

                $record['additional'] = (object) array('addnaziv' => 'karakteristika', 'naziv' => $item->karakteristika);

                $record['additionalKoji'] = 'vrednost karakteristike';
                $record['additionalKojiNaziv'] = $item->naziv;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'SIFARNICI_GRUPA_KARAKTERISTIKE_DODAJ',
                'SIFARNICI_GRUPA_NAZIV_IZMENI',
                'SIFARNICI_GRUPA_KARAKTERISTIKA_AKTIVNA_IZMENI',
                'SIFARNICI_GRUPA_KARAKTERISTIKA_POZICIJA',
                'GRUPA_KARAKTERISTIKA_OBRISI'
            )
        )){

           $pre_results = DB::select('SELECT grupa_pr_naziv_id as id, (SELECT grupa from grupa_pr where grupa_pr_id = gpn.grupa_pr_id limit 1) as grupa, naziv as naziv from grupa_pr_naziv gpn WHERE grupa_pr_naziv_id in ('.implode(',', $ids).') limit 1');

           $results = array();
            foreach ($pre_results as $item) {
                $record = array('id' => 'grupa', 'naziv' => $item->grupa);


                $record['additional'] = (object) array('addnaziv' => 'karakteristika', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'KARAKTERISTIKE_DODAVANJE_GENERISANE',
                'KARAKTERISTIKA_DODAVANJE',
                'G_KARAKTERISTIKE_OBRISI',
            )
        )){

           $pre_results = DB::select('SELECT grupa_pr_naziv_id as id, roba_id, (SELECT naziv from grupa_pr_naziv where grupa_pr_naziv_id = wrk.grupa_pr_naziv_id limit 1) as naziv from web_roba_karakteristike wrk WHERE web_roba_karakteristike_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('roba')->where('roba_id',$item->roba_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $addon->naziv_web);
                $record['additional'] = (object) array('addnaziv' => 'generisana karakteristika', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_G_KARAK_VREDNOST_DODAJ',
                'G_KARAKTERISTIKE_IZMENA'
            )
        )){

           $pre_results = DB::select('SELECT grupa_pr_vrednost_id as id, grupa_pr_naziv_id, naziv as naziv from grupa_pr_vrednost WHERE grupa_pr_vrednost_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {
                $addon = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$item->grupa_pr_naziv_id)->first();


                $grupa_pr_id = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$item->grupa_pr_naziv_id)->first();

                $newAddon = DB::table('roba')->where('grupa_pr_id',$grupa_pr_id->grupa_pr_id)->first();

                $record = array('id' => 'artikal', 'naziv' => $newAddon->naziv_web);

                $record['additional'] = (object) array('addnaziv' => 'karakteristika', 'naziv' => $addon->naziv);

                $record['additionalKoji'] = 'vrednost generisane karakteristike';
                $record['additionalKojiNaziv'] = $item->naziv;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ADMINISTRATOR_DODAJ',
                'ADMINISTRATOR_IZMENI',
                'ADMINISTRATOR_OBRISI'
            )
        )){
           $results = DB::table('imenik')->select('ime AS naziv','imenik_id AS id')->whereIn('imenik_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'NIVOI_PRISTUPA_DODAJ',
                'NIVOI_PRISTUPA_IZMENI',
                'NIVOI_PRISTUPA_OBRISI'
            )
        )){
           $results = DB::table('ac_group')->select('naziv AS naziv','ac_group_id AS id')->whereIn('ac_group_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'STRANICA_DODAJ',
                'STRANICA_IZMENI',
                'STRANICA_OBRISI',
                'POZICIJA_STRANICA'
            )
        )){
           $results = DB::table('web_b2c_seo')->select('naziv_stranice AS naziv','web_b2c_seo_id AS id')->whereIn('web_b2c_seo_id', $ids)->get();
        }
        else if(in_array($logCode, array(            
                'CSS_IZMENI_CUSTOM',
                'CSS_OBRISI_CUSTOM'
            ))){
            $results = DB::table('custom_css')->select('sadrzaj AS naziv','custom_css_id AS id')->whereIn('custom_css_id', $ids)->get();
        }

        else if (in_array($logCode, 
            array(
                'BANERI_DODAJ',
                'BANERI_OBRISI',
                'BANERI_IZMENI',
                'SLAJDERI_DODAJ',
                'SLAJDERI_OBRISI',
                'SLAJDERI_IZMENI',
                'POPUP_BANERI_DODAJ',
                'POPUP_BANERI_OBRISI',
                'POPUP_BANERI_IZMENI',
                'BANER_POZICIJA',
                'SLAJDER_POZICIJA',
                'POP_UP_POZICIJA'
            )
        )){
           $results = DB::table('baneri')->select('naziv AS naziv','baneri_id AS id')->whereIn('baneri_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_IZMENI'
            )
        )){

           $pre_results = DB::table('web_options')->select('naziv AS naziv','web_options_id AS id', 'int_data')->whereIn('web_options_id', $ids)->get();
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                if($item->naziv != 'Prikaz gril ili list view' && $item->naziv != 'Prikaz sifre,id,sifre_d ili seo' && $item->naziv != 'Vrsta sifre webu' && $item->naziv != 'Cena u prodavnici (1-web_cena, 0-mpcena)'){
                    if($item->int_data == 0){
                        $record['additional'] = (object) array('addnaziv' => '', 'naziv' => 'NE');
                    }elseif ($item->int_data == 1) {
                        $record['additional'] = (object) array('addnaziv' => '', 'naziv' => 'DA');
                    }
                }

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_OPTIONS_IZMENI'
            )
        )){

           $pre_results = DB::table('options')->select('naziv AS naziv','options_id AS id', 'int_data')->whereIn('options_id', $ids)->get();
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                if($item->int_data == 0){
                    $record['additional'] = (object) array('addnaziv' => '', 'naziv' => 'NE');
                }elseif ($item->int_data == 1) {
                    $record['additional'] = (object) array('addnaziv' => '', 'naziv' => 'DA');
                }
                
                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_TEME',
                'TEMA_ZAKLJUCAJ'
            )
        )){
           $pre_results = DB::table('prodavnica_stil')->select('naziv AS naziv','prodavnica_stil_id AS id', 'prodavnica_tema_id')->whereIn('prodavnica_stil_id', $ids)->get();

           $results = array();
            foreach ($pre_results as $item) {

                $tema = DB::table('prodavnica_tema')->where('prodavnica_tema_id',$item->prodavnica_tema_id)->pluck('naziv');

                $record = array('id' => 'tema', 'naziv' => $tema);

                $record['additional'] = (object) array('addnaziv' => 'stil', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PRODAVNICA_PODESAVANJA_PRIKAZ_ARTIKALA_DA',
                'PRODAVNICA_PODESAVANJA_PRIKAZ_ARTIKALA_NE'
            )
        )){
           $results = DB::table('options')->select('naziv AS naziv','options_id AS id')->whereIn('options_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_NACIN_PLACANJA'
            )
        )){
           $results = DB::table('web_nacin_placanja')->select('naziv AS naziv','web_nacin_placanja_id AS id')->whereIn('web_nacin_placanja_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_NACIN_ISPORUKE'
            )
        )){
           $results = DB::table('web_nacin_isporuke')->select('naziv AS naziv','web_nacin_isporuke_id AS id')->whereIn('web_nacin_isporuke_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_MAGACIN',
                'PODESAVANJA_PRODAVNICE_NE_MAGACIN',
                'PODESAVANJA_PRODAVNICE_MAGACIN_PRIMARNI'
            )
        )){
           $results = DB::table('orgj')->select('naziv AS naziv','orgj_id AS id')->whereIn('orgj_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_VRSTA_CENE_OMOGUCI',
                'PODESAVANJA_PRODAVNICE_VRSTA_CENE_ONEMOGUCI'
            )
        )){
           $results = DB::table('vrsta_cena')->select('naziv AS naziv','vrsta_cena_id AS id')->whereIn('vrsta_cena_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_VRSTA_CENE'
            )
        ))
        {
           $pre_results = DB::table('vrsta_cena')->select('naziv AS naziv','vrsta_cena_id AS id', 'valuta_id')->whereIn('vrsta_cena_id', $ids)->get();

           $results = array();
            foreach ($pre_results as $item) {

                $valuta = DB::table('valuta')->where('valuta_id',$item->valuta_id)->pluck('valuta_naziv');

                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $record['additional'] = (object) array('addnaziv' => 'valuta', 'naziv' => $valuta);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_EKSPORT_OMOGUCI',
                'PODESAVANJA_PRODAVNICE_EKSPORT_ONEMOGUCI'
            )
        )){
           $results = DB::table('export')->select('naziv AS naziv','export_id AS id')->whereIn('export_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PODESAVANJA_PRODAVNICE_JEZIK_DA',
                'PODESAVANJA_PRODAVNICE_JEZIK_NE',
                'PODESAVANJA_PRODAVNICE_JEZIK_PRIMARNI'
            )
        )){
           $results = DB::table('jezik')->select('naziv AS naziv','jezik_id AS id')->whereIn('jezik_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_IMPORT'
            )
        )){
           $results = DB::table('export')->select('naziv AS naziv','export_id AS id')->whereIn('export_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_FAJLOVI'
            )
        )){
           $results = DB::table('web_files')->select('naziv AS naziv','web_file_id AS id')->whereIn('web_file_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'SIFARNICI_GRUPA_OBRISI',
                'SIFARNICI_GRUPA_DODAJ',
                'SIFARNICI_GRUPA_IZMENI'
            )
        )){
           $results = DB::table('grupa_pr')->select('grupa AS naziv','grupa_pr_id AS id')->whereIn('grupa_pr_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'B2B_RABAT_IZMENI'
            )
        )){
           $pre_results = DB::table('grupa_pr')->select('grupa AS naziv','grupa_pr_id AS id', 'rabat')->whereIn('grupa_pr_id', $ids)->get();

           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $record['additional'] = (object) array('addnaziv' => 'rabat', 'naziv' => $item->rabat);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PROIZVODJAC_DODAJ',
                'PROIZVODJAC_IZMENI',
                'PROIZVODJAC_OBRISI'
            )
        )){
           $results = DB::table('proizvodjac')->select('naziv AS naziv','proizvodjac_id AS id')->whereIn('proizvodjac_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'RABAT_PROIZVODJAC_IZMENI'
            )
        )){
            $partner = DB::table('partner')->where('partner_id', $ids['partner'])->pluck('naziv');
            $results = array();
            $record = array('id' => 'partner', 'naziv' => $partner);


            $proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id', $ids['proizvodjac'])->pluck('naziv');
            $record['additional'] = (object) array('addnaziv' => 'proizvođač', 'naziv' => $proizvodjac);

            $rabat = DB::table('partner_rabat_grupa')->where('proizvodjac_id', $ids['proizvodjac'])->where('partner_id', $ids['partner'])->where('grupa_pr_id', -1)->pluck('rabat');

            $record['additionalKoji'] = 'rabat';
                $record['additionalKojiNaziv'] = $rabat;

            $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'TIP_ARTIKLA_IZMENI',
                'TIP_ARTIKLA_DODAJ',
                'TIP_ARTIKLA_OBRISI'
            )
        )){
           $results = DB::table('tip_artikla')->select('naziv AS naziv','tip_artikla_id AS id')->whereIn('tip_artikla_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'JEDINICA_MERE_IZMENI',
                'JEDINICA_MERE_DODAJ',
                'JEDINICA_MERE_OBRISI'
            )
        )){
           $results = DB::table('jedinica_mere')->select('naziv AS naziv','jedinica_mere_id AS id')->whereIn('jedinica_mere_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'PORESKA_STOPA_IZMENI',
                'PORESKA_STOPA_DODAJ',
                'PORESKA_STOPA_OBRISI'
            )
        )){
           $results = DB::table('tarifna_grupa')->select('naziv AS naziv','tarifna_grupa_id AS id')->whereIn('tarifna_grupa_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'STANJE_ARTIKLA_IZMENI',
                'STANJE_ARTIKLA_DODAJ',
                'STANJE_ARTIKLA_OBRISI'
            )
        )){
           $results = DB::table('roba_flag_cene')->select('naziv AS naziv','roba_flag_cene_id AS id')->whereIn('roba_flag_cene_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'STATUS_NARUDZBINE_IZMENI',
                'STATUS_NARUDZBINE_DODAJ',
                'STATUS_NARUDZBINE_OBRISI'
            )
        )){
           $results = DB::table('narudzbina_status')->select('naziv AS naziv','narudzbina_status_id AS id')->whereIn('narudzbina_status_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'KURIRSKA_SLUZBA_DODAJ',
                'KURIRSKA_SLUZBA_IZMENI',
                'KURIRSKA_SLUZBA_OBRISI'
            )
        )){
           $results = DB::table('posta_slanje')->select('naziv AS naziv','posta_slanje_id AS id')->whereIn('posta_slanje_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'KONFIGURATOR_DODAJ',
                'KONFIGURATOR_IZMENI',
                'KONFIGURATOR_OBRISI'
            )
        )){
           $results = DB::table('konfigurator')->select('naziv AS naziv','konfigurator_id AS id')->whereIn('konfigurator_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'KONFIGURATOR_GRUPE_DODAJ',
                'KONFIGURATOR_GRUPE_IZMENI',
                'KONFIGURATOR_GRUPE_OBRISI'
            )
        )){
           
            $item = DB::table('grupa_pr')->where('grupa_pr_id', $ids['grupa'])->first();
            $results = array();
            $addon = DB::table('konfigurator')->where('konfigurator_id',$ids['konfigurator'])->first();
            $record = array('id' => 'konfigurator', 'naziv' => $addon->naziv);
            $record['additional'] = (object) array('addnaziv' => 'grupa', 'naziv' => $item->grupa);

            $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'KOMENTAR_IZMENI',
                'KOMENTAR_IZMENI_ODOBRAVANJE',
                'KOMENTAR_OBRISI'
            )
        )){
           $results = DB::table('web_b2c_komentari')->select('pitanje AS naziv','web_b2c_komentar_id AS id')->whereIn('web_b2c_komentar_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'SIFARNIK_OSOBINA_DODAJ',
                'SIFARNIK_OSOBINA_IZMENI',
                'SIFARNIK_OSOBINA_OBRISI'
            )
        )){
           $results = DB::table('osobina_naziv')->select('naziv AS naziv','osobina_naziv_id AS id')->whereIn('osobina_naziv_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'SIFARNIK_OSOBINA_VREDNOST_DODAJ',
                'SIFARNIK_OSOBINA_VREDNOST_IZMENI',
                'SIFARNIK_OSOBINA_VREDNOST_OBRISI',
                'SIFARNIK_OSOBINA_VREDNOST_POZICIJA'
            )
        )){
           $pre_results = DB::table('osobina_vrednost')->select('vrednost AS naziv','osobina_vrednost_id AS id', 'osobina_naziv_id as naziv_osobine')->whereIn('osobina_vrednost_id', $ids)->get();

           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('osobina_naziv')->where('osobina_naziv_id',$item->naziv_osobine)->first();

                $record = array('id' => 'osobina', 'naziv' => $addon->naziv);

                $record['additional'] = (object) array('addnaziv' => 'vrednost', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'SIFARNIK_NACIN_PLACANJA_DODAJ',
                'SIFARNIK_NACIN_PLACANJA_IZMENI',
                'SIFARNIK_NACIN_PLACANJA_OBRISI'
            )
        )){
           $results = DB::table('web_nacin_placanja')->select('naziv AS naziv','web_nacin_placanja_id AS id')->whereIn('web_nacin_placanja_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'SIFARNIK_NACIN_ISPORUKE_DODAJ',
                'SIFARNIK_NACIN_ISPORUKE_IZMENI',
                'SIFARNIK_NACIN_ISPORUKE_OBRISI'
            )
        )){
           $results = DB::table('web_nacin_isporuke')->select('naziv AS naziv','web_nacin_isporuke_id AS id')->whereIn('web_nacin_isporuke_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'SIFARNIK_TROSKOVI_ISPORUKE_DODAJ',
                'SIFARNIK_TROSKOVI_ISPORUKE_IZMENI',
                'SIFARNIK_TROSKOVI_ISPORUKE_OBRISI'
            )
        )){

           $pre_results = DB::select('SELECT web_troskovi_isporuke_id as id, tezina_gr as naziv from web_troskovi_isporuke WHERE web_troskovi_isporuke_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $itemDoId = $item->id - 1;


                while (empty(DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id', $itemDoId)->pluck('tezina_gr')) && $itemDoId != 0) {
                    $itemDoId--;
                }
                if ($itemDoId == 0) {
                        $itemDo = 0;
                }elseif (!empty(DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id', $itemDoId)->pluck('tezina_gr'))) {
                    $itemDo = DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id', $itemDoId)->pluck('tezina_gr');
                }

                $record = array('id' => 'težina od', 'naziv' => $itemDo);
                $record['additional'] = (object) array('addnaziv' => 'težina do', 'naziv' => $item->naziv);
                 $results[] = (object) $record;

            }
        }
        else if (in_array($logCode, 
            array(
                'SIFARNIK_DEFINISANE_MARZE_DODAJ',
                'SIFARNIK_DEFINISANE_MARZE_IZMENI',
                'SIFARNIK_DEFINISANE_MARZE_OBRISI'
            )
        )){

           $pre_results = DB::select('SELECT definisane_marze_id as id, nc as naziv from definisane_marze WHERE definisane_marze_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $itemDoId = $item->id - 1;


                while (empty(DB::table('definisane_marze')->where('definisane_marze_id', $itemDoId)->pluck('nc')) && $itemDoId != 0) {
                    $itemDoId--;
                }
                if ($itemDoId == 0) {
                        $itemDo = 0;
                }elseif (!empty(DB::table('definisane_marze')->where('definisane_marze_id', $itemDoId)->pluck('nc'))) {
                    $itemDo = DB::table('definisane_marze')->where('definisane_marze_id', $itemDoId)->pluck('nc');
                }

                $record = array('id' => 'nabavna cena od', 'naziv' => $itemDo);
                $record['additional'] = (object) array('addnaziv' => 'nabavna cena do', 'naziv' => $item->naziv);
                 $results[] = (object) $record;

            }
        }
        else if (in_array($logCode, 
            array(
                'CENA_IZMENI',
                'CENA_DODAJ',
                'CENA_OBRISI'
            )
        )){

           $pre_results = DB::select('SELECT cena_id as id, cena_do, cena from cena_isporuke WHERE cena_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $record = array('id' => 'za težinu manju od', 'naziv' => $item->cena_do);
                $record['additional'] = (object) array('addnaziv' => 'dostava je', 'naziv' => $item->cena);
                 $results[] = (object) $record;

            }
        }
        else if (in_array($logCode, 
            array(
                'VESTI_DODAJ',
                'VESTI_IZMENI',
                'VESTI_OBRISI'
            )
        )){
           $results = DB::table('web_vest_b2c_jezik')->select('naslov AS naziv','web_vest_b2c_id AS id')->whereIn('web_vest_b2c_id', $ids)->where('jezik_id', 1)->get();
        }
        else if (in_array($logCode, 
            array(
                'KURS_IZMENI'
            )
        )){
           $results = DB::table('valuta')->select('valuta_naziv AS naziv','valuta_id AS id')->whereIn('valuta_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'NARUDZBINA_IZMENI',
                'NARUDZBINA_DODAJ',
                'NARUDZBINA_OBRISI'
            )
        )){
           $results = DB::table('web_b2c_narudzbina')->select('broj_dokumenta AS naziv','web_b2c_narudzbina_id AS id')->whereIn('web_b2c_narudzbina_id',$ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'NARUDZBINA_STAVKA_IZMENI',
                'NARUDZBINA_STAVKA_DODAJ',
                'NARUDZBINA_STAVKA_OBRISI'
            )
        )){

            $pre_results = DB::select('SELECT web_b2c_narudzbina_stavka_id as id, web_b2c_narudzbina_id, (SELECT naziv_web from roba where roba_id = wbns.roba_id limit 1) as naziv from web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_stavka_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$item->web_b2c_narudzbina_id)->first();

                $record = array('id' => 'narudžbina', 'naziv' => $addon->broj_dokumenta);

                $record['additional'] = (object) array('addnaziv' => 'stavka', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'NARUDZBINA_STAVKA_CENA_IZMENI'
            )
        )){

            $pre_results = DB::select('SELECT web_b2c_narudzbina_stavka_id as id, web_b2c_narudzbina_id, (SELECT naziv_web from roba where roba_id = wbns.roba_id limit 1) as naziv, jm_cena from web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_stavka_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$item->web_b2c_narudzbina_id)->first();

                $record = array('id' => 'narudžbina', 'naziv' => $addon->broj_dokumenta);

                $record['additional'] = (object) array('addnaziv' => 'stavka', 'naziv' => $item->naziv);

                $record['additionalKoji'] = 'cena';
                $record['additionalKojiNaziv'] = $item->jm_cena;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'NARUDZBINA_IZMENI_KOLICINA'
            )
        )){

            $pre_results = DB::select('SELECT web_b2c_narudzbina_stavka_id as id, web_b2c_narudzbina_id, (SELECT naziv_web from roba where roba_id = wbns.roba_id limit 1) as naziv, kolicina from web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_stavka_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$item->web_b2c_narudzbina_id)->first();

                $record = array('id' => 'narudžbina', 'naziv' => $addon->broj_dokumenta);

                $record['additional'] = (object) array('addnaziv' => 'stavka', 'naziv' => $item->naziv);

                $record['additionalKoji'] = 'količina';
                $record['additionalKojiNaziv'] = sprintf("%.0f", $item->kolicina);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'PARTNER_OBRISI',
                'PARTNER_DODAJ',
                'PARTNER_IZMENI',
                
            )
        )){
           $results = DB::table('partner')->select('naziv AS naziv','partner_id AS id')->whereIn('partner_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'RABAT_PARTNER_IZMENI'
            )
        )){
        $pre_results = DB::table('partner')->select('naziv AS naziv','partner_id AS id', 'rabat')->whereIn('partner_id', $ids)->get();
            foreach ($pre_results as $item) {


                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $record['additional'] = (object) array('addnaziv' => 'kategorija', 'naziv' => $item->rabat);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'KATEGORIJA_PARTNER_IZMENI'
            )
        )){
        $pre_results = DB::table('partner')->select('naziv AS naziv','partner_id AS id', 'id_kategorije')->whereIn('partner_id', $ids)->get();
            foreach ($pre_results as $item) {


                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                if($item->id_kategorije == 0){
                    $kategorija = 'Bez kategorije';
                }else{
                    $kategorija = DB::table('partner_kategorija')->where('id_kategorije', $item->id_kategorije)->pluck('naziv');
                }

                $record['additional'] = (object) array('addnaziv' => 'kategorija', 'naziv' => $kategorija);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'GRUPA_REDNI_BROJ_IZMENI'
            )
        )){
           $results = DB::table('grupa_pr')->select('grupa AS naziv','grupa_pr_id AS id')->whereIn('grupa_pr_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'NARUDZBINA_STATUS_PRIHVATI',
                'NARUDZBINA_STATUS_REALIZUJ',
                'NARUDZBINA_STATUS_STORNIRAJ',
                'NARUDZBINA_ODSTORNIRAJ',
                ''
            )
        )){
           $results = DB::table('web_b2c_narudzbina')->select('broj_dokumenta AS naziv','web_b2c_narudzbina_id AS id')->whereIn('web_b2c_narudzbina_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'GLAVNI_MODUL_IZMENA'
            )
        )){
           
            $item = DB::table('ac_module')->where('ac_module_id', $ids['modul'])->first();
            $results = array();
            $addon = DB::table('ac_group')->where('ac_group_id',$ids['nivo_pristupa'])->first();

            $record = array('id' => 'nivo pristupa', 'naziv' => $addon->naziv);

            $record['additional'] = (object) array('addnaziv' => 'glavni modul', 'naziv' => $item->opis);

            $results[] = (object) $record;
        }
        else if (in_array($logCode, 
            array(
                'SPOREDNI_MODUL_IZMENA'
            )
        )){

            $item = DB::table('ac_module')->where('ac_module_id', $ids['modul'])->first();
            $results = array();

            $addon = DB::table('ac_module')->where('ac_module_id',$item->parrent_ac_module_id)->first();

            $newAddon = DB::table('ac_group')->where('ac_group_id',$ids['nivo_pristupa'])->first();

            $record = array('id' => 'nivo pristupa', 'naziv' => $newAddon->naziv);

            $record['additional'] = (object) array('addnaziv' => 'glavni modul', 'naziv' => $addon->opis);

            $record['additionalKoji'] = 'sporedni modul';
            $record['additionalKojiNaziv'] = $item->opis;

            $results[] = (object) $record;
            
        }
        else if (in_array($logCode, 
            array(
                'B2B_NARUDZBINA_DODAJ',
                'B2B_NARUDZBINA_IZMENI',
                'B2B_NARUDZBINA_STATUS_PRIHVATI',
                'B2B_NARUDZBINA_STATUS_REALIZUJ',
                'B2B_NARUDZBINA_STATUS_STORNIRAJ',
                'B2B_NARUDZBINA_ODSTORNIRAJ',
                'B2B_NARUDZBINA_(STAVKA)_OBRISI',
            )
        )){
           $results = DB::table('web_b2b_narudzbina')->select('broj_dokumenta AS naziv','web_b2b_narudzbina_id AS id')->whereIn('web_b2b_narudzbina_id', $ids)->get();
        }

        else if (in_array($logCode, 
            array(
                'B2B_NARUDZBINA_STAVKA_OBRISI',
                'B2B_NARUDZBINA_STAVKA_DODAJ'
            )
        )){

            $pre_results = DB::select('SELECT web_b2b_narudzbina_stavka_id as id, web_b2b_narudzbina_id, (SELECT naziv_web from roba where roba_id = wbns.roba_id limit 1) as naziv from web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_stavka_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$item->web_b2b_narudzbina_id)->first();

                $record = array('id' => 'narudžbina', 'naziv' => $addon->broj_dokumenta);

                $record['additional'] = (object) array('addnaziv' => 'stavka', 'naziv' => $item->naziv);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'B2B_CENA_IZMENI'
            )
        )){

            $pre_results = DB::select('SELECT web_b2b_narudzbina_stavka_id as id, web_b2b_narudzbina_id, (SELECT naziv_web from roba where roba_id = wbns.roba_id limit 1) as naziv, jm_cena from web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_stavka_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$item->web_b2b_narudzbina_id)->first();

                $record = array('id' => 'narudžbina', 'naziv' => $addon->broj_dokumenta);

                $record['additional'] = (object) array('addnaziv' => 'stavka', 'naziv' => $item->naziv);

                $record['additionalKoji'] = 'cena';
                $record['additionalKojiNaziv'] = $item->jm_cena;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'B2B_NARUDZBINA_STAVKA_IZMENI_KOLICINA'
            )
        )){

            $pre_results = DB::select('SELECT web_b2b_narudzbina_stavka_id as id, web_b2b_narudzbina_id, (SELECT naziv_web from roba where roba_id = wbns.roba_id limit 1) as naziv, kolicina from web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_stavka_id in ('.implode(',', $ids).')');
           $results = array();
            foreach ($pre_results as $item) {

                $addon = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$item->web_b2b_narudzbina_id)->first();

                $record = array('id' => 'narudžbina', 'naziv' => $addon->broj_dokumenta);

                $record['additional'] = (object) array('addnaziv' => 'stavka', 'naziv' => $item->naziv);

                $record['additionalKoji'] = 'količina';
                $record['additionalKojiNaziv'] = sprintf("%.0f", $item->kolicina);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'B2B_MAGACIN_OMOGUCI',
                'B2B_MAGACIN_ONEMOGUCI',
                'B2B_MAGACIN_DODAJ',
                'B2B_MAGACIN_OBRISI',
                'B2B_MAGACIN_PRIMARNI'

            )
        )){
           $results = DB::table('orgj')->select('naziv AS naziv','orgj_id AS id')->whereIn('orgj_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'B2B_MAGACIN_PODESAVANJA',
                'B2B_OPSTA_PODESAVANJA_DA',
                'B2B_OPSTA_PODESAVANJA_NE'
            )
        )){
           $results = DB::table('options')->select('naziv AS naziv','options_id AS id')->whereIn('options_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'FUTER_DODAJ',
                'FUTER_IZMENI',
                'FUTER_OBRISI',
                'POZICIJA_FUTER_SEKCIJA'
            )
        )){
           $results = DB::table('futer_sekcija_jezik')->distinct('naslov')->select('naslov AS naziv','futer_sekcija_id AS id')->whereIn('futer_sekcija_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'RABAT_KOMBINACIJE_OBRISI',
                'B2B_RABAT_KOMBINACIJA_IZMENI',
                'RABAT_KOMBINACIJA_DODAJ'
            )
        )){
           $pre_results = DB::select('SELECT partner_id, grupa_pr_id, rabat, proizvodjac_id FROM partner_rabat_grupa WHERE id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $partner = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');
                $grupa = DB::table('grupa_pr')->where('grupa_pr_id', $item->grupa_pr_id)->pluck('grupa');
                $proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id', $item->proizvodjac_id)->pluck('naziv');

                $record = array('id' => 'partner', 'naziv' => $partner);


                $record['additional'] = (object) array('addnaziv' => 'grupa', 'naziv' => $grupa);

                $record['additionalKoji'] = 'proizvođač';
                $record['additionalKojiNaziv'] = $proizvodjac;

                $record['additionalKojiKoji'] = 'rabat';
                $record['additionalKojiKojiNaziv'] = $item->rabat;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'B2B_PRODAVNICA_DODAJ',
                'B2B_PRODAVNICA_IZMENI',
                'B2B_PRODAVNICA_OBRISI',
                'B2B_STRANICE_POZICIJA'
            )
        )){
           $results = DB::table('web_b2b_seo')->select('naziv_stranice AS naziv','web_b2b_seo_id AS id')->whereIn('web_b2b_seo_id', $ids)->get();
        } 
        else if (in_array($logCode, 
            array(
                'B2B_VESTI_DODAJ',
                'B2B_VESTI_IZMENI',
                'B2B_VESTI_OBRISI'
            )
        )){
           $results = DB::table('web_vest_b2b_jezik')->select('naslov AS naziv','web_vest_b2b_id AS id')->whereIn('web_vest_b2b_id', $ids)->get();
        } 
        else if (in_array($logCode, 
            array(
                'B2B_KATEGORIJA_IZMENI',
                'B2B_KATEGORIJA_DODAJ',
                'B2B_KATEGORIJA_OBRISI'
            )
        )){
           $results = DB::table('partner_kategorija')->select('naziv AS naziv','id_kategorije AS id')->whereIn('id_kategorije', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'B2B_KATEGORIJA_IZMENI_RAB'
            )
        )){
           $pre_results = DB::table('partner_kategorija')->select('naziv AS naziv','id_kategorije AS id', 'rabat')->whereIn('id_kategorije', $ids)->get();
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $record['additional'] = (object) array('addnaziv' => 'rabat', 'naziv' => $item->rabat);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'KUPAC_IZMENI',
                'KUPAC_DODAJ',
                'KUPCI_OBRISI'
            )
        )){
           $results = DB::select('SELECT (CASE WHEN flag_vrsta_kupca = 0 then ime else naziv END) as naziv, web_kupac_id AS id from web_kupac where web_kupac_id in ('.implode(',', $ids).')');
        }
        else if (in_array($logCode, 
            array(
                'DOBAVLJAC_ARTIKLI_UNOS_NOVIH',
                'WEB_IMPORT_PRIHVATI_LAGER',
                'WEB_IMPORT_PRIHVATI_CENE',
                'WEB_IMPORT_PRERACUNAJ_CENU',
                'WEB_IMPORT_SKINI_SA_WEBA',
                'WEB_IMPORT_STAVI_NA_WEB',
                'WEB_IMPORT_OTKLJUCAJ',
                'WEB_IMPORT_ZAKLJUCAJ',
                'WEB_IMPORT_STORNIRAJ_KOLICINA_0',
                'WEB_IMPORT_PREUZMI_KARAKTERISTIKE_SLIKE',
                'WEB_IMPORT_OBRISI',
                'WEB_IMPORT_PREPISI_CENU',
                'WEB_IMPORT_POVEZI_ARTIKLE',
                'WEB_IMPORT_RAZVEZI_ARTIKLE',
                'WEB_IMPORT_PREBACI_U_AKTIVNE',
                'WEB_IMPORT_PREBACI_U_NEAKTIVNE',
                'WEB_IMPORT_ARTIKAL_IZMENI',
                'WEB_IMPORT_NAZIV_BRZA_IZMENA',
                'WEB_IMPORT_SIFRA_BRZA_IZMENA' 
            )
        )){
            $pre_results = array();
            if(count($ids) > 0){
               $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
            }
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => '', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'WEB_IMPORT_DODAJ_TEKST',
                'WEB_IMPORT_IZMENI_TEKST'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                if ($item->id == -1) {
                    $record = array('id' => $item->id, 'naziv' => $item->naziv);

                    $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                    $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                    $record['additionalKoji'] = 'šifra';
                    $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                    $results[] = (object) $record;
                }
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_DOBAVLJAC_GRUPA_DODAJ'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id, grupa_pr_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $newAddon = DB::table('grupa_pr')->where('grupa_pr_id', $item->grupa_pr_id)->pluck('grupa');

                $record['additionalKojiKoji'] = 'grupa';
                $record['additionalKojiKojiNaziv'] = $newAddon;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_DOBAVLJAC_PROIZVODJAC_DODAJ'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id, proizvodjac_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $newAddon = DB::table('proizvodjac')->where('proizvodjac_id', $item->proizvodjac_id)->pluck('naziv');

                $record['additionalKojiKoji'] = 'proizvođač';
                $record['additionalKojiKojiNaziv'] = $newAddon;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_DOBAVLJAC_TARIFNA_GRUPA_DODAJ'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id, tarifna_grupa_id FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $newAddon = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $item->tarifna_grupa_id)->pluck('porez');

                $record['additionalKojiKoji'] = 'tarifna grupa';
                $record['additionalKojiKojiNaziv'] = $newAddon;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_DOBAVLJAC_MP_MARZA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id, mp_marza FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $record['additionalKojiKoji'] = 'MP marža';
                $record['additionalKojiKojiNaziv'] = $item->mp_marza;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_DOBAVLJAC_WEB_MARZA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id, web_marza FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $record['additionalKojiKoji'] = 'web marža';
                $record['additionalKojiKojiNaziv'] = $item->web_marza;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'ARTIKLI_DOBAVLJAC_KOLICINA',
                'WEB_IMPORT_KOLICINA_BRZA_IZMENA'
            )
        )){
           $pre_results = DB::select('SELECT roba_id as id, naziv as naziv, sifra_kod_dobavljaca, partner_id, kolicina FROM dobavljac_cenovnik WHERE dobavljac_cenovnik_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {
                $record = array('id' => $item->id, 'naziv' => $item->naziv);

                $dobavljac = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');

                $record['additional'] = (object) array('addnaziv' => 'dobavljač', 'naziv' => $dobavljac);

                $record['additionalKoji'] = 'šifra';
                $record['additionalKojiNaziv'] = $item->sifra_kod_dobavljaca;

                $record['additionalKojiKoji'] = 'količina';
                $record['additionalKojiKojiNaziv'] = sprintf("%.0f", $item->kolicina);

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'WEB_IMPORT_DODAJ_GRUPU',
                'WEB_IMPORT_IZMENI_GRUPU',
                'WEB_IMPORT_OBRISI_GRUPU'
            )
        )){
           $pre_results = DB::select('SELECT partner_id, grupa_pr_id, grupa FROM partner_grupa WHERE partner_grupa_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {

                $partner = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');
                $record = array('id' => 'partner', 'naziv' => $partner);


                $record['additional'] = (object) array('addnaziv' => 'grupa dobavljača', 'naziv' => $item->grupa);

                $nasa_grupa = DB::table('grupa_pr')->where('grupa_pr_id', $item->grupa_pr_id)->pluck('grupa');

                $record['additionalKoji'] = 'naša grupa';
                $record['additionalKojiNaziv'] = $nasa_grupa;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'WEB_IMPORT_PROIZVODJAC_DODAJ',
                'WEB_IMPORT_PROIZVODJAC_IZMENI',
                'WEB_IMPORT_PROIZVODJAC_OBRISI'
            )
        )){
           $pre_results = DB::select('SELECT partner_id, proizvodjac_id, proizvodjac FROM partner_proizvodjac WHERE partner_proizvodjac_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {

                $partner = DB::table('partner')->where('partner_id', $item->partner_id)->pluck('naziv');
                $record = array('id' => 'dobavljač', 'naziv' => $partner);

                $record['additional'] = (object) array('addnaziv' => 'proizvođač dobavljača', 'naziv' => $item->proizvodjac);

                $nas_proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id', $item->proizvodjac_id)->pluck('naziv');

                $record['additionalKoji'] = 'naš proizvođač';
                $record['additionalKojiNaziv'] = $nas_proizvodjac;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'B2B_BANERI_DODAJ',
                'B2B_BANERI_IZMENI',
                'B2B_BANERI_OBRISI',
                'B2B_BANER_POZICIJA'
            )
        )){
           $results = DB::table('baneri_b2b')->select('naziv AS naziv','baneri_id AS id')->whereIn('baneri_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'B2B_SLAJDERI_DODAJ',
                'B2B_SLAJDERI_IZMENI',
                'B2B_SLAJDERI_OBRISI',
                'B2B_SLAJDER_POZICIJA'
            )
        )){
           $results = DB::table('baneri_b2b')->select('naziv AS naziv','baneri_id AS id')->whereIn('baneri_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'B2B_POP_UP_DODAJ',
                'B2B_POP_UP_IZMENI',
                'B2B_POP_UP_OBRISI',
                'B2B_POP_UP_POZICIJA'
            )
        )){
           $results = DB::table('baneri_b2b')->select('naziv AS naziv','baneri_id AS id')->whereIn('baneri_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'RADNI_NALOG_IZMENI',
                'RADNI_NALOG_DODAJ'
            )
        )){
           $results = DB::table('radni_nalog')->select('broj_naloga AS naziv','radni_nalog_id AS id')->whereIn('radni_nalog_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'RMA_OPERACIJA_GRUPA_DODAJ',
                'RMA_OPERACIJA_GRUPA_IZMENI',
                'RMA_OPERACIJA_GRUPA_OBRISI'
            )
        )){
           $results = DB::table('operacija_grupa')->select('naziv AS naziv','operacija_grupa_id AS id')->whereIn('operacija_grupa_id', $ids)->get();
        }
        else if (in_array($logCode, 
            array(
                'RMA_OPERACIJA_DODAJ',
                'RMA_OPERACIJA_IZMENI',
                'RMA_OPERACIJA_OBRISI'
            )
        )){
           $pre_results = DB::select('SELECT operacija_id, naziv, cena, operacija_grupa_id FROM operacija WHERE operacija_id in ('.implode(',', $ids).')');
           $results = array();
           foreach ($pre_results as $item) {

                $record = array('id' => $item->operacija_id, 'naziv' => $item->naziv);

                $record['additional'] = (object) array('addnaziv' => 'cena', 'naziv' => $item->cena);

                $grupa = DB::table('operacija_grupa')->where('operacija_grupa_id', $item->operacija_grupa_id)->pluck('naziv');

                $record['additionalKoji'] = 'grupa';
                $record['additionalKojiNaziv'] = $grupa;

                $results[] = (object) $record;
            }
        }
        else if (in_array($logCode, 
            array(
                'RMA_SERVISER_DODAJ',
                'RMA_SERVISER_IZMENI',
                'RMA_SERVISER_OBRISI'
            )
        )){
           $results = DB::table('serviser')->select('ime AS naziv','serviser_id AS id')->whereIn('serviser_id', $ids)->get();
        }


        if (count($results)==0) {
            foreach ($ids as $id) {
                $results[] = (object) array('id'=>$id, 'naziv'=>'');
            }
        }
        return json_encode($results);
    }
    public static function aktivniAdminModuli($parent_id=null){
        if(isset($parent_id)){
            $parent = ' AND am.parrent_ac_module_id = '.$parent_id.' AND tip = 1';
        }else{
            $parent = ' AND tip = 0';
        }
        // $is_shop = "";
        // if(!AdminOptions::is_shop()){
        //     $is_shop .= " AND ac_module_id IN (5600,5800,6000,6600,7000,7100,8000)";
        // }

        // if(Session::get('b2c_admin'.AdminOptions::server()) == 0){
            if(AdminOptions::web_options(130)==0){
                return DB::select("SELECT ac_module_id, opis FROM ac_module am WHERE aktivan = 1".$parent." AND opis NOT LIKE '%B2B%' ORDER BY ac_module_id ASC");
            }elseif(AdminOptions::web_options(130)==2) {
                return DB::select("SELECT ac_module_id, opis FROM ac_module am WHERE aktivan = 1".$parent." AND ac_module_id NOT in (1, 2, 3, 7, 8, 9, 10, 23, 24, 25, 29, 30, 31, 36, 37, 38) ORDER BY ac_module_id ASC");
            }else{
                return DB::select("SELECT ac_module_id, opis FROM ac_module am WHERE aktivan = 1".$parent." ORDER BY ac_module_id ASC");
            }
        // }else{
        //     return DB::select("SELECT am.ac_module_id, am.opis FROM ac_group_module agm INNER JOIN ac_module am ON agm.ac_module_id = am.ac_module_id WHERE ac_group_id = 0".$parent." AND alow = 1 AND aktivan = 1 ORDER BY am.ac_module_id ASC");
        // }
    }

    public static function find_narudzbina($id){
        return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$id)->pluck('broj_dokumenta');
    }

    public static function artikalFajlovi($roba_id){
        return DB::select("SELECT DISTINCT vrsta_fajla_id, (SELECT ekstenzija FROM vrsta_fajla WHERE vrsta_fajla_id = wf.vrsta_fajla_id) AS ext FROM web_files wf WHERE roba_id = ".$roba_id."");
    }
    public static function find_status_narudzbine($narudzbina_status_id, $column)
    {
        return DB::table('narudzbina_status')->select($column)->where('narudzbina_status_id', $narudzbina_status_id)->pluck($column);
    }

    public static function find_kurirska_sluzba($posta_slanje_id, $column)
    {
        return DB::table('posta_slanje')->select($column)->where('posta_slanje_id', $posta_slanje_id)->pluck($column);
    }

    public static function posta_status($posta_status_id)
    {
        return DB::table('posta_status')->where('posta_status_id', $posta_status_id)->first();
    }

    public static function getExtension($extension_id){
        return DB::select("SELECT ekstenzija FROM vrsta_fajla WHERE vrsta_fajla_id = ".$extension_id."")[0]->ekstenzija;
    }
    public static function getExporti(){
        return DB::table('export')->where('dozvoljen',1)->get();
    }
    public static function getCreatedKatalog(){
        return DB::table('katalog')->where('aktivan',1)->get();
    }
    public static function encodeForPdf($string){
        return iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
        // return $string;
    }

    public static function categoryOpen($active_id,$parents,$curr_id){
        $status = '';
        if($active_id == $curr_id){
            $status = 'active-cat';
        }
        elseif(in_array($curr_id,$parents)){
            $status = 'jstree-open';
        }
        return $status;

    }

    public static function grupaParents($grupa_id,&$parents){

        $parrent_grupa_pr_id = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_id)->pluck('parrent_grupa_pr_id');
        if($parrent_grupa_pr_id > 0){
            array_push($parents,$parrent_grupa_pr_id);
            self::grupaParents($parrent_grupa_pr_id,$parents);
        }
    }
    public static function find_mesto($mesto_id,$column){
        return DB::table('mesto')->where('mesto_id',$mesto_id)->pluck($column);
    }
    // public static function find_mesto_narudzbina($mesto_id){
    //     return DB::table('web_kupac')->where('mesto',$mesto_id)->pluck('mesto');
    // }
    public static function narudzbina_kupac_pdf($web_b2c_narudzbina_id){
        $web_kupac=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');

        foreach(DB::table('web_kupac')->where('web_kupac_id',$web_kupac)->get() as $row){
        if($row->flag_vrsta_kupca == 0){
            $name = '<tr>
                        <td>Kupac:</td> 
                        <td>'.$row->ime.' '.$row->prezime.'</td>
                    </tr>';
        }else{
            $name = '<tr>
                        <td>Kupac:</td>
                        <td>' .$row->naziv. '</td>
                    </tr>
                    <tr>
                        <td>PIB:</td>
                        <td>' .$row->pib. '</td>
                    </tr>
                    <tr>
                        <td>Matični br:</td>
                        <td>' .$row->maticni_br. '</td>
                    </tr>';
            }

        echo ' 
            <table>
           
            '.$name.'
          
            <tr>
            <td>Adresa:</td>
            <td>'.$row->adresa.'</td>
            </tr>
            <tr>
            <td>Mesto:</td>
            <td>'.$row->mesto.'</td>
            </tr>
            <tr>
            <td>Telefon:</td>
            <td>'.$row->telefon.'</td>
            </tr>
            <tr>
            <td>Email:</td>
            <td>'.$row->email.'</td>
            </tr>
            </table>';
        }
    } 

    public static function allComments(){
        $comments = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', '!=', -1)->orderBy('web_b2c_komentar_id', 'DESC')->get();
        return $comments;
    }

    public static function getComment($id) {
        $comment = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', $id)->first();
        return $comment;
    }

    public static function countNewComments(){
        return DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', '!=', -1)->where('komentar_odobren', 0)->count();
    }

    public static function getBGimg(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('img');
    }

    public static function getlink(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('link');
    }

    public static function getlink2(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('link2');
    }


    public static function grupa_title($grupa_pr_id){
        
        foreach(DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->get() as $row){
                
                return $row->grupa;
            }
        
    }

    public static function seo_description($roba_id){
        
        $product_seo=DB::table('roba')->where('roba_id', $roba_id)->first();
        
        if($product_seo->description!="" ){
            return self::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->description), 0, 130)." ".AdminOptions::company_name();
        }
        elseif($product_seo->web_opis!="" ){
            return self::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->web_opis), 0, 130)." ".AdminOptions::company_name();
        }
        else {
            return self::grupa_title($product_seo->grupa_pr_id)." ".strip_tags($product_seo->naziv_web)." ".AdminOptions::company_name();
        }
    }

    public static function getCurrencyName($id) {
        return DB::table('valuta')->where('valuta_id', $id)->pluck('valuta_naziv');
    }

    public static function getLevelGroupsShopmania($grupa_id){
        return DB::table('shopmania_grupe')->where('parent_id',$grupa_id)->orderBy('shopmania_grupa_id', 'asc')->get();
    }
    // public static function selectGroupsShopmania($grupa_id=null){
    //     $render = '<option value="">Izaberite grupu</option>';
    //     foreach(self::getLevelGroupsShopmania(0) as $row_gr){
    //             $render .= '<option value="'.$row_gr->shopmania_grupa_id.'" class="'.(!isset($row_gr->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr->shopmania_grupa_id?'selected':'').'>'.$row_gr->naziv.'  <->  '.(isset($row_gr->grupa_pr_ids)?self::linkedGroups($row_gr->grupa_pr_ids):'').'</option>';
    //         foreach(self::getLevelGroupsShopmania($row_gr->shopmania_grupa_id) as $row_gr1){
    //                 $render .= '<option value="'.$row_gr1->shopmania_grupa_id.'" class="'.(!isset($row_gr1->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr1->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->naziv.'  <->  '.(isset($row_gr1->grupa_pr_ids)?self::linkedGroups($row_gr1->grupa_pr_ids):'').'</option>';
    //             foreach(self::getLevelGroupsShopmania($row_gr1->shopmania_grupa_id) as $row_gr2){
    //                     $render .= '<option value="'.$row_gr2->shopmania_grupa_id.'" class="'.(!isset($row_gr2->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr2->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->naziv.'  <->  '.(isset($row_gr2->grupa_pr_ids)?self::linkedGroups($row_gr2->grupa_pr_ids):'').'</option>';
    //                 foreach(self::getLevelGroupsShopmania($row_gr2->shopmania_grupa_id) as $row_gr3){
    //                         $render .= '<option value="'.$row_gr3->shopmania_grupa_id.'" class="'.(!isset($row_gr3->grupa_pr_ids)?'warn':'').'" '.($grupa_id==$row_gr3->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->naziv.'  <->  '.(isset($row_gr3->grupa_pr_ids)?self::linkedGroups($row_gr3->grupa_pr_ids):'').'</option>';
    //                 }
    //             }
    //         }
    //     }
    //     return $render;
    // }

    // public static function linkedGroups($grupa_pr_ids){
    //     $groups = '';
    //     foreach(explode(',',$grupa_pr_ids) as $grupa_pr_id){
    //         $groups .= AdminGroups::find($grupa_pr_id,'grupa').' <> ';
    //     }
    //     return substr($groups,0,-4);
    // }
    // public static function selectParentGroupsShopmania($grupa_id=null){
    //     $render = '<option value="0">Bez parenta</option>';
    //     foreach(self::getLevelGroupsShopmania(0) as $row_gr){
    //             $render .= '<option value="'.$row_gr->shopmania_grupa_id.'" '.($grupa_id==$row_gr->shopmania_grupa_id?'selected':'').'>'.$row_gr->naziv.'</option>';
    //         foreach(self::getLevelGroupsShopmania($row_gr->shopmania_grupa_id) as $row_gr1){
    //                 $render .= '<option value="'.$row_gr1->shopmania_grupa_id.'" '.($grupa_id==$row_gr1->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->naziv.'</option>';
    //             foreach(self::getLevelGroupsShopmania($row_gr1->shopmania_grupa_id) as $row_gr2){
    //                     $render .= '<option value="'.$row_gr2->shopmania_grupa_id.'" '.($grupa_id==$row_gr2->shopmania_grupa_id?'selected':'').'>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->naziv.'</option>';
    //             }
    //         }
    //     }
    //     return $render;
    // }

    // public static function selectGroupsSm($shopmania_grupe,$grupa_pr_id=null){
    //     $render = '<option value="">Izaberite grupu</option>';
    //     foreach(self::getLevelGroups(0) as $row_gr){
    //         if($row_gr->grupa_pr_id == $grupa_pr_id){
    //             $render .= '<option value="'.$row_gr->grupa_pr_id.'" class="'.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected>'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //         }else{
    //             $render .= '<option value="'.$row_gr->grupa_pr_id.'" class="'.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':'warn').'">'.$row_gr->grupa.' - '.$row_gr->web_b2c_prikazi.(in_array($row_gr->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //         }
    //         foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
    //             if($row_gr1->grupa_pr_id == $grupa_pr_id){
    //                 $render .= '<option value="'.$row_gr1->grupa_pr_id.'" class="'.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //             }else{
    //                 $render .= '<option value="'.$row_gr1->grupa_pr_id.'" class="'.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':'warn').'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.' - '.$row_gr1->web_b2c_prikazi.(in_array($row_gr1->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //             }
    //             foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
    //                 if($row_gr2->grupa_pr_id == $grupa_pr_id){
    //                     $render .= '<option value="'.$row_gr2->grupa_pr_id.'" class="'.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //                 }else{
    //                     $render .= '<option value="'.$row_gr2->grupa_pr_id.'" class="'.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':'warn').'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.' - '.$row_gr2->web_b2c_prikazi.(in_array($row_gr2->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //                 }
    //                 foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
    //                     if($row_gr3->grupa_pr_id == $grupa_pr_id){
    //                         $render .= '<option value="'.$row_gr3->grupa_pr_id.'" class="'.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':'warn').'" selected>|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //                     }else{
    //                         $render .= '<option value="'.$row_gr3->grupa_pr_id.'" class="'.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':'warn').'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.' - '.$row_gr3->web_b2c_prikazi.(in_array($row_gr3->grupa_pr_id,$shopmania_grupe)?'':' (NIJE POVEZANO)').'</option>';
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     return $render;
    // }

    public static function getTemplate($roba_id) {
        $template = DB::table('roba')
            ->join('grupa_pr', 'roba.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->join('grupa_pr_jezik', 'grupa_pr_jezik.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->select('grupa_pr_jezik.sablon_opis')
            ->where(array('grupa_pr_jezik.jezik_id'=> 1,'roba.roba_id'=> $roba_id))
            ->get();

        return $template;
    }

    public static function custom_encrypt($string){
        return base64_encode($string);
    }

    public static function custom_decrypt($string){
        return base64_decode($string);
    }

    public static function DBAOPConnection(){
        $db_host = Config::get('database.connections')['pgsql']['host'];
        $db_port = Config::get('database.connections')['pgsql']['port'];
        $db_name = 'aop';
        $db_user = Config::get('database.connections')['pgsql']['username'];
        $db_pass = Config::get('database.connections')['pgsql']['password'];

        $conn = pg_connect("host=".$db_host." port=".$db_port." user=".$db_user." password=".$db_pass." password=".$db_pass." dbname=".$db_name."") or die('Connection to DB failed');
        return $conn;
    }
    public static function closeDBConnenction($connection){
        pg_close($connection);
    }

    public static function updateAOPUser($user){
        if(AdminOptions::gnrl_options(3013) == 1 && $user->imenik_id = 1){
            $conn = self::DBAOPConnection();

            $users = pg_fetch_all(pg_query($conn, "SELECT * FROM public.user WHERE email = '".$user->login."' AND id <> ".$user->aop_user_id.""));
            
            if(!isset($users[0])){
              pg_query($conn, "UPDATE public.user SET name = '".$user->ime." ".$user->prezime."', email = '".$user->login."', password = '".$user->password."' WHERE id = ".$user->aop_user_id."");          
            }
            self::closeDBConnenction($conn);

            if(isset($users[0])){
                return false;
            }
        }
        return true;
    }

    public static function trial_date(){
        $user = DB::table('imenik')->where('imenik_id',1)->first();
        if(!is_null($user->datum_otvaranja)){
            $open_date = $user->datum_otvaranja;
            $open_date = DateTime::createFromFormat('Y-m-d H:i:s', $open_date);
            $now_date = new DateTime();
            $limit_date = 14*86400;

            $difference_date = $limit_date - ($now_date->getTimestamp() - $open_date->getTimestamp());
        
            if($difference_date > 0){
                $result_array = self::seconds_to_time($difference_date);
                $result_array['timestamp'] = $open_date->getTimestamp();
                $result_array['difference_timestamp'] = $difference_date;
                return (object) $result_array;
            }
        }
        return false;
    }

    public static function seconds_to_time($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        $diff = $dtF->diff($dtT);
        return array(
            'days' => $diff->format('%a')
            );
    }

    public static function zip_images(){
        $path = 'images/products/big/';
        $files = array_diff(scandir($path), array('.', '..'));
        $zipname = 'files/exporti/backup/backup_images.zip';
        if(File::exists($zipname)){
            File::delete($zipname);
        }
        $zip = new ZipArchive;
        $zip->open($zipname, ZipArchive::CREATE);
        foreach($files as $file){
            $zip->addFile($path.$file);
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=backup_images.zip');
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);

        File::delete($zipname);
    }
    public static function extract_images(){
        $path = 'files/backup_images.zip';
        $destination = './';
        $zip = new ZipArchive;
        if($zip->open($path) === TRUE) {
            $zip->extractTo($destination);
            $zip->close();
            return true;
        }
        return false;
    }
    public static function getLabela($active=null)
    {
        if($active){
            return DB::table('labela_artikla')->select('labela_artikla_id','naziv','rbr')->where('labela_artikla_id','<>',-1)->where('labela_artikla_id','<>',0)->orderBy('rbr','asc')->get();
        }else{
            return DB::table('labela_artikla')->select('labela_artikla_id','naziv')->where('active',1)->where('labela_artikla_id','<>',-1)->where('labela_artikla_id','<>',0)->orderBy('rbr','asc')->get();
        }  
    }
    public static function find_labela_artikla($labela_artikla_id, $column)
    {
        return DB::table('labela_artikla')->select($column)->where('labela_artikla_id', $labela_artikla_id)->pluck($column);
    }

    public static function proizvodjac_id($naziv){
        if(trim($naziv) != ''){
            $proizvodjac = DB::table('proizvodjac')->where('naziv',$naziv)->first();
            if(!is_null($proizvodjac)){
                return $proizvodjac->proizvodjac_id;
            }
            $next_id = DB::select("SELECT MAX(proizvodjac_id) AS max FROM proizvodjac")[0]->max + 1;
            DB::table('proizvodjac')->insert(array(
                'proizvodjac_id' => $next_id,
                'naziv' => trim($naziv)
                ));
            DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id) FROM proizvodjac), FALSE)");
            return $next_id;
        }
        return -1;
    }
    public static function jedinica_mere_id($naziv){
        if(trim($naziv) != ''){
            $jedinica_mere = DB::table('jedinica_mere')->where('naziv',$naziv)->first();
            if(!is_null($jedinica_mere)){
                return $jedinica_mere->jedinica_mere_id;
            }
            $jedinica_mere_id_new = DB::select("SELECT MAX(jedinica_mere_id) AS max FROM jedinica_mere")[0]->max + 1;
            DB::table('jedinica_mere')->insert(array(
                'jedinica_mere_id' => $jedinica_mere_id_new,
                'naziv' => trim($naziv)
                ));
            return $jedinica_mere_id_new;
        }
        return 1;
    }
    public static function roba_flag_cene_id($naziv){
        if(trim($naziv) != ''){
            $roba_flag_cene = DB::table('roba_flag_cene')->where('naziv',$naziv)->first();
            if(!is_null($roba_flag_cene)){
                return $roba_flag_cene->roba_flag_cene_id;
            }
            $roba_flag_cene_id_new = DB::select("SELECT MAX(roba_flag_cene_id) AS max FROM roba_flag_cene")[0]->max + 1;
            DB::table('roba_flag_cene')->insert(array(
                'roba_flag_cene_id' => $roba_flag_cene_id_new,
                'naziv' => trim($naziv),
                'selected' => 1
                ));
            AdminTranslator::addToTranslatorAll(trim($naziv));
            return $roba_flag_cene_id_new;
        }
        return 1;
    }
    public static function tip_artikla_id($naziv){
        if(trim($naziv) != ''){
            $tip_artikla = DB::table('tip_artikla')->where('naziv',$naziv)->first();
            if(!is_null($tip_artikla)){
                return $tip_artikla->tip_artikla_id;
            }

            DB::table('tip_artikla')->insert(array(
                'naziv' => trim($naziv),
                'active' => 1,
                'rbr' => DB::select("SELECT MAX(rbr) AS max FROM tip_artikla")[0]->max + 1
                ));
            AdminTranslator::addToTranslatorAll(trim($naziv));
            return DB::select("SELECT currval('tip_artikla_tip_artikla_id_seq') as new_id")[0]->new_id;
        }
        return -1;
    }
    
    public static function xml_node($xml,$name,$value,$parent){
        $xmltag = $xml->createElement($name);
        $xmltagText = $xml->createTextNode($value);
        $xmltag->appendChild($xmltagText);
        $parent->appendChild($xmltag);
    }

    public static function upload_directory_opis(){
        $dir    = "./images/upload_opis";
        //return $dir;
        $files = scandir($dir);
        $slike="<ul>";
        $br=0;
           foreach ($files as $row) {
         if($br>1){
         
               $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='#' data-url='/images/upload_opis/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
               }
          $br+=1;
            }
        $slike.="</ul>";
        return $slike;
    }

    public static function footer_section_name($futer_sekcija_id){
        $futer_sekcija_jezik = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$futer_sekcija_id,'jezik_id'=>DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id')))->first();
        if(!is_null($futer_sekcija_jezik)){
            return $futer_sekcija_jezik->naslov;
        }
        return '';
    }
    // public static function broj_cerki($web_b2c_seo_id){     
    //     return DB::table('web_b2c_seo')->where(array('parrent_id'=>$web_b2c_seo_id,'header_menu'=>1))->count();
    // }

    public static function komercijaliste($partner_id=null){
        if(is_null($partner_id)){
            return DB::table('imenik')->where(array('kvota'=>32.00,'flag_aktivan'=>1))->get();
        }else{
            return DB::table('imenik')->select('imenik')->rightJoin('partner_komercijalista','partner_komercijalista.imenik_id','=','imenik.imenik_id')->where('partner_id',$partner_id)->get();
        }
    }

    // public static function imenik($imenik_id){
    //     return DB::table('imenik')->where(array('imenik_id'=> $imenik_id))->first();
    // }

    public static function getKupac()
    {        
        return DB::table('web_kupac')->select('web_kupac_id', 'ime', 'prezime')->where('web_kupac_id', '<>', -1)->orderBy('web_kupac_id','ASC')->get();
    }

    public static function find_kupac($kupac_id, $column)
    {
        return DB::table('web_kupac')->select($column)->where('web_kupac_id', $kupac_id)->pluck($column);
    }
    
    // public function bay_account($field, $value, $parameters){
    //     if(substr_count($value,'-') == 2){
    //         preg_match_all('/(.*)-(.*)-(.*)/',$value,$result);
            
    //         if(strlen(strval($result[1][0])) == 3 && strlen(strval($result[2][0])) == 13 && strlen(strval($result[3][0])) == 2){
    //             return true;
    //         }
    //     }
    //     return false;
    // }

    // public function clear_explode($string){
    //     $arr = explode(',',$string);
    //     return array_filter($arr,
    //         function($element){
    //             return !empty($element);
    //         }
    //     );
    // }
    
    public function parseGroupProizvodi($string){
        $grupeProizvoda = explode('>+<',$string);
        $proizvodi = array();
        foreach($grupeProizvoda as $grupaProizvoda){
            $proizvodi = array_merge($proizvodi,json_decode($grupaProizvoda));
        }
        return $proizvodi;
    }
    public static function fileExtension($image_path){
        $extension = 'jpg';
        $slash_parts = explode('/',$image_path);
        $old_name = $slash_parts[count($slash_parts)-1];
        $old_name_arr = explode('.',$old_name);
        $extension = $old_name_arr[count($old_name_arr)-1];
        return $extension;
    }

}
