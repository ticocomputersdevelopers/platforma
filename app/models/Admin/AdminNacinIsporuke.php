<?php

class AdminNacinIsporuke {

	public static function all() {
		return DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id', '!=', -1)->get();
	}

	public static function getSingle($id){
		return DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id', $id)->get();
	}

}