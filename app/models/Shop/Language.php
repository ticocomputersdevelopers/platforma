<?php 


class Language {
    public static function multi(){
        return Cache::remember('LANG_MULTI',60,function(){
            return DB::table('jezik')->where('aktivan',1)->count() > 1;
        });
    }
    public static function segment_offset(){
        return self::multi() ? 1 : 0;
    }   
    public static function lang(){
        if(!Session::has('lang')){
            try {            
                $data = json_decode(file_get_contents("https://www.geoip-db.com/json"));
                $lang = self::getLangByCountryCode($data->country_code);
            } catch (Exception $e) {
                $lang = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
            }

            Session::put('lang', $lang);
        }else{
            $lang = Session::get('lang');
        }
        return $lang;
    }
    public static function lang_id(){
        if(Session::has('lang')){
            $lang = Session::get('lang');
        }else{
            $lang = DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
        }
        $jezik = DB::table('jezik')->where(array('kod'=>$lang, 'aktivan'=>1))->first();
        if(!is_null($jezik)){
            return $jezik->jezik_id;
        }
        return 1;
    }



    //prevodi default reci u tekuci jezik (ili koji mi odaberemo kroz lang parametar)
    public static function translate($string,$lang=null){
        $string = trim($string);
        if(is_null($lang)){
            $lang = self::lang();
        }
        $jezik_id = Cache::remember('LANG_'.strtoupper($lang).'_ID',60,function() use ($lang) { return DB::table('jezik')->where(array('kod'=>$lang))->pluck('jezik_id'); });
        $izabrani_jezik_id = Cache::remember('DEFAULT_LANG_ID',60,function(){ return DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id'); });

        if($izabrani_jezik_id == $jezik_id){
            return $string;
        }else{

            $result = Cache::remember('LANG_'.strtoupper($lang).'_'.strtoupper($string),60,function() use ($izabrani_jezik_id,$jezik_id,$string){ 
                $query = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($string)."'");
                if(count($query) > 0){
                    return $query[0]->reci;
                }else{
                    return null;
                }
            });

            if(!is_null($result)){
                return $result;
            }else{
                // if(count(DB::table('prevodilac')->where(array('reci' => $string))->get()) == 0){
                //     DB::table('prevodilac')->insert(array('izabrani_jezik_reci' => 'jedan', 'reci' => $string, 'is_js' => 1));
                //     return $string;
                // }
                $str_arr = explode(' ',$string);
                $trans_arr = array();
                foreach($str_arr as $str){
                    $result = Cache::remember('LANG_'.strtoupper($lang).'_'.strtoupper($str),60,function() use ($izabrani_jezik_id,$jezik_id,$str){ 
                        $query_str = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND izabrani_jezik_reci IS NOT NULL AND izabrani_jezik_reci ILIKE '".pg_escape_string($str)."'");
                        if(count($query_str) > 0){
                            return $query_str[0]->reci;
                        }
                        return null;
                    });

                    if(!is_null($result)){
                        $trans_arr[] = $result;
                    }else{

                        $trans_arr[] = $str;
                    }
                }
                return implode(' ',$trans_arr);
            }
        }
    }

    //prevodi reci iz bilo kog jezika u defaultni jezik
    public static function trans_def($string,$lang=null){
        $string = trim($string);
        if(is_null($lang)){
            $lang = self::lang();
        }
        $jezik_id = Cache::remember('LANG_'.strtoupper($lang).'_ID',60,function() use ($lang) { return DB::table('jezik')->where(array('kod'=>$lang))->pluck('jezik_id'); });
        $izabrani_jezik_id = Cache::remember('DEFAULT_LANG_ID',60,function(){ return DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('jezik_id'); });

        if($izabrani_jezik_id == $jezik_id){
            return $string;
        }else{
            $result = Cache::remember('DEFAULT_LANG_'.strtoupper($lang).'_'.strtoupper($string),60,function() use ($izabrani_jezik_id,$jezik_id,$string){ 
                $query = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND reci IS NOT NULL AND reci ILIKE '".pg_escape_string($string)."'");
                if(count($query) > 0){
                    return $query[0]->izabrani_jezik_reci;
                }else{
                    return null;
                }
            });

            if(!is_null($result)){
                return $result;
            }else{
                $str_arr = explode(' ',$string);
                $trans_arr = array();
                foreach($str_arr as $str){
                    $result = Cache::remember('DEFAULT_LANG_'.strtoupper($lang).'_'.strtoupper($str),60,function() use ($izabrani_jezik_id,$jezik_id,$str){ 
                        $query_str = DB::select("SELECT * FROM prevodilac WHERE izabrani_jezik_id = ".$izabrani_jezik_id." AND jezik_id = ".$jezik_id." AND reci IS NOT NULL AND reci ILIKE '".pg_escape_string($str)."'");
                        if(count($query_str) > 0){
                            return $query_str[0]->izabrani_jezik_reci;
                        }
                        return null;
                    });

                    if(!is_null($result)){
                        $trans_arr[] = $result;
                    }else{

                        $trans_arr[] = $str;
                    }
                }
                return implode(' ',$trans_arr);
            }
        }
    }

    //prevodi srpske reci u tekuci jezik (ili koji mi odaberemo kroz lang parametar)
    public static function trans($string,$lang=null){
        $string = self::trans_def($string,'sr');
        return self::translate($string,$lang);
    }

    public static function validator_messages(){
        $messages = array();
        
        $validator_messages = DB::table('validator')->select('kod','poruka')->join('validator_jezik','validator.validator_id','=','validator_jezik.validator_id')->where(array('aktivan'=>1,'jezik_id'=>self::lang_id()))->get();
        foreach($validator_messages as $message){
            $messages[$message->kod] = $message->poruka;
        }
        return $messages;
    }

    public static function cir_to_latin($string){
        return str_replace(array('А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш'),array('A','B','V','G','D','Đ','E','Ž','Z','I','J','K','L','LJ','M','N','NJ','O','P','R','S','T','Ć','U','F','H','C','Č','DŽ','Š','a','b','v','g','d','đ','e','ž','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','ć','u','f','h','c','č','dž','š'), $string);
    }

    public static function latin_to_cir($string){
        $paragraphArray = preg_split('/(<.*?>)|\s/', $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        $newParagraphArray = array();
        foreach($paragraphArray as $word){
            if($word{0} == '<'){
                $newParagraphArray[] = $word;
            }else{
                $newParagraphArray[] = self::latin_to_cir_word($word);
            }
        }
        return implode(' ', $newParagraphArray);
    }

    public static function latin_to_cir_word($string){
        $string = str_replace(array('NJ','LJ','DŽ','nj','lj','dž'), array('Њ','Љ','Џ','њ','љ','џ'), $string);
        return str_replace(array('A','B','V','G','D','Đ','E','Ž','Z','I','J','K','L','M','N','O','P','R','S','T','Ć','U','F','H','C','Č','Š','W','a','b','v','g','d','đ','e','ž','z','i','j','k','l','m','n','o','p','r','s','t','ć','u','f','h','c','č','š','w'),array('А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','М','Н','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Ш','В','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','м','н','о','п','р','с','т','ћ','у','ф','х','ц','ч','ш','в'), $string);
    }

    public static function trans_chars($string){
        if(self::lang() == 'cir'){
            return self::latin_to_cir($string);
        }
        return $string;
    }

    // public static function convert_chars($string){
    //     if(self::lang() == 'cir'){
    //         return self::cir_to_latin($string);
    //     }
    //     return $string;
    // }

    public static function js_translates(){
        $sr_jezik_id = DB::table('jezik')->where('kod','sr')->pluck('jezik_id');
        $izabrani_jezik_id = DB::table('jezik')->where('izabrani',1)->pluck('jezik_id');
        $curr_jezik_id = self::lang_id();

        $translates = array();
        if($sr_jezik_id != $curr_jezik_id){
            if($curr_jezik_id == $izabrani_jezik_id){
                $translates = DB::table('prevodilac')->select('izabrani_jezik_reci','reci')->where(array('izabrani_jezik_id'=>$curr_jezik_id,'jezik_id'=>$sr_jezik_id,'is_js'=>1))->get();
            }else{
                $translates = DB::table('prevodilac')->select('prevodilac.reci as reci','current_prevodilac.reci as izabrani_jezik_reci')->join('prevodilac as current_prevodilac','prevodilac.izabrani_jezik_reci','=','current_prevodilac.izabrani_jezik_reci')->where('prevodilac.jezik_id',$sr_jezik_id)->where('current_prevodilac.jezik_id',$curr_jezik_id)->where('prevodilac.is_js',1)->get();
            }
        }

        return json_encode($translates);
    }

    public static function getLangByCountryCode($code){
        $code = strtolower($code);
        if ($code == 'rs') {
            return 'sr';
        }else if ($code == 'uk' || $code == 'us') {
            return 'en';
        }else{
            return DB::table('jezik')->where(array('aktivan'=>1, 'izabrani'=>1))->pluck('kod');
        }
    }
}