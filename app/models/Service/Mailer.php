<?php 
namespace Service;

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;
use DB;
use Mail;
use Validator;
use Config;
use Swift_TransportException;
use Throwable;

class Mailer {

    public static function send($mailFrom,$mailTo,$subject,$body,$attchFile=null,$config=null){
        if(Config::get('app.livemode')){
            if(!is_null($config)){
                Config::set('mail', $config);
            }else{
                $config = Config::get('mail');
            }

            $validator = Validator::make(array('email_from'=>$mailFrom,'email_to'=>$mailTo,'subject'=>$subject,'body'=>$body),
            array(
                'email_from' => 'required|email',
                'email_to' => 'required|email',
                'subject' => 'required',
                'body' => 'required'
            )); 

            if(!$validator->fails()){
                try {
                    Mail::send('mail',['body'=>$body], function($message) use ($mailFrom,$mailTo,$subject,$body,$attchFile,$config)
                    {
                        $from = $config['username'];

                        $message->from($from,DB::table('preduzece')->where('preduzece_id',1)->pluck('naziv'));

                        $message->to($mailTo)->replyTo($mailFrom)->subject($subject);
                        
                        if(!is_null($attchFile)){
                            $message->attach($attchFile);
                        }
                    });
                } catch (Exception $e) {
                    // echo $e->getMessage();
                } catch (Throwable $e) {
                    // echo $e->getMessage();
                } catch (Swift_TransportException $e) {
                    // echo $e->getMessage();
                }
            }
        }
    }

    public static function sendOld($mailFrom,$mailTo,$subject,$body,$attchFile=null){
        $mail = new PHPMailer(true);
        try {

            if($_SERVER['SERVER_ADDR'] == '94.130.216.109'){
                $mail->isSMTP();
                $mail->CharSet = 'UTF-8';
                $mail->Host = 'server1.selltico.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'info@selltico.com';
                $mail->Password = 'cjyhN@wSSV2';
                $mail->SMTPSecure = 'starttls';
                $mail->Port = 587;
                $from = 'info@selltico.com';
            }else{
                // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                // $mail->isSMTP();                                    // Set mailer to use SMTP
                $mail->CharSet = 'UTF-8';
                $mail->Host = 'mail.tico.rs';                   // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'info@tico.rs';                 // SMTP username
                $mail->Password = 'rfStad13';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 465;
                $from = 'info@tico.rs';                                // TCP port to connect to
            }


            //Recipients
            $mail->setFrom($from, DB::table('preduzece')->where('preduzece_id',1)->pluck('naziv'));
            $mail->addAddress($mailTo);     // Name is optional
            $mail->addReplyTo($mailFrom);
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            //Attachments
            if(!is_null($attchFile)){
                $mail->addAttachment($attchFile);         // Add attachments
            }
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $body;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            // echo 'Message has been sent';
        } catch (Exception $e) {
            // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

    public static function dexpress_send($email,$attch){

    	$content = '<h2>Poruđžbina</h2>';
		$subject = 'Poruđžbina';
        self::send('info@tico.rs',trim($email),$subject,$content,$attch);
    }


}