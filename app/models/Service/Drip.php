<?php 
namespace Service;

use DateTime;
use Validator;
use DB;
use Cart;
use Product;
use Url_mod;
use Options;
use All;


class Drip {
	private $appName = 'DevTico';
	private $appDomain = 'dev.tico.rs';
	private $accessToken = 'a5a1fd0eacab51737633367d92b73ab6';
	private $provider = 'my_platform';
	private $accountId = null;

	public function __construct(){
		$this->getAccountId();
	}

	private function getAccountId(){
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v2/accounts",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	    	return false;
	    } else {
	    	$result = json_decode($response);
	    	if(isset($result->accounts[0])){
				$this->accountId = $result->accounts[0]->id;
				return true;
			}
	    }
	    return false;
	}

	public function addOrUpdateSubscribers(array $dbSubscribers){
		$postFields = $this->subscribersPayload($dbSubscribers);

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v2/".$this->accountId."/subscribers/batches",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}
	public function getSubscriber($email){

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v2/".$this->accountId."/subscribers/".trim($email),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			)
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return null;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return count($response->subscribers) > 0 ? $response->subscribers[0] : null;
	    		}
	    	}
	    	
	    }
	    return null;
	}

	public function addOrUpdateSubscriber($dbSubscriber){
		$postFields = $this->subscriberPayload($dbSubscriber);

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v2/".$this->accountId."/subscribers",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}

	public function addOrUpdateProducts(array $dbProducts,$action){
		$postFields = $this->productsPayload($dbProducts,$action);
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v3/".$this->accountId."/shopper_activity/product/batch",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}
	public function addOrUpdateProduct($dbProduct,$action){
		$postFields = $this->productPayload($dbProduct,$action);

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v3/".$this->accountId."/shopper_activity/product",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}

	public function addOrUpdateCarts(array $dbcarts,$action){
		$postFields = $this->cartsPayload($dbcarts,$action);
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v3/".$this->accountId."/shopper_activity/cart/batch",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}
	public function addOrUpdateCart($dbcart,$action){
		$postFields = $this->cartPayload($dbcart,$action);
		if(is_null($postFields)){
			return false;
		}

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v3/".$this->accountId."/shopper_activity/cart",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}

	public function addOrUpdateLegacyOrders(array $dbcarts){
		$postFields = $this->legacyOrdersPayload($dbcarts);
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v2/".$this->accountId."/orders/batches",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}

	public function addOrUpdateOrder($dborder,$action){
		$postFields = $this->orderPayload($dborder,$action);
		if(is_null($postFields)){
			return false;
		}

	    $curl = curl_init();
	    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.getdrip.com/v3/".$this->accountId."/shopper_activity/order",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_USERPWD => $this->accessToken,
			CURLOPT_POSTFIELDS => json_encode($postFields),
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"User-Agent: ".$this->appName." (".$this->appDomain.")"
			),
	    ));
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if($err) {
	    	return false;
	    } else {
	    	$response = json_decode($response);
	    	if(is_object($response)){
	    		if(!(isset($response->errors) && count($response->errors) > 0)){
	    			return true;
	    		}
	    	}
	    	
	    }
	    return false;
	}












	private function subscribersPayload(array $dbSubscribers){
		$subscribers = array('batches' => array(array('subscribers' => array())));
		$uniqueSubscribers = array();
		foreach($dbSubscribers as $kupac){
			$emailArr = explode(' ',trim($kupac->email));
			if(isset($emailArr[0])){
				$validator = Validator::make(array('email'=>trim($emailArr[0])),array('email'=>'email'));
				if($validator->passes() && !in_array(trim($emailArr[0]),$uniqueSubscribers)){
					$subscribers['batches'][0]['subscribers'][] = array(
			          "email"=> trim($emailArr[0]),
			          "time_zone"=> "Europe/Belgrade",
			          "tags"=> array("Customer")
					);
					$uniqueSubscribers[] = $kupac->email;
				}
			}
		}
		return $subscribers;	
	}

	private function subscriberPayload($dbCustumer){
		$subscribers = array('subscribers' => array());

		$emailArr = explode(' ',trim($dbCustumer->email));
		if(isset($emailArr[0])){
			$validator = Validator::make(array('email'=>trim($emailArr[0])),array('email'=>'email'));
			if($validator->passes()){
				$subscribers['subscribers'][] = array(
		          "email"=> trim($emailArr[0]),
		          "time_zone"=> "Europe/Belgrade",
		          "tags"=> array("Customer"),
		          "first_name" => $dbCustumer->flag_vrsta_kupca == 0 ? $dbCustumer->ime : $dbCustumer->naziv,
		          "last_name" => $dbCustumer->flag_vrsta_kupca == 0 ? $dbCustumer->prezime : $dbCustumer->pib,
		          "address1" => $dbCustumer->adresa,
		          "country" => $dbCustumer->mesto,
		          "state" => 'Srbija',
		          "phone" => $dbCustumer->telefon,
		          "user_id" => $dbCustumer->web_kupac_id
				);
			}
		}
		return $subscribers;	
	}

	private function productsPayload(array $dbProducts,$action){
		$products = array('products' => array());
		$uniqueproducts = array();
		foreach($dbProducts as $product){
			$products['products'][] = array(
		        "provider"=> $this->provider,
		        "action"=> $action,
		        // "occurred_at"=> "2019-01-28T12=>15=>23Z",
		        "product_id"=> strval($product->roba_id),
		        "product_variant_id"=> strval($product->roba_id).'-01',
		        "sku"=> trim($product->sku),
		        "name"=> $product->naziv_web,
		        "brand"=> $product->proizvodjac_id > 0 ? DB::table('proizvodjac')->where('proizvodjac_id',$product->proizvodjac_id)->pluck('naziv') : '',
		        "categories"=> [
		          $product->grupa_pr_id > 0 ? DB::table('grupa_pr')->where('grupa_pr_id',$product->grupa_pr_id)->pluck('grupa') : 'Bez grupe'
		        ],
		        "price"=> floatval($product->web_cena),
		        "inventory"=> ($inventory = Cart::check_avaliable($product->roba_id)) > 0 ? intval($inventory) : 0,
		        "product_url"=> Options::base_url() . Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($product->roba_id)),
		        "image_url"=> Options::domain() . Product::web_slika($product->roba_id)
			);
		}
		return $products;	
	}
	private function productPayload($dbProduct,$action){
		return array(
	        "provider"=> $this->provider,
	        "action"=> $action,
	        // "occurred_at"=> "2019-01-28T12=>15=>23Z",
	        "product_id"=> strval($dbProduct->roba_id),
	        "product_variant_id"=> strval($dbProduct->roba_id).'-01',
	        "sku"=> trim($dbProduct->sku),
	        "name"=> $dbProduct->naziv_web,
	        "brand"=> $dbProduct->proizvodjac_id > 0 ? DB::table('proizvodjac')->where('proizvodjac_id',$dbProduct->proizvodjac_id)->pluck('naziv') : '',
	        "categories"=> [
	          $dbProduct->grupa_pr_id > 0 ? DB::table('grupa_pr')->where('grupa_pr_id',$dbProduct->grupa_pr_id)->pluck('grupa') : 'Bez grupe'
	        ],
	        "price"=> floatval($dbProduct->web_cena),
	        "inventory"=> ($inventory = Cart::check_avaliable($dbProduct->roba_id)) > 0 ? intval($inventory) : 0,
	        "product_url"=> Options::base_url() . Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($dbProduct->roba_id)),
	        "image_url"=> Options::domain() . Product::web_slika($dbProduct->roba_id)
		);
	}

	private function cartsPayload(array $dbCarts,$action){
		$carts = array('carts' => array());

		foreach($dbCarts as $dbCart){
			$customer = DB::table('web_kupac')->where('web_kupac_id',$dbCart->web_kupac_id)->first();
			$emailArr = explode(' ',trim($customer->email));
			if(isset($emailArr[0])){
				$validator = Validator::make(array('email'=>trim($emailArr[0])),array('email'=>'email'));
				if($validator->passes()){
					$cartItems = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',$dbCart->web_b2c_korpa_id)->get();

					$cart = array(
				        'provider' => $this->provider,
				        'email' => trim($emailArr[0]),
				        'action' => $action,
				        'cart_id' => strval($dbCart->web_b2c_korpa_id),
				        'occurred_at' => (new DateTime($dbCart->datum))->format('c'),
				        'cart_public_id' => '#'.strval($dbCart->web_b2c_korpa_id),
				        'grand_total' => round(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',$dbCart->web_b2c_korpa_id)->sum(DB::raw('jm_cena*kolicina')),2),
				        'total_discounts' => 0,
				        'currency' => 'RSD',
				        'cart_url' => Options::base_url() . Url_mod::slug_trans('korpa'),
				        'items' => array()			
					);

					foreach($cartItems as $item){
						$product = DB::table('roba')->where('roba_id',$item->roba_id)->first();
						$cart['items'][] = array(
					        'product_id'=> strval($product->roba_id),
					        'product_variant_id'=> strval($product->roba_id).'-01',
					        'sku'=> trim($product->sku),
					        'name'=> $product->naziv_web,
					        'brand'=> $product->proizvodjac_id > 0 ? DB::table('proizvodjac')->where('proizvodjac_id',$product->proizvodjac_id)->pluck('naziv') : '',
					        'categories'=> [
					          $product->grupa_pr_id > 0 ? DB::table('grupa_pr')->where('grupa_pr_id',$product->grupa_pr_id)->pluck('grupa') : 'Bez grupe'
					        ],
					        'price'=> floatval($item->jm_cena),
					        'quantity'=> intval($item->kolicina),
					        'discounts'=> 0,
					        'total'=> floatval($item->kolicina*$item->jm_cena),
					        'product_url'=> Options::base_url() . Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($product->roba_id)),
					        'image_url'=> Options::domain() . Product::web_slika($product->roba_id)
						);
					}
					$carts['carts'][] = $cart;
				}
			}

		}
		return $carts;		
	}
	private function cartPayload($dbCart,$action){
		$cart = null;
		$customer = DB::table('web_kupac')->where('web_kupac_id',$dbCart->web_kupac_id)->first();
		$emailArr = explode(' ',trim($customer->email));
		if(isset($emailArr[0])){
			$validator = Validator::make(array('email'=>trim($emailArr[0])),array('email'=>'email'));
			if($validator->passes()){
				$cartItems = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',$dbCart->web_b2c_korpa_id)->get();

				$cart = array(
			        'provider' => $this->provider,
			        'email' => trim($emailArr[0]),
			        'action' => $action,
			        'cart_id' => strval($dbCart->web_b2c_korpa_id),
			        'occurred_at' => (new DateTime($dbCart->datum))->format('c'),
			        'cart_public_id' => '#'.strval($dbCart->web_b2c_korpa_id),
			        'grand_total' => round(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',$dbCart->web_b2c_korpa_id)->sum(DB::raw('jm_cena*kolicina')),2),
			        'total_discounts' => 0,
			        'currency' => 'RSD',
			        'cart_url' => Options::base_url() . Url_mod::slug_trans('korpa'),
			        'items' => array()			
				);

				foreach($cartItems as $item){
					$product = DB::table('roba')->where('roba_id',$item->roba_id)->first();
					$cart['items'][] = array(
				        'product_id'=> strval($product->roba_id),
				        'product_variant_id'=> strval($product->roba_id).'-01',
				        'sku'=> trim($product->sku),
				        'name'=> $product->naziv_web,
				        'brand'=> $product->proizvodjac_id > 0 ? DB::table('proizvodjac')->where('proizvodjac_id',$product->proizvodjac_id)->pluck('naziv') : '',
				        'categories'=> [
				          $product->grupa_pr_id > 0 ? DB::table('grupa_pr')->where('grupa_pr_id',$product->grupa_pr_id)->pluck('grupa') : 'Bez grupe'
				        ],
				        'price'=> floatval($item->jm_cena),
				        'quantity'=> intval($item->kolicina),
				        'discounts'=> 0,
				        'total'=> floatval($item->kolicina*$item->jm_cena),
				        'product_url'=> Options::base_url() . Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($product->roba_id)),
				        'image_url'=> Options::domain() . Product::web_slika($product->roba_id)
					);
				}
			}
		}

		return $cart;		
	}

	private function legacyOrdersPayload(array $dbOrders){
		$orders = array('batches' => array(array('orders' => array())));
		$uniquecarts = array();
		foreach($dbOrders as $dbOrder){
			$customer = DB::table('web_kupac')->where('web_kupac_id',$dbOrder->web_kupac_id)->first();
			$emailArr = explode(' ',trim($customer->email));
			if(isset($emailArr[0])){
				$validator = Validator::make(array('email'=>trim($emailArr[0])),array('email'=>'email'));
				if($validator->passes()){
					$orderItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$dbOrder->web_b2c_narudzbina_id)->get();
					if(count($orderItems) == 0){
						continue;
					}

					$amount = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$dbOrder->web_b2c_narudzbina_id)->sum(DB::raw('jm_cena*kolicina'));
					$tax = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$dbOrder->web_b2c_narudzbina_id)->sum(DB::raw('jm_cena*kolicina*((select porez from tarifna_grupa where tarifna_grupa_id = web_b2c_narudzbina_stavka.tarifna_grupa_id limit 1)/100)'));
					$order = array(
						"email" => trim($emailArr[0]),
						"provider" => "my_platform",
						"upstream_id" => strval($dbOrder->web_b2c_narudzbina_id),
						"identifier" => $dbOrder->broj_dokumenta,
						"amount" => round($amount,2),
						"tax" => round($tax,2),
						"fees" => 0,
						"discount" => $dbOrder->popust,
						"permalink" => Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$dbOrder->web_b2c_narudzbina_id,
						"currency_code" => "RSD",
						// "properties" => array(
						// 	"shirt_size" => "Medium",
						// 	"color" => "red"
						// ),
						"occurred_at" => (new DateTime($dbOrder->datum_dokumenta))->format('c'),
						"closed_at" => null,
						"cancelled_at" => null,
						"financial_state" => $dbOrder->prihvaceno == 1 ? ($dbOrder->stornirano == 0 ? ($dbOrder->realizovano == 1 ? 'paid' : 'pending') : 'voided') : 'pending',
						"fulfillment_state" => $dbOrder->prihvaceno == 1 && $dbOrder->stornirano == 0 && $dbOrder->realizovano == 1 ? 'fulfilled' : 'not_fulfilled'
						// "billing_address" => array(),
						// "shipping_address" => array()
		
					);

					foreach($orderItems as $item){
						$product = DB::table('roba')->where('roba_id',$item->roba_id)->first();
						$itemTax = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$item->tarifna_grupa_id)->pluck('porez')/100;

						$order['items'][] = array(
				            // "id" => "8888888",
				            "product_id" => strval($product->roba_id),
				            "sku" => trim($product->sku),
				            "amount" => floatval($item->kolicina*$item->jm_cena),
				            "name" => $product->naziv_web,
				            "quantity" => intval($item->kolicina),
				            "upstream_id" => strval($product->roba_id),
				            "upstream_product_id" => strval($product->roba_id),
				            "upstream_product_variant_id" => strval($product->roba_id).'-01',
				            "price" => floatval($item->jm_cena*(1-$itemTax)),
				            "tax" => floatval($item->kolicina*$item->jm_cena*$itemTax),
				            "fees" => 0,
				            "discount" => 0,
				            "taxable" => true,
				            // "properties" => array(
				            //   "color" => "black"
				            // )
						);
					}
					$orders['batches'][0]['orders'][] = $order;
				}
			}

		}
		return $orders;		
	}

	private function orderPayload($dbOrder,$action){
		$order = null;

		$customer = DB::table('web_kupac')->where('web_kupac_id',$dbOrder->web_kupac_id)->first();
		$emailArr = explode(' ',trim($customer->email));
		if(isset($emailArr[0])){
			$validator = Validator::make(array('email'=>trim($emailArr[0])),array('email'=>'email'));
			if($validator->passes()){
				$orderItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$dbOrder->web_b2c_narudzbina_id)->get();

				$amount = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$dbOrder->web_b2c_narudzbina_id)->sum(DB::raw('jm_cena*kolicina'));
				$tax = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$dbOrder->web_b2c_narudzbina_id)->sum(DB::raw('jm_cena*kolicina*((select porez from tarifna_grupa where tarifna_grupa_id = web_b2c_narudzbina_stavka.tarifna_grupa_id limit 1)/100)'));
				$order = array(
					"provider" => "my_platform",
					"email" => trim($emailArr[0]),
					"action"=> $action,
					"occurred_at" => (new DateTime($dbOrder->datum_dokumenta))->format('c'),
					"order_id" => strval($dbOrder->web_b2c_narudzbina_id),
					"order_public_id" => $dbOrder->broj_dokumenta,
					"grand_total" => round($amount,2),
					"total_discounts" => floatval($dbOrder->popust),
					"total_taxes" => round($tax,2),
					"total_fees" => 0,
					"total_shipping"=> floatval(Cart::troskovi($dbOrder->web_b2c_narudzbina_id)),
					"currency" => "RSD",
					"order_url" => Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$dbOrder->web_b2c_narudzbina_id

				);

				foreach($orderItems as $item){
					$product = DB::table('roba')->where('roba_id',$item->roba_id)->first();
					$itemTax = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$item->tarifna_grupa_id)->pluck('porez')/100;

					$order['items'][] = array(
			            "product_id" => strval($product->roba_id),
			            "product_variant_id"=> strval($product->roba_id).'-01',
			            "sku" => trim($product->sku),
			            "name" => $product->naziv_web,
				        "brand"=> $product->proizvodjac_id > 0 ? DB::table('proizvodjac')->where('proizvodjac_id',$product->proizvodjac_id)->pluck('naziv') : '',
				        "categories"=> [
				          $product->grupa_pr_id > 0 ? DB::table('grupa_pr')->where('grupa_pr_id',$product->grupa_pr_id)->pluck('grupa') : 'Bez grupe'
				        ],
			            "price" => floatval($item->jm_cena*(1-$itemTax)),
			            "sale_price" => $product->akcija_flag_primeni == 1 ? floatval($item->jm_cena*(1-$itemTax)) : 0,
			            "quantity" => intval($item->kolicina),
			            "discounts" => 0,
			            "taxes" => floatval($item->kolicina*$item->jm_cena*$itemTax),
			            "fees" => 0,
			            "shipping" => floatval(Cart::troskovi(null,$product->roba_id)),
			            "total" => floatval($item->kolicina*$item->jm_cena),
				        "product_url"=> Options::base_url() . Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($product->roba_id)),
				        "image_url"=> Options::domain() . Product::web_slika($product->roba_id)
			            // "product_tag": "Best Seller"
					);
				}
			}
		}

		return $order;		
	}

}