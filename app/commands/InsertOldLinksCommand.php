<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class InsertOldLinksCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:old-links';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[strval($article->sifra_is)] = $article->roba_id;
		}
		return $mapped;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$mappedArticles = self::getMappedArticles();
		$xml = simplexml_load_file(__DIR__.'/../../old_links.xml');

		$data_body = "";
		foreach($xml as $item) {
			$link = substr(strval($item->link),1);
			if(isset($link) && $link != '' && isset($mappedArticles[strval($item->sifra)])){

				$data_body .= "('".$link."','".$mappedArticles[strval($item->sifra)]."'),";
			}
		}

		if($data_body != ''){
		    // DB::statement("INSERT INTO stari_linkovi (link,id) VALUES ".substr($data_body,0,-1));
		}

		$this->info('Links has been inserted successfully.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
