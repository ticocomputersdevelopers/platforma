<?php

class AdminB2BNarudzbinaController extends Controller {

       
	public function narudzbine($status,$narudzbina_status_id, $datum_od, $datum_do ,$search)
	{
        if(!AdminB2BOptions::check_admin(array('B2B_NARUDZBINE')) && !AdminB2BOptions::check_admin(array('B2B_NARUDZBINE_PREGLED'))){
            return Redirect::to(Option::base_url().'admin/b2b');
        }
        
        $statusi = explode('-',$status);
        $query_osnovni=0;
        
        $select = "SELECT * FROM web_b2b_narudzbina LEFT JOIN partner ON partner.partner_id = web_b2b_narudzbina.partner_id ";
        $where="";
        $where_status= "";
        
        if (count($statusi)>1 && in_array('sve', $statusi)) {
            unset($statusi[array_search('sve', $statusi)]);
        }

        if (!in_array('sve', $statusi)) {
         
          
            if (in_array('nove', $statusi)) {
            $where_status.= "OR (prihvaceno=0 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('prihvacene', $statusi)) {
            $where_status.= "OR (prihvaceno=1 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('realizovane', $statusi)) {
            $where_status.= "OR (realizovano=1 AND stornirano=0) ";
            }
            if (in_array('stornirane', $statusi)) {
            $where_status.= "OR stornirano=1 ";
            }           
        }


        if ($where_status != '') {
            $where.="AND (".substr($where_status, 3).") ";
        }

        if($datum_od==0 and $datum_do==0){
        }
        else if($datum_od!=0 and $datum_do==0){
            $where.= "AND datum_dokumenta >= '".$datum_od."' ";
        }
        else if($datum_od==0 and $datum_do!=0){
            $where.= "AND datum_dokumenta <= '".$datum_do."' ";
        }
        else if($datum_od!=0 and $datum_do!=0){
            $where.= "AND datum_dokumenta >= '".$datum_od."' AND datum_dokumenta <= '".$datum_do."' ";
        }
        if($narudzbina_status_id==0){
        }
        else if($narudzbina_status_id !=0){
            $where.="AND narudzbina_status_id ='".$narudzbina_status_id."' ";
        }

        if($search=='' || $search==0){
            $where.="AND web_b2b_narudzbina_id != -1 "; 
        }
        else if($search !='' && $search!=0){
            $where_search="";
            foreach (explode(' ',$search) as $word) {                   
                      $where_search.= "broj_dokumenta ILIKE '%" . strtoupper($word) . "%' OR adresa ILIKE '%" . $word . "%' OR naziv ILIKE '%" . $word . "%' OR telefon ILIKE '%" . $word . "%' OR ";
                       }           
            $where.="AND (". substr($where_search, 0,-3) .") ";
        }
 
        if(Input::get('page')){
            $pageNo = Input::get('page');
        }else{
            $pageNo = 1;
        }

        $limit = 20;
        $offset = ($pageNo-1)*$limit;

        $pagination = " ORDER BY web_b2b_narudzbina_id DESC LIMIT ".$limit." OFFSET ".$offset."";

        $query_basic = DB::select($select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3));
        $query = DB::select($select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3).$pagination);

		$data=array(
	                "strana" => 'b2b_narudzbine',
	                "title" => 'B2B narudzbine',
                    "search"=>$search,
                    "statusi"=>$statusi,
                    "query"=>$query,
                    "count"=>count($query_basic),
                    "limit"=>$limit,
                    "narudzbina_status_id"=>$narudzbina_status_id,	                
                    "datum_od"=>$datum_od,
                    "datum_do"=>$datum_do
	            );
		return View::make('adminb2b/pages/b2b_narudzbine', $data);
		
	}

    function narudzbina($web_b2b_narudzbina_id, $roba_id = null)
    {   
        
        if(isset($roba_id)){
            $check_roba_id = DB::table('web_b2b_narudzbina_stavka')->where(array('web_b2b_narudzbina_id'=>$web_b2b_narudzbina_id,'roba_id'=>$roba_id))->count();
            if($check_roba_id == 0){            
                $stavka_data = DB::table('roba')->select('tarifna_grupa_id','racunska_cena_nc')->where('roba_id',$roba_id)->first();
                $insert_array = array(
                    'web_b2b_narudzbina_id' => $web_b2b_narudzbina_id,
                    'broj_stavke' => 1,
                    'roba_id' => $roba_id,
                    'kolicina' => 1,
                    'jm_cena' => AdminCommon::get_price($roba_id),
                    'tarifna_grupa_id' => $stavka_data->tarifna_grupa_id,
                    'racunska_cena_nc' => $stavka_data->racunska_cena_nc
                    );
                if(AdminB2BOptions::vodjenje_lageraB2B() == 1){
                    $new_lager = intval(AdminSupport::lager($roba_id)) - 1;
                    DB::table('lager')->where('roba_id',$roba_id)->update(array('kolicina'=>$new_lager));
                }

                DB::table('web_b2b_narudzbina_stavka')->insert($insert_array);

                AdminSupport::saveLog('B2B_NARUDZBINA_STAVKA_DODAJ', array(DB::table('web_b2b_narudzbina_stavka')->max('web_b2b_narudzbina_stavka_id')));
            }
        }

        $web_b2b_narudzbina_id == 0 ? $check_old = false : $check_old = true;
        $data=array(
        "strana"=>'narudzbina',
        "title"=>"Narudzbina",
        "web_b2b_narudzbina_id" => $web_b2b_narudzbina_id,
        "partner_id"=> $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'partner_id') : '',
        "broj_dokumenta" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'broj_dokumenta') : AdminB2BNarudzbina::brojNarudzbine(),
        "datum_dokumenta" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'datum_dokumenta') : date('Y-m-d'),
        "web_nacin_placanja_id" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'web_nacin_placanja_id') : '',
        "web_nacin_isporuke_id" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'web_nacin_isporuke_id') : '',
        "ip_adresa" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'ip_adresa') : '',
        "napomena" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'napomena') : '',
        "posta_slanje_broj_posiljke" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'posta_slanje_broj_posiljke') : '',
        "posta_slanje_poslato" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id,'posta_slanje_poslato') : 0,
        "narudzbina_stavke" => $check_old ? DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->orderBy('web_b2b_narudzbina_stavka_id', 'asc')->get() : array(),
        "ukupna_cena" => $check_old ? AdminB2BNarudzbina::orderTotal($web_b2b_narudzbina_id) : null,
        "troskovi_isporuke" => $check_old ? AdminB2BNarudzbina::troskovi($web_b2b_narudzbina_id) : '0.00',
        "prihvaceno" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'prihvaceno') : '',
        "realizovano" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'realizovano') : '',
        "stornirano" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'stornirano') : '',
        "posta_slanje_id" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'posta_slanje_id') : 1,
        "narudzbina_status_id" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'narudzbina_status_id') : '',
        "avans" => $check_old ? AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'avans') : '0.00',
        "komercijalista" => $check_old ? (!is_null($imenik_id = AdminB2BNarudzbina::find($web_b2b_narudzbina_id, 'imenik_id')) ? DB::table('imenik')->where('imenik_id',$imenik_id)->first() : null) : null
        );
    return View::make('adminb2b/pages/narudzbina',$data);

    }

	public function b2b_promena_statusa_narudzbine(){
        
        $status=Input::get('status_target');
        $web_b2b_narudzbina_id=Input::get('web_b2b_narudzbina_id');

        switch($status){
            case "1":
                DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->update(array('prihvaceno'=>1));

                AdminSupport::saveLog('B2B_NARUDZBINA_STATUS_PRIHVATI', array($web_b2b_narudzbina_id));
              
                echo '1';
            break;
            
             case "2":
                foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
                    AdminB2BNarudzbina::rezervacija($row->roba_id,$row->kolicina);
                }
                
                DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->update(array('realizovano'=>1));
                AdminSupport::saveLog('B2B_NARUDZBINA_STATUS_REALIZUJ', array($web_b2b_narudzbina_id));
                
                echo '2';
            break;
            case "3":
                $realizovna=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('realizovano');
                foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
                   if($realizovna==1){
                        AdminB2BNarudzbina::rezervacija($row->roba_id,-1*$row->kolicina);
                    }
                    AdminB2BNarudzbina::rezervacija($row->roba_id,$row->kolicina,true);
                }
                
                DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->update(array('stornirano'=>1,'realizovano'=>0));
                AdminSupport::saveLog('B2B_NARUDZBINA_STATUS_STORNIRAJ', array($web_b2b_narudzbina_id));
                
                echo '3';
            break;
        }
     
    }

    public function porudzbina_vise(){
        $web_b2b_narudzbina_id = Input::get('web_b2b_narudzbina_id');
        
        foreach(DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
            echo '  <ul class="order-more-details-list">
                
                    <li class="row">
                        <span class="medium-6 columns">Broj porudžbine:</span>
                        <span class="medium-6 columns">'.$row->broj_dokumenta.'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Datum porudžbine:</span>
                        <span class="medium-6 columns">'.AdminB2BNarudzbina::datum_narudzbine($row->web_b2b_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Nacin isporuke:</span>
                        <span class="medium-6 columns">'.AdminB2BNarudzbina::narudzbina_ni($row->web_b2b_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Način plaćanja:</span>
                        <span class="medium-6 columns">'.AdminB2BNarudzbina::narudzbina_np($row->web_b2b_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Napomena:</span>
                        <span class="medium-6 columns">'.$row->napomena.'</span>
                    </li>
            
                </ul>';
        }
        echo '
        <hr>
        <ul class="order-more-product-list">
            <li class="row titles">
            <span class="medium-7 columns">Naziv proizvoda:</span>
            <span class="medium-2 columns">Cena:</span>
            <span class="medium-1 columns">Količina:</span>
            <span class="medium-2 columns">Ukupna cena:</span>
        </li>
        ';
        foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row2){
            echo '<li class="row">
                        <span class="medium-7 columns">'.AdminB2BNarudzbina::artikal_narudzbina($row2->web_b2b_narudzbina_stavka_id).'</span>
                        <span class="medium-2 columns">'.AdminB2BNarudzbina::artikal_cena_naruzbina($row2->web_b2b_narudzbina_stavka_id).'</span>
                        <span class="medium-1 columns">'.$row2->kolicina.'</span>
                        <span class="medium-2 columns">'.AdminB2BNarudzbina::artikal_cena_stavka($row2->web_b2b_narudzbina_stavka_id).'</span>
                    </li>           
                ';
        }
        echo '
        <hr>
        <li class="row">
            <span class="medium-9 columns"><b>Ukupno:</b></span>
            <span class="medium-3 columns">'.AdminB2BArticles::cena(AdminB2BNarudzbina::narudzbina_iznos_ukupno($web_b2b_narudzbina_id)).'</span>
        </li>
        </ul>';
    }  
    public function updateNarudzbinu()
    {
               $data = Input::get();
        if(AdminB2BNarudzbina::find($data['web_b2b_narudzbina_id'], 'realizovano') || AdminB2BNarudzbina::find($data['web_b2b_narudzbina_id'], 'stornirano')){
            $data_rel_stor = (array) DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$data['web_b2b_narudzbina_id'])->first();
            $data_rel_stor['napomena'] = $data['napomena'];
            $data = $data_rel_stor;
        }

        $rules = array(
            'partner_id' => 'not_in:0',
            'broj_dokumenta' => 'required|between:2,10|unique:web_b2b_narudzbina,broj_dokumenta,'.$data['web_b2b_narudzbina_id'].',web_b2b_narudzbina_id',
            'posta_slanje_broj_posiljke' => 'max:15',
            'ip_adresa' => 'max:20',
            'napomena' => 'max:1000'
        );

        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/narudzbina/'.$data['web_b2b_narudzbina_id'])->withInput()->withErrors($validator->messages());
        }else{

            $narudzbina = AdminB2BNarudzbina::save($data);
            $data['web_b2b_narudzbina_id'] == 0 ? $web_b2b_narudzbina_id = DB::select("SELECT currval('web_b2b_narudzbina_web_b2b_narudzbina_id_seq')")[0]->currval : $web_b2b_narudzbina_id = $data['web_b2b_narudzbina_id'];

            // AdminSupport::saveLog('Narudzbine, INSERT/EDIT web_b2b_narudzbina_id -> '.$web_b2b_narudzbina_id);
            return Redirect::to(AdminOptions::base_url().'admin/b2b/narudzbina/'.$web_b2b_narudzbina_id);
        }
    }

    public function prihvati(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $narudzbina_id)->update(['prihvaceno' => 1]);

        AdminSupport::saveLog('B2B_NARUDZBINA_STATUS_PRIHVATI', array($narudzbina_id));
    }

    public function realizuj(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $narudzbina_id)->update(['realizovano' => 1]);
        $stavke = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $narudzbina_id)->get();
        foreach($stavke as $stavka){
            AdminB2BNarudzbina::rezervacija($stavka->roba_id,$stavka->kolicina);
        }

        AdminSupport::saveLog('B2B_NARUDZBINA_STATUS_REALIZUJ', array($narudzbina_id));

    }

    public function storniraj(){
        $narudzbina_id = Input::get('narudzbina_id');
        $realizovna=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$narudzbina_id)->pluck('realizovano');
        $stavke = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $narudzbina_id)->get();
        foreach($stavke as $stavka){
            if($realizovna != 1){
                AdminB2BNarudzbina::rezervacija($stavka->roba_id,$stavka->kolicina,true);
            }
            
        }
        DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $narudzbina_id)->update(['stornirano' => 1]);
        AdminSupport::saveLog('B2B_NARUDZBINA_STATUS_STORNIRAJ', array($narudzbina_id));
    }

    public function nestorniraj(){
        $narudzbina_id = Input::get('narudzbina_id');
        $stavke = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $narudzbina_id)->get();
        foreach($stavke as $stavka){
            AdminB2BNarudzbina::rezervacija($stavka->roba_id,-1*$stavka->kolicina,true);
        }
        DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $narudzbina_id)->update(['stornirano' => 0]);
        AdminSupport::saveLog('B2B_NARUDZBINA_ODSTORNIRAJ', array($narudzbina_id));
    }

    public function pdf($web_b2b_narudzbina_id)
    {
        $data = [
            'web_b2b_narudzbina_id' => $web_b2b_narudzbina_id,
            "ukupna_cena" => AdminB2BNarudzbina::orderTotal($web_b2b_narudzbina_id),
            "troskovi_isporuke" => AdminB2BNarudzbina::troskovi($web_b2b_narudzbina_id)
        ];

        $pdf = App::make('dompdf');
        
        $pdf->loadView('adminb2b.pages.pdf_narudzbina', $data);
        return $pdf->stream();

        // AdminB2BNarudzbina::createPdf($web_b2b_narudzbina_id);

    }

    public function pdf_racun($web_b2b_narudzbina_id)
    {
         $data = [
             'web_b2b_narudzbina_id' => $web_b2b_narudzbina_id,
            "ukupna_cena" => AdminB2BNarudzbina::orderTotal($web_b2b_narudzbina_id),
            "troskovi_isporuke" => AdminB2BNarudzbina::troskovi($web_b2b_narudzbina_id)
         ];

        $pdf = App::make('dompdf');
        $pdf->loadView('adminb2b.pages.pdf_racun', $data);

        AdminSupport::saveLog('B2B_NARUDZBINA_PDF_RACUN', array($web_b2b_narudzbina_id));
        return $pdf->stream();    

    }

    public function pdf_ponuda($web_b2b_narudzbina_id)
    {
         $data = [
             'web_b2b_narudzbina_id' => $web_b2b_narudzbina_id,
            "ukupna_cena" => AdminB2BNarudzbina::orderTotal($web_b2b_narudzbina_id),
            "troskovi_isporuke" => AdminB2BNarudzbina::troskovi($web_b2b_narudzbina_id)
         ];

        $pdf = App::make('dompdf');
        $pdf->loadView('adminb2b.pages.pdf_ponuda', $data);

        AdminSupport::saveLog('B2B_NARUDZBINA_PDF_PONUDA' , array($web_b2b_narudzbina_id));
        return $pdf->stream();
    }

    public function pdf_predracun($web_b2b_narudzbina_id)
    {
        $data = [
             'web_b2b_narudzbina_id' => $web_b2b_narudzbina_id,
             "ukupna_cena" => AdminB2BNarudzbina::orderTotal($web_b2b_narudzbina_id),
             "troskovi_isporuke" => AdminB2BNarudzbina::troskovi($web_b2b_narudzbina_id)
        ];

        $pdf = App::make('dompdf');
        $pdf->loadView('adminb2b.pages.pdf_predracun', $data);

        AdminSupport::saveLog('B2B_NARUDZBINA_PDF_PREDRACUN', array($web_b2b_narudzbina_id));
        return $pdf->stream();
    }

    public function editCena(){
            $narudzbina_id = Input::get('narudzbina_id');  
            $stavka_id = Input::get('stavka_id');         
            $val = Input::get('val');   
                       
            DB::table('web_b2b_narudzbina_stavka')->where(array('web_b2b_narudzbina_stavka_id'=> $stavka_id ))->update(['jm_cena' => $val]);
            AdminSupport::saveLog('B2B_CENA_IZMENI', array($stavka_id)); 

        $cena_artikala = AdminB2BNarudzbina::ukupnaCena($narudzbina_id);
        if(AdminOptions::web_options(133) == 1 AND AdminCommon::troskovi_isporuke($narudzbina_id)>0){
            $troskovi_isporuke = AdminCommon::troskovi_isporuke($narudzbina_id);
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
            $results['troskovi_isporuke'] = $troskovi_isporuke;
            $results['cena_artikla'] = $cena_artikala;
        }else{
            $results['ukupna_cena'] = $cena_artikala;
        }
        return json_encode($results);      
    }

     public function deleteStavka() {
        $stavka_id = Input::get('stavka_id');
        $stavka = DB::table('web_b2b_narudzbina_stavka')->select('roba_id','web_b2b_narudzbina_id')->where('web_b2b_narudzbina_stavka_id', $stavka_id)->first();
        $roba_id = $stavka->roba_id;
        $web_b2b_narudzbina_id = $stavka->web_b2b_narudzbina_id;

        if(AdminB2BOptions::web_options(131) == 1){
            $kolicina = AdminB2BNarudzbina::kolicina($web_b2b_narudzbina_id,$roba_id);

            $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
            $orgj_id = DB::table('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
            $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
            if($lager && ($lager->rezervisano - $kolicina) >= 0){
                DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('rezervisano'=>($lager->rezervisano - $kolicina)));
            }
        }

        
        AdminSupport::saveLog('B2B_NARUDZBINA_STAVKA_OBRISI', array($stavka_id));
        DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id', $stavka_id)->delete();

    }

    public function obrisi(){
        $narudzbina_id = Input::get('narudzbina_id');

        if(AdminB2BOptions::web_options(131) == 1){
            $stavke = DB::table('web_b2b_narudzbina_stavka')->select('roba_id','web_b2b_narudzbina_id')->where('web_b2b_narudzbina_id', $narudzbina_id)->get();
            foreach($stavke as $stavka){
                $roba_id = $stavka->roba_id;

                $kolicina = AdminB2BNarudzbina::kolicina($narudzbina_id,$roba_id);

                $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
                $orgj_id = DB::table('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
                $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
                if($lager && ($lager->rezervisano - $kolicina) >= 0){
                    DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('rezervisano'=>($lager->rezervisano - $kolicina)));
                }
            }
        }
        AdminSupport::saveLog('B2B_NARUDZBINA_(STAVKA)_OBRISI', array($narudzbina_id));

        DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $narudzbina_id)->delete();
        DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $narudzbina_id)->delete();


    }

     public function narudzbineSearch() {

        $rec = trim(Input::get('articles'));
        $narudzbina_id = Input::get('narudzbina_id');

        $not_ids = 'r.roba_id NOT IN (';
        $artikli = DB::table('web_b2b_narudzbina_stavka')->select('roba_id')->where('web_b2b_narudzbina_id',$narudzbina_id)->get();
        
        if(count($artikli) > 0){            
            foreach ($artikli as $row) {
                $not_ids .= $row->roba_id.',';

            }
            $not_ids = substr($not_ids, 0, -1).') AND ';
        }else{
             $not_ids = '';
        }

        if(AdminOptions::vodjenje_lagera() == 1){
            $lager_join = 'INNER JOIN lager l ON r.roba_id = l.roba_id ';
            $lager_where = 'l.kolicina <> 0 AND ';
        }else{
            $lager_join = '';
            $lager_where = '';
        }

        is_numeric($rec) ? $robaIdFormatiran = "r.roba_id = '" . $rec . "' OR " : $robaIdFormatiran = "";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = DB::select("SELECT r.roba_id, naziv_web, grupa FROM roba r ".$lager_join."INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id INNER JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE ".$lager_where."flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$not_ids."(" . $robaIdFormatiran . "" . $nazivFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . ") ORDER BY grupa ASC");
        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list'>";
        foreach ($articles as $article) {
            $list .= "
                <li class='articles_list__item'>
                    <a class='articles_list__item__link' href='" . AdminOptions::base_url() . "admin/b2b/narudzbina/".$narudzbina_id."/" . $article->roba_id . "'>"
                        ."<span class='articles_list__item__link__small'>" . $article->roba_id . "</span>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </a>
                </li>";
        }

        $list .= "</ul>";
        echo $list; 
    }

    public function updateKolicina() {

        $stavka_id = Input::get('stavka_id');
        $kolicina = Input::get('kolicina');
        $narudzbina_id = Input::get('narudzbina_id');
        $results = array(
            'cena_artikla' => 0,
            'troskovi_isporuke' => 0,
            'ukupna_cena' => 0,
            'lager' => 0
            );

        $roba_id = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id', $stavka_id)->pluck('roba_id');
        $results['lager'] = AdminB2BSupport::lager($roba_id);

        if(AdminB2BOptions::web_options(131) == 1){
            $oldKolicina = AdminB2BNarudzbina::kolicina($narudzbina_id,$roba_id);
            $razlika = intval($kolicina) - $oldKolicina;

            $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
            $orgj_id = DB::table('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
            $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
            if($lager){
                DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('rezervisano'=>($lager->rezervisano + $razlika)));
            }
            $results['lager'] = AdminB2BSupport::lager($roba_id);
        }

        DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id', $stavka_id)->update(['kolicina' => $kolicina]);
        AdminSupport::saveLog('B2B_NARUDZBINA_STAVKA_IZMENI_KOLICINA', array($stavka_id));

        $cena_artikala = AdminB2BNarudzbina::ukupnaCena($narudzbina_id);
        if(AdminOptions::web_options(133) == 1 AND AdminCommon::troskovi_isporuke($narudzbina_id)>0){
            $troskovi_isporuke = AdminCommon::troskovi_isporuke($narudzbina_id);
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
            $results['troskovi_isporuke'] = $troskovi_isporuke;
            $results['cena_artikla'] = $cena_artikala;
        }else{
            $results['ukupna_cena'] = $cena_artikala;
        }
        return json_encode($results);
    }
    
    public function editAvans(){
        $data = Input::get();
        DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$data['narudzbina_id'])->update(['avans' => (($data['avans']==1) ? 3 : 0)]);
    }

    public function narudzbina_kupac(){
        $data = Input::get();
        if($data['partner_id']){
            $kupac = DB::table('partner')->where('partner_id',$data['partner_id'])->first(); 
        }else{ 
            $kupac = (object) array('adresa'=>'','mesto'=>'','telefon'=>'');
        }
        return View::make('adminb2b/partials/ajax/narudzbina_partner',array('kupac' => $kupac))->render();
    }


}