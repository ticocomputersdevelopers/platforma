<?php

class AdminB2BAnalitikaController extends Controller {

    function analitika($datum_od_din=null, $datum_do_din=null)
    {      
        if(!AdminB2BOptions::check_admin(array('B2B_ANALITIKA'))){
            return Redirect::to(Option::base_url().'admin/b2b');
        }  

        $postojeNarudzbine = DB::table('web_b2b_narudzbina')->where(array('realizovano'=>1,'stornirano'=>0))->count() > 0;

        $between = "";
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $between .= " AND datum_dokumenta BETWEEN '".$datum_od_din."' AND '".$datum_do_din."'";
        }

        $grupa_pr_ids1=array();
        $analitikaSkraceno=array();

        if($postojeNarudzbine){
        $analitikaSkraceno=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
            (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
            sum(kolicina*jm_cena) AS ukupno
            FROM web_b2b_narudzbina_stavka wbns join web_b2b_narudzbina n
             on wbns.web_b2b_narudzbina_id = n.web_b2b_narudzbina_id 
             WHERE wbns.web_b2b_narudzbina_id IN 
            (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1  and stornirano=0)
            ".$between."
            GROUP BY grupa_pr_id, grupa
            order by ukupno desc
            limit 5
            ");

        $grupa_pr_ids1=array_map('current', $analitikaSkraceno);
            }

            $ukupno1 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno1 = $ukupno1->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno1 = $ukupno1->count();
    
        $ukupno2 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('stornirano', '=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno2 = $ukupno2->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno2 = $ukupno2->count();
   
        $ukupno3 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 1)->where('stornirano', '!=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno3 = $ukupno3->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno3 = $ukupno3->count();


        $ukupno4 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('prihvaceno', 1)->where('realizovano','!=', 1)->where('stornirano', '!=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno4 = $ukupno4->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno4 = $ukupno4->count();

        $analitikaOstalo=array();
        if($postojeNarudzbine && count($grupa_pr_ids1) > 0){
            $analitikaOstalo=DB::select(
                "SELECT sum(kolicina*jm_cena) AS ostalo FROM web_b2b_narudzbina_stavka wbns WHERE wbns.web_b2b_narudzbina_id IN (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0) AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",$grupa_pr_ids1)."))");
        }
        $ostalo = 0;
        if(isset($analitikaOstalo[0])){
            $ostalo = $analitikaOstalo[0]->ostalo;
        }

        $analitikaRuc=array();
        if($postojeNarudzbine){
            $analitikaRuc=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
                sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika
                FROM web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_id IN 
                (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0)
                GROUP BY grupa_pr_id, grupa
                order by razlika desc
                limit 5");
        }
        $analitikaGrupaPregled=DB::select("SELECT grupa_pr_id, (select(select grupa from grupa_pr  where grupa_pr.grupa_pr_id=roba.grupa_pr_id)) as grupa1, sum(pregledan_puta_b2b) from roba
            group by grupa_pr_id
            order by sum desc
            limit 5
            ");

        $grupa_pr_ids2=array_map('current', $analitikaRuc);
        $rucOstalo=array();
        if($postojeNarudzbine && count($grupa_pr_ids2) > 0){
            $rucOstalo=DB::select("SELECT sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika FROM web_b2b_narudzbina_stavka wbns WHERE wbns.web_b2b_narudzbina_id IN (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0) AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",$grupa_pr_ids2)."))");
        }

        $ostalo1 = 0;
        if(isset($rucOstalo[0])){
            $ostalo1 = $rucOstalo[0]->razlika;

        }

        $artikli = DB::select("
            SELECT roba_id, SUM(kolicina) as count 
            FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
            ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id 
            WHERE realizovano = 1 AND stornirano != 1
            GROUP BY roba_id 
            ORDER BY count DESC LIMIT 10");

        $analitikaGrupa=array();
        if($postojeNarudzbine){
            $analitikaGrupa=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                sum(kolicina), sum(kolicina*jm_cena) AS ukupno, sum(kolicina), sum(kolicina*jm_cena) AS ukupno,
                sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika,
                sum(CASE WHEN racunska_cena_nc > 0 THEN kolicina ELSE 0 END) as nc_cena
                FROM web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_id IN 
                (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0)
                GROUP BY grupa_pr_id, grupa
                ORDER BY ukupno DESC");
        }

        $prihod = DB::select("
            SELECT SUM(kolicina * jm_cena) as count 
            FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
            ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id 
            WHERE realizovano = 1 AND stornirano != 1 
            ".$between."
            ");

        $razlika = DB::select("
            SELECT SUM((jm_cena/1.2 - racunska_cena_nc)*kolicina) as count 
            FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
            ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id
            WHERE realizovano = 1 and racunska_cena_nc>0
            ".$between."
            ");

        $realizovano = DB::select("
            SELECT COUNT(realizovano) as count 
            FROM web_b2b_narudzbina           
            WHERE realizovano = 1 and stornirano !=1
            ".$between."
            ");

        $stornirano = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE stornirano =1
            ".$between."
            ");

        $prihvaceno = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE prihvaceno =1 AND stornirano != 1 AND realizovano != 1
            ".$between."
            ");

        $nove = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE prihvaceno =0 AND stornirano = 0 AND realizovano = 0
            ".$between."
            ");

        $ukupno = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE web_b2b_narudzbina_id != -1
            ".$between."
            ");

        $partner = array();
        if($postojeNarudzbine){
        $partner=DB::select("select par.naziv, sum(wbns.jm_cena*wbns.kolicina) as promet
            from web_b2b_narudzbina wbn
            join partner par on wbn.partner_id = par.partner_id
            join web_b2b_narudzbina_stavka wbns on wbns.web_b2b_narudzbina_id = wbn.web_b2b_narudzbina_id
            ".$between."
            group by par.naziv order by sum(wbns.jm_cena*wbns.kolicina) desc
            limit 5");
        }

        $prikazGrupa='';

        // $partners = DB::select("SELECT lp.partner_id, p.naziv, lp.datum as start, lp.ip FROM log_b2b_akcija la RIGHT JOIN log_b2b_partner lp on la.log_b2b_akcija_id=lp.log_b2b_akcija_id LEFT JOIN partner p ON lp.partner_id=p.partner_id WHERE  la.sifra = 'PARTNER_B2B_LOGIN' ORDER BY lp.datum ASC");


        $visits = DB::select("SELECT lp.partner_id, p.naziv, count(lp.partner_id) as posecenost FROM log_b2b_akcija la RIGHT JOIN log_b2b_partner lp ON la.log_b2b_akcija_id=lp.log_b2b_akcija_id LEFT JOIN partner p ON lp.partner_id=p.partner_id WHERE la.sifra='PARTNER_B2B_LOGIN' GROUP BY lp.partner_id, p.naziv ORDER BY posecenost DESC limit 5");

        // $logovanja_arr = array();
        // foreach($partners as $log){
        //     $log_arr = (array) $log;
        //     $logsOut = DB::select("SELECT datum as finish FROM log_b2b_partner lp LEFT JOIN log_b2b_akcija la ON la.log_b2b_akcija_id = lp.log_b2b_akcija_id AND lp.partner_id = ".$log->partner_id." WHERE (la.sifra = 'PARTNER_B2B_LOGOUT' or la.sifra = 'PARTNER_B2B_LOGIN') AND lp.datum > '".$log->start."' ORDER BY lp.datum ASC");
        //     if(count($logsOut) > 0){
        //         $log_arr['finish'] = $logsOut[0]->finish;
        //     }else{
        //         $log_arr['finish'] = date('Y-m-d H:i:s'); 
        //     }
        //     $logovanja_arr[] = (object) $log_arr;
        // }

 $data=array(
            'strana'=>'b2b_analitika',
            'title'=>'B2BAnalitika',
            'datum_od'=>isset($datum_od) ? $datum_od : '',
            'datum_do'=>isset($datum_do) ? $datum_do : '',
            'artikli'=>$artikli,
            'analitikaGrupa'=>$analitikaGrupa,
            'analitikaGrupaPregled'=>$analitikaGrupaPregled,
            'prikazGrupa'=>$prikazGrupa,
            'analitikaSkraceno'=>$analitikaSkraceno,
            'analitikaRuc'=>$analitikaRuc,
            'ostalo'=>$ostalo,
            'ostalo1'=>$ostalo1,
            'partner'=>$partner,
            'ukupno1'=>$ukupno1,
            'ukupno2'=>$ukupno2,
            'ukupno3'=>$ukupno3,
            'ukupno4'=>$ukupno4,
            'prihod'=>$prihod[0]->count,
            'razlika'=>$razlika[0]->count,
            'od' => isset($datum_od_din) ? $datum_od_din : '',
            'do' => isset($datum_do_din) ? $datum_do_din : '',
            // 'partners' => $logovanja_arr,
            'visits' => $visits
            );


            
        return View::make('adminb2b/pages/analitika', $data);
    }

    public function analitikaPartneri(){
        $limit = 30;
        $page = Input::get('page') ? Input::get('page') : 1;
        $sortColumn = Input::get('sort_column') ? Input::get('sort_column') : 'naziv';
        $sortDirection = Input::get('sort_direction') ? Input::get('sort_direction') : 'asc';
        $search = Input::get('search');
        $export = Input::get('export') && Input::get('export') == '1' ? true : false;


        $selectSifra = "p.id_is";
        $selectNaziv = "p.naziv";
        $selectBrLogovanja = "(select count(*) from log_b2b_partner where partner_id = p.partner_id and log_b2b_akcija_id=1)";
        $selectBrNarudzbina = "count(wbn.web_b2b_narudzbina_id)";
        $selectSumCena = "case when count(wbns.web_b2b_narudzbina_stavka_id)>0 then sum(jm_cena*kolicina) else 0 end";
        $selectProsekNarudzbine = "case when ".$selectBrNarudzbina.">0 then (".$selectSumCena." / ".$selectBrNarudzbina.") else 0 end";

        $partneriQuery = "select ".$selectSifra.", ".$selectNaziv." as naziv, ".$selectBrLogovanja." as br_logovanja, ".$selectBrNarudzbina." as br_narudzbina, ".$selectSumCena." as sum_cena, ".$selectProsekNarudzbine." as cena_prosek from partner p left join web_b2b_narudzbina wbn on wbn.partner_id = p.partner_id left join web_b2b_narudzbina_stavka wbns on wbns.web_b2b_narudzbina_id = wbn.web_b2b_narudzbina_id";

        if(!is_null($search) && $search != ''){
            $partneriQuery .= " where (p.naziv ilike '%".$search."%' or p.id_is ilike '%".$search."%')";
        }

        $partneriQuery .= " group by p.partner_id";
        $column = $selectNaziv;
        if($sortColumn == 'naziv'){
            $column = $selectNaziv;
        }else if($sortColumn == 'sifra'){
            $column = $selectSifra;
        }else if($sortColumn == 'br_logovanja'){
            $column = $selectBrLogovanja;
        }else if($sortColumn == 'br_narudzbina'){
            $column = $selectBrNarudzbina;
        }else if($sortColumn == 'sum_cena'){
            $column = $selectSumCena;
        }else if($sortColumn == 'cena_prosek'){
            $column = $selectProsekNarudzbine;
        }
        $partneriQuery .= " order by ".$column." ".$sortDirection;



        if($export){
            $data = array(
                array('Šifra','Naziv','Br. logovanja','Br. narudžbina','Vrednost narudžbina','Prosečna vrednost')
            );
            foreach(DB::select($partneriQuery) as $item){
                $data[] = array(
                    $item->id_is, $item->naziv, strval($item->br_logovanja), strval($item->br_narudzbina), number_format($item->sum_cena,2,",","."), number_format($item->cena_prosek,2,",",".")
                );
            }
            
            $doc = new PHPExcel();
            $doc->setActiveSheetIndex(0);
            $doc->getActiveSheet()->fromArray($data);
             
            $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

            $store_path = 'files/export_analitika_partneri_'.date('Y-m-d').'.xls';
            $objWriter->save($store_path);
            return Redirect::to(RmaOptions::base_url().$store_path); 
        }

        $partneriCount = count(DB::select($partneriQuery));
        $partneriQuery .= " limit ".strval($limit)." offset ".strval(($page-1)*$limit);
        $partneri = DB::select($partneriQuery);
        $data=array(
            'strana' => 'b2b_analitika_partneri',
            'title' => 'B2B Analitika Partneri',
            'search' => !is_null($search) ? $search : '',
            'partneri' => $partneri,
            'count' => $partneriCount,
            'limit' => $limit,
            'sort_column' => $sortColumn,
            'sort_direction' => $sortDirection,
            );
      
        return View::make('adminb2b/pages/analitika-partneri', $data); 
    }


    // update log_b2b_partner lbp set ip = ids, ids = partner_id, naziv_proizvoda = concat('[{','"id":',partner_id,',','"naziv":"',(select naziv from partner where partner_id = lbp.partner_id),'","datum":"',to_char(datum, 'YYYY-MM-DD HH24:MI:SS'),'"}]')
    public function analitikaPartnerLogovi(){
        $limit = 10;
        $page = Input::get('page') ? Input::get('page') : 1;
        $sortColumn = Input::get('sort_column') ? Input::get('sort_column') : 'naziv';
        $sortDirection = Input::get('sort_direction') ? Input::get('sort_direction') : 'asc';
        $export = Input::get('export') && Input::get('export') == '1' ? true : false;
        $search = Input::get('search');
        $partner_id = Input::get('partner_id') && is_numeric(Input::get('partner_id')) ? intval(Input::get('partner_id')) : null;
        $log_b2b_akcija_id = Input::get('log_b2b_akcija_id') && is_numeric(Input::get('log_b2b_akcija_id')) ? intval(Input::get('log_b2b_akcija_id')) : null;
        $log = Input::get('log');
        $od = Input::get('datum_od_analitika');
        $do = Input::get('datum_do_analitika');

        $selectPartnerId = "p.partner_id";
        $selectPartnerNaziv = "p.naziv";
        $selectLogNaziv = "lba.naziv";
        $selectBrojLogova = "count(*)";
        $selectDatumi = "STRING_AGG(COALESCE(to_char(lbp.datum, 'YYYY-MM-DD HH24:MI:SS'),''), '\r\n' ORDER BY lbp.datum DESC) as datumi";
        $selectProizvodi = "('[' || STRING_AGG(SUBSTRING(naziv_proizvoda,2,length(naziv_proizvoda)-2), ',' ORDER BY lbp.datum DESC) || ']')";

        $logoviQuery = "select ".$selectPartnerId." as partner_id, ".$selectPartnerNaziv." as partner_naziv, ".$selectLogNaziv." as log_naziv, ".$selectBrojLogova." as broj_logova, ".$selectProizvodi." as proizvodi from log_b2b_partner lbp left join log_b2b_akcija lba on lbp.log_b2b_akcija_id = lba.log_b2b_akcija_id left join (select partner_id, naziv from partner) p on lbp.partner_id = p.partner_id";



        $partneri = array();
        if(!is_null($search) && $search != ''){
            $partneri = DB::select("select partner_id, naziv from partner where (naziv ilike '%".$search."%' or id_is ilike '%".$search."%')");
        }
        $logoviQuery .= " where lbp.datum is not null and lbp.naziv_proizvoda <> '[]'";

        $logovanja_arr = array();
        if(!is_null($partner_id) && !empty($partner_id)){
            $logoviQuery .= " and lbp.partner_id = ".$partner_id;

            $logovanja = DB::select("SELECT p.naziv, lbp.datum as start FROM log_b2b_partner lbp LEFT JOIN log_b2b_akcija lba ON lbp.log_b2b_akcija_id = lba.log_b2b_akcija_id LEFT JOIN (select partner_id, naziv from partner) p ON lbp.partner_id = p.partner_id WHERE lbp.partner_id = ".$partner_id." and lba.sifra = 'PARTNER_B2B_LOGIN' ORDER BY lbp.datum DESC");
            foreach($logovanja as $logovanje){
                $log_arr = (array) $logovanje;
                $logsOut = DB::select("SELECT lbp.datum as finish FROM log_b2b_partner lbp LEFT JOIN log_b2b_akcija lba ON lbp.log_b2b_akcija_id = lba.log_b2b_akcija_id WHERE lbp.partner_id = ".$partner_id." and lba.sifra = 'PARTNER_B2B_LOGIN' and (lba.sifra = 'PARTNER_B2B_LOGOUT' or lba.sifra = 'PARTNER_B2B_LOGIN') AND lbp.datum > '".$logovanje->start."' ORDER BY lbp.datum ASC");
                if(count($logsOut) > 0){
                    $log_arr['finish'] = $logsOut[0]->finish;
                }else{
                    $log_arr['finish'] = date('Y-m-d H:i:s'); 
                }
                $logovanja_arr[] = (object) $log_arr;
            }
        }else if(count($partneri)>0){
            $logoviQuery .= " and lbp.partner_id in (".implode(',',array_map('current',$partneri)).")";
        }
        if(!is_null($log_b2b_akcija_id) && !empty($log_b2b_akcija_id)){
            $logoviQuery .= " and lbp.log_b2b_akcija_id = ".$log_b2b_akcija_id;
        }

        $datumOd = null;
        $datumDo = null;
        if(!is_null($log) && !empty($log)){
            $logArr = explode('-',$log);
            $datumOd = date('Y-m-d H:i:s',intval($logArr[0]));
            $datumDo = date('Y-m-d H:i:s',intval($logArr[1]));         
            $logoviQuery .= " and lbp.datum BETWEEN '".$datumOd."'::timestamp and '".$datumDo."'::timestamp";
        }
        if(isset($od) AND isset($do)){
        $logoviQuery .= " and (lbp.datum)::date >= '".$od."'::timestamp and (lbp.datum)::date <= '".$do."'::timestamp";
        }
        $logoviQuery .= " group by lba.sifra, p.partner_id, p.naziv, lba.naziv";

        $column = $selectPartnerNaziv;
        if($sortColumn == 'partner_id'){
            $column = $selectPartnerId;
        }else if($sortColumn == 'partner_naziv'){
            $column = $selectPartnerNaziv;
        }else if($sortColumn == 'log_naziv'){
            $column = $selectLogNaziv;
        }else if($sortColumn == 'broj_logova'){
            $column = $selectBrojLogova;
        }
        $logoviQuery .= " order by ".$column." ".$sortDirection;

       


        if($export){
            $data = array(
                array('ID','Partner','Log','Broj akcija','Br. akcija po stavki','Akcije')
            );
            foreach(DB::select($logoviQuery) as $item){
                $brAkcijaPoStavki = '';
                foreach(AdminB2BSupport::partner_b2b_logs_details($item->proizvodi) as $proizvod){
                    $brAkcijaPoStavki .= $proizvod->naziv ." : ". $proizvod->count ."\n";
                }
                $akcije = '';
                foreach(json_decode($item->proizvodi) as $key => $proizvod){
                    if(($key+1)<50){
                        $akcije .= $proizvod->naziv ." : ". $proizvod->datum ."\n";
                    }
                }

                $data[] = array(
                    $item->partner_id, $item->partner_naziv, $item->log_naziv, strval($item->broj_logova), $brAkcijaPoStavki, $akcije
                );
            }
            
            $doc = new PHPExcel();
            $doc->setActiveSheetIndex(0);
            $doc->getActiveSheet()->fromArray($data);
             
            $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

            $store_path = 'files/export_analitika_partner_logovi_'.date('Y-m-d').'.xls';
            $objWriter->save($store_path);
            return Redirect::to(RmaOptions::base_url().$store_path); 
        }


        $logoviCount = count(DB::select($logoviQuery));
        $logoviQuery .= " limit ".strval($limit)." offset ".strval(($page-1)*$limit);
        $logovi = DB::select($logoviQuery);
        $data=array(
            'strana' => 'b2b_analitika_partner_logovi',
            'title' => 'B2B Analitika Partner Logovi',
            'search' => !is_null($search) ? $search : '',
            'partneri' => $partneri,
            'log_akcije' => DB::select("select * from log_b2b_akcija order by log_b2b_akcija_id asc"),
            'logovanja' => $logovanja_arr,
            'logovi' => $logovi,
            'count' => $logoviCount,
            'limit' => $limit,
            'sort_column' => $sortColumn,
            'sort_direction' => $sortDirection,
            'partner_id' => $partner_id,
            'log_b2b_akcija_id' => $log_b2b_akcija_id,
            'od' => $od,
            'do' => $do,
            'datum_od' => $datumOd,
            'datum_do' => $datumDo
            );
      
        return View::make('adminb2b/pages/analitika-partner-logovi', $data);  
    }
}

    