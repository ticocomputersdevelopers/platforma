<?php

use Illuminate\Support\Facades\Response;


class TicketingLoginController extends Controller {

    function login(){
        if(TicketingOptions::user()){
            return Redirect::to(TicketingOptions::base_url().'ticketing');
        }

        $data=array(
            'strana'=>'login',
            "title"=>'Prijava',
            "description"=>'Prijava',
            "keywords"=>'prijava',

        );
        return View::make('ticketing.pages.login',$data);
    }

    function loginStore(){
        $email=Input::get('username');
        $lozinka=Input::get('password');
        $lozinka=base64_encode($lozinka);
        $validator = Validator::make(array('username'=>$email,'password'=>$lozinka),
        array(
            'username' => 'required|email',
            'password' => 'required|regex:'.Support::regex().'|between:3,50|exists:web_kupac,lozinka,email,'.$email.',servis,1'
        ),
        Language::validator_messages());

       if($validator->fails()){
            return Redirect::to(TicketingOptions::base_url().'ticketing/login')->withInput()->withErrors($validator);
        }
        else {
           $web_kupac_id = DB::table('web_kupac')->where(array('email'=>$email,'lozinka'=>$lozinka))->pluck('web_kupac_id');
           Session::put('b2c_kupac',$web_kupac_id);
           Session::put('ticketing_user',$web_kupac_id);
           return Redirect::to(TicketingOptions::base_url().'ticketing');
        }
    }

    public function logout(){
        Session::forget('ticketing_user');
        return Redirect::to(TicketingOptions::base_url().'ticketing/login');
    }

    public function promena_lozinke(){
        $data=array(
            'strana'=>'promena_lozinke',
            "title"=>'Promena lozinke',
            "description"=>'Promena lozinke',
            "keywords"=>'Promena lozinke',

        );
        return View::make('ticketing.pages.promena_lozinke',$data);
    }

    public function promena_lozinke_post(){
        $data=Input::get();
        $data['old_password']=base64_encode($data['old_password']);

        $validator = Validator::make($data,
        array(
            'old_password' => 'required|regex:'.Support::regex().'|between:3,50|exists:web_kupac,lozinka,web_kupac_id,'.Session::get('ticketing_user'),
            'password' => 'required|regex:'.Support::regex().'|between:3,50|confirmed',
            'password_confirmation' => 'required|regex:'.Support::regex().'|between:3,50'
        ),
        Language::validator_messages());
        if($validator->fails()){
            return Redirect::to(TicketingOptions::base_url().'ticketing/promena-lozinke')->withInput()->withErrors($validator);
        }
        else {
            DB::table('web_kupac')->where('web_kupac_id',Session::get('ticketing_user'))->update(array('lozinka'=>base64_encode($data['password'])));
            return Redirect::to(TicketingOptions::base_url().'ticketing/promena-lozinke')->with('message','Uspešno ste promenili lozinku.');
        }


    }
    

}