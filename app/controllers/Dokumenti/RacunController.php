<?php

class RacunController extends Controller {

    public function racuni(){
        if(AdminOptions::gnrl_options(3050)==0){
            return Redirect::to(DokumentiOptions::base_url().'dokumenti');
        }
        $limit = 30;
        $page = Input::get('page') ? Input::get('page') : 1;
        $sortColumn = Input::get('sort_column') ? Input::get('sort_column') : 'broj_dokumenta';
        $sortDirection = Input::get('sort_direction') ? Input::get('sort_direction') : 'desc';
        $search = Input::get('search');
        $vrstaDokumenta = Input::get('vrsta_dokumenta') ? Input::get('vrsta_dokumenta') : 'racun';
        $partner_id = Input::get('partner_id') ? Input::get('partner_id') : null;
        $dokumenti_status_id = !is_null(Input::get('dokumenti_status_id')) ? Input::get('dokumenti_status_id') : 'all';


        $selectBrojDokumenta = "p.broj_dokumenta";
        $selectNazivPartnera = "(select naziv from partner where partner_id = p.partner_id limit 1)";
        $selectDatumRacuni = "p.datum_racuna";
        $selectRokPlacanja = "p.rok_placanja";
        $selectIznos = "p.iznos";

        $racuniQuery = "select racun_id, ".$selectBrojDokumenta.", ".$selectNazivPartnera." as naziv_partnera, ".$selectDatumRacuni.", ".$selectRokPlacanja.", ".$selectIznos." from racun p";

        $whereArr = array();
        if(!is_null($partner=DokumentiOptions::user('saradnik'))){
            $whereArr[] = "(select parent_id from partner where partner_id = p.partner_id limit 1) = ".$partner->partner_id."";
        }else{
            if(is_null($partner_id)){
                $whereArr[] = "p.partner_id not in (select partner_id from partner where parent_id is not null)";
            }
        }       
        if(!is_null($search) && $search != ''){
            $whereArr[] = "(".$selectBrojDokumenta." ilike '%".$search."%' or ".$selectNazivPartnera." ilike '%".$search."%')";
        }
        if(!is_null($partner_id) && intval($partner_id) > 0){
            $whereArr[] = "(partner_id = ".$partner_id." or p.partner_id in (select partner_id from partner where parent_id = ".$partner_id."))";
        }
        if(!is_null($dokumenti_status_id) && $dokumenti_status_id != 'all' && intval($dokumenti_status_id) >= 0){
            $whereArr[] = "dokumenti_status_id ".($dokumenti_status_id==0 ? "IS NULL" : "= ".$dokumenti_status_id);
        }
        if(count($whereArr) > 0){
            $racuniQuery .= " where ".implode(" and ",$whereArr);
        }

        $column = $selectNazivPartnera;
        if($sortColumn == 'naziv'){
            $column = $selectNazivPartnera;
        }else if($sortColumn == 'broj_dokumenta'){
            $column = $selectBrojDokumenta;
        }else if($sortColumn == 'datum_racuna'){
            $column = $selectDatumRacuni;
        }else if($sortColumn == 'rok_placanja'){
            $column = $selectRokPlacanja;
        }else if($sortColumn == 'iznos'){
            $column = $selectIznos;
        }
        $racuniQuery .= " order by ".$column." ".$sortDirection;


        $racuniCount = count(DB::select($racuniQuery));
        $racuniQuery .= " limit ".strval($limit)." offset ".strval(($page-1)*$limit);
        $racuni = DB::select($racuniQuery);
        $data=array(
            'strana' => 'racuni',
            'title' => 'racuni',
            'search' => !is_null($search) ? $search : '',
            'racuni' => $racuni,
            'count' => $racuniCount,
            'limit' => $limit,
            'sort_column' => $sortColumn,
            'sort_direction' => $sortDirection,
            'vrsta_dokumenta' => $vrstaDokumenta,
            'partner_id' => $partner_id,
            'dokumenti_status_id' => $dokumenti_status_id
            );
      
        return View::make('dokumenti/pages/racuni', $data); 
    }

    public function partner_pretraga(){
        $search = trim(Input::get('partner'));
        $reci = explode(" ",$search);

        $nazivFormatiran = array();
        foreach($reci as $rec){
            $nazivFormatiran[] = "naziv ILIKE '%" . $rec . "%'";
        }

        $partnerQuery = "SELECT * FROM partner ".(count($nazivFormatiran)>0 ? "WHERE (".implode(' OR ',$nazivFormatiran).")" : "");
        if(!is_null($partner=DokumentiOptions::user('saradnik'))){
            $partnerQuery .= (count($nazivFormatiran)>0 ? " AND" : "WHERE")." parent_id = ".$partner->partner_id;
        }
        $partnerQuery .= " ORDER BY naziv ASC";

        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='JSPartnerSearchList'>";
        foreach (DB::select($partnerQuery) as $partner) {
            $list .= "
                <li class='JSPartnerSearchItem' data-partner_id='".$partner->partner_id."'>
                    <div class='JSPartnerSearchLink'>"
                        ."<span class='JSPartnerSearchNaziv'>" . $partner->naziv . "</span>"
                        ."<span class='JSPartnerSearchPib'>" . $partner->pib . "</span>"
                        ."<span class='JSPartnerSearchAdresa'>" . $partner->adresa . "</span>
                    </div>
                </li>";
        }
        $list .= "</ul>";
        echo $list;      
    }

    public function partner_podaci(){
        $partner_id = Input::get('partner_id');
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $partnerData = array(
            'pib' => $partner->pib,
            'adresa' => $partner->adresa,
            'mesto' => $partner->mesto,
            'kontakt_osoba' => $partner->kontakt_osoba,
            'mail' => $partner->mail,
            'telefon' => $partner->telefon
        );
        return json_encode($partnerData);
    }

    public function racun($racun_id){
        if($racun_id > 0 && !is_null($partner=DokumentiOptions::user('saradnik'))){
            if(count(DB::select("select * from ponuda p where (select parent_id from partner where partner_id = p.partner_id limit 1) = ".$partner->partner_id." and racun_id=".$racun_id)) == 0){
                return Response::make('Forbidden', 403);
            }
        }
        if(AdminOptions::gnrl_options(3050)==0){
            return Redirect::to(DokumentiOptions::base_url().'dokumenti');
        }
        $novaStavka = Input::get('nova_stavka') ? Input::get('nova_stavka') : 0;
        $racun = $racun_id > 0 ? DB::table('racun')->where('racun_id',$racun_id)->first() : (object) array('racun_id'=>0,'ponuda_id'=>0,'predracun_id'=>0,'partner_id'=>null,'broj_dokumenta'=>'Novi racun','datum_racuna'=>date('Y-m-d'),'rok_placanja'=>date('Y-m-d'),'dokumenti_status_id'=>null,'rok_isporuke'=>date('Y-m-d'),'nacin_placanja'=>null);

        $partner = $racun_id > 0 ? DB::table('partner')->where('partner_id',$racun->partner_id)->first() : (object) array('partner_id'=>0);

        $stavke = $racun_id > 0 ? DB::table('racun_stavka')->where('racun_id',$racun->racun_id)->orderBy('broj_stavke','asc')->get() : array();
        $stavkeRoba = $racun_id > 0 ? DB::table('racun_stavka')->whereNotNull('roba_id')->where('racun_id',$racun->racun_id)->orderBy('broj_stavke','asc')->get() : array();

        $data=array(
            'strana' => 'racun',
            'title' => 'racun '.$racun->broj_dokumenta,
            'partner' => $partner,
            'racun' => $racun,
            'stavke' => $stavke,
            'stavkeUkupanIznos' => DB::select("select sum(pcena*kolicina) as iznos from racun_stavka where racun_id=".$racun_id)[0]->iznos,
            'roba_ids' => array_map(function($item){ return $item->roba_id; },$stavkeRoba),
            'ponuda' => DB::table('ponuda')->where('ponuda_id',$racun->ponuda_id)->first(),
            'predracun' => DB::table('predracun')->where('predracun_id',$racun->predracun_id)->first(),
            'novaStavka' => ($novaStavka == 1)
            );
      
        return View::make('dokumenti/pages/racun', $data); 
    }
    public function racun_post(){
        $racun_id = Input::get('racun_id');
        $ponuda_id = Input::get('ponuda_id');
        $partner_id = Input::get('partner_id');
        $naziv = Input::get('naziv');
        $adresa = Input::get('adresa');
        $mesto = Input::get('mesto');
        $pib = Input::get('pib');
        $kontakt_osoba = Input::get('kontakt_osoba');
        $mail = Input::get('mail');
        $telefon = Input::get('telefon');
        $naziv_stavka = Input::get('naziv_stavka');
        $datum_racuna = Input::get('datum_racuna');
        $rok_placanja = Input::get('rok_placanja');
        $rok_isporuke = Input::get('rok_isporuke');
        $nacin_placanja = Input::get('nacin_placanja');
        $dokumenti_status_id = Input::get('dokumenti_status_id');


        $data = array(
            'partner_id' => $partner_id,
            'naziv' => $naziv,
            'adresa' => $adresa,
            'mesto' => $mesto,
            'pib' => $pib,
            'kontakt_osoba' => $kontakt_osoba,
            'mail' => $mail,
            'telefon' => $telefon,
            'naziv_stavka' => $naziv_stavka,
            'datum_racuna' => $datum_racuna,
            'rok_placanja' => $rok_placanja,
            'rok_isporuke' => $rok_isporuke,
            'nacin_placanja' => $nacin_placanja,
            'dokumenti_status_id' => $dokumenti_status_id
        );
        $validator_arr = array(
            'partner_id' => 'required|integer',
            'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
            'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
            'mesto' => 'required|regex:'.Support::regex().'|between:2,100', 
            'pib' => 'numeric|digits_between:9,9',
            'kontakt_osoba' => 'regex:'.Support::regex().'|max:50',
            'mail' => 'regex:'.Support::regex().'|email|max:50',
            'telefon' => 'regex:'.Support::regex().'|max:50',
            'datum_racuna' => 'required|date',
            'rok_placanja' => 'date',
            'rok_isporuke' => 'date',
            'nacin_placanja' => 'regex:'.Support::regex().'|max:200',
            'dokumenti_status_id' => 'integer',
            'naziv_stavka' => 'regex:'.Support::regex().'|max:255'
        );
        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'digits_between' => 'Dužina sadržaja nije dozvoljena!',
            'between' => 'Dužina sadržaja nije dozvoljena!',
            'date' => 'Neodgovarajući format!'
        );

        $validator = Validator::make($data, $validator_arr, $messages);

        if($validator->fails()){
            return Redirect::to(DokumentiOptions::base_url().'dokumenti/racun/'.$racun_id)->withInput()->withErrors($validator);
        }

        $partnerdata = array(
            'partner_id' => $data['partner_id'],
            'naziv' => $data['naziv'],
            'adresa' => $data['adresa'],
            'mesto' => $data['mesto'],
            'pib' => $data['pib'],
            'kontakt_osoba' => $data['kontakt_osoba'],
            'mail' => $data['mail'],
            'telefon' => $data['telefon']     
        );
        if($partnerdata['partner_id'] == 0){
            $data['partner_id'] = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
            $partnerdata['partner_id'] = $data['partner_id'];
            $partnerdata['drzava_id'] = 0;
            $partnerdata['rabat'] = 0.00;
            $partnerdata['limit_p'] = 0.00;
            $partnerdata['parent_id'] = !is_null($partnerSaradnik=DokumentiOptions::user('saradnik')) ? $partnerSaradnik->partner_id : null;
            DB::table('partner')->insert($partnerdata);            
        }else{
            unset($partnerdata['partner_id']);
            DB::table('partner')->where('partner_id', $data['partner_id'])->update($partnerdata);
        }

        $dbdata = array(
            'partner_id' => $data['partner_id'],
            'datum_racuna' => $data['datum_racuna'],
            'rok_placanja' => $data['rok_placanja'],
            'rok_isporuke' => $data['rok_isporuke'],
            'nacin_placanja' => $data['nacin_placanja'],
            'dokumenti_status_id' => $data['dokumenti_status_id'] == 0 ? null : $data['dokumenti_status_id']
        );
        if($racun_id==0){
            $racun_id = DB::select("SELECT nextval('racun_racun_id_seq1') as racun_id")[0]->racun_id;
            $dbdata['racun_id'] = $racun_id;
            $dbdata['broj_dokumenta'] = "RAC".str_pad($racun_id,5,"0", STR_PAD_LEFT);
            $dbdata['vrsta_dokumenta_id'] = 100;
            DB::table('racun')->insert($dbdata);
        }else{
            DB::table('racun')->where('racun_id',$racun_id)->update($dbdata);
        }

        return Redirect::to(DokumentiOptions::base_url().'dokumenti/racun/'.$racun_id)->with('message','Uspešno ste sačuvali račun!');
    }

    public function racun_stavka_save(){
        $data = Input::get();
        $racun_stavka = null;

        if($data['racun_stavka_id'] == 0){
            if($data['change'] == 'roba'){
                $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
                $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $roba->tarifna_grupa_id)->pluck('porez');
                $data['racun_stavka_id'] = DB::select("SELECT nextval('racun_stavka_racun_stavka_id_seq')")[0]->nextval;

                $insertData = array(
                    'racun_stavka_id' => $data['racun_stavka_id'],
                    'racun_id' => $data['racun_id'],
                    'broj_stavke' => DB::table('racun_stavka')->where('racun_id',$data['racun_id'])->max('broj_stavke')+1,
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'kolicina' => 1,
                    'pdv' => $pdv,
                    'nab_cena' => $roba->racunska_cena_end,
                    'pcena' => $roba->racunska_cena_end * (1+$pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $data['racun_stavka_id'] = DB::select("SELECT nextval('racun_stavka_racun_stavka_id_seq')")[0]->nextval;
                $insertData = array(
                    'racun_stavka_id' => $data['racun_stavka_id'],
                    'racun_id' => $data['racun_id'],
                    'broj_stavke' => DB::table('racun_stavka')->where('racun_id',$data['racun_id'])->max('broj_stavke')+1,
                    'naziv_stavke' => $data['naziv_stavke'],
                    'kolicina' => 1,
                    'pdv' => 20,
                    'nab_cena' => 0,
                    'pcena' => 0
                );                
            }
            DB::table('racun_stavka')->insert($insertData);
        }else{

            $racun_stavka = DB::table('racun_stavka')->where('racun_stavka_id',$data['racun_stavka_id'])->first();

            if($data['change'] == 'roba'){
                $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
                $updateData = array(
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'nab_cena' => $roba->racunska_cena_end,
                    'pcena' => $roba->racunska_cena_end * (1+$racun_stavka->pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $updateData = array(
                    'roba_id' => null,
                    'naziv_stavke' => $data['naziv_stavke']
                );
            }else if($data['change'] == 'kolicina'){
                $updateData = array(
                    'kolicina' => $data['kolicina']
                );
            }else if($data['change'] == 'pdv'){
                $updateData = array(
                    'pdv' => $data['pdv'],
                    'pcena' => $racun_stavka->nab_cena * (1+$data['pdv']/100)
                );
            }else if($data['change'] == 'nab_cena'){
                $updateData = array(
                    'nab_cena' => $data['nab_cena'],
                    'pcena' => $data['nab_cena'] * (1+$racun_stavka->pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $updateData = array(
                    'naziv_stavke' => $data['naziv_stavke'],
                    'roba_id' => null
                );
            }

            DB::table('racun_stavka')->where('racun_stavka_id',$data['racun_stavka_id'])->update($updateData);
        }

        $racun_stavka = DB::table('racun_stavka')->where('racun_stavka_id',$data['racun_stavka_id'])->first();

        $iznos = DB::select("select sum(pcena*kolicina) as iznos from racun_stavka where racun_id=".$racun_stavka->racun_id);
        DB::table('racun')->where('racun_id',$racun_stavka->racun_id)->update(array('iznos'=>$iznos[0]->iznos));

        $responseData = array(
            'roba_id' => $racun_stavka->roba_id,
            'nab_cena' => $racun_stavka->nab_cena,
            'pdv' => $racun_stavka->pdv,
            'pcena' => $racun_stavka->pcena,
            'kolicina' => $racun_stavka->kolicina,
            'opis_robe' => $racun_stavka->opis_robe,
            'naziv_stavke' => $racun_stavka->naziv_stavke,
            'iznos' => $iznos[0]->iznos
        );
        return json_encode($responseData);
    }

    public function racun_stavka_delete($racun_stavka_id){
        $racun_id = DB::table('racun_stavka')->where(array('racun_stavka_id'=>$racun_stavka_id))->pluck('racun_id');

        DB::table('racun_stavka')->where(array('racun_stavka_id'=>$racun_stavka_id))->delete();

        $iznos = DB::select("select sum(pcena*kolicina) as iznos from racun_stavka where racun_id=".$racun_id);
        DB::table('racun')->where('racun_id',$racun_id)->update(array('iznos'=>$iznos[0]->iznos));

        return Redirect::to(DokumentiOptions::base_url().'dokumenti/racun/'.$racun_id)->with('message','Uspešno ste uklonili stavku!');
    }
    public function racun_delete($racun_id){
        DB::table('racun_stavka')->where(array('racun_id'=>$racun_id))->delete();
        DB::table('racun')->where(array('racun_id'=>$racun_id))->delete();
        return Redirect::to(DokumentiOptions::base_url().'dokumenti/racuni');
    }

    public function racun_pdf($racun_id){
        $racun = DB::table('racun')->where('racun_id',$racun_id)->first();
        $racunCene = (object) array(
            'ukupno' => DB::select("select sum(nab_cena*kolicina) as ukupno from racun_stavka where racun_id=".$racun_id."")[0]->ukupno,
            'pdv' => DB::select("select sum(nab_cena*kolicina*(pdv/100)) as pdv from racun_stavka where racun_id=".$racun_id."")[0]->pdv,
            'ukupno_pdv' => DB::select("select sum(nab_cena*kolicina*(1+pdv/100)) as ukupno_pdv from racun_stavka where racun_id=".$racun_id."")[0]->ukupno_pdv,
        );

        $show_header = true;
        $limit = 10;
        $stavke_count = DB::table('racun_stavka')->where('racun_id',$racun->racun_id)->count();
        $count_pages = intdiv($stavke_count,$limit);
        $mod = fmod($stavke_count,$limit);
        if($mod > 0){ $count_pages++; }
        if($mod > 5){
            $count_pages++;
            $show_header = false;
        }else if($mod == 0){
            $count_pages++;
            $show_header = false;
        }
        $rbr = 1;

        $data = array(
            'racun' => $racun,
            'partner' => DB::table('partner')->where('partner_id',$racun->partner_id)->first(),
            'podesavanja' => Dokumenti::podesavanja(),
            'racunCene' => $racunCene,
            'show_header' => $show_header,
            'limit' => $limit,
            'count_pages' => $count_pages,
            'rbr' => $rbr
        );

        $pdf = App::make('dompdf');
        $pdf->loadView('dokumenti.pdf.racun', $data);

        return $pdf->stream();
    }


}