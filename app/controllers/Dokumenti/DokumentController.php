<?php

class DokumentController extends Controller {

	public function podesavanja(){
		$podesavanja = Dokumenti::podesavanja();

        $data=array(
            'strana' => 'podesavanja',
            'title' => 'Podešavanja',
            'podesavanja' => $podesavanja
            );
        return View::make('dokumenti/pages/podesavanja', $data); 
	}

	public function podesavanja_post(){
		$user = DokumentiOptions::user('saradnik');
		$podesavanja = Dokumenti::podesavanja();

		$data = Input::get();
		$dataValidation = array();
		$validationRules = array();
		if(!is_null($user)){
			$dataValidation['logo'] = Input::file('logo');
			$validationRules['logo'] = (is_null($podesavanja->logo) ? 'required|' : '').'mimes:jpg,png,jpeg|max:5000';
		}
        $validator = Validator::make($dataValidation,$validationRules);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }

		if(is_null($user)){
			$data['partner_id']=null;
			$data['admin']=1;
			$podesavanja = DB::table('dokument_podesavanja')->where('admin',1)->first();
		}else{
			$data['partner_id']=$user->id;
			$data['admin']=0;
			$podesavanja = DB::table('dokument_podesavanja')->where('partner_id',$user->id)->first();

	        $logo = Input::file('logo');
	        if($logo){
		        $extension = $logo->getClientOriginalExtension();
		        $putanja = 'partner_logo_'.$user->id .'.'.$extension;
		        $logo->move('images/', $putanja);
		        $data['logo'] = 'images/'.$putanja;
		    }
		}


		if(!is_null($podesavanja)){
			DB::table('dokument_podesavanja')->where('dokument_podesavanja_id',$podesavanja->dokument_podesavanja_id)->update($data);
		}else{
			DB::table('dokument_podesavanja')->insert($data);
		}
	
		return Redirect::to(DokumentiOptions::base_url().'dokumenti/podesavanja')->with('message','Uspešno ste sačuvali podatke.');
	}

	public function change_sablon(){
		$kind = Input::get('kind');
		$id = Input::get('id');

		$document = null;
		if($kind=='ponuda'){
			$document = DB::table('ponuda')->where('ponuda_id',$id)->first();
		}else if($kind=='predracun'){
			$document = DB::table('predracun')->where('predracun_id',$id)->first();
		}
		$data = array(
			'sablon' => $document->tekst,
			'footer' => $document->tekst_footer
		);
		$user = DokumentiOptions::user('saradnik');
		if(is_null($user)){
			$data['partner_id']=null;
			$data['admin']=1;
			$podesavanja = DB::table('dokument_podesavanja')->where('admin',1)->first();
		}else{
			$data['partner_id']=$user->id;
			$data['admin']=0;
			$podesavanja = DB::table('dokument_podesavanja')->where('partner_id',$user->id)->first();
		}

		if(!is_null($podesavanja)){
			DB::table('dokument_podesavanja')->where('dokument_podesavanja_id',$podesavanja->dokument_podesavanja_id)->update($data);
		}else{
			DB::table('dokument_podesavanja')->insert($data);
		}

	}

}