<?php

class AdminBodoviController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tr_isp = DB::table('bodovi_popust')->orderBy('bodovi_popust_id','asc')->get();
		$tr_count = count($tr_isp);

		$bodovi_popust = array();
		if($tr_count>0){		
			for($i=0;$i<$tr_count;$i++){
				if($i==0){
					$bodovi_popust[$i] = (object) array('bodovi_popust_id'=>$tr_isp[$i]->bodovi_popust_id,
						'web_cena_od'=>0,
						'web_cena_do'=>$tr_isp[$i]->web_cena,
						'popust'=>$tr_isp[$i]->popust);
				}else{
					$bodovi_popust[$i] = (object) array('bodovi_popust_id'=>$tr_isp[$i]->bodovi_popust_id,
						'web_cena_od'=>$tr_isp[$i-1]->web_cena,
						'web_cena_do'=>$tr_isp[$i]->web_cena,
						'popust'=>$tr_isp[$i]->popust);
				}
			}
			$bodovi_popust[$tr_count] = (object) array('bodovi_popust_id'=>0,
				'web_cena_od'=>$tr_isp[$tr_count-1]->web_cena,
				'web_cena_do'=>'','popust'=>'');
		}else{
			$bodovi_popust[] = (object) array('bodovi_popust_id'=>0,'web_cena_od'=>0,'web_cena_do'=>'','popust'=>'');
		}
//ostvareni
		$ostv_isp = DB::table('bodovi_ostvareni')->orderBy('bodovi_ostvareni_id','asc')->get();
		$ostv_count = count($ostv_isp);

		$bodovi_ostvareni = array();
		if($ostv_count>0){		
			for($i=0;$i<$ostv_count;$i++){
				if($i==0){
					$bodovi_ostvareni[$i] = (object) array('bodovi_ostvareni_id'=>$ostv_isp[$i]->bodovi_ostvareni_id,
						'web_cena_od'=>0,
						'web_cena_do'=>$ostv_isp[$i]->web_cena,
						'broj_bodova'=>$ostv_isp[$i]->broj_bodova);
				}else{
					$bodovi_ostvareni[$i] = (object) array('bodovi_ostvareni_id'=>$ostv_isp[$i]->bodovi_ostvareni_id,
						'web_cena_od'=>$ostv_isp[$i-1]->web_cena,
						'web_cena_do'=>$ostv_isp[$i]->web_cena,
						'broj_bodova'=>$ostv_isp[$i]->broj_bodova);
				}
			}
			$bodovi_ostvareni[$ostv_count] = (object) array('bodovi_ostvareni_id'=>0,
				'web_cena_od'=>$ostv_isp[$ostv_count-1]->web_cena,
				'web_cena_do'=>'','broj_bodova'=>'');
		}else{
			$bodovi_ostvareni[] = (object) array('bodovi_ostvareni_id'=>0,'web_cena_od'=>0,'web_cena_do'=>'','broj_bodova'=>'');
		}

		$data = array(
	                'strana'=>'bodovi',
	                'title'=> 'Bodovi',
	                'popust'=>DB::table('bodovi_popust')->get(),
	                'bodovi_popust'=>$bodovi_popust,
	                'bodovi_podesavanja'=>DB::table('web_options')->whereIn('web_options_id',array(315,316,317))->orderBy('web_options_id','asc')->get(),
	                'bodovi_ostvareni'=>$bodovi_ostvareni
	            );

		
		return View::make('admin/page', $data);
	}


	public function save()	
	{
		$data = Input::get();
		$max_id = DB::table('bodovi_popust')->max('bodovi_popust_id');

		$validator_messages = array('required'=>'Polje ne sme biti prazno!',
									'numeric'=>'Polje sme da sadrži samo brojeve!',
									'digits_between'=>'Dužina unetih cifara je predugačka!',
									'min'=>'Minimalna vrednost polja je neodgovarajuća!',
									'max'=>'Prekoračili ste maksimalnu vrednost polja!'
								);

		$validator_rules = array(
        		'popust' => 'required|numeric|digits_between:0,10',
        		'web_cena' => 'required|numeric|digits_between:0,20'
        	);
		
		if($data['bodovi_popust_id']!=0 && $data['bodovi_popust_id'] < $max_id){
			$validator_rules['web_cena'] = 'required|numeric|digits_between:0,20|min:'. DB::table('bodovi_popust')->where('bodovi_popust_id', $data['bodovi_popust_id']-1)->pluck('web_cena');

		}elseif($data['bodovi_popust_id'] == 0){
        	$validator_rules['web_cena'] = 'required|numeric|digits_between:0,20|min:'. DB::table('bodovi_popust')->where('bodovi_popust_id', $max_id)->pluck('web_cena');
		}

		$validator = Validator::make($data, $validator_rules, $validator_messages);		
		if($validator->fails()){
	       	return Redirect::to(AdminOptions::base_url().'admin/bodovi')->withInput()->withErrors($validator->messages());
        }


		if($data['bodovi_popust_id']==0){
        	$data['bodovi_popust_id'] = $max_id+1;
        	DB::table('bodovi_popust')->insert(array('bodovi_popust_id'=>$data['bodovi_popust_id'],'web_cena'=>$data['web_cena'],'popust'=>$data['popust']));
        	AdminSupport::saveLog('SIFARNIK_BODOVI_POPUST_DODAJ', array(DB::table('bodovi_popust')->max('bodovi_popust_id')));
        }else{
        	DB::table('bodovi_popust')->where('bodovi_popust_id',$data['bodovi_popust_id'])->update(array('web_cena'=>$data['web_cena'],'popust'=>$data['popust']));
        	AdminSupport::saveLog('SIFARNIK_BODOVI_POPUST_IZMENI', array($data['bodovi_popust_id']));
        }
        return Redirect::to(AdminOptions::base_url().'admin/bodovi')->with('success',true);
	}


	public function delete($id) {
		AdminSupport::saveLog('SIFARNIK_BODOVI_POPUST_OBRISI', array($id));
		DB::table('bodovi_popust')->where('bodovi_popust_id',$id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/bodovi')->with('success-delete',true);
	}

//PODESAVANJE BODOVA

	public function b_save()	
	{
		$data = Input::get();
		
		$validator_messages = array('required'=>'Polje ne sme biti prazno!',
									'integer'=>'Polje sme da sadrži samo brojeve!',
									'digits_between'=>'Dužina unetih cifara je predugačka!',
								);

		$validator_rules = array(
        		'int_data' => 'required|integer|digits_between:0,10'	
        	);
		
		
		$validator = Validator::make($data, $validator_rules, $validator_messages);	
	

		if($validator->fails())
		{
	       	return Redirect::to(AdminOptions::base_url().'admin/bodovi')->withInput()->withErrors($validator->messages());
        }

        DB::table('web_options')->where('web_options_id',$data['web_options_id'])->update(array('int_data'=>$data['int_data']));
        	
      
        return Redirect::to(AdminOptions::base_url().'admin/bodovi')->with('success',true);
	}

	public function o_save()	
	{
		$data = Input::get();
		$max_id = DB::table('bodovi_ostvareni')->max('bodovi_ostvareni_id');

		$validator_messages = array('required'=>'Polje ne sme biti prazno!',
									'numeric'=>'Polje sme da sadrži samo brojeve!',
									'digits_between'=>'Dužina unetih cifara je predugačka!',
									'min'=>'Minimalna vrednost polja je neodgovarajuća!',
									'max'=>'Prekoračili ste maksimalnu vrednost polja!'
								);

		$validator_rules = array(
        		'broj_bodova' => 'required|numeric|digits_between:0,10',
        		'web_cena' => 'required|numeric|digits_between:0,20'
        	);
		
		if($data['bodovi_ostvareni_id']!=0 && $data['bodovi_ostvareni_id'] < $max_id){
			$validator_rules['web_cena'] = 'required|numeric|digits_between:0,20|min:'. DB::table('bodovi_ostvareni')->where('bodovi_ostvareni_id', $data['bodovi_ostvareni_id']-1)->pluck('web_cena');

		}elseif($data['bodovi_ostvareni_id'] == 0){
        	$validator_rules['web_cena'] = 'required|numeric|digits_between:0,20|min:'. DB::table('bodovi_ostvareni')->where('bodovi_ostvareni_id', $max_id)->pluck('web_cena');
		}

		$validator = Validator::make($data, $validator_rules, $validator_messages);		
		if($validator->fails()){
	       	return Redirect::to(AdminOptions::base_url().'admin/bodovi')->withInput()->withErrors($validator->messages());
        }


		if($data['bodovi_ostvareni_id']==0){
        	$data['bodovi_ostvareni_id'] = $max_id+1;
        	DB::table('bodovi_ostvareni')->insert(array('bodovi_ostvareni_id'=>$data['bodovi_ostvareni_id'],'web_cena'=>$data['web_cena'],'broj_bodova'=>$data['broj_bodova']));
        	AdminSupport::saveLog('SIFARNIK_BODOVI_OSTVARENI_DODAJ', array(DB::table('bodovi_ostvareni')->max('bodovi_ostvareni_id')));
        }else{
        	DB::table('bodovi_ostvareni')->where('bodovi_ostvareni_id',$data['bodovi_ostvareni_id'])->update(array('web_cena'=>$data['web_cena'],'broj_bodova'=>$data['broj_bodova']));
        	AdminSupport::saveLog('SIFARNIK_BODOVI_OSTVARENI_IZMENI', array($data['bodovi_ostvareni_id']));
        }
        return Redirect::to(AdminOptions::base_url().'admin/bodovi')->with('success',true);
	}


	public function o_delete($id) {
		AdminSupport::saveLog('SIFARNIK_BODOVI_OSTVARENI_OBRISI', array($id));
		DB::table('bodovi_ostvareni')->where('bodovi_ostvareni_id',$id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/bodovi')->with('success-delete',true);
	}









	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
