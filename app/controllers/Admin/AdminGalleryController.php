<?php

class AdminGalleryController extends Controller {
    
    function uploadImageDelete($image){
        AdminSupport::saveLog('UCITANE_SLIKE_OBRISI');
        File::delete('images/upload_image/'.$image);
        // return Redirect::to(AdminOptions::base_url().'admin/upload-image-article');
    }
    
    function uploadImageAdd(){
        $data = Input::get();
        $slike = Input::file('slike');

        foreach($slike as $slika){

            $validator = Validator::make(array('slika' => $slika), array('slika' => 'required'));
            if($validator->fails()){
                die('error');
            }else{
                $originalName = $slika->getClientOriginalName();
                $name = $originalName;
                        
                $slika->move('images/upload_image/', $name);
                $width=Image::make('images/upload_image/'.$name)->getWidth();

                $sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
                if($width>$sirina_big){
                Image::make('images/upload_image/'.$name)->resize($sirina_big, null, function ($constraint) {
                    $constraint->aspectRatio(); })->save();
                }
                AdminSupport::saveLog('UCITANE_SLIKE_DODAJ');

            }        
        }
        die('success');

    }
    public function galleryContent(){
        $images = scandir('images/upload_image');
        unset($images[0]);
        unset($images[1]);

        $search = Input::get('search') && !empty(Input::get('search')) ? trim(Input::get('search')) : null;
        $page = Input::get('page') ? Input::get('page') : 1;
        $limit = 20;
        $offset = ($page-1)*$limit;

        if(!is_null($search)){
            $images = array_filter($images,function($image) use ($search){
                return strpos(strtolower($image),strtolower($search)) !== false;
            });
        }
        $maxPages = intdiv(count($images),$limit) + (fmod(count($images),$limit) > 0 ? 1 : 0);
        $images = array_slice($images,$offset,$limit);

        $content = View::make('admin/partials/gallery_images',array('images'=>$images))->render();

        return Response::json(['content'=>$content, 'max_page'=>$maxPages],200);
    }

}