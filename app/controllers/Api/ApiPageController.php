<?php

class ApiPageController extends Controller {

	public function pages(){
		$pages = Page::where('parrent_id',0)->with('childs','pageLangs.lang','childs.pageLangs.lang','childs.childs.pageLangs.lang','childs.childs.childs.pageLangs.lang')->get();

		return Response::json($pages,200);
	}
}